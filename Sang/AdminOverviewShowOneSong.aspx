﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminOverviewShowOneSong.aspx.cs" Inherits="Sang.AdminOverviewShowOneSong" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

 <!--  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script> -->

      <link rel="stylesheet" href="/bootstrap-3.3.5-dist/css/bootstrap.min.css" />
    <script src="/jquery/jquery-1.11.3.min.js"></script> 
    <script src="/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script> 

        <script type="text/javascript">
        window.onload = function () {
            var scrollY = parseInt('<%=Request.Form["scrollY"] %>');
            if (!isNaN(scrollY)) {
                window.scrollTo(0, scrollY);
            }
        };
        window.onscroll = function () {
            var scrollY = document.body.scrollTop;
            if (scrollY == 0) {
                if (window.pageYOffset) {
                    scrollY = window.pageYOffset;
                }
                else {
                    scrollY = (document.body.parentElement) ? document.body.parentElement.scrollTop : 0;
                }
            }
            if (scrollY > 0) {
                var input = document.getElementById("scrollY");
                if (input == null) {
                    input = document.createElement("input");
                    input.setAttribute("type", "hidden");
                    input.setAttribute("id", "scrollY");
                    input.setAttribute("name", "scrollY");
                    document.forms[0].appendChild(input);
                }
                input.value = scrollY;
            }
        };
    </script>



    <link rel="stylesheet" type="text/css" href="/css/StyleSheet1.css"/>

    <title> AdminOverviewShowOneSong</title>
    <style type="text/css">
        .min-style {
            vertical-align: top;
        }

        .auto-style4 {
            height: 22px;
            width: 208px;
        }

        .auto-style6 {
            text-align: center;
        }
        .auto-style7 {
            width: 100%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <table style="width: 40%;">
                <tr>
                    <td class="auto-style4">

                        <div>
                            <input type="image" runat="server" name="coord" id="Image1" src="BitMapNewSong.aspx" onserverclick="xymetoden" />
                        </div>

                    </td>
                    <td class="min-style">
                        <table style="width: 100%;">
                            <tr>
                                <td class="auto-style6">Chord</td>
                                <td class="auto-style6">Extra</td>
                                <td class="auto-style6">Bass</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:DropDownList ID="DropDownList1" runat="server" Font-Size="16pt" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" TabIndex="1">
                                        <asp:ListItem></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBox1" runat="server" Width="75px" Font-Size="14pt" TabIndex="2"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:DropDownList ID="DropDownList2" runat="server" Font-Size="16pt" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" TabIndex="3">
                                        <asp:ListItem></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="CheckBoxChord" runat="server" TabIndex="6" />
                                </td>
                                <td>
                                    <asp:CheckBox ID="CheckBoxExtra" runat="server" TabIndex="6" />
                                </td>
                                <td>
                                    <asp:CheckBox ID="CheckBoxBass" runat="server" TabIndex="6" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:Button CssClass="btn-default" ID="btn_UndoChord" runat="server" Height="60px" OnClick="btn_UndoChord_Click" Text="Undo Chord" Width="180px" Font-Bold="True" Font-Size="16pt" CausesValidation="False" TabIndex="3" />
                                    <asp:Button CssClass="btn-default" ID="btn_SaveSong" runat="server" Height="60px" OnClick="btn_SaveSong_Click" Text="Save copy" Width="180px" Font-Bold="True" Font-Size="16pt" TabIndex="4" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:CheckBox ID="CheckBox1" runat="server" Visible="False" TabIndex="7" />
                                </td>
                                <td>
                        <asp:Label ID="Label1" runat="server" Text="Label" Visible="False"></asp:Label>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <asp:TextBox ID="TextBoxBesked" runat="server" Height="141px" TextMode="MultiLine" Visible="False" Width="173px" TabIndex="5">You can not make changes to the song after it is saved.

To save the song, you must check the check box, and 
push the &quot;Save song&quot; button again.</asp:TextBox>
                        <br />
                                    <asp:DropDownList ID="DropDownListText" runat="server" Font-Size="16pt" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" TabIndex="3" Width="339px">
                                        <asp:ListItem></asp:ListItem>
                                    </asp:DropDownList>
                                <br />
                        <table class="auto-style7">
                            <tr>
                                <td>
                                    <asp:Button CssClass="btn-default" ID="ButtonEditText" runat="server" OnClick="ButtonEditText_Click" Text="Load text" />
                                </td>
                                <td>
                                    <asp:Button CssClass="btn-default" ID="ButtonEditText0" runat="server" OnClick="ButtonEditText0_Click" Text="Save new text" />
                                </td>
                            </tr>
                        </table>
                        <asp:TextBox ID="TextBoxEditText" runat="server" Width="260px"></asp:TextBox>
                        <br />
                        <br />
                    </td>
                    <td class="min-style">
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="SqlDataSource1" EmptyDataText="There are no data records to display.">
                            <Columns>
                                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                                <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="True" SortExpression="Id" />
                                <asp:BoundField DataField="sangID" HeaderText="sangID" SortExpression="sangID" />
                                <asp:BoundField DataField="posX" HeaderText="posX" SortExpression="posX" />
                                <asp:BoundField DataField="posY" HeaderText="posY" SortExpression="posY" />
                                <asp:BoundField DataField="keyNumber" HeaderText="keyNumber" SortExpression="keyNumber" />
                                <asp:BoundField DataField="extraName" HeaderText="extraName" SortExpression="extraName" />
                                <asp:BoundField DataField="akkordID" HeaderText="akkordID" SortExpression="akkordID" />
                                <asp:BoundField DataField="bass" HeaderText="bass" SortExpression="bass" />
                            </Columns>
                        </asp:GridView>
                        </td>
                </tr>
                <tr>
                    <td class="auto-style4">

                        &nbsp;</td>
                    <td class="min-style">
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:wenningsdbConnectionString1 %>" DeleteCommand="DELETE FROM [SangAkkorder] WHERE [Id] = @Id" InsertCommand="INSERT INTO [SangAkkorder] ([sangID], [posX], [posY], [keyNumber], [extraName], [akkordID], [bass]) VALUES (@sangID, @posX, @posY, @keyNumber, @extraName, @akkordID, @bass)" ProviderName="<%$ ConnectionStrings:wenningsdbConnectionString1.ProviderName %>" SelectCommand="SELECT [Id], [sangID], [posX], [posY], [keyNumber], [extraName], [akkordID], [bass] FROM [SangAkkorder] WHERE sangID=@sangID" UpdateCommand="UPDATE [SangAkkorder] SET [sangID] = @sangID, [posX] = @posX, [posY] = @posY, [keyNumber] = @keyNumber, [extraName] = @extraName, [akkordID] = @akkordID, [bass] = @bass WHERE [Id] = @Id">
                            <DeleteParameters>
                                <asp:Parameter Name="Id" Type="Int32" />
                            </DeleteParameters>
                            <InsertParameters>
                                <asp:Parameter Name="sangID" Type="Int32" />
                                <asp:Parameter Name="posX" Type="Int32" />
                                <asp:Parameter Name="posY" Type="Int32" />
                                <asp:Parameter Name="keyNumber" Type="Int32" />
                                <asp:Parameter Name="extraName" Type="String" />
                                <asp:Parameter Name="akkordID" Type="Int32" />
                                <asp:Parameter Name="bass" Type="Int32" />
                            </InsertParameters>
                            <SelectParameters>
                                <asp:SessionParameter Name="sangID" SessionField="SessionSangID" />
                            </SelectParameters>
                            <UpdateParameters>
                                <asp:Parameter Name="sangID" Type="Int32" />
                                <asp:Parameter Name="posX" Type="Int32" />
                                <asp:Parameter Name="posY" Type="Int32" />
                                <asp:Parameter Name="keyNumber" Type="Int32" />
                                <asp:Parameter Name="extraName" Type="String" />
                                <asp:Parameter Name="akkordID" Type="Int32" />
                                <asp:Parameter Name="bass" Type="Int32" />
                                <asp:Parameter Name="Id" Type="Int32" />
                            </UpdateParameters>
                        </asp:SqlDataSource>
                    </td>
                    <td class="min-style"> &nbsp;</td>
                </tr>
                </table>

        </div>
    </form>

</body>
</html>
