﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sang
{
    public partial class AdminOverviewShowSongs : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Bruger"] != null && Session["Admin"] == null)
            {
                //Så har almindelig bruger lige ændret tilstanden af en sang, og skal tilbage.
                Server.Transfer("FindAllSongs.aspx");
            }

            if (Session["Admin"] == null)
            {
                Server.Transfer("Default.aspx");
            }
            //     Response.Write("<script LANGUAGE='JavaScript' >alert('" + valg + "')</script>");
            if (Session["BrugerValgt"] != null)
            {
                Bruger BrugerValgt = (Bruger)Session["BrugerValgt"];
                LabelAktivBruger.Text = BrugerValgt.name + ":" + BrugerValgt.email;
                visListeAfSangeForBruger(BrugerValgt.ID);
            }
            if (Session["DirekteLink"] != null)
            {
                String dirlink = (String)Session["DirekteLink"];
                Session["DirekteLink"] = null;

                TextBoxBeskedTilBruger.Visible = true;
                TextBoxBeskedTilBruger.Text = dirlink;
            }
        }
        protected void ButtonBack_Click(object sender, EventArgs e)
        {
            if (Session["KommerFra"] != null)
            {
                List<String> listeKommerFra = (List<String>)Session["KommerFra"];
                String tilbageTil = listeKommerFra[listeKommerFra.Count - 1];
                listeKommerFra.RemoveAt(listeKommerFra.Count - 1);
                Session["KommerFra"] = listeKommerFra;

                Server.Transfer(tilbageTil);
            }
            else
            {
                Server.Transfer("Default.aspx");
            }
        }
        protected void ButtonActivateDelet_Click(object sender, EventArgs e)
        {
            CheckBox1.Visible = true;

            //---Lukker den anden checkbox----------
            CheckBoxOwnerChange.Checked = false;
            CheckBoxOwnerChange.Visible = false;
            DropDownListUsers.Visible = false;
        }
        protected void ButtonActivateChangeOwnerOfSong_Click(object sender, EventArgs e)
        {
            CheckBoxOwnerChange.Visible = true;
            DropDownListUsers.Visible = true;

            //---Lukker den anden checkbox----------
            CheckBox1.Checked = false;
            CheckBox1.Visible = false;
        }

        public void visListeAfSangeForBruger(int ID)
        {
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["wenningsdbConnectionString1"].ToString());
                con.Open();

                //------------HENTE SANG START------------------------------------------------
                //------------HENTE SANG START------------------------------------------------
                //------------HENTE SANG START------------------------------------------------
                SqlCommand cmd;
                if (ID == -1)//-1 betyder at ALLE sange skal vises.
                {
                    cmd = new SqlCommand("SELECT * FROM Sang ORDER BY titel ASC;", con);
                }
                else
                {
                    cmd = new SqlCommand("SELECT * FROM Sang WHERE userID=@userID  ORDER BY titel ASC;", con);
                    cmd.Parameters.AddWithValue("@userID", ID);
                }
                SqlDataReader reader = cmd.ExecuteReader();
                List<SangAdminValgt> listSang = new List<SangAdminValgt>();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        //    Response.Write("<script LANGUAGE='JavaScript' >alert('" + "listBrugere.Count" + "')</script>");
                        //   ListBox1.Items.Add("Song har item");
                        Bruger bruger = new Bruger();

                        //---titel-------------------   
                        String titel = "ingen titel";
                        if (!reader.IsDBNull(1))
                        {
                            titel = reader.GetString(1);
                        }
                        //---------------------------
                        //---type-------------------   
                        int type = 0;
                        if (!reader.IsDBNull(6))
                        {
                            type = reader.GetInt32(6);
                        }
                        //---------------------------
                        //---ID-------------------   
                        int id = 0;
                        if (!reader.IsDBNull(0))
                        {
                            id = reader.GetInt32(0);
                        }
                        //---------------------------

                        SangAdminValgt sangValg = new SangAdminValgt();
                        sangValg.admOverS = this;
                        sangValg.title = titel;
                        sangValg.type = type;
                        sangValg.SangID = id;


                        Button button = new Button();
                        button.Click += new EventHandler(sangValg.eventMetode);
                        sangValg.knap = button;

                        Button buttonTilstand = new Button();
                        buttonTilstand.Click += new EventHandler(sangValg.eventTilstand);
                        sangValg.knapTilstand = buttonTilstand;


                        //-------Add to listen---------------------
                        listSang.Add(sangValg);
                    }
                }
                con.Close();
                //------------HENTE SANG SLUT------------------------------------------------
                //------------HENTE SANG SLUT------------------------------------------------
                //------------HENTE SANG SLUT------------------------------------------------


                //------------HENTE SANG START------------------------------------------------
                //------------HENTE SANG START------------------------------------------------
                //------------HENTE SANG START------------------------------------------------
                con.Open();
                if (ID == -1)//-1 betyder at ALLE sange skal vises.
                {
                    cmd = new SqlCommand("SELECT * FROM SangSheet ORDER BY titel ASC;", con);
                    //  Response.Write("<script LANGUAGE='JavaScript' >alert('" + "OK" + "')</script>");

                }
                else
                {
                    cmd = new SqlCommand("SELECT * FROM SangSheet WHERE userID=@userID  ORDER BY titel ASC;", con);
                    cmd.Parameters.AddWithValue("@userID", ID);
                }
                reader = cmd.ExecuteReader();
                List<SangAdminValgt> listSangSheet = new List<SangAdminValgt>();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        //   Response.Write("<script LANGUAGE='JavaScript' >alert('" + "listBrugere.Count" + "')</script>");
                        Bruger bruger = new Bruger();

                        //---titel-------------------   
                        String titel = "ingen titel";
                        if (!reader.IsDBNull(1))
                        {
                            titel = reader.GetString(1);
                            titel = titel.TrimStart(' ');
                        }
                        //---------------------------
                        //---type-------------------   
                        int type = 0;
                        if (!reader.IsDBNull(6))
                        {
                            type = reader.GetInt32(6);
                        }
                        //---------------------------
                        //---id-------------------   
                        int id = 0;
                        if (!reader.IsDBNull(0))
                        {
                            id = reader.GetInt32(0);
                        }
                        //---------------------------

                        SangAdminValgt sangValg = new SangAdminValgt();
                        sangValg.admOverS = this;
                        sangValg.title = titel;
                        sangValg.type = type;
                        sangValg.SangID = id;
                        sangValg.sheetSong = true;


                        Button button = new Button();
                        button.Click += new EventHandler(sangValg.eventMetode);
                        sangValg.knap = button;

                        Button buttonTilstand = new Button();
                        buttonTilstand.Click += new EventHandler(sangValg.eventTilstand);
                        sangValg.knapTilstand = buttonTilstand;


                        //-------Add to listen---------------------
                        listSangSheet.Add(sangValg);
                    }
                }
                con.Close();
                listSangSheet.Sort();
                //------------HENTE SANG SLUT------------------------------------------------
                //------------HENTE SANG SLUT------------------------------------------------
                //------------HENTE SANG SLUT------------------------------------------------


                //----------SANG TIL KNAP START---------------------------------------------
                //----------SANG TIL KNAP START---------------------------------------------
                //----------SANG TIL KNAP START---------------------------------------------
                foreach (SangAdminValgt sa in listSang)
                {
                    // Response.Write("<script LANGUAGE='JavaScript' >alert('" + "listBrugere.Count" + "')</script>");

                    // Create new row and add it to the table.
                    TableRow tRow = new TableRow();
                    Table1.Rows.Add(tRow);

                    // Create a new cell and add it to the row.
                    TableCell tCell = new TableCell();
                    sa.knap.Text = sa.title;
                    sa.knap.Width = 300;
                    sa.knap.Height = 40;
                    sa.knap.Font.Size = 14;
                    sa.knap.Font.Bold = true;
                    sa.knap.CssClass = "btn-default";
                    tCell.Controls.Add(sa.knap);

                    // Create a new cell and add it to the row.
                    TableCell tCell2 = new TableCell();
                    sa.knapTilstand.Text = sa.type.ToString();
                    sa.knapTilstand.Width = 100;
                    sa.knapTilstand.Height = 40;
                    if (sa.type == 0)
                    {
                        sa.knapTilstand.BackColor = Color.Goldenrod;
                        sa.knapTilstand.Text = "Private";
                    }
                    if (sa.type == 1)
                    {
                        sa.knapTilstand.BackColor = Color.LightGreen;
                        sa.knapTilstand.Text = "Public";
                    }
                    if (sa.type == 2)
                    {
                        sa.knapTilstand.BackColor = Color.LightYellow;
                        sa.knapTilstand.Text = "Link";
                    }
                    if (sa.type == 3)
                    {
                        sa.knapTilstand.BackColor = Color.LightBlue;
                        sa.knapTilstand.Text = "Request";
                    }

                    sa.knapTilstand.Font.Size = 14;
                    sa.knapTilstand.Font.Bold = true;
                    tCell2.Controls.Add(sa.knapTilstand);

                    //br.reFresh(); //Tegner titel og hvem ejer sangen
                    tRow.Cells.Add(tCell);
                    tRow.Cells.Add(tCell2);
                }
                //----------SANG TIL KNAP SLUT---------------------------------------------
                //----------SANG TIL KNAP SLUT---------------------------------------------
                //----------SANG TIL KNAP SLUT---------------------------------------------


                //----------SANG TIL KNAP START---------------------------------------------
                //----------SANG TIL KNAP START---------------------------------------------
                //----------SANG TIL KNAP START---------------------------------------------
                foreach (SangAdminValgt sa in listSangSheet)
                {
                    // Response.Write("<script LANGUAGE='JavaScript' >alert('" + "listBrugere.Count" + "')</script>");

                    // Create new row and add it to the table.
                    TableRow tRow = new TableRow();
                    Table1.Rows.Add(tRow);

                    // Create a new cell and add it to the row.
                    TableCell tCell = new TableCell();
                    sa.knap.Text = "(" + sa.SangID + ")" + sa.title;
                    sa.knap.Width = 300;
                    sa.knap.Height = 40;
                    sa.knap.Font.Size = 14;
                    sa.knap.Font.Bold = false;
                    sa.knap.Font.Italic = true;
                    sa.knap.CssClass = "btn-default";
                    tCell.Controls.Add(sa.knap);

                    // Create a new cell and add it to the row.
                    TableCell tCell2 = new TableCell();
                    sa.knapTilstand.Text = sa.type.ToString();
                    sa.knapTilstand.Width = 100;
                    sa.knapTilstand.Height = 40;

                    if (sa.type == 0)
                    {
                        sa.knapTilstand.BackColor = Color.Goldenrod;
                        sa.knapTilstand.Text = "Private";
                    }
                    if (sa.type == 1)
                    {
                        sa.knapTilstand.BackColor = Color.LightGreen;
                        sa.knapTilstand.Text = "Public";
                    }
                    if (sa.type == 2)
                    {
                        sa.knapTilstand.BackColor = Color.LightYellow;
                        sa.knapTilstand.Text = "Link";
                    }
                    if (sa.type == 3)
                    {
                        sa.knapTilstand.BackColor = Color.LightBlue;
                        sa.knapTilstand.Text = "Request";
                    }
                    sa.knapTilstand.Font.Size = 14;
                    sa.knapTilstand.Font.Bold = true;
                    tCell2.Controls.Add(sa.knapTilstand);

                    //br.reFresh(); //Tegner titel og hvem ejer sangen
                    tRow.Cells.Add(tCell);
                    tRow.Cells.Add(tCell2);
                }
                //----------SANG TIL KNAP SLUT---------------------------------------------
                //----------SANG TIL KNAP SLUT---------------------------------------------
                //----------SANG TIL KNAP SLUT---------------------------------------------
                con.Close();
            }
            catch (Exception lskd)
            {

            }

        }
        //-------COPI fra FindSong---------------
        //-------COPI fra FindSong---------------
        public void createSong(String idValgt)
        {
            // String idValgt = GridView1.SelectedValue.ToString();
            Sang sang = new Sang();
            try
            {
                int id = Convert.ToInt32(idValgt);
                sang.ID = id;

            }
            catch (Exception lkjs) { }
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["wenningsdbConnectionString1"].ToString());
                con.Open();
                //------Henter én Sang, som passer til ID------------------------------
                SqlCommand cmd = new SqlCommand("Select * FROM  Sang WHERE Id=@sangID  ;", con);

                cmd.Parameters.AddWithValue("@sangID", idValgt);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {

                        sang.title = reader.GetString(1);
                        sang.lyricWriter = reader.GetString(2);
                        sang.melodyWriter = reader.GetString(3);
                        sang.userID = reader.GetInt32(4);
                    }
                }
                reader.Close();

                //------Henter textLinjer-------------------------------------------
                SqlCommand cmd2 = new SqlCommand("Select * FROM  SangTextLinjer WHERE sangID=@sangID  ;", con);
                cmd2.Parameters.AddWithValue("@sangID", idValgt);
                SqlDataReader reader2 = cmd2.ExecuteReader();

                if (reader2.HasRows)
                {
                    while (reader2.Read())
                    {
                        TextLinje t = new TextLinje();
                        t.text = reader2.GetString(2);
                        t.pos = new Point(reader2.GetInt32(3), reader2.GetInt32(4));
                        t.id = reader2.GetInt32(5);
                        sang.listTexter.Add(t);
                    }
                    foreach (TextLinje tl in sang.listTexter)
                    {
                    }

                }
                else
                {
                }
                reader2.Close();

                //------Henter Akkorder-------------------------------------------
                SqlCommand cmd3 = new SqlCommand("Select * FROM  SangAkkorder WHERE sangID=@sangID  ;", con);
                cmd3.Parameters.AddWithValue("@sangID", idValgt);
                SqlDataReader reader3 = cmd3.ExecuteReader();

                if (reader3.HasRows)
                {
                    while (reader3.Read())
                    {
                        Akkord ak = new Akkord();
                        ak.pos = new Point(reader3.GetInt32(2), reader3.GetInt32(3));
                        ak.keyNumber = reader3.GetInt32(4);
                        int bassen = 0;
                        try
                        {
                            bassen = reader3.GetInt32(7);
                        }
                        catch (Exception els)
                        {

                        }

                        ak.bass = bassen;


                        String extra = reader3.GetString(5);
                        if (extra != null)
                            ak.extraName = extra;
                        else
                            ak.extraName = "";
                        //  ak.id = reader3.GetInt32(6);
                        sang.listAkkorder.Add(ak);
                    }
                }
                else
                {
                }
                reader3.Close();


                Session["Sang"] = sang;
                //                Server.Transfer("ShowSong.aspx");
            }
            catch (Exception lkjd)
            {

            }

        }


        public void visHvilkenSangDerErValgt(int SangID, String title)
        {
            //                  Response.Write("<script LANGUAGE='JavaScript' >alert('" + SangID +":"+title+ "')</script>");
            //----------Kommer fra------------------------------         
            List<String> listeKommerFra = (List<String>)Session["KommerFra"];
            if (listeKommerFra == null) { listeKommerFra = new List<String>(); }
            listeKommerFra.Add("FindAllSongs.aspx");
            Session["KommerFra"] = listeKommerFra;
            //-----------------------------------------------------
            Session["SessionSangID"] = SangID;

            createSong(SangID.ToString());
            Server.Transfer("AdminOverviewShowOneSong.aspx");
        }
        public void changeSangDerErValgtTilstand(int SangID, int type)
        {
            if (CheckBox1 == null) //Så kaldes denne fra Bruger og ikke Admin.
            {
                CheckBox1 = new CheckBox();
                CheckBox1.Checked = false; //Må IKKE slette.
            }

            if (CheckBox1.Checked) //SLET sang ADMINISTRATOR.
            {
                if (Session["Admin"] != null) //Extra sikkerhed således at det KUN er admin som kan slette.
                {
                    CheckBox1.Checked = false;
                    CheckBox1.Visible = false;
                    try
                    {
                        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["wenningsdbConnectionString1"].ToString());
                        con.Open();
                        //------Delete TextLinjer-------------------------------------------
                        SqlConnection con2 = new SqlConnection(ConfigurationManager.ConnectionStrings["wenningsdbConnectionString1"].ToString());
                        con2.Open();
                        SqlCommand cmd2 = new SqlCommand("DELETE FROM SangTextLinjer WHERE SangID=@sangID;", con2);
                        cmd2.Parameters.AddWithValue("@sangID", SangID);
                        int antalTextLinjerSlettet = cmd2.ExecuteNonQuery();
                        con2.Close();
                        //------Delete Akkorder-------------------------------------------
                        SqlConnection con3 = new SqlConnection(ConfigurationManager.ConnectionStrings["wenningsdbConnectionString1"].ToString());
                        con3.Open();
                        SqlCommand cmd3 = new SqlCommand("DELETE FROM SangAkkorder WHERE SangID=@sangID;", con3);
                        cmd3.Parameters.AddWithValue("@sangID", SangID);
                        int antalAkkorderSlettet = cmd3.ExecuteNonQuery();
                        con3.Close();
                        //----Denne må være til sidst fordi dette er primer tabellen. 
                        //------Delete Sang-------------------------------------------
                        SqlCommand cmd = new SqlCommand("DELETE FROM Sang WHERE Id=@id;", con);
                        cmd.Parameters.AddWithValue("@id", SangID);
                        int antalSangeSlettet = cmd.ExecuteNonQuery();
                        con.Close();

                        Response.Write("<script LANGUAGE='JavaScript' >alert('" + "Number of deleted songs=" + antalSangeSlettet + "  Number of Chords deleted=" + antalAkkorderSlettet + "  Number of deleted text lines=" + antalTextLinjerSlettet + "')</script>");


                        //Refresh tilbage til sig selv, så man kan se opdateringen.
                        Server.Transfer("AdminOverviewShowSongs.aspx");
                    }
                    catch (Exception edk)
                    {
                        Response.Write("<script LANGUAGE='JavaScript' >alert('" + "Delete went wrong: " + edk.ToString() + "')</script>");
                    }
                }

            }
            else // ÆNDRE TYPE TILSTAND.
            {
                // 2016.11.04.Fejle i CheckBoxOwnerChange.Checked == false, fordi den er 
                // lavet visible = false og derfor ikke eksisterer. Denne fejle har jeg nok
                // indført selv når jeg fornyligt har lavet ændring...Derfor vælger jeg 
                // at lave en ny variabel lavChange som hånderer alle tilfælde, den erstatter
                // nu CheckBoxOwnerChange.Checked == false i conditionen.
                bool lavChange = false;
                if (CheckBoxOwnerChange == null)
                {
                    lavChange = true;
                }
                else
                {
                    if (CheckBoxOwnerChange.Checked == false)
                    {
                        lavChange = true;
                    }
                    else
                    {
                        lavChange = false;
                    }
                }
                if (lavChange) //lavChange erstatter CheckBoxOwnerChange.Checked == false.
                {
                    try
                    {
                        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["wenningsdbConnectionString1"].ToString());
                        con.Open();
                        //------Gemmer Sangen-------------------------------------------

                        SqlCommand cmd = new SqlCommand("UPDATE Sang SET type=@type WHERE id=@id;", con);
                        cmd.Parameters.AddWithValue("@id", SangID);
                        cmd.Parameters.AddWithValue("@type", type);

                        SqlDataReader reader = cmd.ExecuteReader();
                        con.Close();

                        //Refresh tilbage til sig selv, så man kan se opdateringen.
                        Server.Transfer("AdminOverviewShowSongs.aspx");
                    }
                    catch (Exception edk)
                    {

                    }
                }
                //-----2016.08.04----Tilført ændring af Owner af sangen------
                if (CheckBoxOwnerChange.Checked)
                {
                    if (DropDownListUsers.SelectedIndex > -1)
                    {
                        String userID = DropDownListUsers.SelectedValue;
                        try
                        {
                            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["wenningsdbConnectionString1"].ToString());
                            con.Open();
                            //------Gemmer Sangen-------------------------------------------

                            SqlCommand cmd = new SqlCommand("UPDATE Sang SET userID=@userID WHERE id=@id;", con);
                            cmd.Parameters.AddWithValue("@id", SangID);
                            cmd.Parameters.AddWithValue("@userID", userID);

                            SqlDataReader reader = cmd.ExecuteReader();
                            con.Close();

                            //Refresh tilbage til sig selv, så man kan se opdateringen.
                            Server.Transfer("AdminOverviewShowSongs.aspx");
                        }
                        catch (Exception edk)
                        {

                        }
                    }
                }
            }
        }
        public void changeSheetSangDerErValgtTilstand(int SangID, int type)
        {
            if (CheckBox1 == null) //Så kaldes denne fra Bruger og ikke Admin.
            {
                CheckBox1 = new CheckBox();
                CheckBox1.Checked = false; //Må IKKE slette.
            }

            if (CheckBox1.Checked) //SLET sang
            {
                CheckBox1.Checked = false;
                CheckBox1.Visible = false;
                try
                {
                    //------Delete TextLinjer-------------------------------------------
                    SqlConnection con2 = new SqlConnection(ConfigurationManager.ConnectionStrings["wenningsdbConnectionString1"].ToString());
                    con2.Open();
                    SqlCommand cmd2 = new SqlCommand("DELETE FROM SangSheetTextLinjer WHERE sangID=@sangID;", con2);
                    cmd2.Parameters.AddWithValue("@sangID", SangID);
                    int antalTextLinjerSlettet = cmd2.ExecuteNonQuery();
                    con2.Close();
                    //------Delete Akkorder-------------------------------------------
                    SqlConnection con3 = new SqlConnection(ConfigurationManager.ConnectionStrings["wenningsdbConnectionString1"].ToString());
                    con3.Open();
                    SqlCommand cmd3 = new SqlCommand("DELETE FROM SangSheetAkkorder WHERE sangID=@sangID;", con3);
                    cmd3.Parameters.AddWithValue("@sangID", SangID);
                    int antalAkkorderSlettet = cmd3.ExecuteNonQuery();
                    con3.Close();

                    //------Delete Noder-------------------------------------------
                    //-----Noder slettes før NodeLinjer, fordi de er relaterede.------------
                    SqlConnection con5 = new SqlConnection(ConfigurationManager.ConnectionStrings["wenningsdbConnectionString1"].ToString());
                    con5.Open();
                    SqlCommand cmd5 = new SqlCommand("DELETE FROM SangSheetNoder WHERE SangID=@sangID;", con5);
                    cmd5.Parameters.AddWithValue("@sangID", SangID);
                    int antalNoderSlettet = cmd5.ExecuteNonQuery();
                    con5.Close();

                    //------Delete NodeLinjer-------------------------------------------
                    SqlConnection con4 = new SqlConnection(ConfigurationManager.ConnectionStrings["wenningsdbConnectionString1"].ToString());
                    con4.Open();
                    SqlCommand cmd4 = new SqlCommand("DELETE FROM SangSheetNodeLinjer WHERE SangID=@sangID;", con4);
                    cmd4.Parameters.AddWithValue("@sangID", SangID);
                    int antalNodeLinjerSlettet = cmd4.ExecuteNonQuery();
                    con4.Close();



                    //----Denne må være til sidst fordi dette er primer tabellen. 
                    //------Delete Sang-------------------------------------------
                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["wenningsdbConnectionString1"].ToString());
                    con.Open();
                    SqlCommand cmd = new SqlCommand("DELETE FROM SangSheet WHERE Id=@id;", con);
                    cmd.Parameters.AddWithValue("@id", SangID);
                    int antalSangeSlettet = cmd.ExecuteNonQuery();
                    con.Close();

                    Response.Write("<script LANGUAGE='JavaScript' >alert('" + "Number of deleted songs=" + antalSangeSlettet + "  Number of Chords deleted=" + antalAkkorderSlettet + "  Number of deleted text lines=" + antalTextLinjerSlettet + "  Number of deleted Notes=" + antalNoderSlettet + "')</script>");


                    //Refresh tilbage til sig selv, så man kan se opdateringen.
                    Server.Transfer("AdminOverviewShowSongs.aspx");
                }
                catch (Exception edk)
                {
                    Response.Write("<script LANGUAGE='JavaScript' >alert('" + "Delete went wrong: " + edk.ToString() + "')</script>");

                }

            }
            else // ÆNDRE TYPE TILSTAND.
            {
                // 2016.11.04.Fejle i CheckBoxOwnerChange.Checked == false, fordi den er 
                // lavet visible = false og derfor ikke eksisterer. Denne fejle har jeg nok
                // indført selv når jeg fornyligt har lavet ændring...Derfor vælger jeg 
                // at lave en ny variabel lavChange som hånderer alle tilfælde, den erstatter
                // nu CheckBoxOwnerChange.Checked == false i conditionen.
                bool lavChange = false;
                if(CheckBoxOwnerChange == null)
                {
                    lavChange = true;
                }
                else
                {
                    if(CheckBoxOwnerChange.Checked == false)
                    {
                        lavChange = true;
                    }
                    else
                    {
                        lavChange = false;
                    }
                }
                if (lavChange) //lavChange erstatter CheckBoxOwnerChange.Checked == false.
                {
                    try
                    {
                        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["wenningsdbConnectionString1"].ToString());
                        con.Open();
                        //------Gemmer Sangen-------------------------------------------

                        SqlCommand cmd = new SqlCommand("UPDATE SangSheet SET type=@type WHERE id=@id;", con);
                        cmd.Parameters.AddWithValue("@id", SangID);
                        cmd.Parameters.AddWithValue("@type", type);

                        SqlDataReader reader = cmd.ExecuteReader();
                        con.Close();


                        //Refresh tilbage til sig selv, så man kan se opdateringen.
                        Server.Transfer("AdminOverviewShowSongs.aspx");
                    }
                    catch (Exception edk)
                    {

                    }
                }
                //-----2016.08.04----Tilført ændring af Owner af sangen------
                if (CheckBoxOwnerChange.Checked)
                {
                    if (DropDownListUsers.SelectedIndex > -1)
                    {
                        String userID = DropDownListUsers.SelectedValue;
                        try
                        {
                            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["wenningsdbConnectionString1"].ToString());
                            con.Open();
                            //------Gemmer Sangen-------------------------------------------

                            SqlCommand cmd = new SqlCommand("UPDATE SangSheet SET userID=@userID WHERE id=@id;", con);
                            cmd.Parameters.AddWithValue("@id", SangID);
                            cmd.Parameters.AddWithValue("@userID", userID);

                            SqlDataReader reader = cmd.ExecuteReader();
                            con.Close();


                            //Refresh tilbage til sig selv, så man kan se opdateringen.
                            Server.Transfer("AdminOverviewShowSongs.aspx");
                        }
                        catch (Exception edk)
                        {

                        }

                    }
                }
            }

        }
        private void sendMailLokal(String email, String name, String beskedTilBruger)
        {
            sendMail(email + "-" + name + "-" + beskedTilBruger);
            String besked = "Hi " + name + Environment.NewLine + "Your song has been approved, and is public for everyone to see." + Environment.NewLine + beskedTilBruger;
            SendMailMessage("localhost", "info@theSongApp.com", "theSongApp.com", email, name, "Song is approved. ", besked, true);
        }
        public void sendMail(String besked)
        {
            SendMailMessage("localhost", "info@theSongApp.com", "Sangur er góðtikint", "jakupwhansen@gmail.com", "Jákup", "Sangur er góðtikin ", besked, true);
        }

        public static bool SendMailMessage(string SMTPServer, string fromAddress, string fromName, string toAddress, string toName, string msgSubject, string msgBody, bool IsBodyHtml)
        {
            // Use the new v2.0 mail class to send an E-mail.

            try
            {
                SmtpClient client = new SmtpClient(SMTPServer);
                MailAddress from = new MailAddress(fromAddress, fromName);
                MailAddress to = new MailAddress(toAddress, toName);
                MailMessage message = new MailMessage(from, to);
                message.BodyEncoding = System.Text.Encoding.GetEncoding("iso-8859-1");
                message.Subject = msgSubject;
                message.IsBodyHtml = IsBodyHtml;
                message.Body = msgBody;
                client.Send(message);
            }
            catch (System.Net.Mail.SmtpException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }



        protected void ButtonSendApprovement_Click(object sender, EventArgs e)
        {
            if (Session["BrugerValgt"] != null)
            {
                Bruger BrugerValgt = (Bruger)Session["BrugerValgt"];
                if (BrugerValgt.ID != -1)
                {
                    sendMailLokal(BrugerValgt.email, BrugerValgt.name, TextBoxBeskedTilBruger.Text);
                }
            }

        }

    }
    class SangAdminValgt : IComparable
    {
        public AdminOverviewShowSongs admOverS;
        public FindAllSongs findAllS;
        public Button knap;
        public Button knapTilstand;
        public String title;
        public int SangID = 0;
        public int type = 0; //Tilstand.
        public bool sheetSong = false;
        public int CompareTo(object obj)
        {
            SangAdminValgt shtv = (SangAdminValgt)obj;
            return title.CompareTo(shtv.title);
        }


        public void eventMetode(object sender, EventArgs e)
        {
            if (sheetSong == false)
            {
                if (HttpContext.Current.Session["Admin"] != null)
                {
                    admOverS.visHvilkenSangDerErValgt(SangID, title);
                }
                else if (findAllS != null)
                {
                    createSongs cs = new createSongs();
                    Sang sangen = cs.createSong(SangID.ToString());

                    //---------------Kommer Fra--------------------------------------------------
                    List<String> listeKommerFra = (List<String>)HttpContext.Current.Session["KommerFra"];
                    if (!listeKommerFra[listeKommerFra.Count - 1].Equals("FindAllSongs.aspx"))
                    {
                        //Hvis man trykker flere gange på skift tilstand, så skal der ikke gemmes hver gang kun først gang.
                        listeKommerFra.Add("FindAllSongs.aspx");
                        HttpContext.Current.Session["KommerFra"] = listeKommerFra;
                    }
                    //--------------------------------------------------------------------------

                    HttpContext.Current.Session["Sang"] = sangen;
                    HttpContext.Current.Server.Transfer("ShowSong.aspx");

                }
            }
            else
            {
                //SHEET SONG VALGT.
                //                HttpContext.Current.Response.Write("<script LANGUAGE='JavaScript' >alert('" + "Sheet valgt" + "')</script>");

                if (findAllS != null)
                {
                    createSongs cs = new createSongs();
                    Sang sangen = cs.createSheetSong(SangID.ToString());

                    HttpContext.Current.Session["Sang"] = sangen;

                    //---------------Kommer Fra--------------------------------------------------
                    List<String> listeKommerFra = (List<String>)HttpContext.Current.Session["KommerFra"];
                    if (!listeKommerFra[listeKommerFra.Count - 1].Equals("FindAllSongs.aspx"))
                    {
                        //Hvis man trykker flere gange på skift tilstand, så skal der ikke gemmes hver gang kun først gang.
                        listeKommerFra.Add("FindAllSongs.aspx");
                        HttpContext.Current.Session["KommerFra"] = listeKommerFra;
                    }
                    //--------------------------------------------------------------------------
                    HttpContext.Current.Server.Transfer("ShowSheetSong.aspx");
                }
                else
                {
                    createSongs cs = new createSongs();
                    Sang sangen = cs.createSheetSong(SangID.ToString());

                    HttpContext.Current.Session["Sang"] = sangen;

                    //---------------Kommer Fra--------------------------------------------------
                    List<String> listeKommerFra = (List<String>)HttpContext.Current.Session["KommerFra"];
                    listeKommerFra.Add("AdminOverviewShowSongs.aspx");
                    HttpContext.Current.Session["KommerFra"] = listeKommerFra;
                    //----------------------------------------------------------------------------

                    HttpContext.Current.Server.Transfer("NewSheetSong.aspx");
                }
            }
            // String idstring = ID.ToString();
            //findS.createSong(idstring);
        }

        public void eventTilstand(object sender, EventArgs e)
        {
            // 0 = Private
            // 1 = Public
            // 2 = Link
            // 3 = Request

            int nyType = 0;
            if (HttpContext.Current.Session["Admin"] == null)
            {  //ALMINDELIG bruger her..
                if (type == 0) //Private.
                {
                    nyType = 2;
                    if (sheetSong == true)
                    {
                        //directLink
                        String dirLink = "http://theSongApp.com/FindSheetSong.aspx?Sheet=" + SangID + "_" + title.Length * SangID;
                        HttpContext.Current.Session["DirekteLink"] = dirLink;
                    }
                    else
                    {
                        //directLink
                        String dirLink = "http://theSongApp.com/FindSong.aspx?Song=" + SangID + "_" + title.Length * SangID;
                        HttpContext.Current.Session["DirekteLink"] = dirLink;
                    }
                }
                else if (type == 1) //Public
                {
                    nyType = 1; //Hvis type 1 som er Public, må bruger ikke ændre tilstand mere.
                    //-----ÆNDRING 2016.06.20...Ønsker at public sange også skal kunne linkes til, selvf.
                    if (sheetSong == true)
                    {
                        //directLink
                        String dirLink = "http://theSongApp.com/FindSheetSong.aspx?Sheet=" + SangID + "_" + title.Length * SangID;
                        HttpContext.Current.Session["DirekteLink"] = dirLink;
                    }
                    else
                    {
                        //directLink
                        String dirLink = "http://theSongApp.com/FindSong.aspx?Song=" + SangID + "_" + title.Length * SangID;
                        HttpContext.Current.Session["DirekteLink"] = dirLink;
                    }


                }
                else if (type == 2) //Link
                {
                    nyType = 3;
                }
                else if (type == 3) //Request
                {
                    nyType = 0;
                }
            }
            else
            {  //ADMINISTRATOR her..
                if (type == 0)
                {
                    nyType = 1;
                    //-----ÆNDRING 2016.06.29...Når admin kompierer en sang og laver sin egen udgave, skal
                    //-----han også kunne lave link til sangen.
                    if (sheetSong == true)
                    {
                        //directLink
                        String dirLink = "http://theSongApp.com/FindSheetSong.aspx?Sheet=" + SangID + "_" + title.Length * SangID;
                        HttpContext.Current.Session["DirekteLink"] = dirLink;
                    }
                    else
                    {
                        //directLink
                        String dirLink = "http://theSongApp.com/FindSong.aspx?Song=" + SangID + "_" + title.Length * SangID;
                        HttpContext.Current.Session["DirekteLink"] = dirLink;
                    }

                }
                else if (type == 1)
                {
                    nyType = 0;
                }
            }

            if (sheetSong == false)
            {
                admOverS.changeSangDerErValgtTilstand(SangID, nyType);
            }
            else if (sheetSong == true)
            {
                admOverS.changeSheetSangDerErValgtTilstand(SangID, nyType);
            }
        }
    }
}