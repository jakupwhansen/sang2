﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewSongAddChords.aspx.cs" Inherits="Sang.NewSongAddChords" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!--  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script> -->

    <link rel="stylesheet" href="/bootstrap-3.3.5-dist/css/bootstrap.min.css" />
    <script src="/jquery/jquery-1.11.3.min.js"></script>
    <script src="/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/css/StyleSheet1.css" />

    <script type="text/javascript">
        window.onload = function () {
            var scrollY = parseInt('<%=Request.Form["scrollY"] %>');
            if (!isNaN(scrollY)) {
                window.scrollTo(0, scrollY);
            }
        };
        window.onscroll = function () {
            var scrollY = document.body.scrollTop;
            if (scrollY == 0) {
                if (window.pageYOffset) {
                    scrollY = window.pageYOffset;
                }
                else {
                    scrollY = (document.body.parentElement) ? document.body.parentElement.scrollTop : 0;
                }
            }
            if (scrollY > 0) {
                var input = document.getElementById("scrollY");
                if (input == null) {
                    input = document.createElement("input");
                    input.setAttribute("type", "hidden");
                    input.setAttribute("id", "scrollY");
                    input.setAttribute("name", "scrollY");
                    document.forms[0].appendChild(input);
                }
                input.value = scrollY;
            }
        };

    </script>

    <title>NewSongAddChords</title>
    <style type="text/css">
        .min-style {
            vertical-align: top;
        }

        .auto-style3 {
            width: 66px;
        }

        .auto-style4 {
            height: 22px;
            width: 208px;
        }

        .auto-style5 {
            width: 208px;
        }

        .auto-style6 {
            text-align: center;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <table style="width: 40%;">
                <tr>
                    <td class="auto-style4">

                        <div>
                            <input type="image" runat="server" name="coord" id="Image1" src="BitMapNewSong.aspx" onserverclick="xymetoden" />
                        </div>

                    </td>
                    <td class="min-style">
                        <table style="width: 100%;">
                            <tr>
                                <td class="auto-style6">Chord</td>
                                <td class="auto-style6">Extra</td>
                                <td class="auto-style6">Bass</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:DropDownList ID="DropDownList1" runat="server" Font-Size="16pt" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" TabIndex="1">
                                        <asp:ListItem></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBox1" runat="server" Width="60px" Font-Size="14pt" TabIndex="2"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:DropDownList ID="DropDownList2" runat="server" Font-Size="16pt" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" TabIndex="3">
                                        <asp:ListItem></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="CheckBoxChord" runat="server" TabIndex="6" AutoPostBack="True" OnCheckedChanged="CheckBoxChord_CheckedChanged" Checked="True" />
                                </td>
                                <td>
                                    <asp:CheckBox ID="CheckBoxExtra" runat="server" TabIndex="6" />
                                </td>
                                <td>
                                    <asp:CheckBox ID="CheckBoxBass" runat="server" TabIndex="6" AutoPostBack="True" OnCheckedChanged="CheckBoxBass_CheckedChanged" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button CssClass="btn-default" ID="btn_UndoChord" runat="server" Height="43px" OnClick="btn_UndoChord_Click" Text="Undo" Width="79px" Font-Bold="True" Font-Size="16pt" CausesValidation="False" TabIndex="3" />
                                </td>
                                <td>
                                    <asp:Button CssClass="btn-default" ID="btn_Edit" runat="server" Height="43px" OnClick="btn_Edit_Click" Text="Edit" Width="79px" Font-Bold="True" Font-Size="16pt" CausesValidation="False" TabIndex="3" />
                                </td>
                                <td>
                                    <asp:CheckBox ID="CheckBoxSameHeight" runat="server" TabIndex="6" AutoPostBack="True" OnCheckedChanged="CheckBoxSameHeight_CheckedChanged" Text="Same height" Checked="True" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:Button CssClass="btn-default" ID="btn_SaveSong" runat="server" Height="60px" OnClick="btn_SaveSong_Click" Text="Save song" Width="180px" Font-Bold="True" Font-Size="16pt" TabIndex="4" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:CheckBox ID="CheckBox1" runat="server" Visible="False" TabIndex="7" />
                                </td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                        <asp:TextBox ID="TextBoxBesked" runat="server" Height="141px" TextMode="MultiLine" Visible="False" Width="173px" TabIndex="5">You can not make changes to the song after it is saved.

To save the song, you must check the check box, and 
push the &quot;Save song&quot; button again.</asp:TextBox>
                        <br />
                    </td>
                    <td class="min-style">&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style5">&nbsp;</td>
                    <td class="auto-style3">&nbsp;</td>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text="Label" Visible="False"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style5">&nbsp;</td>
                    <td class="auto-style3">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>

        </div>
    </form>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-54970963-3', 'auto');
        ga('send', 'pageview');

    </script>
</body>
</html>
