﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShowSongTouchChords.aspx.cs" Inherits="Sang.ShowSongTouchChords" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ShowSongTouchChords</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />




    <!-- Før Canvas -->
    <link rel="stylesheet" href="/bootstrap-3.3.5-dist/css/bootstrap.min.css" />
    <script src="/jquery/jquery-1.11.3.min.js"></script>
    <script src="/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>

    <script type='application/javascript' src='/fastClick/fastclick.js'></script>
    <script type="application/javascript">
        window.addEventListener('load', function () {
            var testB;
            testB = document.getElementById('iPadKnap');
            // Android 2.2 needs FastClick to be instantiated before the other listeners so that the stopImmediatePropagation hack can work.
            FastClick.attach(testB);

            testB.addEventListener('touchend', function (event) {
            }, false);

            testB.addEventListener('click', function (event) {
                playOnClick();
                testB.style.backgroundColor = testB.style.backgroundColor ? '' : 'YellowGreen';
            }, false);
        }, false);
    </script>



    <link rel="stylesheet" type="text/css" href="/css/StyleSheet1.css" />

    <style type="text/css">
        /* Disable certain interactions on touch devices */
        body {
            -webkit-touch-callout: none;
            -webkit-text-size-adjust: none;
            -webkit-user-select: none;
            -webkit-highlight: none;
            -webkit-tap-highlight-color: rgba(0,0,0,0);
        }


        .my-style {
            vertical-align: top;
        }

        #nav {
            width: 247px;
        }

        .jakupImgsize {
            height: 50px;
            width: 50px;
        }
    </style>
</head>
<body onload="init()">

    <form id="form1" runat="server">
        <div>

            <div class="container-fluid">
                <div>

                    <br />

                    <asp:Button CssClass="btn-default" ID="ButtonBack" runat="server" Text="Back" OnClick="ButtonBack_Click" />

                    &nbsp;

                    <asp:CheckBox ID="CheckBox1" runat="server" AutoPostBack="True" OnCheckedChanged="CheckBox1_CheckedChanged" TabIndex="5" Text="AutoOrder" />

                    <asp:Label ID="LabelBeskedOmBrowsere" runat="server" Text="Label" Visible="False"></asp:Label>
                    <br />
                    <br />

                    <button type="button" id="iPadKnap" class="btn btn-default btn-lg" style="height: 50px; width: 120px; background-repeat: no-repeat; background-size: 100%;">Next</button>
                    <button type="button" id="knap_tilStart" class="btn btn-default btn-lg" style="height: 50px; width: 20px; background-repeat: no-repeat; background-size: 100%;"></button>

                    <br />

                    <br />

                    <!--    <img src="BitMapNewSheetSongShow.aspx" alt="song" style="width: 100%; max-width: 600px; visibility:hidden;" /><br />
               -->
                    <canvas id="canvas" style="width: 100%; max-width: 600px">

                    </canvas>

                    <br />

                    <br />

                </div>
                <br />


                <div>
                </div>

            </div>

        </div>
    </form>
    <script>


        var aktivAudio1;
        var aktivAudio2;
        var aktivAudio3;
        var aktivAudio4;
        var aktivAudio5;
        var aktivAudio6;
        var aktivAudioBass;
        var audioAkkorder;
        var audioBass;
        var volume = 0.1;

        function init() {



            audioAkkorder = [
                          new Audio('Audio/c1.mp3'),
                          new Audio('Audio/db1.mp3'),
                          new Audio('Audio/d1.mp3'),
                          new Audio('Audio/eb1.mp3'),
                          new Audio('Audio/e1.mp3'),
                          new Audio('Audio/f1.mp3'),
                          new Audio('Audio/gb1.mp3'),
                          new Audio('Audio/g1.mp3'),
                          new Audio('Audio/ab1.mp3'),
                          new Audio('Audio/a1.mp3'),
                          new Audio('Audio/bb1.mp3'),
                          new Audio('Audio/b1.mp3'),
                          new Audio('Audio/c2.mp3'),
                          new Audio('Audio/db2.mp3'),
                          new Audio('Audio/d2.mp3'),
                          new Audio('Audio/eb2.mp3'),
                          new Audio('Audio/e2.mp3'),
                          new Audio('Audio/f2.mp3'),
                          new Audio('Audio/gb2.mp3'),
                          new Audio('Audio/g2.mp3'),
                          new Audio('Audio/ab2.mp3'),
                          new Audio('Audio/a2.mp3'),
                          new Audio('Audio/bb2.mp3'),
                          new Audio('Audio/b2.mp3'),
                          new Audio('Audio/c2.mp3'),
                          new Audio('Audio/db2.mp3'),
                          new Audio('Audio/d2.mp3'),
                          new Audio('Audio/eb2.mp3'),
                          new Audio('Audio/e2.mp3'),
                          new Audio('Audio/f2.mp3'),
                          new Audio('Audio/gb2.mp3'),
                          new Audio('Audio/g2.mp3'),
                          new Audio('Audio/ab2.mp3'),
                          new Audio('Audio/a2.mp3'),
                          new Audio('Audio/bb2.mp3'),
                          new Audio('Audio/b2.mp3')];
            audioBass = [
                         new Audio('Audio/c0.mp3'),
                         new Audio('Audio/db0.mp3'),
                         new Audio('Audio/d0.mp3'),
                         new Audio('Audio/eb0.mp3'),
                         new Audio('Audio/e0.mp3'),
                         new Audio('Audio/f0.mp3'),
                         new Audio('Audio/gb0.mp3'),
                         new Audio('Audio/g0.mp3'),
                         new Audio('Audio/ab0.mp3'),
                         new Audio('Audio/a0.mp3'),
                         new Audio('Audio/bb0.mp3'),
                         new Audio('Audio/b0.mp3')];

            can = document.getElementById("canvas");
            ctx = can.getContext("2d");

            can.addEventListener("mousedown", mouseDown, false);
            can.addEventListener("mousemove", mouseXY, false);
            can.addEventListener("touchstart", touchDown, false);
            can.addEventListener("touchmove", touchXY, true);
            can.addEventListener("touchend", touchUp, false);

            document.body.addEventListener("mouseup", mouseUp, false);
            document.body.addEventListener("touchcancel", touchUp, false);


            document.getElementById("knap_tilStart").addEventListener("click", function () {
                ctx.drawImage(background, 0, 0);
                position = 1;               
            });



            //document.getElementById("knap_nyBogstav").addEventListener("click", function ()
            //{
            //    playOnClick();
            //});
        }



        function mouseUp() {
            mouseIsDown = 0;
            mouseXY();
        }

        function touchUp() {
            mouseIsDown = 0;
            // no touch to track, so just show state
            showPos();
        }

        function mouseDown() {
            mouseIsDown = 1;
            mouseXY();
        }

        function touchDown() {
            mouseIsDown = 1;
            touchXY();
        }

        function mouseXY(e) {
            if (!e)
                var e = event;
            canX = e.pageX - can.offsetLeft;
            canY = e.pageY - can.offsetTop;
            showPos();
        }

        function touchXY(e) {
            if (!e)
                var e = event;
            e.preventDefault();
            canX = e.targetTouches[0].pageX - can.offsetLeft;
            canY = e.targetTouches[0].pageY - can.offsetTop;
            showPos();
        }
        function showPos() {
            // large, centered, bright green text
            ctx.font = "24pt Helvetica";
            ctx.textAlign = "center";
            ctx.textBaseline = "middle";

            var str = canX + ", " + canY;
            if (mouseIsDown) {

               var teller = 0;
                var arlength = arrayHeight.length;
                while (teller < arlength) {
                    var x = parseInt(arrayX[teller]);
                    var y = parseInt(arrayY[teller]);
                    //ctx.fillText(arrayHeight[teller], x, y, 30);
                    //ctx.strokeRect(x, y, 40, 30);

                    if (collision(x + 20, y + 15, canX, canY, 20, 15)) {
                        playChord(x, y, teller);
                        position = teller + 1;//Så bliver teller stående //2017.04.21 ændret til +1, 
                        //så når man trykker knappen lyder den næste akkord og ikke den samme igen.

                        //2017.04.21 Sætter fokus på knappen efter hver gang musen bruges, så kan man nemlig bruge 
                        //knappen med det samme efter mus, uden først at skulle trykke på knappen med musen, 
                        //men bare bruger ENTER på tastaturet.
                        document.getElementById("iPadKnap").focus();
                        
                        break; 
                    }
                    teller++;
                }
            }
            if (!mouseIsDown) {
                //     ctx.clearRect(0, 0, can.width, can.height);
            }
        }

        function collision(aX, aY, bX, bY, width, height) {
            var svar;
            if (aX - bX > 0 && aX - bX < width || bX - aX < width && bX - aX > 0) {
                if (aY - bY > 0 && aY - bY < height || bY - aY < height && bY - aY > 0) {
                    svar = true;
                }
            }
            return svar;
        }
        var position = 1;
        

        function playOnClick() {
            if (position == 1)
            {
                ctx.drawImage(background, 0, 0);
            }
            ctx.strokeStyle = "black";
            x = parseInt(arrayX[position]);
            y = parseInt(arrayY[position]);
            playChord(x, y, position);
            position = position + 1;
            if (position >= arrayX.length) {
                position = 1;
            }
        }


        function playChord(x, y, teller) {

            var ii = arrayHeight[teller];

            //if (aktivAudio1 != undefined) {
            //    aktivAudio1.pause();
            //}
            //if (aktivAudio2 != undefined) {
            //    aktivAudio2.pause();
            //}
            //if (aktivAudio3 != undefined) {
            //    aktivAudio3.pause();
            //}
            //if (aktivAudio4 != undefined) {
            //    aktivAudio4.pause();
            //}
            //if (aktivAudio5 != undefined) {
            //    aktivAudio5.pause();
            //}
            //if (aktivAudio6 != undefined) {
            //    aktivAudio6.pause();
            //}

            //if (aktivAudioBass != undefined) {
            //    aktivAudioBass.pause();
            //}
          

            if (arrayExtra[teller] == "") //Dur
            {
                aktivAudio1 = audioAkkorder[ii - 1];
                aktivAudio2 = audioAkkorder[(ii - 1) + 4];
                aktivAudio3 = audioAkkorder[(ii - 1) + 7];

                aktivAudio1.volume = volume;
                aktivAudio2.volume = volume;
                aktivAudio3.volume = volume;

                aktivAudio1.play();
                aktivAudio2.play();
                aktivAudio3.play();
            }
            else if (arrayExtra[teller].toLowerCase() == "b5" || arrayExtra[teller].toLowerCase() == "-5") //Dur
            {
                aktivAudio1 = audioAkkorder[ii - 1];
                aktivAudio2 = audioAkkorder[(ii - 1) + 4];
                aktivAudio3 = audioAkkorder[(ii - 1) + 6];

                aktivAudio1.volume = volume;
                aktivAudio2.volume = volume;
                aktivAudio3.volume = volume;

                aktivAudio1.play();
                aktivAudio2.play();
                aktivAudio3.play();
            }
            else if (arrayExtra[teller].toLowerCase() == "+") //Dur
            {
                aktivAudio1 = audioAkkorder[ii - 1];
                aktivAudio2 = audioAkkorder[(ii - 1) + 4];
                aktivAudio3 = audioAkkorder[(ii - 1) + 8];

                aktivAudio1.volume = volume;
                aktivAudio2.volume = volume;
                aktivAudio3.volume = volume;

                aktivAudio1.play();
                aktivAudio2.play();
                aktivAudio3.play();
            }
            else if (arrayExtra[teller].toLowerCase() == "6") //Dur
            {
                aktivAudio1 = audioAkkorder[ii - 1];
                aktivAudio2 = audioAkkorder[(ii - 1) + 4];
                aktivAudio3 = audioAkkorder[(ii - 1) + 7];
                aktivAudio4 = audioAkkorder[(ii - 1) + 9];

                aktivAudio1.volume = volume;
                aktivAudio2.volume = volume;
                aktivAudio3.volume = volume;
                aktivAudio4.volume = volume;

                aktivAudio1.play();
                aktivAudio2.play();
                aktivAudio3.play();
                aktivAudio4.play();

            }
            else if (arrayExtra[teller].toLowerCase() == "m6") {
                aktivAudio1 = audioAkkorder[ii - 1];
                aktivAudio2 = audioAkkorder[(ii - 1) + 3];
                aktivAudio3 = audioAkkorder[(ii - 1) + 7];
                aktivAudio4 = audioAkkorder[(ii - 1) + 9];

                aktivAudio1.volume = volume;
                aktivAudio2.volume = volume;
                aktivAudio3.volume = volume;
                aktivAudio4.volume = volume;

                aktivAudio1.play();
                aktivAudio2.play();
                aktivAudio3.play();
                aktivAudio4.play();

            }
            else if (arrayExtra[teller].toLowerCase() == "7") //Dur
            {
                aktivAudio1 = audioAkkorder[ii - 1];
                aktivAudio2 = audioAkkorder[(ii - 1) + 4];
                aktivAudio3 = audioAkkorder[(ii - 1) + 7];
                aktivAudio4 = audioAkkorder[(ii - 1) + 10];

                aktivAudio1.volume = volume;
                aktivAudio2.volume = volume;
                aktivAudio3.volume = volume;
                aktivAudio4.volume = volume;

                aktivAudio1.play();
                aktivAudio2.play();
                aktivAudio3.play();
                aktivAudio4.play();

            }
            else if (arrayExtra[teller].toLowerCase() == "maj") //Dur
            {
                aktivAudio1 = audioAkkorder[ii - 1];
                aktivAudio2 = audioAkkorder[(ii - 1) + 4];
                aktivAudio3 = audioAkkorder[(ii - 1) + 7];
                aktivAudio4 = audioAkkorder[(ii - 1) + 11];

                aktivAudio1.volume = volume;
                aktivAudio2.volume = volume;
                aktivAudio3.volume = volume;
                aktivAudio4.volume = volume;

                aktivAudio1.play();
                aktivAudio2.play();
                aktivAudio3.play();
                aktivAudio4.play();
            }
            else if (arrayExtra[teller].toLowerCase() == "9") //
            {
                aktivAudio1 = audioAkkorder[ii - 1];
                aktivAudio2 = audioAkkorder[(ii - 1) + 4];
                aktivAudio3 = audioAkkorder[(ii - 1) + 7];
                aktivAudio4 = audioAkkorder[(ii - 1) + 10];

                aktivAudio5 = audioAkkorder[(ii - 1) + 14];


                aktivAudio1.volume = volume;
                aktivAudio2.volume = volume;
                aktivAudio3.volume = volume;
                aktivAudio4.volume = volume;

                aktivAudio5.volume = volume;

                aktivAudio1.play();
                aktivAudio2.play();
                aktivAudio3.play();
                aktivAudio4.play();

                aktivAudio5.play();
            }
            else if (arrayExtra[teller].toLowerCase() == "-9") //Flat 9
            {
                aktivAudio1 = audioAkkorder[ii - 1];
                aktivAudio2 = audioAkkorder[(ii - 1) + 4];
                aktivAudio3 = audioAkkorder[(ii - 1) + 7];
                aktivAudio4 = audioAkkorder[(ii - 1) + 10];

                aktivAudio5 = audioAkkorder[(ii - 1) + 13];


                aktivAudio1.volume = volume;
                aktivAudio2.volume = volume;
                aktivAudio3.volume = volume;
                aktivAudio4.volume = volume;

                aktivAudio5.volume = volume;

                aktivAudio1.play();
                aktivAudio2.play();
                aktivAudio3.play();
                aktivAudio4.play();

                aktivAudio5.play();
            }
            else if (arrayExtra[teller].toLowerCase() == "11") //
            {
                aktivAudio1 = audioAkkorder[ii - 1];
                aktivAudio2 = audioAkkorder[(ii - 1) + 4];
                aktivAudio3 = audioAkkorder[(ii - 1) + 7];
                aktivAudio4 = audioAkkorder[(ii - 1) + 10];

                aktivAudio5 = audioAkkorder[(ii - 1) + 14];
                aktivAudio6 = audioAkkorder[(ii - 1) + 17];

                aktivAudio1.volume = volume;
                aktivAudio2.volume = volume;
                aktivAudio3.volume = volume;
                aktivAudio4.volume = volume;

                aktivAudio5.volume = volume;
                aktivAudio6.volume = volume;

                aktivAudio1.play();
                aktivAudio2.play();
                aktivAudio3.play();
                aktivAudio4.play();

                aktivAudio5.play();
                aktivAudio6.play();
            }
            else if (arrayExtra[teller].toLowerCase() == "sus") {
                aktivAudio1 = audioAkkorder[ii - 1];
                aktivAudio2 = audioAkkorder[(ii - 1) + 5];
                aktivAudio3 = audioAkkorder[(ii - 1) + 7];

                aktivAudio1.volume = volume;
                aktivAudio2.volume = volume;
                aktivAudio3.volume = volume;

                aktivAudio1.play();
                aktivAudio2.play();
                aktivAudio3.play();
            }
            else if (arrayExtra[teller].toLowerCase() == "dim" || arrayExtra[teller].toLowerCase() == "o") {
                aktivAudio1 = audioAkkorder[ii - 1];
                aktivAudio2 = audioAkkorder[(ii - 1) + 3];
                aktivAudio3 = audioAkkorder[(ii - 1) + 6];

                aktivAudio1.volume = volume;
                aktivAudio2.volume = volume;
                aktivAudio3.volume = volume;

                aktivAudio1.play();
                aktivAudio2.play();
                aktivAudio3.play();
            }
            else if (arrayExtra[teller].toLowerCase() == "m") //Mol
            {
                aktivAudio1 = audioAkkorder[ii - 1];
                aktivAudio2 = audioAkkorder[(ii - 1) + 3];
                aktivAudio3 = audioAkkorder[(ii - 1) + 7];

                aktivAudio1.volume = volume;
                aktivAudio2.volume = volume;
                aktivAudio3.volume = volume;

                aktivAudio1.play();
                aktivAudio2.play();
                aktivAudio3.play();
            }
            else if (arrayExtra[teller].toLowerCase() == "m7") {
                aktivAudio1 = audioAkkorder[ii - 1];
                aktivAudio2 = audioAkkorder[(ii - 1) + 3];
                aktivAudio3 = audioAkkorder[(ii - 1) + 7];
                aktivAudio4 = audioAkkorder[(ii - 1) + 10];

                aktivAudio1.volume = volume;
                aktivAudio2.volume = volume;
                aktivAudio3.volume = volume;
                aktivAudio4.volume = volume;

                aktivAudio1.play();
                aktivAudio2.play();
                aktivAudio3.play();
                aktivAudio4.play();
            }
            else //Ukendt. Spiller en DUR.
            {
                aktivAudio1 = audioAkkorder[ii - 1];
                aktivAudio2 = audioAkkorder[(ii - 1) + 4];
                aktivAudio3 = audioAkkorder[(ii - 1) + 7];

                aktivAudio1.volume = volume;
                aktivAudio2.volume = volume;
                aktivAudio3.volume = volume;

                aktivAudio1.play();
                aktivAudio2.play();
                aktivAudio3.play();
            }
            var taln = 0;
            var talm = -1;

            if (arrayBass[teller] == talm || arrayBass[teller] == taln) {
                aktivAudioBass = audioBass[(ii - 1)]; //GrundBass

                aktivAudioBass.volume = volume;
                aktivAudioBass.play();
            }
            else {
                var tal = parseInt(arrayBass[teller]);
                aktivAudioBass = audioBass[tal - 1]; //GrundBass
                aktivAudioBass.volume = volume;
                aktivAudioBass.play();
            }
            //tegner firkant om aktiv akkord
            ctx.strokeRect(x, y, 40, 30);

        }


        //---Variabler --------------------------
        var t = 1;

        var arrayX;
        var arrayY;
        var arrayHeight;
        var arrayExtra;
        var arrayBass;
        var nodeFundet = false;
        var ctx;
        var player;
        var tilstandIsPlaying = "stoped";
        var canvas;
        var background;
        //---Opsætning-----------------------------
        canvas = document.getElementById("canvas");
        ctx = canvas.getContext("2d");
        ctx.strokeStyle = "black";
        canvas.hidden = false;
        canvas.width = 400;
        canvas.height = 300;
        background = new Image();
        background.src = "BitMapNewSong.aspx";
        background.onload = function () {
            canvas.width = background.width;
            canvas.height = background.height;
            ctx.drawImage(background, 0, 0);




            var aX = "<%=Session["samletAkkorderX"]%>";
            arrayX = aX.split(":");
            var aY = "<%=Session["samletAkkorderY"]%>";
            arrayY = aY.split(":");
            var aH = "<%=Session["samletAkkorderKeyNumber"]%>";
            arrayHeight = aH.split(":");
            var aE = "<%=Session["samletAkkorderExtraName"]%>";
            arrayExtra = aE.split(":");
            var aB = "<%=Session["samletAkkorderBass"]%>";
            arrayBass = aB.split(":");
        }
    </script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-54970963-3', 'auto');
        ga('send', 'pageview');

    </script>


</body>
</html>
