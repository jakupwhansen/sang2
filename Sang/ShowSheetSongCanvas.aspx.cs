﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sang
{
    public partial class ShowSheetSongCanvas : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["Bruger"] == null)
            {
                Server.Transfer("Default.aspx");
            }
            if (!IsPostBack)//FØRSTE GANG.
            {
                DropDownList1.DataSource = GeneralData.Beats;
                DropDownList1.DataBind();
            }
            if (Session["BeskedOmBrowsere"] == null)
            {
                LabelBeskedOmBrowsere.Visible = true;
                LabelBeskedOmBrowsere.Text = "   Works best with: Chrome, Opera and Firefox.";
                Session["BeskedOmBrowsere"] = "Besked";
            }
            else
            {
                LabelBeskedOmBrowsere.Visible = false;
                LabelBeskedOmBrowsere.Text = "";
            }

        }

        protected void ButtonBack_Click(object sender, EventArgs e)
        {
            if (Session["Bruger"] != null)
            {
                Bruger bruger = (Bruger)Session["Bruger"];
                if (bruger.ID == -1)
                {
                    //-1 betyder direkte link...Så skal skal slettes så vi ikke kan går til andre sider,
                    //Denne her bruger skal KUN se sheet song og ikke andet.
                    // fordi denne bruger eksisterer ikke rigtig.
            //        Session["Bruger"] = null;
                }
            }

            if (Session["KommerFra"] != null)
            {
                List<String> listeKommerFra = (List<String>)Session["KommerFra"];
                String tilbageTil = listeKommerFra[listeKommerFra.Count - 1];
                listeKommerFra.RemoveAt(listeKommerFra.Count - 1);
                Session["KommerFra"] = listeKommerFra;

                Server.Transfer(tilbageTil);
            }
            else
            {
                Server.Transfer("Default.aspx");
            }
        }

        protected void ButtonUp_Click(object sender, EventArgs e)
        {
            if (Session["sang"] != null)
            {
                Sang sang = (Sang)Session["sang"];
                foreach (Akkord ak in sang.listAkkorder)
                {
                    if (ak.keyNumber < 12)
                    {
                        ak.keyNumber = ak.keyNumber + 1;
                    }
                    else
                    {
                        ak.keyNumber = 1;
                    }

                    if (ak.bass > 0) //0 er INGEN bass
                    {
                        if (ak.bass < 12)
                        {
                            ak.bass = ak.bass + 1;
                        }
                        else
                        {
                            ak.bass = 1;
                        }
                    }
                }
                Session["sang"] = sang;
            }

        }

        protected void ButtonDown_Click(object sender, EventArgs e)
        {
            if (Session["sang"] != null)
            {
                Sang sang = (Sang)Session["sang"];
                foreach (Akkord ak in sang.listAkkorder)
                {
                    if (ak.keyNumber > 1)
                    {
                        ak.keyNumber = ak.keyNumber - 1;
                    }
                    else
                    {
                        ak.keyNumber = 12;
                    }
                    if (ak.bass > 0) //0 er INGEN bass
                    {

                        if (ak.bass > 1)
                        {
                            ak.bass = ak.bass - 1;
                        }
                        else
                        {
                            ak.bass = 12;
                        }
                    }
                }
                Session["sang"] = sang;
            }

        }


        protected void PlaySong_Click(object sender, EventArgs e)
        {
            PlaySong.Visible = false;
            showPlaySong();
        }
        private void showPlaySong()
        {
            //---Så laver vi Sheet sangin til animation-----------------------
            if (Session["Sang"] == null)
            {
                Server.Transfer("Default.aspx");
            }
            else
            {
                Sang sangen = (Sang)Session["Sang"];
                //---Bruges til JavaScript til at vise hvilken node spilles.-----------------
                String samletLength = "";
                String samletNodeX = "";
                String samletNodeY = "";
                String samletNodeHeight = "";
                int samletLengthint = 0;
                foreach (NodeLinjer nl in sangen.listNodeLinjer)
                {
                    foreach (Noder no in nl.listNoder)
                    {
                        if (no.length > 0 && no.length < 99)
                        {
                            samletLengthint = samletLengthint + no.length;
                            samletLength = samletLength + ":" + samletLengthint;
                            samletNodeX = samletNodeX + ":" + no.pos.X;
                            samletNodeY = samletNodeY + ":" + no.pos.Y;
                            samletNodeHeight = samletNodeHeight + ":" + (23 + 12 + no.keyNumber);
                        }
                    }
                }
                Session["TempoValgt"] = sangen.tempo;
                Session["CanvasNodeLength"] = samletLength;
                Session["CanvasNodeX"] = samletNodeX;
                Session["CanvasNodeY"] = samletNodeY;
                Session["samletNodeHeight"] = samletNodeHeight;
            }


            //---------------------------------------------------------------
            NewSheetSong nss = new NewSheetSong();
            nss.createMidiSong(0, 0, CheckBoxChordsIncluded.Checked);
          //  createMidiSong();          
            PanelPlay.Visible = true;
            DropDownList1.SelectedIndex = 4; //Tempo DropDownBox
          
        }
        private void hidePlaySong()
        {
          


        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
                Session["TempoValgt"] = DropDownList1.SelectedIndex;
        }

        protected void CheckBoxInstruments_CheckedChanged(object sender, EventArgs e)
        {
        }

        protected void buttonMore_Click(object sender, EventArgs e)
        {
        }

        protected void CheckBoxChordsIncluded_CheckedChanged(object sender, EventArgs e)
        {
          
            NewSheetSong nss = new NewSheetSong();
            nss.createMidiSong(0, 0, CheckBoxChordsIncluded.Checked);

        }

        protected void CheckBoxShowNodeName_CheckedChanged(object sender, EventArgs e)
        {
            if (Session["Sang"] != null)
            {
                Sang sangen = (Sang)Session["Sang"];
                sangen.tegneNodeNavn = true;
            }
            
        } 



        
    }
}