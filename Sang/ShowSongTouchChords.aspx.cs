﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sang
{
    public partial class ShowSongTouchChords : System.Web.UI.Page
    {
       
        protected void Page_Load(object sender, EventArgs e)
        {

            //if (Session["Bruger"] == null)
            //{
            //    Server.Transfer("Default.aspx");
            //}
            if (!IsPostBack)//FØRSTE GANG.
            {
                showPlaySong();
            }
            if (Session["BeskedOmBrowsere"] == null)
            {
                Session["BeskedOmBrowsere"] = "Besked";
                LabelBeskedOmBrowsere.Visible = true;
                LabelBeskedOmBrowsere.Text = "Works in all Browsers and iPad's";
            }
            else
            {
                LabelBeskedOmBrowsere.Visible = false;
                LabelBeskedOmBrowsere.Text = "";
            }

        }

        protected void ButtonBack_Click(object sender, EventArgs e)
        {
            if (Session["Bruger"] != null)
            {
                Bruger bruger = (Bruger)Session["Bruger"];
                if (bruger.ID == -1)
                {
                    //-1 betyder direkte link...Så skal skal slettes så vi ikke kan går til andre sider,
                    //Denne her bruger skal KUN se sheet song og ikke andet.
                    // fordi denne bruger eksisterer ikke rigtig.
            //        Session["Bruger"] = null;
                }
            }

            if (Session["KommerFra"] != null)
            {
                List<String> listeKommerFra = (List<String>)Session["KommerFra"];
                String tilbageTil = listeKommerFra[listeKommerFra.Count - 1];
                listeKommerFra.RemoveAt(listeKommerFra.Count - 1);
                Session["KommerFra"] = listeKommerFra;

                Server.Transfer(tilbageTil);
            }
            else
            {
                Server.Transfer("Default.aspx");
            }
        }

        protected void ButtonUp_Click(object sender, EventArgs e)
        {
            if (Session["sang"] != null)
            {
                Sang sang = (Sang)Session["sang"];
                foreach (Akkord ak in sang.listAkkorder)
                {
                    if (ak.keyNumber < 12)
                    {
                        ak.keyNumber = ak.keyNumber + 1;
                    }
                    else
                    {
                        ak.keyNumber = 1;
                    }

                    if (ak.bass > 0) //0 er INGEN bass
                    {
                        if (ak.bass < 12)
                        {
                            ak.bass = ak.bass + 1;
                        }
                        else
                        {
                            ak.bass = 1;
                        }
                    }
                }
                Session["sang"] = sang;
            }

        }

        protected void ButtonDown_Click(object sender, EventArgs e)
        {
            if (Session["sang"] != null)
            {
                Sang sang = (Sang)Session["sang"];
                foreach (Akkord ak in sang.listAkkorder)
                {
                    if (ak.keyNumber > 1)
                    {
                        ak.keyNumber = ak.keyNumber - 1;
                    }
                    else
                    {
                        ak.keyNumber = 12;
                    }
                    if (ak.bass > 0) //0 er INGEN bass
                    {

                        if (ak.bass > 1)
                        {
                            ak.bass = ak.bass - 1;
                        }
                        else
                        {
                            ak.bass = 12;
                        }
                    }
                }
                Session["sang"] = sang;
            }

        }


        protected void PlaySong_Click(object sender, EventArgs e)
        {
        }
        private void showPlaySong()
        {
            //---Så laver vi Sheet sangin til animation-----------------------
            if (Session["Sang"] == null)
            {
                Server.Transfer("Default.aspx");
            }
            else
            {
                Sang sangen = (Sang)Session["Sang"];
                List<Akkord> listAkkorde = sangen.listAkkorder;
                //------Nogle akkorder kommer i forkert rækkefølge fordi de er indsat sådan------
                if (CheckBox1.Checked == true)
                {
                    Normalisering normal = new Normalisering();
                    foreach (Akkord akk in sangen.listAkkorder)
                    {
                        normal.insertChord(akk, akk.pos.X, akk.pos.Y);
                    }

                    List<object> nlist = normal.SorterAlt();
                    listAkkorde = nlist.OfType<Akkord>().ToList();

                    //------Normalisering færdig----------------------------------------------

                }
                //---Bruges til JavaScript til at vise hvilken Akkord der trykkes på.-----------------
                //------AKKORDER----------------
                String samletAkkorderKeyNumber = "";
                String samletAkkorderExtraName = "";
                String samletAkkorderBass = "";
                String samletAkkorderX = "";
                String samletAkkorderY = "";

                foreach (Akkord ak in listAkkorde)
                {
                    samletAkkorderKeyNumber = samletAkkorderKeyNumber + ":"+ak.keyNumber;
                    samletAkkorderX = samletAkkorderX + ":"+ ak.pos.X;
                    samletAkkorderY = samletAkkorderY + ":"+ ak.pos.Y;
                    samletAkkorderExtraName = samletAkkorderExtraName + ":" + ak.extraName;
                    samletAkkorderBass = samletAkkorderBass + ":" + ak.bass;
                }
                //------------------------------------
               
                Session["TempoValgt"] = sangen.tempo;
                Session["samletAkkorderKeyNumber"] = samletAkkorderKeyNumber;
                Session["samletAkkorderX"] = samletAkkorderX;
                Session["samletAkkorderY"] = samletAkkorderY;
                Session["samletAkkorderExtraName"] = samletAkkorderExtraName;
                Session["samletAkkorderBass"] = samletAkkorderBass;

            }


            //---------------------------------------------------------------
           
        }
        private void hidePlaySong()
        {
        }
        protected void buttonMore_Click(object sender, EventArgs e)
        {
        }

        protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            showPlaySong();
        }
    }
    class Normalisering
    {
        int maxAfstand = 30;//Det er både ovenover og nedenfor. Samlet 60.
        private List<Punkt> listPunkt = new List<Punkt>();
        private List<PunktNormal> listNormalisering = new List<PunktNormal>();
        private void calculate(Punkt p)
        {
            Boolean nytPunktLaves = true;
            if (listNormalisering.Count == 0)
            {
                nytPunktLaves = true;
            }
            else
            {
                foreach (PunktNormal po in listNormalisering)
                {
                    int y = Math.Abs(p.pos.Y - po.pos.Y);
                    if (y < maxAfstand)
                    {
                        p.closest = po;
                        po.listeMedPunkt.Add(p);
                        nytPunktLaves = false;
                        break;
                    }
                }
            }
            if (nytPunktLaves)
            {
                PunktNormal pa = new PunktNormal();
                pa.listeMedPunkt = new List<Punkt>();
                pa.Nr = listNormalisering.Count;
                pa.pos = new Point(p.pos.X, p.pos.Y);
                p.closest = pa;
                pa.listeMedPunkt.Add(p);
                listNormalisering.Add(pa);
            }
        }
        public void insertChord(object obj, int x, int y)
        {
            Punkt punkt = new Punkt();
            punkt.objekt = obj; //Akkord senere
            punkt.Nr = listPunkt.Count;
            punkt.pos = new Point(x, y);
            calculate(punkt);
            listPunkt.Add(punkt);
        }
        public List<object> SorterAlt()
        {
            List<PunktNormal> sortedList = listNormalisering.OrderBy(e => e.pos.Y).ToList();
            List<PunktNormal> nyListe = new List<PunktNormal>();
            List<object> listeSvar = new List<object>();
            foreach (PunktNormal pn in sortedList)
            {
                List<Punkt> nyl = pn.listeMedPunkt.OrderBy(e => e.pos.X).ToList();
                PunktNormal pnn = new PunktNormal();
                pnn.listeMedPunkt = nyl;
                pnn.Nr = pn.Nr;
                pnn.pos = pn.pos;
                nyListe.Add(pnn);
            }

            foreach (PunktNormal pu in nyListe)
            {
                foreach (Punkt pun in pu.listeMedPunkt)
                {
                    listeSvar.Add(pun.objekt);
                }
            }
            return listeSvar;
        }
        public void nullStill()
        {
            listPunkt = new List<Punkt>();
            listNormalisering = new List<PunktNormal>();

        }
        public void paintAll(Graphics g)
        {
            g.Clear(Color.LightGoldenrodYellow);
            Pen pen1 = new Pen(Color.Black, 4F);
            foreach (Punkt p in listPunkt)
            {
                g.DrawEllipse(pen1, p.pos.X, p.pos.Y, 10, 10);
                Font drawFont = new Font("Arial", 8);
                SolidBrush drawBrush = new SolidBrush(Color.Red);

                g.DrawString(p.pos.Y + " Nr. " + p.Nr, drawFont, drawBrush, p.pos.X - 10, p.pos.Y + 10);
                if (p.closest != null)
                {
                    g.DrawLine(pen1, p.pos, p.closest.pos);
                }
            }
            //---NOrmaliseringpunkterne
            foreach (PunktNormal p in listNormalisering)
            {
                g.DrawLine(pen1, 0, p.pos.Y, 300, p.pos.Y);
            }
        }

        class Punkt
        {
            public PunktNormal closest;
            public Point pos;
            public int Nr;
            public Object objekt;
        }
        class PunktNormal
        {
            public Point pos;
            public List<Punkt> listeMedPunkt;
            public int Nr;
        }
    }

}