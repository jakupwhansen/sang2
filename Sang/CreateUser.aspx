﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateUser.aspx.cs" Inherits="Sang.CreateUser" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Create User</title>
        <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
  
    <!--  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script> -->

    <link rel="stylesheet" href="/bootstrap-3.3.5-dist/css/bootstrap.min.css" />
    <script src="/jquery/jquery-1.11.3.min.js"></script>
    <script src="/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/css/StyleSheet1.css" />



    <style type="text/css">
        .auto-style1 {
            width: 104px;
        }
        .auto-style2 {
            text-align: left;
        }
        .auto-style3 {
            text-align: left;
            width: 220px;
        }
        .auto-style4 {
            width: 104px;
            height: 22px;
        }
        .auto-style5 {
            text-align: left;
            width: 220px;
            height: 22px;
        }
        .auto-style6 {
            height: 22px;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
    <div class="container-fluid">
         <div>

             <br />
             <asp:Button CssClass="btn-default" ID="Button1" runat="server" OnClick="Button1_Click" Text="back" />
             <br />
             <table style="width:100%;">
                 <tr>
                     <td class="auto-style1">
                         &nbsp;</td>
                     <td class="auto-style2" colspan="2">
                         Create your own songbook her.
                         <br />
                         <br />
                         Push the &quot;Create songbook&quot; button and<br />
                         you will receive your password.</td>
                 </tr>
                 <tr>
                     <td class="auto-style4">
                         <asp:Label ID="Label1" runat="server" Text="Email:"></asp:Label>
                     </td>
                     <td class="auto-style5">
                         <asp:TextBox ID="TextBoxEmail" runat="server" TextMode="Email" Width="216px"></asp:TextBox>
                     </td>
                     <td class="auto-style6"></td>
                 </tr>
                 <tr>
                     <td class="auto-style1">
                         <asp:Label ID="Label2" runat="server" Text="Name:"></asp:Label>
                     </td>
                     <td class="auto-style3">
                         <asp:TextBox ID="TextBoxName" runat="server" Width="216px"></asp:TextBox>
                     </td>
                     <td>&nbsp;</td>
                 </tr>
                 <tr>
                     <td class="auto-style1">&nbsp;</td>
                     <td class="auto-style3">
                         <asp:Button CssClass="btn-default" ID="Button2" runat="server" OnClick="Button2_Click" Text="Create songbook" Width="136px" />
                     </td>
                     <td>&nbsp;</td>
                 </tr>
             </table>

         </div>
    </div>
    </form>
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-54970963-3', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>
