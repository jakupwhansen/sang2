﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sang.books
{
    public partial class Sanghefti : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["wenningsdbConnectionString1"].ToString());
                con.Open();

                //------------HENTE SANG START------------------------------------------------
                //------------HENTE SANG START------------------------------------------------
                //------------HENTE SANG START------------------------------------------------
                SqlCommand cmd;
                cmd = new SqlCommand("SELECT * FROM Sang WHERE type=1 ORDER BY ID ASC;", con);

                SqlDataReader reader = cmd.ExecuteReader();
                List<SangheftiSang> listSang = new List<SangheftiSang>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        //    Response.Write("<script LANGUAGE='JavaScript' >alert('" + "listBrugere.Count" + "')</script>");
                        //   ListBox1.Items.Add("Song har item");
                        Bruger bruger = new Bruger();

                        //---titel-------------------   
                        String titel = "ingen titel";
                        if (!reader.IsDBNull(1))
                        {
                            titel = reader.GetString(1);
                        }
                        //---------------------------
                        //---type-------------------   
                        int type = 0;
                        if (!reader.IsDBNull(6))
                        {
                            type = reader.GetInt32(6);
                        }
                        //---------------------------
                        //---ID-------------------   
                        int id = 0;
                        if (!reader.IsDBNull(0))
                        {
                            id = reader.GetInt32(0);
                        }
                        //---------------------------
                        SangheftiSang sangValg = new SangheftiSang();
                        titel = titel.TrimStart(' ');
                        sangValg.title = titel;
                        sangValg.type = type;
                        sangValg.SangID = id;
                        //    sangValg.direktLink = "http://theSongApp.com/FindSheetSong.aspx?Sheet=" + id + "_" + titel.Length * id;
                        sangValg.direktLink = "../FindSong.aspx?Song=" + id + "_" + (titel.Length * id);

                        Button button = new Button();
                        button.Font.Bold = true;
                        button.Font.Italic = false;
                        button.Click += new EventHandler(sangValg.eventMetode);

                        sangValg.knap = button;

                        //-------Add to listen---------------------
                        listSang.Add(sangValg);
                    }

                }
                fyldTabelMedKnapper(listSang);
                con.Close();
            }
            catch (Exception kj) { }

            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["wenningsdbConnectionString1"].ToString());
                con.Open();

                //------------HENTE SheetSANG START------------------------------------------------
                //------------HENTE SheetSANG START------------------------------------------------
                //------------HENTE SheetSANG START------------------------------------------------
                SqlCommand cmd;
                cmd = new SqlCommand("SELECT * FROM SangSheet WHERE userID=32 AND type=1 ORDER BY ID ASC;", con);

                SqlDataReader reader = cmd.ExecuteReader();
                List<SangheftiSang> listSang = new List<SangheftiSang>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        //    Response.Write("<script LANGUAGE='JavaScript' >alert('" + "listBrugere.Count" + "')</script>");
                        //   ListBox1.Items.Add("Song har item");
                        Bruger bruger = new Bruger();

                        //---titel-------------------   
                        String titel = "ingen titel";
                        if (!reader.IsDBNull(1))
                        {
                            titel = reader.GetString(1);
                        }
                        //---------------------------
                        //---type-------------------   
                        int type = 0;
                        if (!reader.IsDBNull(6))
                        {
                            type = reader.GetInt32(6);
                        }
                        //---------------------------
                        //---ID-------------------   
                        int id = 0;
                        if (!reader.IsDBNull(0))
                        {
                            id = reader.GetInt32(0);
                        }
                        //---------------------------
                        SangheftiSang sangValg = new SangheftiSang();
                        titel = titel.TrimStart(' ');
                        sangValg.title = titel;
                        sangValg.type = type;
                        sangValg.SangID = id;
                        //    sangValg.direktLink = "http://theSongApp.com/FindSheetSong.aspx?Sheet=" + id + "_" + titel.Length * id;
                        sangValg.direktLink = "../FindSheetSong.aspx?Sheet=" + id + "_" + (titel.Length * id);

                        Button button = new Button();
                        button.Click += new EventHandler(sangValg.eventMetode);
                        button.Font.Bold = false;
                        button.Font.Italic = true;
                        sangValg.knap = button;

                        //-------Add to listen---------------------
                        listSang.Add(sangValg);
                    }

                }
                fyldTabelMedKnapper(listSang);
                con.Close();
            }
            catch (Exception kj) { }


        }
        private void fyldTabelMedKnapper(List<SangheftiSang> listS)
        {
            List<String> listMedNavne = new List<string>();
            listMedNavne.Add("Rasmus");
            listMedNavne.Add("Sára");
            listMedNavne.Add("Fagra blóma");
            listMedNavne.Add("Eins og áarstreymur rennur");
            listMedNavne.Add("Eg siti so eina");
            listMedNavne.Add("Vandringsmenn synger");
            listMedNavne.Add("Leygarkvøld í Havn");
            listMedNavne.Add("Sildavalsurin");
            listMedNavne.Add("Så længe jeg lever");
            listMedNavne.Add("So leingi eg livi");
            listMedNavne.Add("Tað er leygarkvøld");
            listMedNavne.Add("Kom og dansa");
            listMedNavne.Add("Vakra Helen");
            listMedNavne.Add("Eitt sunnukvøld í plantasjuni");
            listMedNavne.Add("Í góðum veðri hann dyrgdi");
            listMedNavne.Add("Niklas hjá Pena");
            listMedNavne.Add("Sektus og Sekterius");
            listMedNavne.Add("Mín dingeling");
            listMedNavne.Add("Lívsmynd");
            listMedNavne.Add("Alt, hann átti á fold");
            listMedNavne.Add("");
            listMedNavne.Add("");
            listMedNavne.Add("");
            listMedNavne.Add("");

            List<SangheftiSang> listNy = new List<SangheftiSang>();

            foreach (SangheftiSang sa in listS)
            {
                String extra = "";
                if (listMedNavne.Contains(sa.title))
                {
                    int nr = listMedNavne.IndexOf(sa.title) + 1;
                    if (nr < 10)
                        extra = "0";
                    else
                        extra = "";
                    sa.title = extra+ nr+ " "+ sa.title;
                    listNy.Add(sa);
                }
            }
            listNy.Sort();
            foreach (SangheftiSang sa in listNy)
            {


                TableRow tRow = new TableRow();
                Table1.Rows.Add(tRow);

                // Create a new cell and add it to the row.
                TableCell tCell = new TableCell();
                sa.knap.Text = sa.title;
                sa.knap.Width = 300;
                sa.knap.Height = 40;
                sa.knap.Font.Size = 14;
                // sa.knap.Font.Bold = true;
                sa.knap.CssClass = "btn-default";
                tCell.Controls.Add(sa.knap);

                tRow.Cells.Add(tCell);
                }
            
        }

        protected void ButtonBack_Click(object sender, EventArgs e)
        {
            if (Session["Bruger"] != null)
            {
                Bruger bruger = (Bruger)Session["Bruger"];
                if (bruger.ID == -1)
                {
                    //-1 betyder direkte link...Så skal skal slettes så vi ikke kan går til andre sider,
                    //Denne her bruger skal KUN se sheet song og ikke andet.
                    // fordi denne bruger eksisterer ikke rigtig.
                    //       Session["Bruger"] = null;
                }
            }

            if (Session["KommerFra"] != null)
            {
                List<String> listeKommerFra = (List<String>)Session["KommerFra"];
                String tilbageTil = "Default.aspx";
                if (listeKommerFra.Count > 0)
                {
                    tilbageTil = listeKommerFra[listeKommerFra.Count - 1];
                    listeKommerFra.RemoveAt(listeKommerFra.Count - 1);
                }
                Session["KommerFra"] = listeKommerFra;

                Server.Transfer("../" + tilbageTil);
            }
            else
            {
                Server.Transfer("Default.aspx");
            }
        }

    }
    class SangheftiSang : IComparable
    {
        public Button knap;
        public String title;
        public String direktLink;
        public int SangID = 0;
        public int type = 0; //Tilstand.
        public bool sheetSong = false;
        public int CompareTo(object obj)
        {
            SangheftiSang shtv = (SangheftiSang)obj;
            return title.CompareTo(shtv.title);
        }


        public void eventMetode(object sender, EventArgs e)
        {
            //----------Kommer fra------------------------------         
            List<String> listeKommerFra = (List<String>)HttpContext.Current.Session["KommerFra"];
            //2017.09.01 fordi man ikke kunne komme direkt ind (her er det sangbog)
            if (listeKommerFra == null) { listeKommerFra = new List<String>(); }

            listeKommerFra.Add("/books/Sanghefti.aspx");
            HttpContext.Current.Session["KommerFra"] = listeKommerFra;
            //-----------------------------------------------------

            HttpContext.Current.Response.Redirect(direktLink);
        }
    }

}
