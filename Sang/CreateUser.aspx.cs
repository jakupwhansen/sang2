﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sang
{
    public partial class CreateUser : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Server.Transfer("Default.aspx");
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            if (TextBoxEmail.Text.Length > 2 && TextBoxName.Text.Length > 2)
            {
                if (findsUser(TextBoxEmail.Text) == false)
                {
                    createNewUser();
                    Response.Write("<script LANGUAGE='JavaScript' >alert('A password will be sent to you. To log on you must user this email along with the password.')</script>");
                    Server.Transfer("Default.aspx");
                }
                else
                {
                    Response.Write("<script LANGUAGE='JavaScript' >alert('Email address allready in use.')</script>");
                }
            }
        }
        private int generatePassword()
        {
            int svar = 0;
            Random r = new Random();
            svar = r.Next(1000,999999);
            return svar;
        }
        private bool findsUser(String email)
        {
            bool svar = true;
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["wenningsdbConnectionString1"].ToString());
                con.Open();
                //------Gemmer Sangen-------------------------------------------
            //    SqlCommand cmd = new SqlCommand("SELECT * FROM SangBruger WHERE email=@email;", con);
                SqlCommand cmd = new SqlCommand("SELECT COUNT(*) FROM SangBruger WHERE email=@email;", con);
                cmd.Parameters.AddWithValue("@email", email);
                Int32 count = (Int32) cmd .ExecuteScalar(); 
                con.Close();
                if (count == 0)
                {
                    svar = false;
                }
            }
            catch (Exception lskd)
            {
                svar = true;
            }
            return svar;

        }
        protected void createNewUser()
        {
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["wenningsdbConnectionString1"].ToString());
                con.Open();
                //------Gemmer Sangen-------------------------------------------
                SqlCommand cmd = new SqlCommand("insert into SangBruger  (email,name,password,date, type) values(@email,@name,@password,@date, @type);", con);


                String email = TextBoxEmail.Text;
                String name =TextBoxName.Text;
                int password = generatePassword();
                
                cmd.Parameters.AddWithValue("@email",email );
                cmd.Parameters.AddWithValue("@name", name);
                cmd.Parameters.AddWithValue("@password",password);
                cmd.Parameters.AddWithValue("@date", DateTime.Now.ToString("yyyy.MM.dd"));
                cmd.Parameters.AddWithValue("@type", 1);
                int antal = cmd.ExecuteNonQuery();
                if(antal == 1)
                {
                    sendMailLokal(email, name, password);
                }
            }
            catch(Exception lskd)
            {
                
            }
        }
        private void sendMailLokal(String email, String name, int Password)
        {
            sendMail(email + "-" + name + "-" + Password);
            String besked = "Hi " + name + Environment.NewLine + "Welcome to Sang. Your password is:" + Password;
            SendMailMessage("localhost", "info@theSongApp.com", "theSongApp.com", email, name, "Welcome to Sang ", besked, true);
        }
        public void sendMail(String besked)
        {
            SendMailMessage("localhost", "info@theSongApp.com", "New User from SANG", "jakupwhansen@gmail.com", "Jákup", "New User to Sang ", besked, true);
        }

        public static bool SendMailMessage(string SMTPServer, string fromAddress, string fromName, string toAddress, string toName, string msgSubject, string msgBody, bool IsBodyHtml)
        {
            // Use the new v2.0 mail class to send an E-mail.

            try
            {
                SmtpClient client = new SmtpClient(SMTPServer);
                MailAddress from = new MailAddress(fromAddress, fromName);
                MailAddress to = new MailAddress(toAddress, toName);
                MailMessage message = new MailMessage(from, to);
                message.BodyEncoding = System.Text.Encoding.GetEncoding("iso-8859-1");
                message.Subject = msgSubject;
                message.IsBodyHtml = IsBodyHtml;
                message.Body = msgBody;
                client.Send(message);
            }
            catch (System.Net.Mail.SmtpException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }


    }
}