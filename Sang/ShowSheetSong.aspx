﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShowSheetSong.aspx.cs" Inherits="Sang.ShowSheetSong" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Show Sheet Song</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!--  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script> -->

    <script type='text/javascript' src='http://www.midijs.net/lib/midi.js'></script>
    
    

    <link rel="stylesheet" href="/bootstrap-3.3.5-dist/css/bootstrap.min.css" />
    <script src="/jquery/jquery-1.11.3.min.js"></script>
    <script src="/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>

    
    <link rel="stylesheet" type="text/css" href="/css/StyleSheet1.css" />

    <style type="text/css">
        .my-style {
            vertical-align: top;
        }


        .auto-style2 {
            height: 22px;
        }

        .auto-style5 {
            width: 36%;
            height: 38px;
        }


        .auto-style8 {
            width: 32px;
        }
        #nav {
            width: 247px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <div class="container-fluid">
                <div>

                    <br />

                    <asp:Button CssClass="btn-default" ID="ButtonBack" runat="server" Text="Back" OnClick="ButtonBack_Click" />
                    <asp:Button  CssClass="btn-default" ID="ButtonKeyDown" runat="server" Text="Down" OnClick="ButtonKeyDown_Click"   />
                    <asp:Button  CssClass="btn-default" ID="ButtonKeyUp" runat="server" Text="Up" OnClick="ButtonKeyUp_Click"  />

                    <asp:Button CssClass="btn-default" ID="buttonMore" runat="server" Text="More" OnClick="buttonMore_Click" Visible="False" />

                    <asp:Button CssClass="btn-default" ID="ButtonAnimation" runat="server" Text="Animation" OnClick="ButtonAnimation_Click" Visible="True" />
                     <asp:Button CssClass="btn-default" ID="PlaySong" runat="server" Text="Play" OnClick="PlaySong_Click" Visible="True" />
                   
                    <br />
                    <br />
                    <asp:Panel ID="PanelPlay" runat="server" Width="40%" Visible="False">

                        <!--  <span class="auto-style5">
                            <a id="LinkPlayMidi" class="auto-style7" href="#" onclick="MIDIjs.play('midi/sang.mid');"><strong>Play(Midi)</strong>
                            </a>
                             </span> -->

                        <span class="auto-style5">
                            <a href="#" onclick="changeImage()"> </a>

                        </span><table style="width:100%;">
                            <tr>
                                <td class="auto-style8"><span class="auto-style5"><a href="#" onclick="changeImage()">
                                    <img id="trykMidi" alt="" src="/images/play.png" width="100" height="50" />
                                    </a></span></td>
                                <td>
                                    <span class="auto-style5" >
                                     <input id="CheckboxRepeater"  type="checkbox"  style="display:none"/>
                                     </span>
                                   
                                   

                                </td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>

                    </asp:Panel>
                    <br />

                    <asp:Panel ID="Panel1" runat="server" Visible="False" Width="40%" ScrollBars="Vertical">
                        <table class="nav-justified">
                            <tr>
                                <td class="auto-style2">
                                    <asp:CheckBox ID="CheckBoxChordsIncluded" runat="server" AutoPostBack="True" Checked="True" Font-Size="8pt" OnCheckedChanged="DropDownList1_SelectedIndexChanged" Text="Chords" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" Font-Size="8pt" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="CheckBoxInstruments" runat="server" AutoPostBack="True" Font-Size="8pt" OnCheckedChanged="CheckBoxInstruments_CheckedChanged" Text="More" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:DropDownList ID="DropDownListSelectMelody" runat="server" AutoPostBack="True" Font-Size="8pt" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" Visible="False">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:DropDownList ID="DropDownListSelectChords" runat="server" AutoPostBack="True" Font-Size="8pt" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" Visible="False">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:HyperLink ID="LinkDownloadMid" runat="server" Font-Size="10pt" NavigateUrl="~/midi/sang.mid" Visible="False">Download(Midi)</asp:HyperLink>
                                </td>
                            </tr>
                        </table>

                    </asp:Panel>

                    <br />

                    <br />
                    <img src="BitMapNewSheetSongShow.aspx" alt="song" style="width: 100%; max-width: 600px" /><br />

                    <br />
                    <br />

                    &nbsp;<br />

                    <br />

                </div>
                <br />


                <div>
                </div>

            </div>
        </div>
        <p id="demo"></p>
         <p id="demo2"></p>
         <p id="demo3"></p>
    </form>
    <script>
        function changeImage() {           
            var img = document.getElementById("trykMidi");
            img.width = 50;
            img.src = "/images/processing.gif";
            MIDIjs.play('midi/sang.mid');
            MIDIjs.message_callback = display_message;
            //MIDIjs.player_callback = display_time;
            return false;
        }
        var running = false;
        
        function display_message(mes)
        {
            //var stringen = JSON.stringify(mes);
            //document.getElementById("demo").innerHTML = stringen;

            if (mes.indexOf("Playing") != -1)
            {
              //  document.getElementById("demo").innerHTML = "playing now";
                running = true;
            
                var img = document.getElementById("trykMidi");
                img.width = 100;
                img.src = "/images/play.png";
            }
            //----2017.04.21 ---Virker ikke-------------------------
            //if (mes.indexOf("Playing") == -1)
            //{
            //    document.getElementById("demo").innerHTML = "stopped";
            //    //----REPEAT---2016.07.11------------------------
            //   var check = document.getElementById("CheckboxRepeater");
            //   if (check.checked && running == true)
            //   {
            //       running = false;
                
            //    }
            //}
        };
        // Define a function to handle player events
       
        //function display_time(ev)
        //{
        //    var stringen = JSON.stringify(ev);
        //    document.getElementById("demo2").innerHTML = stringen; // time in seconds, since start of playback
         
           
        //};
     

   </script>

    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-54970963-3', 'auto');
        ga('send', 'pageview');

    </script>
</body>
</html>
