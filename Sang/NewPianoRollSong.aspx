﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewPianoRollSong.aspx.cs" Inherits="Sang.NewPianoRollSong" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <title>Sang: Melody Sequencer</title>
    <meta charset="utf-8" />
    <style type="text/css">
        .buttonDefault {
            background-color: gray;
            border: none;
            color: white;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
        }

        .buttonRed {
            background-color: Red;
            border: none;
            color: white;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
        }

        .buttonGreen {
            background-color: Green;
            border: none;
            color: white;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
        }

        .buttonYellow {
            background-color: yellow;
            border: none;
            color: black;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
        }


        .buttonBlue {
            background-color: blue;
            border: none;
            color: white;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
        }

        .buttonGray {
            background-color: gray;
            border: none;
            color: white;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
        }
    </style>

</head>
<body style="background-color: #66FF33">
    <table>
        <tr>
            <td></td>
        </tr>
        <tr>
            <td>
                <button id="startMidi" class="buttonDefault" type="button" onclick="startMidi()">Connect Midi Keyboard</button>
                <button id="startRecoding" class="buttonRed" type="button" onclick="startRecoding()" style="visibility: hidden">Record</button>
                <button id="stopRecording" class="buttonGreen" type="button" onclick="stopRecording()" style="visibility: hidden">Stop</button>
                <button id="playAgain" class="buttonBlue" type="button" onclick="playAgain()" style="visibility: hidden">Play</button>
                <button id="playRewind" class="buttonYellow" type="button" onclick="playRewind()" style="visibility: hidden">Rewind</button>
                <button id="playForward" class="buttonYellow" type="button" onclick="playForward()" style="visibility: hidden">Forward</button>
                <button id="playPause" class="buttonGreen" type="button" onclick="playPause()" style="visibility: hidden">Pause</button>
                <button id="playStop" class="buttonGreen" type="button" onclick="playStop()" style="visibility: hidden">Stop</button>
                <label id="volume" style="visibility: hidden" for="male">M.Volume</label>
                <input id="sliderVolume" style="visibility: hidden" type="range" min="1" max="100" step="1" onchange="sliderVolume('sliderVolume')" />
                <label id="tempo" style="visibility: hidden" for="male">Tempo</label>
                <input id="sliderTempo" style="visibility: hidden" type="range" min="0" max="40" step="1" onchange="sliderTempo('sliderTempo','rangeValue1')" />
            </td>
        </tr>
        <tr>
            <td>
                <button id="undo" class="buttonGray" type="button" onclick="undo()" style="visibility: hidden">Undo</button>
                <button id="autoQuantize" class="buttonGray" type="button" onclick="AutoQuantize()" style="visibility: hidden">Auto Quantize</button>
                <label id="quantizetypes" style="visibility: hidden" for="male">Quantize 1/4 or 1/8 </label>
                <input id="checkBoxQuantize" type="checkbox" onclick="checkBoxQuantizeClick()" checked="checked" style="visibility: hidden" />
                <label id="grid" style="visibility: hidden" for="male">Grid</label>
                <input id="checkBoxLines" type="checkbox" onclick="checkBoxLinesClick()" style="visibility: hidden" />
                <label id="moreDetails" style="visibility: hidden" for="male">More</label>
                <input id="checkBoxMoreDetails" type="checkbox" onclick="checkBoxMoreDetails()" style="visibility: hidden" />

                <button id="findRest" class="buttonGray" type="button" onclick="findRest()" style="visibility: hidden">Find Rest</button>
                <button id="moveStartAndEnd" class="buttonGray" type="button" onclick="moveStartAndEnd()" style="visibility: hidden">Move Start And End</button>
                <button id="Quantize4" class="buttonGray" type="button" onclick="Quantize4()" style="visibility: hidden">Quantize</button>
                <button id="calculateNoteLengthAndSplitNotes" class="buttonGray" type="button" onclick="calculateNoteLengthAndSplitNotes()" style="visibility: hidden">calculate Note Length And Split Notes</button>
            </td>
        </tr>
        <tr>
            <td>
                <button id="moveLeft" class="buttonYellow" type="button" onclick="moveLeft()" style="visibility: hidden">Move Left</button>
                <button id="moveRight" class="buttonYellow" type="button" onclick="moveRight()" style="visibility: hidden">Move Right</button>
                <button id="bigger" class="buttonYellow" type="button" onclick="bigger()" style="visibility: hidden">Bigger</button>
                <button id="smaller" class="buttonYellow" type="button" onclick="smaller()" style="visibility: hidden">Smaller</button>
                <button id="deleteNote" class="buttonYellow" type="button" onclick="deleteNote()" style="visibility: hidden">Delete</button>
                <label id="editSingleNotes" style="visibility: hidden" for="male">Edit single Notes</label>
                <input id="checkBoxEditSingleNotes" type="checkbox" onclick="checkBoxEditSingleNotes()" style="visibility: hidden" />
                <button id="save" class="buttonRed" type="button" onclick="sendNoteListToCodeBehind()" style="visibility: hidden">Save</button>
            </td>
        </tr>
        <tr>
            <td>
                <canvas id="myCanvas" width="1600" height="1000" style="border: 1px solid #d3d3d3;"></canvas>
            </td>
        </tr>
    </table>
    <script>
        var teller = 0;
        var clock = 20;
        var maxClockFrequence = 40;
        var timerVariabel;
        var timerPlayAgainVariabel;
        var time = 0;
        var metronomOn = 3;
        var tempo = 32;
        var aktuelPlayAgainNode;
        var tellerNode = 0;
        var isRecording = true;

        var SixteentNote;
        var SixteentDotNote;
        var EightNote;
        var EightDotNote;
        var KvartNote;
        var KvartDotNote;
        var HalfNote;
        var HalfDotNote;
        var FullNote;
        var FullDotNote;

        var SixteentNoteRest;
        var EightNoteRest;
        var KvartNoteRest;
        var HalfNoteRest;
        var FullNoteRest;
        var SixteentDotNoteRest;
        var EightDotNoteRest;
        var KvartDotNoteRest;
        var HalfDotNoteRest;
        var FullDotNoteRest;
        var timeSign = 4;

        var NoteList = [];
        var collors = ["red", "green", "yellow", "blue", "black"];
        var select2or4 = 2; //2 = 1/4 and 4=1/8. tempo

        //--------Canvas--------------------------------------------------------
        var c = document.getElementById("myCanvas");
        window.addEventListener('keydown', doKeyDown, true);
        window.addEventListener('keyup', doKeyUp, true);
        c.addEventListener('mousedown', editNoteSelect, true);
        var ctx = c.getContext("2d");

        //---------Hide and show buttons----------------------------
        function hideAllButtons()
        {
            document.getElementById("moveLeft").style.visibility = "hidden";
            document.getElementById("moveRight").style.visibility = "hidden";
            document.getElementById("bigger").style.visibility = "hidden";
            document.getElementById("smaller").style.visibility = "hidden";
            document.getElementById("deleteNote").style.visibility = "hidden";
            document.getElementById("save").style.visibility = "hidden";


            document.getElementById("undo").style.visibility = "hidden";
            document.getElementById("autoQuantize").style.visibility = "hidden";
            document.getElementById("grid").style.visibility = "hidden";

            document.getElementById("findRest").style.visibility = "hidden";
            document.getElementById("moveStartAndEnd").style.visibility = "hidden";
            document.getElementById("Quantize4").style.visibility = "hidden";
            document.getElementById("calculateNoteLengthAndSplitNotes").style.visibility = "hidden";

            document.getElementById("checkBoxLines").style.visibility = "hidden";
            document.getElementById("quantizetypes").style.visibility = "hidden";
            document.getElementById("checkBoxQuantize").style.visibility = "hidden";

            document.getElementById("moreDetails").style.visibility = "hidden";
            document.getElementById("checkBoxMoreDetails").style.visibility = "hidden";


            document.getElementById("volume").style.visibility = "hidden";
            document.getElementById("sliderVolume").style.visibility = "hidden";
            document.getElementById("tempo").style.visibility = "hidden";
            document.getElementById("sliderTempo").style.visibility = "hidden";
            document.getElementById("startMidi").style.visibility = "hidden";
            document.getElementById("startRecoding").style.visibility = "hidden";
            document.getElementById("stopRecording").style.visibility = "hidden";
            document.getElementById("playAgain").style.visibility = "hidden";
            document.getElementById("playStop").style.visibility = "hidden";
            document.getElementById("playPause").style.visibility = "hidden";

            document.getElementById("editSingleNotes").style.visibility = "hidden";
            document.getElementById("checkBoxEditSingleNotes").style.visibility = "hidden";
        }
        function visibleAllButtons()
        {
            document.getElementById("moveLeft").style.visibility = "visible";
            document.getElementById("moveRight").style.visibility = "visible";
            document.getElementById("bigger").style.visibility = "visible";
            document.getElementById("smaller").style.visibility = "visible";
            document.getElementById("deleteNote").style.visibility = "visible";
            document.getElementById("save").style.visibility = "visible";


            document.getElementById("undo").style.visibility = "visible";
            document.getElementById("findRest").style.visibility = "visible";
            document.getElementById("moveStartAndEnd").style.visibility = "visible";
            document.getElementById("Quantize4").style.visibility = "visible";
            document.getElementById("calculateNoteLengthAndSplitNotes").style.visibility = "visible";
            document.getElementById("autoQuantize").style.visibility = "visible";
            document.getElementById("grid").style.visibility = "visible";
            document.getElementById("checkBoxLines").style.visibility = "visible";
            document.getElementById("quantizetypes").style.visibility = "visible";
            document.getElementById("checkBoxQuantize").style.visibility = "visible";
            document.getElementById("moreDetails").style.visibility = "visible";
            document.getElementById("checkBoxMoreDetails").style.visibility = "visible";

            document.getElementById("volume").style.visibility = "visible";
            document.getElementById("sliderVolume").style.visibility = "visible";
            document.getElementById("tempo").style.visibility = "visible";
            document.getElementById("sliderTempo").style.visibility = "visible";
            document.getElementById("startMidi").style.visibility = "visible";
            document.getElementById("startRecoding").style.visibility = "visible";
            document.getElementById("stopRecording").style.visibility = "visible";
            document.getElementById("playAgain").style.visibility = "visible";
            document.getElementById("playStop").style.visibility = "visible";
            document.getElementById("playPause").style.visibility = "visible";

            document.getElementById("editSingleNotes").style.visibility = "visible";
            document.getElementById("checkBoxEditSingleNotes").style.visibility = "visible";
        }
        function visibleMore()
        {
            document.getElementById("findRest").style.visibility = "visible";
            document.getElementById("moveStartAndEnd").style.visibility = "visible";
            document.getElementById("Quantize4").style.visibility = "visible";
            document.getElementById("calculateNoteLengthAndSplitNotes").style.visibility = "visible";
        }
        function hideMore()
        {
            document.getElementById("findRest").style.visibility = "hidden";
            document.getElementById("moveStartAndEnd").style.visibility = "hidden";
            document.getElementById("Quantize4").style.visibility = "hidden";
            document.getElementById("calculateNoteLengthAndSplitNotes").style.visibility = "hidden";
        }
        function visibleEditSingleNotes()
        {
            document.getElementById("moveLeft").style.visibility = "visible";
            document.getElementById("moveRight").style.visibility = "visible";
            document.getElementById("bigger").style.visibility = "visible";
            document.getElementById("smaller").style.visibility = "visible";
            document.getElementById("deleteNote").style.visibility = "visible";
        }
        function hideEditSingleNotes()
        {
            document.getElementById("moveLeft").style.visibility = "hidden";
            document.getElementById("moveRight").style.visibility = "hidden";
            document.getElementById("bigger").style.visibility = "hidden";
            document.getElementById("smaller").style.visibility = "hidden";
            document.getElementById("deleteNote").style.visibility = "hidden";
        }
        function hideWhenRecoding()
        {
            document.getElementById("moveLeft").style.visibility = "hidden";
            document.getElementById("moveRight").style.visibility = "hidden";
            document.getElementById("bigger").style.visibility = "hidden";
            document.getElementById("smaller").style.visibility = "hidden";
            document.getElementById("deleteNote").style.visibility = "hidden";
            document.getElementById("save").style.visibility = "hidden";


            document.getElementById("undo").style.visibility = "hidden";
            document.getElementById("autoQuantize").style.visibility = "hidden";
            document.getElementById("grid").style.visibility = "hidden";

            document.getElementById("findRest").style.visibility = "hidden";
            document.getElementById("moveStartAndEnd").style.visibility = "hidden";
            document.getElementById("Quantize4").style.visibility = "hidden";
            document.getElementById("calculateNoteLengthAndSplitNotes").style.visibility = "hidden";

            document.getElementById("checkBoxLines").style.visibility = "hidden";
            document.getElementById("quantizetypes").style.visibility = "hidden";
            document.getElementById("checkBoxQuantize").style.visibility = "hidden";

            document.getElementById("moreDetails").style.visibility = "hidden";
            document.getElementById("checkBoxMoreDetails").style.visibility = "hidden";


            document.getElementById("volume").style.visibility = "hidden";
            document.getElementById("sliderVolume").style.visibility = "hidden";
            document.getElementById("tempo").style.visibility = "hidden";
            document.getElementById("sliderTempo").style.visibility = "hidden";
            document.getElementById("startMidi").style.visibility = "hidden";
            document.getElementById("startRecoding").style.visibility = "hidden";
            document.getElementById("stopRecording").style.visibility = "visible";
            document.getElementById("playAgain").style.visibility = "hidden";
            document.getElementById("playStop").style.visibility = "hidden";
            document.getElementById("playPause").style.visibility = "hidden";
            document.getElementById("playRewind").style.visibility = "hidden";
            document.getElementById("playForward").style.visibility = "hidden";
            
            document.getElementById("editSingleNotes").style.visibility = "hidden";
            document.getElementById("checkBoxEditSingleNotes").style.visibility = "hidden";
        }
        function visibleAfterRecording()
        {
            document.getElementById("moveLeft").style.visibility = "visible";
            document.getElementById("moveRight").style.visibility = "visible";
            document.getElementById("bigger").style.visibility = "visible";
            document.getElementById("smaller").style.visibility = "visible";
            document.getElementById("deleteNote").style.visibility = "visible";
            document.getElementById("save").style.visibility = "visible";


            document.getElementById("undo").style.visibility = "visible";
            document.getElementById("findRest").style.visibility = "visible";
            document.getElementById("moveStartAndEnd").style.visibility = "visible";
            document.getElementById("Quantize4").style.visibility = "visible";
            document.getElementById("calculateNoteLengthAndSplitNotes").style.visibility = "visible";
            document.getElementById("autoQuantize").style.visibility = "visible";
            document.getElementById("grid").style.visibility = "visible";
            document.getElementById("checkBoxLines").style.visibility = "visible";
            document.getElementById("quantizetypes").style.visibility = "visible";
            document.getElementById("checkBoxQuantize").style.visibility = "visible";
            document.getElementById("moreDetails").style.visibility = "visible";
            document.getElementById("checkBoxMoreDetails").style.visibility = "visible";

            document.getElementById("volume").style.visibility = "visible";
            document.getElementById("sliderVolume").style.visibility = "visible";
            document.getElementById("tempo").style.visibility = "visible";
            document.getElementById("sliderTempo").style.visibility = "visible";
            document.getElementById("startMidi").style.visibility = "visible";
            document.getElementById("startRecoding").style.visibility = "visible";
            document.getElementById("stopRecording").style.visibility = "hidden";
            document.getElementById("playAgain").style.visibility = "visible";
            document.getElementById("playStop").style.visibility = "hidden";
            document.getElementById("playPause").style.visibility = "hidden";
            document.getElementById("playRewind").style.visibility = "hidden";
            document.getElementById("playForward").style.visibility = "hidden";


            document.getElementById("editSingleNotes").style.visibility = "visible";
            document.getElementById("checkBoxEditSingleNotes").style.visibility = "visible";

            //----If checkbox is of, hide -----------------------------------------------
            checkBoxMoreDetails();
            checkBoxEditSingleNotes();
        }
        function hideWhenPlaying()
        {
            document.getElementById("moveLeft").style.visibility = "hidden";
            document.getElementById("moveRight").style.visibility = "hidden";
            document.getElementById("bigger").style.visibility = "hidden";
            document.getElementById("smaller").style.visibility = "hidden";
            document.getElementById("deleteNote").style.visibility = "hidden";
            document.getElementById("save").style.visibility = "hidden";


            document.getElementById("undo").style.visibility = "hidden";
            document.getElementById("autoQuantize").style.visibility = "hidden";
            document.getElementById("grid").style.visibility = "hidden";

            document.getElementById("findRest").style.visibility = "hidden";
            document.getElementById("moveStartAndEnd").style.visibility = "hidden";
            document.getElementById("Quantize4").style.visibility = "hidden";
            document.getElementById("calculateNoteLengthAndSplitNotes").style.visibility = "hidden";

            document.getElementById("checkBoxLines").style.visibility = "hidden";
            document.getElementById("quantizetypes").style.visibility = "hidden";
            document.getElementById("checkBoxQuantize").style.visibility = "hidden";

            document.getElementById("moreDetails").style.visibility = "hidden";
            document.getElementById("checkBoxMoreDetails").style.visibility = "hidden";


            document.getElementById("volume").style.visibility = "hidden";
            document.getElementById("sliderVolume").style.visibility = "hidden";
            document.getElementById("tempo").style.visibility = "hidden";
            document.getElementById("sliderTempo").style.visibility = "hidden";
            document.getElementById("startMidi").style.visibility = "hidden";
            document.getElementById("startRecoding").style.visibility = "hidden";
            document.getElementById("stopRecording").style.visibility = "hidden";
            document.getElementById("playAgain").style.visibility = "hidden";
            document.getElementById("playStop").style.visibility = "visible";
            document.getElementById("playPause").style.visibility = "visible";
            document.getElementById("playRewind").style.visibility = "visible";
            document.getElementById("playForward").style.visibility = "visible";

            document.getElementById("editSingleNotes").style.visibility = "hidden";
            document.getElementById("checkBoxEditSingleNotes").style.visibility = "hidden";
        }
        //-------Ajax-------------------------------------------
        function sendNoteListToCodeBehind()
        {
            var myJSON = JSON.stringify({ 'nodeList': NoteList });

            // alert(myJSON);
            $.ajax({
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                type: 'POST',
                url: 'NewPianoRollSong.aspx/PassThings',
                data: myJSON,
                success: function ()
                {
                  //  alert("success");
                    document.getElementById("Button1").click();
                },
                error: function (response)
                {
                    alert("error:" + response.responseText);
                }
            });
        }
        //-----Edit note select-------------------------
        function editNoteSelect(event)
        {
            var newTime = 0;
            if (time > 900)
                newTime = time - 900;

            //---------------------
            var x = event.offsetX;
            var y = event.offsetY;
            // alert("x:" + x + " y:" + y);
            for (var i = 0; i < NoteList.length; i++)
            {
                if (x > NoteList[i].x - newTime && x < NoteList[i].xStop - newTime)
                {
                    if (NoteList[i].editMode === 0)
                        NoteList[i].editMode = 1;
                    else
                        NoteList[i].editMode = 0;
                }
            }
            Draw();

        }
        //------KeyIndput-------------------------------
        var keyDownOn = 0;
        function doKeyDown(e)
        {
            //alert(e.keyCode);
            if (keyDownOn === 0)
            {
                //-----The white notes----
                if (e.keyCode == 65) //A key
                {
                    noteOn(55); //G note
                    keyDownOn = 1;
                }
                if (e.keyCode == 83) //S key
                {
                    noteOn(57); //A note
                    keyDownOn = 1;
                }
                if (e.keyCode == 68) //D key
                {
                    noteOn(59); //B note
                    keyDownOn = 1;
                }
                if (e.keyCode == 70) //F key
                {
                    noteOn(60); //C note
                    keyDownOn = 1;
                }
                if (e.keyCode == 71) //G key
                {
                    noteOn(62); //D note
                    keyDownOn = 1;
                }
                if (e.keyCode == 72) //H key
                {
                    noteOn(64); //E note
                    keyDownOn = 1;
                }
                if (e.keyCode == 74) //J key
                {
                    noteOn(65); //F note
                    keyDownOn = 1;
                }
                if (e.keyCode == 75) //K key
                {
                    noteOn(67); //G note
                    keyDownOn = 1;
                }
                if (e.keyCode == 76) //L key
                {
                    noteOn(69); //A note
                    keyDownOn = 1;
                }
                if (e.keyCode == 192) //Æ key
                {
                    noteOn(71); //B note
                    keyDownOn = 1;
                }
                if (e.keyCode == 222) //Ø key
                {
                    noteOn(72); //C note
                    keyDownOn = 1;
                }
                //-----The black notes----
                if (e.keyCode == 87) //W key
                {
                    noteOn(56); //G# note
                    keyDownOn = 1;
                }
                if (e.keyCode == 69) //E key
                {
                    noteOn(58); //A# note
                    keyDownOn = 1;
                }
                if (e.keyCode == 84) //T key
                {
                    noteOn(61); //C# note
                    keyDownOn = 1;
                }
                if (e.keyCode == 89) //Y key
                {
                    noteOn(63); //D# note
                    keyDownOn = 1;
                }
                if (e.keyCode == 73) //I key
                {
                    noteOn(66); //F# note
                    keyDownOn = 1;
                }
                if (e.keyCode == 79) //O key
                {
                    noteOn(68); //G# note
                    keyDownOn = 1;
                }
                if (e.keyCode == 80) //P key
                {
                    noteOn(70); //A# note
                    keyDownOn = 1;
                }
            }
        }
        function doKeyUp(e)
        {
            //alert("Up");
            if (keyDownOn === 1)
            {
                if (e.keyCode == 65) //A key
                {
                    noteOff(55); //G note
                    keyDownOn = 0;
                }
                if (e.keyCode == 83) //S key
                {
                    noteOff(57); //A note
                    keyDownOn = 0;
                }
                if (e.keyCode == 68) //D key
                {
                    noteOff(59); //B note
                    keyDownOn = 0;
                }
                if (e.keyCode == 70) //F key
                {
                    noteOff(60); //C note
                    keyDownOn = 0;
                }
                if (e.keyCode == 71) //G key
                {
                    noteOff(62); //D note
                    keyDownOn = 0;
                }
                if (e.keyCode == 72) //H key
                {
                    noteOff(64); //E note
                    keyDownOn = 0;
                }
                if (e.keyCode == 74) //J key
                {
                    noteOff(65); //F note
                    keyDownOn = 0;
                }
                if (e.keyCode == 75) //K key
                {
                    noteOff(67); //G note
                    keyDownOn = 0;
                }
                if (e.keyCode == 76) //L key
                {
                    noteOff(69); //A note
                    keyDownOn = 0;
                }
                if (e.keyCode == 192) //Æ key
                {
                    noteOff(71); //B note
                    keyDownOn = 0;
                }
                if (e.keyCode == 222) //Ø key
                {
                    noteOff(72); //C note
                    keyDownOn = 0;
                }
                //-----The black notes----
                if (e.keyCode == 87) //W key
                {
                    noteOff(56); //G# note
                    keyDownOn = 0;
                }
                if (e.keyCode == 69) //E key
                {
                    noteOff(58); //A# note
                    keyDownOn = 0;
                }
                if (e.keyCode == 84) //T key
                {
                    noteOff(61); //C# note
                    keyDownOn = 0;
                }
                if (e.keyCode == 89) //Y key
                {
                    noteOff(63); //D# note
                    keyDownOn = 0;
                }
                if (e.keyCode == 73) //I key
                {
                    noteOff(66); //F# note
                    keyDownOn = 0;
                }
                if (e.keyCode == 79) //O key
                {
                    noteOff(68); //G# note
                    keyDownOn = 0;
                }
                if (e.keyCode == 80) //P key
                {
                    noteOff(70); //A# note
                    keyDownOn = 0;
                }
            }
        }
        //-----IMAGES-----------------------------------
        downloadImages();
        function downloadImages()
        {

            FullNote = new Image();
            FullNote.src = 'images/FullNoteTrans.png';
            FullDotNote = new Image();
            FullDotNote.src = 'images/FullDotNoteTrans.png';

            HalfNote = new Image();
            HalfNote.src = 'images/HalfNoteTrans.png';
            HalfDotNote = new Image();
            HalfDotNote.src = 'images/HalfDotNoteTrans.png';

            KvartNote = new Image();
            KvartNote.src = 'images/KvartNoteTrans.png';
            KvartDotNote = new Image();
            KvartDotNote.src = 'images/KvartDotNoteTrans.png';

            EightNote = new Image();
            EightNote.src = 'images/EightNoteTrans.png';
            EightDotNote = new Image();
            EightDotNote.src = 'images/EightDotNoteTrans.png';

            SixteentNote = new Image();
            SixteentNote.src = 'images/SixteentNoteTrans.png';
            SixteentDotNote = new Image();
            SixteentDotNote.src = 'images/SixteentDotNoteTrans.png';

            //-----------REST---------------------------------------------------
            FullNoteRest = new Image();
            FullNoteRest.src = 'images/FullNoteRestTrans.png';
            FullDotNoteRest = new Image();
            FullDotNoteRest.src = 'images/FullDotNoteRestTrans.png';

            HalfNoteRest = new Image();
            HalfNoteRest.src = 'images/HalfNoteRestTrans.png';
            HalfDotNoteRest = new Image();
            HalfDotNoteRest.src = 'images/HalfDotNoteRestTrans.png';

            KvartNoteRest = new Image();
            KvartNoteRest.src = 'images/KvartNoteRestTrans.png';
            KvartDotNoteRest = new Image();
            KvartDotNoteRest.src = 'images/KvartDotNoteRestTrans.png';

            EightNoteRest = new Image();
            EightNoteRest.src = 'images/EightNoteRestTrans.png';
            EightDotNoteRest = new Image();
            EightDotNoteRest.src = 'images/EightDotNoteRestTrans.png';

            SixteentNoteRest = new Image();
            SixteentNoteRest.src = 'images/SixteentNoteRestTrans.png';
            SixteentDotNoteRest = new Image();
            SixteentDotNoteRest.src = 'images/SixteentDotNoteRestTrans.png';
        }
        //----------CHECKBOX-----------------------------
        function checkBoxLinesClick()
        {
            Draw();
        }
        function checkBoxQuantizeClick()
        {

            var x = document.getElementById("checkBoxQuantize");
            if (x.checked)
            {
                select2or4 = 2;
            }
            else
            {
                select2or4 = 4;
            }
        }
        function checkBoxMoreDetails()
        {
            var x = document.getElementById("checkBoxMoreDetails");
            if (x.checked)
            {
                visibleMore();
            }
            else
            {
                hideMore();
            }

        }
        function checkBoxEditSingleNotes()
        {
            var x = document.getElementById("checkBoxEditSingleNotes");
            if (x.checked)
            {
                visibleEditSingleNotes();
            }
            else
            {
                hideEditSingleNotes();
            }

        }
        //----SLIDERS-------------------------------------
        function sliderTempo(sliderID, textbox)
        {
            var y = document.getElementById(sliderID);
          //  alert(y.value);
            clock = maxClockFrequence - y.value;
        }
        function sliderVolume(sliderID)
        {
            var y = document.getElementById(sliderID);
            var numb = y.value / 100;

            gainVolume = numb.toFixed(2);
        }
        //-----EDIT---------------------------------
        function moveLeft()
        {
            for (var i = 0; i < NoteList.length; i++)
            {
                if (NoteList[i].editMode === 1)
                {
                    var lokalTempo = tempo / select2or4; // 2 = 1/4(default) and 4 = 1/8.
                    NoteList[i].x = NoteList[i].x - lokalTempo;
                    NoteList[i].xStop = NoteList[i].xStop - lokalTempo;
                    if (i > 0)
                    {
                        if (NoteList[i - 1].noteLength > 100)// Rest
                        {
                            if (NoteList[i].x < NoteList[i - 1].xStop)
                            {
                                //Note goes over Rest. We delete the rest.
                                NoteList.splice(i - 1, 1); //Remove 1 element at position i-1
                            }
                        }
                    }
                }
            }
            Draw();
         
          
        }
        function moveRight()
        {
            for (var i = 0; i < NoteList.length; i++)
            {
                if (NoteList[i].editMode === 1)
                {
                    var lokalTempo = tempo / select2or4; // 2 = 1/4(default) and 4 = 1/8.

                    NoteList[i].x = NoteList[i].x + lokalTempo;
                    NoteList[i].xStop = NoteList[i].xStop + lokalTempo;
                    if (i < NoteList.length - 1)
                    {
                        if (NoteList[i + 1].noteLength > 100)// Rest
                        {
                            if (NoteList[i].xStop > NoteList[i + 1].x)
                            {
                                //Note goes over Rest. We delete the rest.
                                NoteList.splice(i + 1, 1); //Remove 1 element at position i-1
                            }
                        }
                    }
                }
            }
            Draw();
          
        }
        function bigger()
        {
            for (var i = 0; i < NoteList.length; i++)
            {
                if (NoteList[i].editMode === 1)
                {
                    var lokalTempo = tempo / select2or4; // 2 = 1/4(default) and 4 = 1/8.

                    NoteList[i].xStop = NoteList[i].xStop + lokalTempo;
                    if (i < NoteList.length - 1)
                    {
                        if (NoteList[i + 1].noteLength > 100)// Rest
                        {
                            if (NoteList[i].xStop > NoteList[i + 1].x)
                            {
                                //Note goes over Rest. We delete the rest.
                                NoteList.splice(i + 1, 1); //Remove 1 element at position i-1
                            }
                        }
                    }
                }
            }
           
            Draw();
            //2018.01.26
            calculateNoteLengthAndSplitNotes();  
            setAllToEditOff();
           
        }
        function smaller()
        {
            for (var i = 0; i < NoteList.length; i++)
            {
                if (NoteList[i].editMode === 1)
                {
                    var lokalTempo = tempo / select2or4; // 2 = 1/4(default) and 4 = 1/8.

                    NoteList[i].xStop = NoteList[i].xStop - lokalTempo;
                }
            }
            Draw();
            //2018.01.26
            calculateNoteLengthAndSplitNotes();
            setAllToEditOff();
        }
        function deleteNote()
        {
            for (var i = 0; i < NoteList.length; i++)
            {
                if (NoteList[i].editMode === 1)
                {
                    //Note goes over Rest. We delete the rest.
                    NoteList.splice(i, 1); //Remove 1 element at position i-1
                }
            }
            Draw();
        }
        //----RECORDING--------------------------------------------------------------------
        //----RECORDING--------------------------------------------------------------------
        //----RECORDING--------------------------------------------------------------------
        var noteNumber = 80;
        function recordTimer()
        {
            time = time + 1;
            //  ctx.fillText(time, 30,30);


            Draw();
            if (metronomOn === 1)
            {
                noteOnMetronom(noteNumber)
            }
            else if (metronomOn > 2)
            {
                noteOffMetronom(noteNumber);

            }
            metronomOn++;
            if (time % tempo === 0)
            {
                metronomOn = 0;
            }
        }
        function deleteNoteFromCurrentTime()
        {
            for (var i = 0; i < NoteList.length; i++)
            {
                if(NoteList[i].x > time)
                {
                    NoteList.pop();
                    i--;
                }
            }
        }
        function startRecoding()
        {
            isRecording = true;
            // refresh the canvas
            ctx.clearRect(0, 0, c.width, c.height);
            //2018.01.26
            deleteNoteFromCurrentTime();
            //NoteList = [];
            //lastNote = null;
            //teller = 0;
            //time = 0;
            clearTimeout(timerVariabel);
            timerVariabel = setInterval(recordTimer, clock);

            document.getElementById("startRecoding").style.visibility = "hidden";
            document.getElementById("stopRecording").style.visibility = "visible";
            document.getElementById("playAgain").style.visibility = "hidden";
            document.getElementById("playStop").style.visibility = "hidden";
            document.getElementById("playPause").style.visibility = "hidden";
            hideWhenRecoding();

        }
        function stopRecording()
        {
            //close the note playing right now, if any.
            noteOffMetronom(noteNumber);

            isRecording = false;
            clearTimeout(timerVariabel);
            //  clearTimeout(timerPlayAgainVariabel);

            document.getElementById("startRecoding").style.visibility = "visible";
            document.getElementById("stopRecording").style.visibility = "hidden";
            document.getElementById("playAgain").style.visibility = "visible";
            document.getElementById("playStop").style.visibility = "hidden";
            document.getElementById("playPause").style.visibility = "hidden";
            visibleAfterRecording();
            //backupp the NoteList her...
            arrayCopy = NoteList.map(a => Object.assign({}, a));

            time = 0;
            Draw();
        }
        var arrayCopy;
        //---PLAYING------------------------------------------------------------------
        //---PLAYING------------------------------------------------------------------
        //---PLAYING------------------------------------------------------------------
        function playAgain()
        {
            isRecording = false;

            if (timerPlayAgainVariabel != null)
            {
                clearTimeout(timerPlayAgainVariabel);
            }

            //time = 0;
            //tellerNode = 0;
            aktuelPlayAgainNode = NoteList[tellerNode];

            timerPlayAgainVariabel = setInterval(startTimerPlayAgain, clock);

            //document.getElementById("startRecoding").style.visibility = "hidden";
            //document.getElementById("stopRecording").style.visibility = "hidden";
            //document.getElementById("playAgain").style.visibility = "hidden";

            //document.getElementById("playRewind").style.visibility = "visible";
            //document.getElementById("playStop").style.visibility = "visible";
            //document.getElementById("playPause").style.visibility = "visible";

            hideWhenPlaying();
        }

        function startTimerPlayAgain()
        {
            if (time === aktuelPlayAgainNode.x)
            {
                if (aktuelPlayAgainNode.noteLength < 100) // >100 is Rest.
                {
                    noteOn(aktuelPlayAgainNode.noteNumber);
                }
            }
            else if (time === aktuelPlayAgainNode.xStop)
            {
                noteOff(aktuelPlayAgainNode.noteNumber);
                tellerNode++;
                if (tellerNode >= NoteList.length) //Stop playing.
                {
                    playStop();
                }
                else
                {
                    aktuelPlayAgainNode = NoteList[tellerNode];
                }
            }
            time = time + 1;
            Draw();
        }
        function playStop()
        {
            isRecording = false;
            if (aktuelPlayAgainNode != null)
            {
                noteOff(aktuelPlayAgainNode.noteNumber);
            }
            clearTimeout(timerPlayAgainVariabel);
            //----Null stil alt--------------------------------
            time = 0;
            tellerNode = 0;
            aktuelPlayAgainNode = NoteList[tellerNode];

            //document.getElementById("startRecoding").style.visibility = "visible";
            //document.getElementById("stopRecording").style.visibility = "hidden";
            //document.getElementById("playAgain").style.visibility = "visible";
            //document.getElementById("playStop").style.visibility = "hidden";
            //document.getElementById("playPause").style.visibility = "hidden";
            //document.getElementById("playRewind").style.visibility = "hidden";

            visibleAfterRecording();
            Draw();
        }
        function playPause()
        {
            isRecording = false;
            if (aktuelPlayAgainNode != null)
            {
                noteOff(aktuelPlayAgainNode.noteNumber);
            }

            clearTimeout(timerPlayAgainVariabel);

            visibleAfterRecording();
            //document.getElementById("startRecoding").style.visibility = "visible";
            //document.getElementById("stopRecording").style.visibility = "hidden";
            document.getElementById("playAgain").style.visibility = "visible";
            //document.getElementById("playStop").style.visibility = "hidden";
            document.getElementById("playRewind").style.visibility = "visible";
            document.getElementById("playForward").style.visibility = "visible";
        }
        function playRewind()
        {
            isRecording = false;
            if (aktuelPlayAgainNode != null)
            {
                noteOff(aktuelPlayAgainNode.noteNumber);
            }

            //-----Rewind by ONE note---------------------------

            if (tellerNode > 0)
            {
                tellerNode--;
                aktuelPlayAgainNode = NoteList[tellerNode];
                time = aktuelPlayAgainNode.x - 2;
            }
            Draw();
            //document.getElementById("startRecoding").style.visibility = "visible";
            //document.getElementById("stopRecording").style.visibility = "hidden";
            //document.getElementById("playAgain").style.visibility = "visible";
            //document.getElementById("playStop").style.visibility = "hidden";
            //document.getElementById("playPause").style.visibility = "hidden";
        }
        function playForward() {
            isRecording = false;
            if (aktuelPlayAgainNode != null) {
                noteOff(aktuelPlayAgainNode.noteNumber);
            }

            //-----Forward by ONE note---------------------------

            if (tellerNode > 0) {
                tellerNode++;
                aktuelPlayAgainNode = NoteList[tellerNode];
                time = aktuelPlayAgainNode.x + 2;
            }
            Draw();
            //document.getElementById("startRecoding").style.visibility = "visible";
            //document.getElementById("stopRecording").style.visibility = "hidden";
            //document.getElementById("playAgain").style.visibility = "visible";
            //document.getElementById("playStop").style.visibility = "hidden";
            //document.getElementById("playPause").style.visibility = "hidden";
        }



        //----Quantize------Quantize------Quantize------Quantize------Quantize------Quantize---
        //----Quantize------Quantize------Quantize------Quantize------Quantize------Quantize---
        //----Quantize------Quantize------Quantize------Quantize------Quantize------Quantize---
        //----Quantize------Quantize------Quantize------Quantize------Quantize------Quantize---
        function AutoQuantize()
        {
            calculateNoteLengthAndSplitNotes();
            Quantize4();
            moveStartAndEnd();
            calculateNoteLengthAndSplitNotes();
            findRest();
            setAllToEditOff();
        }
        function setAllToEditOff()
        {
            for (var i = 0; i < NoteList.length ; i++)
            {
                NoteList[i].editMode = 0;
            }
            Draw();
        }
        function calculateNoteLengthAndSplitNotes()
        {
            /*
            listNodeLength.Add(32, "Whole");
            listNodeLength.Add(48, "Whole Dot");
            listNodeLength.Add(132, "Whole rest");
            listNodeLength.Add(148, "Whole Dot rest");
            listNodeLength.Add(16, "Half");
            listNodeLength.Add(24, "Half Dot");
            listNodeLength.Add(116, "Half rest");
            listNodeLength.Add(124, "Half Dot rest");
            listNodeLength.Add(8, "Quarter");
            listNodeLength.Add(12, "Quarter Dot");
            listNodeLength.Add(108, "Quarter rest");
            listNodeLength.Add(112, "Quarter Dot rest");
            listNodeLength.Add(4, "Eighth");
            listNodeLength.Add(6, "Eighth Dot");
            listNodeLength.Add(104, "Eighth rest");
            listNodeLength.Add(106, "Eighth Dot rest");
            listNodeLength.Add(2, "Sixteenth");
            listNodeLength.Add(3, "Sixteenth Dot");
            listNodeLength.Add(102, "Sixteenth rest");
            listNodeLength.Add(103, "Sixteenth Dot rest");
            listNodeLength.Add(-1, "line");
            */
            for (var i = 0; i < NoteList.length - 1 ; i++)
            {
                var n1 = NoteList[i];
                var n2 = NoteList[i + 1];

                if (n1.x >= n1.xStop) //Same note: Stop comes before start, not good.
                {
                    n1.xStop = n1.x + 1;
                }

                if (n1.xStop >= n2.x) //Different note: Going over each other.
                {
                    n1.xStop = n2.x - 1;
                }
                //Not Rest
                if (n1.noteLength < 100)
                {
                    n1.noteLength = Math.round((n1.xStop - n1.x + 1) / timeSign); //+1 because we -1 before.
                }
            }
            //the last note fixed her...
            var n1 = NoteList[NoteList.length - 1];
            //Not Rest
            if (n1.noteLength < 100)
            {
                n1.noteLength = Math.round((n1.xStop - n1.x + 1) / timeSign);
            }

            //find notes of length 0 and delete them-------------
            for (var i = 0; i < NoteList.length - 1 ; i++)
            {
                if (NoteList[i].noteLength === 0)
                {
                    NoteList.splice(i, 1); //Delete 1 note at position i.
                    if (NoteList[i].xStop - NoteList[i].x < timeSign / 4) //extra tjeck of the real length.
                    {

                    }
                }
            }

            Draw();
        }
        function findRest()
        {
            //---Remove ALL rest----Clean it up. There could be several rest in a row, after editing.----
            for (var i = 0; i < NoteList.length - 1; i++)
            {
                if (NoteList[i].noteLength > 100)//Rest found.
                {
                    NoteList.splice(i, 1); //Delete 1 note at position i.
                    i--;
                }
            }

            //---Find Rest--------------------------------------
            var arrayTimeRests = [2, 3, 4, 6, 8, 12, 16, 24, 32, 48, 64];

            for (var i = 0; i < NoteList.length - 1; i++)
            {
                for (var j = 0; j < arrayTimeRests.length - 1 ; j++)
                {
                    var v1 = NoteList[i];
                    var v2 = NoteList[i + 1];
                    var dist = (v2.x - v1.xStop) / timeSign;
                    //  Math.round((n1.xStop - n1.x + 1) / timeSign); //+1 because we -1 before.
                    if (dist > arrayTimeRests[j] && dist < arrayTimeRests[j + 1])
                    {
                        var Note = { x: v1.xStop + 1, y: 400, xStop: v2.x - 1, noteNumber: 100 + arrayTimeRests[j], noteLength: 100 + arrayTimeRests[j], editMode: 0 };
                        NoteList.splice(i + 1, 0, Note); //0 means delete non.
                        break;
                    }
                }
            }
            Draw();
        }
        function moveStartAndEnd()
        {
            var lokalTempo = tempo / select2or4; // 2 = 1/4(default) and 4 = 1/8.
            var Max = NoteList[NoteList.length - 1].xStop;
            var antal = Max / lokalTempo;
            //---------- Move the x position ------------------------------
            for (var i = 0; i < NoteList.length; i++)
            {
                for (var j = 0; j < antal; j++)
                {
                    var n = NoteList[i];
                    // <100 not Rest with.
                    if (n.noteLength < 100 && n.x >= (j * lokalTempo) - (lokalTempo / 2) && n.x <= (j * lokalTempo) + (lokalTempo / 2))
                    {
                        var newTempo = j * lokalTempo;
                        var extraToLast = n.x - newTempo;
                        n.x = newTempo;
                        n.xStop = n.xStop - extraToLast;
                    }
                }
            }
            //---------- Move the xStop position ------------------------------
            for (var i = 0; i < NoteList.length; i++)
            {
                for (var j = 0; j < antal; j++)
                {
                    var n = NoteList[i];
                    // <100 not Rest with.
                    if (n.noteLength < 100 && n.xStop >= (j * lokalTempo) - (lokalTempo / 2) && n.xStop <= (j * lokalTempo) + (lokalTempo / 2))
                    {
                        var newTempo = j * lokalTempo;
                        n.xStop = newTempo - 1;
                    }
                }
            }

            //   TROR IKKE DEN SKAL BRUGES MERE..EFTER AT JEG HAR IMPL. QUANTIZA AF TO SLAGS------------------
            //----Some notes might be less then 4 in lengt...That is not allowed.
            var minLenght = 4;
            //select2or4: 2 = 1/4(default) and 4 = 1/8.
            if (select2or4 === 2)
            {
                minLenght = 4;
            }
            else if (select2or4 === 4)
            {
                minLenght = 2;
            }

            for (var i = 0; i < NoteList.length; i++)
            {
                if (NoteList[i].noteLength < minLenght)
                {
                    NoteList[i].xStop = NoteList[i].x + (lokalTempo) - 1;
                }
            }
            Draw();
        }
        function Quantize4()
        {
            var lokalTempo = tempo / select2or4; // 2 = 1/4(default) and 4 = 1/8.
            //var lokalTempo = tempo / 2;
            var Max = NoteList[NoteList.length - 1].xStop;
            var antal = Max / lokalTempo;
            //---------- Move the x position ------------------------------
            //-------- Quantize --- Quantize ---- Quantize ---- Quantize ---- Quantize ------------
            //---------move up if not right on value-----------------------------------------------
            var arrayTime = [2, 3, 4, 6, 8, 12, 16, 24, 32, 48, 64];
            //   var arrayTime = [2, 4, 8, 12, 16, 24, 32, 48, 64];

            for (var i = 0; i < NoteList.length; i++)
            {
                for (var j = 0; j < arrayTime.length - 1; j++)
                {
                    // <100 so we not catches Rests
                    if (NoteList[i].noteLength < 100 && NoteList[i].noteLength > arrayTime[j] && NoteList[i].noteLength < arrayTime[j + 1])
                    {
                        gennemSnit = (arrayTime[j + 1] - arrayTime[j]) / 2;
                        if (NoteList[i].noteLength > gennemSnit)
                        {
                            NoteList[i].noteLength = arrayTime[j + 1];
                            //Then move sStop to eventuel new position.
                            NoteList[i].xStop = NoteList[i].x + NoteList[i].noteLength * 4;
                        }
                        else
                        {
                            NoteList[i].noteLength = arrayTime[j];
                            //Then move sStop to eventuel new position.
                            NoteList[i].xStop = NoteList[i].x + NoteList[i].noteLength * 4;
                        }

                    }
                }
            }
            Draw();
        }
        function undo()
        {
            //Copy back from saved list.
            NoteList = arrayCopy.map(a => Object.assign({}, a));

            Draw();
        }



        //-----Drawing----------Drawing---------------Drawing----------Drawing---------------Drawing----------Drawing----------
        //-----Drawing----------Drawing---------------Drawing----------Drawing---------------Drawing----------Drawing----------
        //-----Drawing----------Drawing---------------Drawing----------Drawing---------------Drawing----------Drawing----------
        function Draw()
        {
            var newTime = 0;
            if (time > 900)
                newTime = time - 900;



            //----refresh the canvas---------------------------------------------------------------------
            ctx.clearRect(0, 0, c.width, c.height);
            ctx.fillStyle = collors[4]; //black

            //---------TEST---------
            ctx.fillStyle = collors[1]; //
            ctx.fillRect(time - newTime, 10, 2, 800);


            //---Drawing ALL notes her.
            for (var i = 0; i < NoteList.length; i++)
            {
                //-----Piano Roll------------------------------------------------------------------------
                lengt = NoteList[i].xStop - NoteList[i].x;
                if (NoteList[i].editMode === 1) //EDIT
                {
                    ctx.fillStyle = collors[0]; //Red
                }
                else
                {
                    ctx.fillStyle = collors[4]; //black
                }
                ctx.fillRect(NoteList[i].x - newTime, NoteList[i].y, lengt, 5);

                //-------IMAGE---------------------------------------------------------------------------
                /*
          listNodeLength.Add(32, "Whole");
          listNodeLength.Add(48, "Whole Dot");
          listNodeLength.Add(132, "Whole rest");
          listNodeLength.Add(148, "Whole Dot rest");
          listNodeLength.Add(16, "Half");
          listNodeLength.Add(24, "Half Dot");
          listNodeLength.Add(116, "Half rest");
          listNodeLength.Add(124, "Half Dot rest");
          listNodeLength.Add(8, "Quarter");
          listNodeLength.Add(12, "Quarter Dot");
          listNodeLength.Add(108, "Quarter rest");
          listNodeLength.Add(112, "Quarter Dot rest");
          listNodeLength.Add(4, "Eighth");
          listNodeLength.Add(6, "Eighth Dot");
          listNodeLength.Add(104, "Eighth rest");
          listNodeLength.Add(106, "Eighth Dot rest");
          listNodeLength.Add(2, "Sixteenth");
          listNodeLength.Add(3, "Sixteenth Dot");
          listNodeLength.Add(102, "Sixteenth rest");
          listNodeLength.Add(103, "Sixteenth Dot rest");
          listNodeLength.Add(-1, "line");
          */
                yPlus = 20;
                if (NoteList[i].noteLength === 2)
                    ctx.drawImage(SixteentNote, NoteList[i].x - newTime, NoteList[i].y + yPlus, 20, 40);
                else if (NoteList[i].noteLength === 3)
                    ctx.drawImage(SixteentDotNote, NoteList[i].x - newTime, NoteList[i].y + yPlus, 20, 40);
                else if (NoteList[i].noteLength === 4)
                    ctx.drawImage(EightNote, NoteList[i].x - newTime, NoteList[i].y + yPlus, 20, 40);
                else if (NoteList[i].noteLength === 6)
                    ctx.drawImage(EightDotNote, NoteList[i].x - newTime, NoteList[i].y + yPlus, 20, 40);
                else if (NoteList[i].noteLength === 8)
                    ctx.drawImage(KvartNote, NoteList[i].x - newTime, NoteList[i].y + yPlus, 20, 40);
                else if (NoteList[i].noteLength === 12)
                    ctx.drawImage(KvartDotNote, NoteList[i].x - newTime, NoteList[i].y + yPlus, 20, 40);
                else if (NoteList[i].noteLength === 16)
                    ctx.drawImage(HalfNote, NoteList[i].x - newTime, NoteList[i].y + yPlus, 20, 40);
                else if (NoteList[i].noteLength === 24)
                    ctx.drawImage(HalfDotNote, NoteList[i].x - newTime, NoteList[i].y + yPlus, 20, 40);
                else if (NoteList[i].noteLength === 32)
                    ctx.drawImage(FullNote, NoteList[i].x - newTime, NoteList[i].y + yPlus, 25, 15);
                else if (NoteList[i].noteLength === 48)
                    ctx.drawImage(FullDotNote, NoteList[i].x - newTime, NoteList[i].y + yPlus, 25, 15);
                //------------REST---------------------------------------
                else if (NoteList[i].noteLength === 102)
                    ctx.drawImage(SixteentNoteRest, NoteList[i].x - newTime, NoteList[i].y + yPlus, 20, 40);
                else if (NoteList[i].noteLength === 103)
                    ctx.drawImage(SixteentDotNoteRest, NoteList[i].x - newTime, NoteList[i].y + yPlus, 20, 40);
                else if (NoteList[i].noteLength === 104)
                    ctx.drawImage(EightNoteRest, NoteList[i].x - newTime, NoteList[i].y + yPlus, 20, 40);
                else if (NoteList[i].noteLength === 106)
                    ctx.drawImage(EightDotNoteRest, NoteList[i].x - newTime, NoteList[i].y + yPlus, 20, 40);
                else if (NoteList[i].noteLength === 108)
                    ctx.drawImage(KvartNoteRest, NoteList[i].x - newTime, NoteList[i].y + yPlus, 20, 40);
                else if (NoteList[i].noteLength === 112)
                    ctx.drawImage(KvartDotNoteRest, NoteList[i].x - newTime, NoteList[i].y + yPlus, 20, 40);
                else if (NoteList[i].noteLength === 116)
                    ctx.drawImage(HalfNoteRest, NoteList[i].x - newTime, NoteList[i].y + yPlus, 20, 40);
                else if (NoteList[i].noteLength === 124)
                    ctx.drawImage(HalfDotNoteRest, NoteList[i].x - newTime, NoteList[i].y + yPlus, 20, 40);
                else if (NoteList[i].noteLength === 132)
                    ctx.drawImage(FullNoteRest, NoteList[i].x - newTime, NoteList[i].y + yPlus, 25, 15);
                else if (NoteList[i].noteLength === 148)
                    ctx.drawImage(FullDotNoteRest, NoteList[i].x - newTime, NoteList[i].y + yPlus, 25, 15);
                //---TEXT-----Mostly for debugging-------------------------------------------------------
                //ctx.font = "10px Arial";
                //ctx.fillText(NoteList[i].noteLength, NoteList[i].x - newTime, NoteList[i].y - 20);
                //ctx.font = "12px Arial";
                //ctx.fillText(NoteList[i].x + "-", NoteList[i].x - newTime, NoteList[i].y - 100);
                //ctx.fillText(NoteList[i].xStop, NoteList[i].x - newTime + 5, NoteList[i].y - 80);

                //-----Pauses-----------------------------------------------------------------------------
                if (NoteList[i].noteLength > 100)
                {
                    ctx.fillStyle = collors[3]; //blue
                    ctx.fillRect(NoteList[i].x - newTime, NoteList[i].y, lengt, 5);
                    ctx.fillStyle = collors[4]; //black
                }

            }

            //----Last node drawing----------------------------------------------------------------------
            // This node is not completed, so we paint it from start to now.
            if (stop === 0)
            {
                ctx.fillRect(NoteList[NoteList.length - 1].x - newTime, NoteList[NoteList.length - 1].y, time - NoteList[NoteList.length - 1].x, 5);
            }
            //-----Takt Lines-------------------------------------------------------------------------
            if (NoteList.length > 0)
            {
                var x = document.getElementById("checkBoxLines");
                if (x.checked)
                {
                    var Max = NoteList[NoteList.length - 1].xStop;
                    var antal = Max / tempo;

                    for (var k = 0; k < antal; k++)
                    {
                        ctx.fillRect(k * tempo - newTime, 10, 1, 800);
                    }
                }

            }

            //---Draw Time position-----------------------------------------------------------------------
            //ctx.fillStyle = collors[1]; //
            //ctx.fillRect(time - newTime, 10, 2, 800);
        }

        var lastNote;
        var stop = 0;
        var firstTimeClockStart = false;
        function saveNewNote(noteNumber)
        {
            //---The timer wont start the metronom, unless it is startet by an key event, dont know why.
            if (firstTimeClockStart)
            {
                timerVariabel = setInterval(recordTimer, clock);
                firstTimeClockStart = false;
            }
            // LastNote stop not set. We have played two note at the same time her.
            // therefor we stop the lastNote now.
            if (lastNote != null)
            {
                if (lastNote.x === lastNote.xStop)  //Her we use xStop.
                {
                    lastNote.xStop = time;// May come after the next note starts. But we fix this later.
                }
            }

            // xStop = time because it is not set until it is stoped. And we can use this value to
            // check wetter it is set or not. See just above..
            var Note = { x: time, y: 800 - (noteNumber * 10), xStop: time, noteNumber: noteNumber, noteLength: 0, editMode: 0 };
            NoteList.push(Note);

            lastNote = Note;
            stop = 0;

        }
        function stopNote(noteNumber)
        {
            //In the case of several note played, the stop may
            //be another note than the lastNote. We only stop the lastNote her.
            if (lastNote.noteNumber === noteNumber)
            {
                lastNote.xStop = time;
                stop = 1;
            }
        }



        // -------Midi----------------------------------------------------------
        // -------Midi----------------------------------------------------------
        // -------Midi----------------------------------------------------------
        var context = null;   // the Web Audio "context" object
        var midiAccess = null;  // the MIDIAccess object.
        var oscillator = null;  // the single oscillator
        var envelope = null;    // the envelope for the single oscillator
        var attack = 0.05;      // attack speed
        var release = 0.05;   // release speed
        var portamento = 0.00;  // portamento/glide speed
        var activeNotes = []; // the stack of actively-pressed keys
        //Midi Copy for the Metronom.
        var context2 = null;   // the Web Audio "context" object
        var midiAccess2 = null;  // the MIDIAccess object.
        var oscillator2 = null;  // the single oscillator
        var envelope2 = null;    // the envelope for the single oscillator
        var attack2 = 0.05;      // attack speed
        var release2 = 0.05;   // release speed
        var portamento2 = 0.00;  // portamento/glide speed
        var activeNotes2 = []; // the stack of actively-pressed keys

        window.addEventListener('load', function ()
        {
        });

        function startMidi()
        {
            //----Setup buttons--------------------------------
            hideAllButtons();
            document.getElementById("startMidi").style.visibility = "visible";
            document.getElementById("startRecoding").style.visibility = "visible";
            ////--------------------------------------------------------


            if (navigator.requestMIDIAccess)
            {
                navigator.requestMIDIAccess().then(onMIDIInit, onMIDIReject);
            }
            else
            {
                alert("No MIDI support present in your browser.  You're gonna have a bad time.")
            }



            // patch up prefixes
            window.AudioContext = window.AudioContext || window.webkitAudioContext;
            // set up the basic oscillator chain, muted to begin with.

            //-------Melody------------------------------------------
            context = new AudioContext();
            oscillator = context.createOscillator();
            oscillator.frequency.setValueAtTime(110, 0);
            envelope = context.createGain();
            oscillator.connect(envelope);
            envelope.connect(context.destination);
            envelope.gain.value = 0.0;  // Mute the sound
            oscillator.start(0);  // Go ahead and start up the oscillator

            //------Metronom------------------------------------
            context2 = new AudioContext();
            oscillator2 = context2.createOscillator();
            oscillator2.frequency.setValueAtTime(110, 0);
            envelope2 = context2.createGain();
            oscillator2.connect(envelope2);
            envelope2.connect(context2.destination);
            envelope2.gain.value = 0.0;  // Mute the sound
            oscillator2.start(0);  // Go ahead and start up the oscillator


        }
        function onMIDIInit(midi)
        {
            midiAccess = midi;


            var haveAtLeastOneDevice = false;
            var inputs = midiAccess.inputs.values();
            var name = "";

            for (var input = inputs.next() ; input && !input.done; input = inputs.next())
            {
                input.value.onmidimessage = MIDIMessageEventHandler;
                haveAtLeastOneDevice = true;
                name = input.value.name;
            }
            if (!haveAtLeastOneDevice)
            {
                alert(name + "try again. Or use the computer keyboard to play notes");

            }
            else if (haveAtLeastOneDevice)
            {
                //---Recording start automatically, show no button...The stop will show when the first note is played.
                //document.getElementById("startMidi").style.visibility = "hidden";
                //document.getElementById("startRecoding").style.visibility = "hidden";
                //document.getElementById("stopRecording").style.visibility = "hidden";
                //document.getElementById("playAgain").style.visibility = "hidden";
                //document.getElementById("playStop").style.visibility = "hidden";
                //document.getElementById("playPause").style.visibility = "hidden";

                alert(name + " Ready. Just start playing on the piano.");

            }
        }

        function onMIDIReject(err)
        {
            alert("The MIDI system failed to start.  You're gonna have a bad time.");
        }

        function MIDIMessageEventHandler(event)
        {
            // Mask off the lower nibble (MIDI channel, which we don't care about)
            switch (event.data[0] & 0xf0)
            {
                case 0x90:
                    if (event.data[2] != 0)
                    {  // if velocity != 0, this is a note-on message
                        noteOn(event.data[1]);
                        return;
                    }
                    // if velocity == 0, fall thru: it's a note-off.  MIDI's weird, y'all.
                case 0x80:
                    noteOff(event.data[1]);
                    return;
            }
        }

        function frequencyFromNoteNumber(note)
        {
            return 440 * Math.pow(2, (note - 69) / 12);
        }
        function noteOn(noteNumber)
        {
            //----Save note in list AND Drawing---------------------------------------------
            if (isRecording)
            {
                saveNewNote(noteNumber);
            }
            //  alert(noteNumber);

            //-----Playing the note----------------------------------------------------------
            activeNotes.push(noteNumber);
            oscillator.frequency.cancelScheduledValues(0);
            oscillator.frequency.setTargetAtTime(frequencyFromNoteNumber(noteNumber), 0, portamento);
            envelope.gain.cancelScheduledValues(0);
            envelope.gain.setTargetAtTime(1.0, 0, attack);
        }

        function noteOff(noteNumber)
        {
            //---Stopping the note---------------------------------
            if (isRecording)
            {
                stopNote(noteNumber);
            }
            //---------------------------------------------
            var position = activeNotes.indexOf(noteNumber);
            if (position != -1)
            {
                activeNotes.splice(position, 1);
            }
            if (activeNotes.length == 0)
            {  // shut off the envelope
                envelope.gain.cancelScheduledValues(0);
                envelope.gain.setTargetAtTime(0.0, 0, release);
            } else
            {
                oscillator.frequency.cancelScheduledValues(0);
                oscillator.frequency.setTargetAtTime(frequencyFromNoteNumber(activeNotes[activeNotes.length - 1]), 0, portamento);
            }
        }
        var gainVolume = 0.2;
        //Metronom
        function noteOnMetronom(noteNumber)
        {
            activeNotes2.push(noteNumber);
            oscillator2.frequency.cancelScheduledValues(0);
            oscillator2.frequency.setTargetAtTime(frequencyFromNoteNumber(noteNumber), 0, portamento2);
            envelope2.gain.cancelScheduledValues(0);
            envelope2.gain.setTargetAtTime(gainVolume, 0, attack2);
        }
        //Metronom
        function noteOffMetronom(noteNumber)
        {
            var position = activeNotes2.indexOf(noteNumber);
            if (position != -1)
            {
                activeNotes2.splice(position, 1);
            }
            if (activeNotes2.length == 0)
            {  // shut off the envelope
                envelope2.gain.cancelScheduledValues(0);
                envelope2.gain.setTargetAtTime(0.0, 0, release2);
            } else
            {
                oscillator2.frequency.cancelScheduledValues(0);
                oscillator2.frequency.setTargetAtTime(frequencyFromNoteNumber(activeNotes2[activeNotes2.length - 1]), 0, portamento2);
            }
        }

    </script>
    <form id="form1" runat="server">
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        <asp:Button ID="Button1" runat="server" Style="display: none;" OnClick="Button1_Click" Text="Button" />
        <div>
        </div>
    </form>
</body>
</html>
