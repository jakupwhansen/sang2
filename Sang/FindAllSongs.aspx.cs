﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sang
{
    public partial class FindAllSongs : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Bruger"] == null)
            {
                Server.Transfer("Default.aspx");
            }
            else
            {
                //-----------DIREKTE LINK----------------------------------------
                Bruger brug = (Bruger)Session["Bruger"];
                if (brug.ID == -1) //Så kommer man fra direkte link. Må stoppes.
                {
                    Session["Bruger"] = null;
                    return;
                }
                //------------------------------------------------------------------

                Bruger BrugerValgt = (Bruger)Session["Bruger"];
                visListeAfSangeForBruger(BrugerValgt.ID);
                if(Session["DirekteLink"] != null)
                {
                    String dirlink = (String)Session["DirekteLink"];
                    Session["DirekteLink"] = null;

                    TextBoxLink.Visible = true;
                    TextBoxLink.Text =  dirlink ;                   
                }
                else
                {
                    TextBoxLink.Visible = false;
                }
            }
        }
        protected void ButtonBack_Click(object sender, EventArgs e)
        {
            if (Session["KommerFra"] != null)
            {
                List<String> listeKommerFra = (List<String>)Session["KommerFra"];
                String tilbageTil = listeKommerFra[listeKommerFra.Count - 1];
                listeKommerFra.RemoveAt(listeKommerFra.Count - 1);
                Session["KommerFra"] = listeKommerFra;
                //Response.Write("<script LANGUAGE='JavaScript' >alert('" + tilbageTil + "')</script>");
                Server.Transfer(tilbageTil);
            }
            else
            {
                Server.Transfer("Default.aspx");
            }
        }
        protected void ButtonActivateDelet_Click(object sender, EventArgs e)
        {
           
        }
       

        public void visListeAfSangeForBruger(int ID)
        {
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["wenningsdbConnectionString1"].ToString());
                con.Open();

                //------------HENTE SANG START------------------------------------------------
                //------------HENTE SANG START------------------------------------------------
                //------------HENTE SANG START------------------------------------------------
                SqlCommand cmd;
                if (ID == -1)//-1 betyder at ALLE sange skal vises.
                {
                    cmd = new SqlCommand("SELECT * FROM Sang ORDER BY titel ASC;", con);
                }
                else
                {
                    cmd = new SqlCommand("SELECT * FROM Sang WHERE userID=@userID  ORDER BY titel ASC;", con);
                    cmd.Parameters.AddWithValue("@userID", ID);
                }
                SqlDataReader reader = cmd.ExecuteReader();
                List<SangAdminValgt> listSang = new List<SangAdminValgt>();
                if (reader.HasRows)
                {
                 //  Response.Write("<script LANGUAGE='JavaScript' >alert('" + "OK" + "')</script>");

                    while (reader.Read())
                    {
                        //    Response.Write("<script LANGUAGE='JavaScript' >alert('" + "listBrugere.Count" + "')</script>");
                        //   ListBox1.Items.Add("Song har item");
                        Bruger bruger = new Bruger();

                        //---titel-------------------   
                        String titel = "ingen titel";
                        if (!reader.IsDBNull(1))
                        {
                            titel = reader.GetString(1);
                        }
                        //---------------------------
                        //---type-------------------   
                        int type = 0;
                        if (!reader.IsDBNull(6))
                        {
                            type = reader.GetInt32(6);
                        }
                        //---------------------------
                        //---ID-------------------   
                        int id = 0;
                        if (!reader.IsDBNull(0))
                        {
                            id = reader.GetInt32(0);
                        }
                        //---------------------------

                        SangAdminValgt sangValg = new SangAdminValgt();
                    
                        sangValg.admOverS = new AdminOverviewShowSongs();
                                         
                      
                        sangValg.findAllS = this;
                        sangValg.title = titel;
                        sangValg.type = type;
                        sangValg.SangID = id;


                        Button button = new Button();
                        button.Click += new EventHandler(sangValg.eventMetode);
                        sangValg.knap = button;

                        Button buttonTilstand = new Button();
                        buttonTilstand.Click += new EventHandler(sangValg.eventTilstand);
                        sangValg.knapTilstand = buttonTilstand;

                        
                        //-------Add to listen---------------------
                        listSang.Add(sangValg);
                    }
                }
                con.Close();
                //------------HENTE SANG SLUT------------------------------------------------
                //------------HENTE SANG SLUT------------------------------------------------
                //------------HENTE SANG SLUT------------------------------------------------


                //------------HENTE SANG START------------------------------------------------
                //------------HENTE SANG START------------------------------------------------
                //------------HENTE SANG START------------------------------------------------
                con.Open();
                if (ID == -1)//-1 betyder at ALLE sange skal vises.
                {
                    cmd = new SqlCommand("SELECT * FROM SangSheet ORDER BY titel ASC;", con);
                     //  Response.Write("<script LANGUAGE='JavaScript' >alert('" + "OK" + "')</script>");
                     
                }
                else
                {
                    cmd = new SqlCommand("SELECT * FROM SangSheet WHERE userID=@userID  ORDER BY titel ASC;", con);
                    cmd.Parameters.AddWithValue("@userID", ID);
                }
                reader = cmd.ExecuteReader();
                List<SangAdminValgt> listSangSheet = new List<SangAdminValgt>();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                      //   Response.Write("<script LANGUAGE='JavaScript' >alert('" + "listBrugere.Count" + "')</script>");
                        Bruger bruger = new Bruger();

                        //---titel-------------------   
                        String titel = "ingen titel";
                        if (!reader.IsDBNull(1))
                        {
                            titel = reader.GetString(1);
                            titel = titel.TrimStart(' ');
                        }
                        //---------------------------
                        //---type-------------------   
                        int type = 0;
                        if (!reader.IsDBNull(6))
                        {
                            type = reader.GetInt32(6);
                        }
                        //---------------------------
                        //---id-------------------   
                        int id = 0;
                        if (!reader.IsDBNull(0))
                        {
                            id = reader.GetInt32(0);
                        }
                        //---------------------------

                        SangAdminValgt sangValg = new SangAdminValgt();
                  
                        sangValg.admOverS = new AdminOverviewShowSongs();
                        
                       
                        sangValg.findAllS = this;
                        sangValg.title = titel;
                        sangValg.type = type;
                        sangValg.SangID = id;
                        sangValg.sheetSong = true;


                        Button button = new Button();
                        button.Click += new EventHandler(sangValg.eventMetode);
                        sangValg.knap = button;

                        Button buttonTilstand = new Button();
                        buttonTilstand.Click += new EventHandler(sangValg.eventTilstand);
                        sangValg.knapTilstand = buttonTilstand;


                        //-------Add to listen---------------------
                        listSangSheet.Add(sangValg);
                    }
                }
                con.Close();
                listSangSheet.Sort();
               
                //------------HENTE SANG SLUT------------------------------------------------
                //------------HENTE SANG SLUT------------------------------------------------
                //------------HENTE SANG SLUT------------------------------------------------


                //----------SANG TIL KNAP START---------------------------------------------
                //----------SANG TIL KNAP START---------------------------------------------
                //----------SANG TIL KNAP START---------------------------------------------
                foreach (SangAdminValgt sa in listSang)
                {
                    // Response.Write("<script LANGUAGE='JavaScript' >alert('" + "listBrugere.Count" + "')</script>");
                 
                    // Create new row and add it to the table.
                    TableRow tRow = new TableRow();
                    Table1.Rows.Add(tRow);

                    // Create a new cell and add it to the row.
                    TableCell tCell = new TableCell();
                    sa.knap.Text = sa.title;
                    sa.knap.Width = 300;
                    sa.knap.Height = 40;
                    sa.knap.Font.Size = 14;
                    //if (sa.type == 0)
                    //{
                    //    sa.knap.Font.Strikeout = true;
                    //}
                    sa.knap.Font.Bold = true;
                    sa.knap.CssClass = "btn-default";
                    tCell.Controls.Add(sa.knap);

                    // Create a new cell and add it to the row.
                    TableCell tCell2 = new TableCell();
                    sa.knapTilstand.Text = sa.type.ToString();
                    sa.knapTilstand.Width = 100;
                    sa.knapTilstand.Height = 40;
                    if (sa.type == 0)
                    {
                        sa.knapTilstand.BackColor = Color.Goldenrod;
                        sa.knapTilstand.Text = "Private";
                    }
                    if (sa.type == 1)
                    {
                        sa.knapTilstand.BackColor = Color.LightGreen;
                        sa.knapTilstand.Text = "Public";
                    }
                    if (sa.type == 2)
                    {
                        sa.knapTilstand.BackColor = Color.LightYellow;
                        sa.knapTilstand.Text = "Link";
                    }
                    if (sa.type == 3)
                    {
                        sa.knapTilstand.BackColor = Color.LightBlue;
                        sa.knapTilstand.Text = "Request";
                    }
                    sa.knapTilstand.Font.Size = 14;
                    sa.knapTilstand.Font.Bold = true;
                    tCell2.Controls.Add(sa.knapTilstand);

                    //br.reFresh(); //Tegner titel og hvem ejer sangen
                    tRow.Cells.Add(tCell);
                    tRow.Cells.Add(tCell2);



                }
                //----------SANG TIL KNAP SLUT---------------------------------------------
                //----------SANG TIL KNAP SLUT---------------------------------------------
                //----------SANG TIL KNAP SLUT---------------------------------------------


                //----------SANG TIL KNAP START---------------------------------------------
                //----------SANG TIL KNAP START---------------------------------------------
                //----------SANG TIL KNAP START---------------------------------------------
                foreach (SangAdminValgt sa in listSangSheet)
                {
                    // Response.Write("<script LANGUAGE='JavaScript' >alert('" + "listBrugere.Count" + "')</script>");

                    // Create new row and add it to the table.
                    TableRow tRow = new TableRow();
                    Table1.Rows.Add(tRow);

                    // Create a new cell and add it to the row.
                    TableCell tCell = new TableCell();
                    sa.knap.Text = sa.title;
                    sa.knap.Width = 300;
                    sa.knap.Height = 40;
                    sa.knap.Font.Size = 14;
                    sa.knap.Font.Bold = false;
                    //if (sa.type == 0)
                    //{
                    //    sa.knap.Font.Strikeout = true;
                    //}
                    sa.knap.Font.Italic = true;
                    sa.knap.CssClass = "btn-default";
                    tCell.Controls.Add(sa.knap);

                    // Create a new cell and add it to the row.
                    TableCell tCell2 = new TableCell();
                    sa.knapTilstand.Text = sa.type.ToString();
                    sa.knapTilstand.Width = 100;
                    sa.knapTilstand.Height = 40;
                    // 0 = Private
                    // 1 = Public
                    // 2 = Link
                    // 3 = Request
                    if (sa.type == 0)
                    {
                        sa.knapTilstand.BackColor = Color.Goldenrod;
                        sa.knapTilstand.Text = "Private";
                    }
                    if (sa.type == 1)
                    {
                        sa.knapTilstand.BackColor = Color.LightGreen;
                        sa.knapTilstand.Text = "Public";
                    }
                    if (sa.type == 2)
                    {
                        sa.knapTilstand.BackColor = Color.LightYellow;
                        sa.knapTilstand.Text = "Link";
                    }
                    if (sa.type == 3)
                    {
                        sa.knapTilstand.BackColor = Color.LightBlue;
                        sa.knapTilstand.Text = "Request";
                    }

                    sa.knapTilstand.Font.Size = 14;
                    sa.knapTilstand.Font.Bold = true;
                    tCell2.Controls.Add(sa.knapTilstand);

                    //br.reFresh(); //Tegner titel og hvem ejer sangen
                    tRow.Cells.Add(tCell);
                    tRow.Cells.Add(tCell2);
                }
                //----------SANG TIL KNAP SLUT---------------------------------------------
                //----------SANG TIL KNAP SLUT---------------------------------------------
                //----------SANG TIL KNAP SLUT---------------------------------------------
                con.Close();
            }
            catch (Exception lskd)
            {

            }

        }
        //-------COPI fra FindSong---------------
        //-------COPI fra FindSong---------------
        public void createSong(String idValgt)
        {
            // String idValgt = GridView1.SelectedValue.ToString();
            Sang sang = new Sang();
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["wenningsdbConnectionString1"].ToString());
                con.Open();
                //------Henter én Sang, som passer til ID------------------------------
                SqlCommand cmd = new SqlCommand("Select * FROM  Sang WHERE Id=@sangID  ;", con);

                cmd.Parameters.AddWithValue("@sangID", idValgt);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {

                        sang.title = reader.GetString(1);
                        sang.lyricWriter = reader.GetString(2);
                        sang.melodyWriter = reader.GetString(3);
                        sang.userID = reader.GetInt32(4);
                    }
                }
                reader.Close();

                //------Henter textLinjer-------------------------------------------
                SqlCommand cmd2 = new SqlCommand("Select * FROM  SangTextLinjer WHERE sangID=@sangID  ;", con);
                cmd2.Parameters.AddWithValue("@sangID", idValgt);
                SqlDataReader reader2 = cmd2.ExecuteReader();

                if (reader2.HasRows)
                {
                    while (reader2.Read())
                    {
                        TextLinje t = new TextLinje();
                        t.text = reader2.GetString(2);
                        t.pos = new Point(reader2.GetInt32(3), reader2.GetInt32(4));
                        t.id = reader2.GetInt32(5);
                        sang.listTexter.Add(t);
                    }
                    foreach (TextLinje tl in sang.listTexter)
                    {
                    }

                }
                else
                {
                }
                reader2.Close();

                //------Henter Akkorder-------------------------------------------
                SqlCommand cmd3 = new SqlCommand("Select * FROM  SangAkkorder WHERE sangID=@sangID  ;", con);
                cmd3.Parameters.AddWithValue("@sangID", idValgt);
                SqlDataReader reader3 = cmd3.ExecuteReader();

                if (reader3.HasRows)
                {
                    while (reader3.Read())
                    {
                        Akkord ak = new Akkord();
                        ak.pos = new Point(reader3.GetInt32(2), reader3.GetInt32(3));
                        ak.keyNumber = reader3.GetInt32(4);
                        int bassen = 0;
                        try
                        {
                            bassen = reader3.GetInt32(7);
                        }
                        catch (Exception els)
                        {

                        }

                        ak.bass = bassen;


                        String extra = reader3.GetString(5);
                        if (extra != null)
                            ak.extraName = extra;
                        else
                            ak.extraName = "";
                        //  ak.id = reader3.GetInt32(6);
                        sang.listAkkorder.Add(ak);
                    }
                }
                else
                {
                }
                reader3.Close();


                Session["Sang"] = sang;
//                Server.Transfer("ShowSong.aspx");
            }
            catch (Exception lkjd)
            {

            }

        }
        public void visHvilkenSangDerErValgt(int SangID, String title)
        {
      //       Response.Write("<script LANGUAGE='JavaScript' >alert('" + SangID +":"+title+ "')</script>");
             createSong(SangID.ToString());
            //----------Kommer fra------------------------------         
            List<String> listeKommerFra = (List<String>)Session["KommerFra"];
            listeKommerFra.Add("FindAllSongs.aspx");
            Session["KommerFra"] = listeKommerFra;
            //-----------------------------------------------------
            Server.Transfer("AdminOverviewShowOneSong.aspx");
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}