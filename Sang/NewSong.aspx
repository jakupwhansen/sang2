﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewSong.aspx.cs" Inherits="Sang.NewSong" %>

<!DOCTYPE html>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!--  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script> -->

    <link rel="stylesheet" href="/bootstrap-3.3.5-dist/css/bootstrap.min.css" />
    <script src="/jquery/jquery-1.11.3.min.js"></script>
    <script src="/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/css/StyleSheet1.css" />


    <title>New Song</title>

    <style type="text/css">
        .auto-style1 {
        }

        .auto-style2 {
            width: 197px;
        }

        .min-style {
            vertical-align: top;
        }
        .auto-style3 {
            vertical-align: top;
            width: 14px;
        }
        .auto-style4 {
            width: 14px;
        }
        .auto-style5 {
            width: 200px;
        }
        .auto-style6 {
            vertical-align: top;
            width: 101px;
        }
        .auto-style7 {
            width: 101px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <br />

            <asp:Button CssClass="btn-default" ID="ButtonBack" runat="server" OnClick="ButtonBack_Click" Text="Back" />
            <br />
            <br />

            <table style="width: 55%;">
                <tr>
                    <td class="auto-style5">
                        <asp:Label ID="Label2" runat="server" Text="Titel: "></asp:Label>
                    </td>
                    <td class="auto-style2">
                        <asp:TextBox ID="TextBoxTitel" runat="server" Width="385px"></asp:TextBox>

                    </td>

                    <td class="auto-style3">
                        &nbsp;</td>
                    <td class="auto-style6">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style5">
                        <asp:Label ID="Label3" runat="server" Text="Lyric:"></asp:Label>
                    </td>
                    <td class="auto-style2">
                        <asp:TextBox ID="TextBoxLyric" runat="server" Width="385px"></asp:TextBox>

                    </td>

                    <td class="auto-style3">
                        &nbsp;</td>
                    <td class="auto-style6">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style5">
                        <asp:Label ID="Label4" runat="server" Text="Melody:"></asp:Label>
                    </td>
                    <td class="auto-style2">
                        <asp:TextBox ID="TextBoxMelody" runat="server" Width="385px"></asp:TextBox>

                    </td>

                    <td class="auto-style3">
                        &nbsp;</td>
                    <td class="auto-style6">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style5">
                        <asp:Label ID="Label1" runat="server"></asp:Label>
                    </td>
                    <td class="auto-style2">
                        <asp:TextBox ID="Txt_SongLine" runat="server" Width="601px" Height="297px" TextMode="MultiLine" Font-Size="16pt" Font-Names="Verdana" OnTextChanged="Txt_SongLine_TextChanged" Wrap="False"></asp:TextBox>
                        <div>
                            <input type="image" runat="server" name="coord" id="Image1" src="BitMapNewSong.aspx"  onserverclick="xymetoden" />
                        </div>

                    </td>

                    <td class="auto-style3">
                        <asp:Button CssClass="btn-default" ID="Btn_SaveLine" runat="server" OnClick="Btn_SaveLine_Click" Text="See result" Font-Bold="True" Font-Size="16pt" Height="50px" Width="180px" />
                        <br />
                        <asp:Label ID="Label_Besked" runat="server"></asp:Label>
                        <br />
                        <asp:Button CssClass="btn-default" ID="Btn_CleanText" runat="server" OnClick="Btn_CleanText_Click" Text="Clean text" Font-Bold="True" Font-Size="16pt" Height="50px" Width="180px" />
                        <br />
                        <br />
                        <br />
                    </td>
                    <td class="auto-style6">
                        <asp:Button CssClass="btn-default" ID="Btn_NullStil" runat="server" OnClick="Btn_NullStil_Click" Text="Undo" Visible="False" Font-Bold="True" Font-Size="16pt" Height="50px" Width="180px" />
                        <asp:Button CssClass="btn-default" ID="Btn_AddChords" runat="server" OnClick="Btn_AddChords_Click" Text="Add Chords &amp; Save" Font-Bold="True" Font-Size="12pt" Height="50px" Visible="False" Width="180px" />
                        <asp:Button CssClass="btn-default" ID="Btn_StretchPlus" runat="server" OnClick="Btn_StretchPlus_Click" Text="Stretch +" Font-Bold="True" Font-Size="16pt" Height="50px" Width="180px" Visible="False" />
                        <asp:Button CssClass="btn-default" ID="Btn_StretchMinus" runat="server" OnClick="Btn_StretchMinus_Click" Text="Stretch -" Font-Bold="True" Font-Size="16pt" Height="50px" Width="180px" Visible="False" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style5">&nbsp;</td>
                    <td class="auto-style2">&nbsp;</td>
                    <td class="auto-style4">&nbsp;</td>
                    <td class="auto-style7">&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style1" colspan="3">&nbsp;</td>
                    <td class="auto-style7">&nbsp;</td>
                </tr>
            </table>

        </div>
    </form>
    <p>
    </p>
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-54970963-3', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>
