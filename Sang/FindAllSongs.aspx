﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FindAllSongs.aspx.cs" Inherits="Sang.FindAllSongs" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Finde ALL Song</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!--  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script> -->

    <link rel="stylesheet" href="/bootstrap-3.3.5-dist/css/bootstrap.min.css" />
    <script src="/jquery/jquery-1.11.3.min.js"></script>
    <script src="/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/css/StyleSheet1.css" />

    <script type="text/javascript">
        window.onload = function () {
            var scrollY = parseInt('<%=Request.Form["scrollY"] %>');
                if (!isNaN(scrollY)) {
                    window.scrollTo(0, scrollY);
                }
            };
            window.onscroll = function () {
                var scrollY = document.body.scrollTop;
                if (scrollY == 0) {
                    if (window.pageYOffset) {
                        scrollY = window.pageYOffset;
                    }
                    else {
                        scrollY = (document.body.parentElement) ? document.body.parentElement.scrollTop : 0;
                    }
                }
                if (scrollY > 0) {
                    var input = document.getElementById("scrollY");
                    if (input == null) {
                        input = document.createElement("input");
                        input.setAttribute("type", "hidden");
                        input.setAttribute("id", "scrollY");
                        input.setAttribute("name", "scrollY");
                        document.forms[0].appendChild(input);
                    }
                    input.value = scrollY;
                }
            };

    </script>



    <style type="text/css">
        .auto-style1 {
            width: 379px;
            vertical-align: top;
        }

        .auto-style2 {
            width: 184px;
            vertical-align: top;
        }

        .style-right {
            text-align: right;
        }

        .auto-style3 {
            width: 184px;
            vertical-align: top;
            text-align: left;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <div class="container-fluid">
            <br />

            <table style="width: 40%;">
                <tr>
                    <td class="auto-style3">&nbsp;<asp:Button CssClass="btn-default" ID="ButtonBack" runat="server" OnClick="ButtonBack_Click" Text="Back" />
                        <br />
                        <br />
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="style-right">
                        <asp:DropDownList CssClass="form-control" ID="DropDownList1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" Width="300px" Font-Bold="True" Font-Italic="True" Font-Size="14pt" Visible="False">
                            <asp:ListItem Value="-1">All</asp:ListItem>
                            <asp:ListItem>A</asp:ListItem>
                            <asp:ListItem>&#193;</asp:ListItem>
                            <asp:ListItem>B</asp:ListItem>
                            <asp:ListItem>C</asp:ListItem>
                            <asp:ListItem>D</asp:ListItem>
                            <asp:ListItem>E</asp:ListItem>
                            <asp:ListItem>F</asp:ListItem>
                            <asp:ListItem>G</asp:ListItem>
                            <asp:ListItem>H</asp:ListItem>
                            <asp:ListItem>I</asp:ListItem>
                            <asp:ListItem>&#205;</asp:ListItem>
                            <asp:ListItem>J</asp:ListItem>
                            <asp:ListItem>K</asp:ListItem>
                            <asp:ListItem>L</asp:ListItem>
                            <asp:ListItem>M</asp:ListItem>
                            <asp:ListItem>N</asp:ListItem>
                            <asp:ListItem>O</asp:ListItem>
                            <asp:ListItem>&#211;</asp:ListItem>
                            <asp:ListItem>P</asp:ListItem>
                            <asp:ListItem>Q</asp:ListItem>
                            <asp:ListItem>R</asp:ListItem>
                            <asp:ListItem>S</asp:ListItem>
                            <asp:ListItem>T</asp:ListItem>
                            <asp:ListItem>U</asp:ListItem>
                            <asp:ListItem>&#218;</asp:ListItem>
                            <asp:ListItem>V</asp:ListItem>
                            <asp:ListItem>Y</asp:ListItem>
                            <asp:ListItem>&#221;</asp:ListItem>
                            <asp:ListItem>&#198;</asp:ListItem>
                            <asp:ListItem>&#216;</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2">
                        <asp:Table ID="Table1" runat="server">
                        </asp:Table>
                    </td>
                    <td class="auto-style1">
                        <asp:TextBox ID="TextBoxLink" runat="server" Visible="False" Width="123px"></asp:TextBox>
                    </td>
                </tr>
            </table>

            <br />
        </div>
        <br />



        <asp:ListBox ID="ListBox1" runat="server" Height="109px" Width="993px" Visible="False"></asp:ListBox>

    </form>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-54970963-3', 'auto');
        ga('send', 'pageview');

    </script>
</body>

</html>
