﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sang
{
    public partial class AdminOverviewShowOneSong : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Admin"] == null)
            {
                Server.Transfer("Default.aspx");
            }
            if (!IsPostBack)
            {
              
                    Dictionary<int, String> listAkkorder = GeneralData.getListAkkorder();

                    DropDownList1.DataSource = listAkkorder;
                    DropDownList1.DataTextField = "Value";
                    DropDownList1.DataValueField = "Key";
                    DropDownList1.DataBind();

                    DropDownList2.DataSource = listAkkorder;
                    DropDownList2.DataTextField = "Value";
                    DropDownList2.DataValueField = "Key";
                    DropDownList2.DataBind();
                
            }
          
            DropDownList1.Focus();
            //if (Session["Sang"] != null)
            //{
            //    Sang sang = (Sang)Session["Sang"];
            //    HttpContext.Current.Response.Write("<script LANGUAGE='JavaScript' >alert('" + "SangID=" +sang.ID + "')</script>");
            //}

        }
        protected void xymetoden(Object sender, ImageClickEventArgs e)
        {
            if (Session["Sang"] != null)
            {
                Sang sang = (Sang)Session["Sang"];
                Akkord akkord = new Akkord();
                akkord.pos = new Point(e.X, e.Y);
                akkord.id = sang.listAkkorder.Count;
                if (DropDownList1.SelectedIndex > 0)
                {
                    //  akkord.name = DropDownList1.SelectedItem.Text;
                    akkord.keyNumber = Convert.ToInt32(DropDownList1.SelectedValue);
                    akkord.bass = Convert.ToInt32(DropDownList2.SelectedValue);
                    if (!CheckBoxChord.Checked)
                    {
                        DropDownList1.SelectedIndex = 0;
                    }
                    if (!CheckBoxBass.Checked)
                    {
                        DropDownList2.SelectedIndex = 0;
                    }
                    if (TextBox1.Text.Length > 0)
                    {
                        akkord.extraName = TextBox1.Text;
                        if (!CheckBoxExtra.Checked)
                        {
                            TextBox1.Text = "";
                        }
                    }
                    akkord.id = sang.listAkkorder.Count + 1;
                    sang.listAkkorder.Add(akkord);
                }
            }
        }


        protected void ButtonBack_Click(object sender, EventArgs e)
        {
            Server.Transfer("FindSong.aspx");
        }

        protected void ButtonUp_Click(object sender, EventArgs e)
        {
            if (Session["sang"] != null)
            {
                Sang sang = (Sang)Session["sang"];
                foreach (Akkord ak in sang.listAkkorder)
                {
                    if (ak.keyNumber < 12)
                    {
                        ak.keyNumber = ak.keyNumber + 1;
                    }
                    else
                    {
                        ak.keyNumber = 1;
                    }
                }
                Session["sang"] = sang;
            }

        }

        protected void ButtonDown_Click(object sender, EventArgs e)
        {
            if (Session["sang"] != null)
            {
                Sang sang = (Sang)Session["sang"];
                foreach (Akkord ak in sang.listAkkorder)
                {
                    if (ak.keyNumber > 1)
                    {
                        ak.keyNumber = ak.keyNumber - 1;
                    }
                    else
                    {
                        ak.keyNumber = 12;
                    }
                }
                Session["sang"] = sang;
            }

        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void btn_UndoChord_Click(object sender, EventArgs e)
        {
            if (Session["Sang"] != null)
            {
                Sang sang = (Sang)Session["Sang"];
                if (sang.listAkkorder.Count > 0)
                {
                    sang.listAkkorder.RemoveAt(sang.listAkkorder.Count - 1);
                    Session["Sang"] = sang;
                }
            }

        }

        protected void btn_SaveSong_Click(object sender, EventArgs e)
        {

            if (CheckBox1.Checked == false)
            {
                TextBoxBesked.Visible = true;
                CheckBox1.Visible = true;
                return;
            }
            else
            {
                Sang sang;
                int userid = 0;
                if (Session["Sang"] != null)
                {
                    sang = (Sang)Session["Sang"];
                    userid = sang.userID;
                }
                else
                {
                    return;
                }              
                saveSong(sang, userid);
            }
        }
       
        protected void ButtonEditText_Click(object sender, EventArgs e)
        {
            if (Session["Sang"] != null)
            {
                Sang sang = (Sang)Session["Sang"];
                DropDownListText.Items.Clear();
                foreach(TextLinje textLinje in sang.listTexter)
                {
                    DropDownListText.Items.Add(textLinje.text+":"+textLinje.id);
                }
            }
        }

        protected void ButtonEditText0_Click(object sender, EventArgs e)
        {
            if (Session["Sang"] != null)
            {
                Sang sang = (Sang)Session["Sang"];
                sang.listTexter[DropDownListText.SelectedIndex].text = TextBoxEditText.Text;
            }
        }

        //COPI fra NewSongAddChords.aspx
        private void saveSong(Sang sang, int userid)
        { //COPI fra NewSongAddChords.aspx
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["wenningsdbConnectionString1"].ToString());
                con.Open();
                //------Gemmer Sangen-------------------------------------------
                SqlCommand cmd = new SqlCommand("insert into Sang  (titel, lyric, melody, date, userID, type) values(@titel,@lyric,@melody, @date, @userID,@type) SELECT SCOPE_IDENTITY();", con);

                cmd.Parameters.AddWithValue("@titel", sang.title);
                cmd.Parameters.AddWithValue("@lyric", sang.lyricWriter);
                cmd.Parameters.AddWithValue("@melody", sang.melodyWriter);
                cmd.Parameters.AddWithValue("@date", DateTime.Now.ToString("yyyy.MM.dd"));
                cmd.Parameters.AddWithValue("@userID", userid);
                cmd.Parameters.AddWithValue("@type", 0); //0=lukket, 1=åben for alle. Dvs, den skal godkendes først.

                object temp = cmd.ExecuteScalar();
                String aktueltID = temp.ToString();//holder ID som lige er blevet oprettet efter insert

                //------Gemmer alle text linjerne---------------------------------------------
                foreach (TextLinje txLinje in sang.listTexter)
                {
                    cmd = new SqlCommand("insert into SangTextLinjer  (sangID, text, posX, posY, linjeID) values(@sangID,@text,@posX,@posY,@linjeID) ;", con);
                    cmd.Parameters.AddWithValue("@sangID", aktueltID);
                    cmd.Parameters.AddWithValue("@text", txLinje.text);
                    cmd.Parameters.AddWithValue("@posX", txLinje.pos.X);
                    cmd.Parameters.AddWithValue("@posY", txLinje.pos.Y);
                    cmd.Parameters.AddWithValue("@linjeID", txLinje.id);

                    int svar = cmd.ExecuteNonQuery();
                }

                //------Gemmer alle Akkorder---------------------------------------------
                foreach (Akkord akkord in sang.listAkkorder)
                {
                    cmd = new SqlCommand("insert into SangAkkorder  (sangID, posX, posY, keyNumber, extraName, bass) values(@sangID, @posX, @posY, @keyNumber, @extraName, @bass) ;", con);
                    cmd.Parameters.AddWithValue("@sangID", aktueltID);
                    cmd.Parameters.AddWithValue("@posX", akkord.pos.X);
                    cmd.Parameters.AddWithValue("@posY", akkord.pos.Y);
                    cmd.Parameters.AddWithValue("@keyNumber", akkord.keyNumber);
                    cmd.Parameters.AddWithValue("@bass", akkord.bass);
                    if (akkord.extraName != null)
                        cmd.Parameters.AddWithValue("@extraName", akkord.extraName);
                    else
                        cmd.Parameters.AddWithValue("@extraName", "");

                    int svar = cmd.ExecuteNonQuery();
                }
                con.Close();
                try  //Behøves ikke fordi dette er ADMINISTRATOREN.
                { 
                }
                catch (Exception edsl)
                { }

                Server.Transfer("Default.aspx");
            }
            catch (Exception exs)
            {
                Label1.Text = "id: Error ";
                TextBoxBesked.Text = "ID: Error" + exs.ToString();
            }


        }

    }
}