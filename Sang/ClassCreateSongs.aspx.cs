﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sang
{
    public partial class ClassCreateSongs : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

    }
    class createSongs
    {
        public Sang createSheetSong(String idValgt)
        {
            //Response.Write("<script LANGUAGE='JavaScript' >alert('"+idValgt+"')</script>");
            //return;

            // String idValgt = GridView1.SelectedValue.ToString();
            Sang sang = new Sang();
            try
            {
                int id = Convert.ToInt32(idValgt);
                sang.ID = id;

            }
            catch (Exception lkjs) { }
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["wenningsdbConnectionString1"].ToString());
                con.Open();



                //------Henter Sangen-------------------------------------------
                SqlCommand cmd = new SqlCommand("Select * FROM  SangSheet WHERE Id=@sangID  ;", con);

                cmd.Parameters.AddWithValue("@sangID", idValgt);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {

                        sang.title = reader.GetString(1);
                        sang.lyricWriter = reader.GetString(2);
                        sang.melodyWriter = reader.GetString(3);
                        sang.type = reader.GetInt32(6);
                        sang.height = reader.GetInt32(7);
                        sang.width = reader.GetInt32(8);
                        sang.tempo = reader.GetInt32(9);
                    }
                }
                reader.Close();

                //------Henter textLinjer-------------------------------------------
                SqlCommand cmd2 = new SqlCommand("Select * FROM  SangSheetTextLinjer WHERE sangID=@sangID  ;", con);
                cmd2.Parameters.AddWithValue("@sangID", idValgt);
                SqlDataReader reader2 = cmd2.ExecuteReader();

                if (reader2.HasRows)
                {
                    while (reader2.Read())
                    {
                        TextLinje t = new TextLinje();
                        t.text = reader2.GetString(2);
                        t.pos = new Point(reader2.GetInt32(3), reader2.GetInt32(4));
                        t.id = reader2.GetInt32(5);
                        sang.listTexter.Add(t);
                    }
                    //foreach (TextLinje tl in sang.listTexter)
                    //{
                    //}

                }
                else
                {
                }
                reader2.Close();

                //------Henter Akkorder-------------------------------------------
                SqlCommand cmd3 = new SqlCommand("Select * FROM  SangSheetAkkorder WHERE sangID=@sangID  ;", con);
                cmd3.Parameters.AddWithValue("@sangID", idValgt);
                SqlDataReader reader3 = cmd3.ExecuteReader();

                if (reader3.HasRows)
                {
                    while (reader3.Read())
                    {
                        Akkord ak = new Akkord();
                        ak.pos = new Point(reader3.GetInt32(2), reader3.GetInt32(3));
                        ak.keyNumber = reader3.GetInt32(4);
                        int bassen = 0;
                        try
                        {
                            bassen = reader3.GetInt32(7);
                        }
                        catch (Exception els)
                        {

                        }

                        ak.bass = bassen;

                        String extra = reader3.GetString(5);
                        if (extra != null)
                            ak.extraName = extra;
                        else
                            ak.extraName = "";
                        //  ak.id = reader3.GetInt32(6);
                        sang.listAkkorder.Add(ak);
                    }
                }
                else
                {
                }
                reader3.Close();

                //------Henter Node Linjer-------------------------------------------
                SqlCommand cmd4 = new SqlCommand("Select * FROM  SangSheetNodeLinjer WHERE sangID=@sangID  ;", con);
                cmd4.Parameters.AddWithValue("@sangID", idValgt);
                SqlDataReader reader4 = cmd4.ExecuteReader();

                if (reader4.HasRows)
                {
                    while (reader4.Read())
                    {
                        NodeLinjer ak = new NodeLinjer(sang);
                        ak.ID = reader4.GetInt32(0);
                        ak.pos = new Point(reader4.GetInt32(2), reader4.GetInt32(3));
                        ak.key = reader4.GetInt32(4);
                        ak.length = reader4.GetInt32(5);
                        ak.maxNumberOfNodes = reader4.GetInt32(6);
                        sang.listNodeLinjer.Add(ak);
                    }
                    //    Response.Write("<script LANGUAGE='JavaScript' >alert('"+sang.listNodeLinjer[0].ID +"')</script>");
                }
                else
                {
                }
                reader4.Close();
                //---Henter Noder som sættes ind i nodeLinjerne-----------------------------------
                foreach (NodeLinjer nl in sang.listNodeLinjer)
                {
                    SqlCommand cmd5 = new SqlCommand("Select * FROM  SangSheetNoder WHERE NodeLinjeID=@ID  ;", con);
                    cmd5.Parameters.AddWithValue("@ID", nl.ID);
                    SqlDataReader reader5 = cmd5.ExecuteReader();

                    if (reader5.HasRows)
                    {
                        //    ListBox1.Items.Add("Node Linjer");
                        while (reader5.Read())
                        {
                            Noder no = new Noder();
                            no.id = reader5.GetInt32(0);
                            no.pos = new Point(reader5.GetInt32(2), reader5.GetInt32(3));
                            no.keyNumber = reader5.GetInt32(4);
                            no.StafNumber = reader5.GetInt32(5);
                            no.length = reader5.GetInt32(6);
                            no.sequenceNummer = reader5.GetInt32(7);
                            no.sharpOrFlat = reader5.GetInt32(8);
                            no.type = reader5.GetInt32(9);
                            no.parrentNodeLinje = nl;
                            nl.listNoder.Add(no);
                        }

                    }
                    else
                    {
                    }
                    reader5.Close();
                }
                //    Response.Write("<script LANGUAGE='JavaScript' >alert('"+sang.listNodeLinjer[0].listNoder.Count +"')</script>");

                foreach (NodeLinjer nl in sang.listNodeLinjer)
                {
                    nl.callibrer();
                }
                //------Alt gemmes tilbage til Sang-----------------------------------------------
                //Session["Sang"] = sang;

                //Server.Transfer("ShowSheetSong.aspx");
                ////  Server.Transfer("ShowSong.aspx");

            }
            catch (Exception lkjd)
            {

            }
            return sang;
        }
        public Sang createSong(String idValgt)
        {
            // String idValgt = GridView1.SelectedValue.ToString();
            Sang sang = new Sang();
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["wenningsdbConnectionString1"].ToString());
                con.Open();



                //------Henter Sangen-------------------------------------------
                SqlCommand cmd = new SqlCommand("Select * FROM  Sang WHERE Id=@sangID  ;", con);

                cmd.Parameters.AddWithValue("@sangID", idValgt);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {

                        sang.title = reader.GetString(1);
                        sang.lyricWriter = reader.GetString(2);
                        sang.melodyWriter = reader.GetString(3);
                    }
                }
                reader.Close();

                //------Henter textLinjer-------------------------------------------
                SqlCommand cmd2 = new SqlCommand("Select * FROM  SangTextLinjer WHERE sangID=@sangID  ;", con);
                cmd2.Parameters.AddWithValue("@sangID", idValgt);
                SqlDataReader reader2 = cmd2.ExecuteReader();

                if (reader2.HasRows)
                {
                    while (reader2.Read())
                    {
                        TextLinje t = new TextLinje();
                        t.text = reader2.GetString(2);
                        t.pos = new Point(reader2.GetInt32(3), reader2.GetInt32(4));
                        t.id = reader2.GetInt32(5);
                        sang.listTexter.Add(t);
                    }
                }
                else
                {
                }
                reader2.Close();

                //------Henter Akkorder-------------------------------------------
                SqlCommand cmd3 = new SqlCommand("Select * FROM  SangAkkorder WHERE sangID=@sangID  ;", con);
                cmd3.Parameters.AddWithValue("@sangID", idValgt);
                SqlDataReader reader3 = cmd3.ExecuteReader();

                if (reader3.HasRows)
                {
                    while (reader3.Read())
                    {
                        Akkord ak = new Akkord();
                        ak.pos = new Point(reader3.GetInt32(2), reader3.GetInt32(3));
                        ak.keyNumber = reader3.GetInt32(4);
                        int bassen = 0;
                        try
                        {
                            bassen = reader3.GetInt32(7);
                        }
                        catch (Exception els)
                        {

                        }

                        ak.bass = bassen;


                        String extra = reader3.GetString(5);
                        if (extra != null)
                            ak.extraName = extra;
                        else
                            ak.extraName = "";
                        //  ak.id = reader3.GetInt32(6);
                        sang.listAkkorder.Add(ak);
                    }
                }
                else
                {
                }
                reader3.Close();
            }
            catch (Exception lkjd)
            {

            }
            return sang;

        }
    }
        class sendMails
        {
            public void sendMail(String besked)
            {
                SendMailMessage("localhost", "info@wenningstedt.dk", "New Song created", "jakupwhansen@gmail.com", "Jákup", "New Song ", besked, true);
            }
            private static bool SendMailMessage(string SMTPServer, string fromAddress, string fromName, string toAddress, string toName, string msgSubject, string msgBody, bool IsBodyHtml)
            {
                // Use the new v2.0 mail class to send an E-mail.

                try
                {
                    SmtpClient client = new SmtpClient(SMTPServer);
                    MailAddress from = new MailAddress(fromAddress, fromName);
                    MailAddress to = new MailAddress(toAddress, toName);
                    MailMessage message = new MailMessage(from, to);
                    message.BodyEncoding = System.Text.Encoding.GetEncoding("iso-8859-1");
                    message.Subject = msgSubject;
                    message.IsBodyHtml = IsBodyHtml;
                    message.Body = msgBody;
                    client.Send(message);
                }
                catch (System.Net.Mail.SmtpException)
                {
                    throw;
                }
                catch (Exception)
                {
                    throw;
                }
                return true;
            }
        }
    
    
}