﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShowSheetSongCanvas.aspx.cs" Inherits="Sang.ShowSheetSongCanvas" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Show Sheet Song Canvas</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- shim -->
    <script src="/MIDI.js-master/inc/shim/Base64.js" type="text/javascript"></script>
    <script src="/MIDI.js-master/inc/shim/Base64binary.js" type="text/javascript"></script>
    <script src="/MIDI.js-master/inc/shim/WebAudioAPI.js" type="text/javascript"></script>
    <script src="/MIDI.js-master/inc/shim/WebMIDIAPI.js" type="text/javascript"></script>
    <!-- jasmid package -->
    <script src="/MIDI.js-master/inc/jasmid/stream.js"></script>
    <script src="/MIDI.js-master/inc/jasmid/midifile.js"></script>
    <script src="/MIDI.js-master/inc/jasmid/replayer.js"></script>
    <!-- midi.js package -->
    <script src="/MIDI.js-master/js/midi/audioDetect.js" type="text/javascript"></script>
    <script src="/MIDI.js-master/js/midi/gm.js" type="text/javascript"></script>
    <script src="/MIDI.js-master/js/midi/loader.js" type="text/javascript"></script>
    <script src="/MIDI.js-master/js/midi/plugin.audiotag.js" type="text/javascript"></script>
    <script src="/MIDI.js-master/js/midi/plugin.webaudio.js" type="text/javascript"></script>
    <script src="/MIDI.js-master/js/midi/plugin.webmidi.js" type="text/javascript"></script>
    <script src="/MIDI.js-master/js/midi/player.js" type="text/javascript"></script>
    <script src="/MIDI.js-master/js/midi/synesthesia.js" type="text/javascript"></script>
    <!-- utils -->
    <script src="/MIDI.js-master/js/util/dom_request_xhr.js" type="text/javascript"></script>
    <script src="/MIDI.js-master/js/util/dom_request_script.js" type="text/javascript"></script>
    <!-- includes -->
    <script src="/MIDI.js-master/examples/inc/timer.js" type="text/javascript"></script>
    <script src="/MIDI.js-master/examples/inc/colorspace.js" type="text/javascript"></script>
    <script src="/MIDI.js-master/examples/inc/event.js" type="text/javascript"></script>
    

    <!-- Før Canvas -->
    <link rel="stylesheet" href="/bootstrap-3.3.5-dist/css/bootstrap.min.css" />
    <script src="/jquery/jquery-1.11.3.min.js"></script>
    <script src="/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>

    <link rel="stylesheet" type="text/css" href="/css/StyleSheet1.css" />

    <style type="text/css">
        .my-style {
            vertical-align: top;
        }


        #nav {
            width: 247px;
        }

        .auto-style11 {
            width: 71px;
        }

        .jakupImgsize {
            height: 50px;
            width: 50px;
        }

        .auto-style12 {
            width: 81px;
        }

        .auto-style13 {
            width: 91px;
        }
    </style>
</head>
<body onload="getMidiPlayer()">


    <form id="form1" runat="server">
        <div>

            <div class="container-fluid">
                <div>

                    <br />

                    <asp:Button CssClass="btn-default" ID="ButtonBack" runat="server" Text="Back" OnClick="ButtonBack_Click" />

                    <asp:Button CssClass="btn-default" ID="PlaySong" runat="server" Text="Load Animation" OnClick="PlaySong_Click" Visible="True" />

                    <asp:Label ID="LabelBeskedOmBrowsere" runat="server" Text="Label" Visible="False"></asp:Label>

                    <br />
                    <br />
                    <asp:Panel ID="PanelPlay" runat="server" Style="width: 100%; max-width: 600px" Visible="False" Height="60px">

                        <!--  <span class="auto-style5">
                            <a id="LinkPlayMidi" class="auto-style7" href="#" onclick="MIDIjs.play('midi/sang.mid');"><strong>Play(Midi)</strong>
                            </a>
                             </span> -->

                        <table style="width: 99%; height: 33px;">
                            <tr>
                                <td class="auto-style11">
                                    <asp:CheckBox ID="CheckBoxChordsIncluded" runat="server" AutoPostBack="True" Font-Size="8pt" OnCheckedChanged="CheckBoxChordsIncluded_CheckedChanged" Text="Chords" />
                                </td>
                                <td class="auto-style12">
                                    <asp:CheckBox ID="CheckBoxShowNodeName" runat="server" AutoPostBack="True" Font-Size="8pt" OnCheckedChanged="CheckBoxShowNodeName_CheckedChanged" Text="Name" />

                                </td>

                                <td class="auto-style13">
                                    <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" Font-Size="8pt" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>

                                <td>
                                    <a href="#" onclick="spilEnlyd()">
                                        <img class="jakupImgsize" id="trykMidiAnimation" alt="" src="/images/processing.gif" width="30" height="30" />
                                    </a>
                                    <input id="CheckboxRepeater" type="checkbox" />
                                    Repeat</td>

                            </tr>
                        </table>

                    </asp:Panel>

                    <!--    <img src="BitMapNewSheetSongShow.aspx" alt="song" style="width: 100%; max-width: 600px; visibility:hidden;" /><br />
               -->
                    <canvas id="canvas" style="width: 100%; max-width: 600px"></canvas>

                    <br />

                    <br />

                </div>
                <br />


                <div>
                </div>

            </div>

        </div>
    </form>

    <script>
        //---Variabler --------------------------
        var t = 1;
        var arrayLength;
        var arrayX;
        var arrayY;
        var arrayHeight;
        var nodeFundet = false;
        var ctx;
        var player;
        var tilstandIsPlaying = "stoped";
        var canvas;
        var running = true;
        //---Opsætning-----------------------------
        canvas = document.getElementById("canvas");
        ctx = canvas.getContext("2d");
        canvas.hidden = false;
        canvas.width = 400;
        canvas.height = 300;
        var background = new Image();
        background.src = "BitMapNewSheetSongShow.aspx";
        background.onload = function () {
            canvas.width = background.width;
            canvas.height = background.height;
            ctx.drawImage(background, 0, 0);
            var a = "<%=Session["CanvasNodeLength"]%>";
            arrayLength = a.split(":");
            var aX = "<%=Session["CanvasNodeX"]%>";
            arrayX = aX.split(":");
            var aY = "<%=Session["CanvasNodeY"]%>";
            arrayY = aY.split(":");
            var aH = "<%=Session["samletNodeHeight"]%>";
            arrayHeight = aH.split(":");
        }
        //------------------------------------------

        function spilEnlyd() {
            if (tilstandIsPlaying == "resume") {
                tilstandIsPlaying = "playing";
                var img = document.getElementById("trykMidiAnimation");
                img.width = 30;
                img.height = 30;
                img.src = "/images/playerPause.png";
                player.resume();
            }
            else if (tilstandIsPlaying == "playing") {
                tilstandIsPlaying = "resume";
                var img = document.getElementById("trykMidiAnimation");
                img.width = 30;
                img.height = 30;
                img.src = "/images/playerResume.png";

                player.pause();
            }
            else if (tilstandIsPlaying == "stoped") {
                tilstandIsPlaying = "playing";
                t = 1;
                var img = document.getElementById("trykMidiAnimation");
                img.width = 30;
                img.height = 30;
                img.src = "/images/playerPause.png";
                player.start();
            }
            else if (tilstandIsPlaying == "atTheEnd") {
                tilstandIsPlaying = "stoped";
                var img = document.getElementById("trykMidiAnimation");
                img.width = 30;
                img.height = 30;
                img.src = "/images/playerStart.png";
                t = 1;
                player.stop();
                running = true;
            }

        }
        function getMidiPlayer() {
            var canvas = document.getElementById("canvas");
            canvas.hidden = false;

            var img = document.getElementById("trykMidiAnimation");
            img.width = 30;
            img.height = 30;
            img.src = "/images/processing.gif";
            t = 1;

            MIDI.loadPlugin({
                soundfontUrl: "/MIDI.js-master/examples/soundfont/",
                Instrument: "Acoustic_grand_piano",
                onprogress: function (state, progress) {
                },
                onsuccess: function () {
                    var img = document.getElementById("trykMidiAnimation");
                    img.width = 30;
                    img.height = 30;
                    img.src = "/images/PlayerStart.png";

                    //Hvis man ikke sætter disse op, vil man KUN høre 1.setemmmen.
                    MIDI.setInstrument(0, 0, 1.0);
                    MIDI.setInstrument(1, 0, 1.0);
                    MIDI.setInstrument(2, 0, 1.0);
                    MIDI.setInstrument(3, 0, 1.0);
                    MIDI.setInstrument(4, 0, 1.0);
                    MIDI.setInstrument(5, 0, 1.0);

                    player = MIDI.Player;

                    //--TEMPO-------
                    //Collaps Ctrl+M+H
                    var tv = "<%=Session["TempoValgt"]%>";
                    if (tv == 0) {
                        player.BPM = 110;
                    }
                    else if (tv == 1) {
                        player.BPM = 140;
                    }
                    else if (tv == 2) {
                        player.BPM = 170;
                    }
                    else if (tv == 3) {
                        player.BPM = 200;
                    }
                    else if (tv == 4) {
                        player.BPM = 230;
                    }
                    else if (tv == 5) {
                        player.BPM = 260;
                    }
                    else if (tv == 6) {
                        player.BPM = 290;
                    }
                    else if (tv == 7) {
                        player.BPM = 320;
                    }
                    else if (tv == 8) {
                        player.BPM = 350;
                    }
                    else if (tv == 9) {
                        player.BPM = 380;
                    }
                    else if (tv == 10) {
                        player.BPM = 410;
                    }
                    else if (tv == 11) {
                        player.BPM = 440;
                    }
                    else if (tv == 12) {
                        player.BPM = 470;
                    }
                    else if (tv == 13) {
                        player.BPM = 500;
                    }
                    else if (tv == 14) {
                        player.BPM = 530;
                    }
                    else if (tv == 15) {
                        player.BPM = 560;
                    }
                    else if (tv == 16) {
                        player.BPM = 590;
                    }
                    else if (tv == 17) {
                        player.BPM = 620;
                    }
                    else if (tv == 18) {
                        player.BPM = 650;
                    }

                    player.loadFile('midi/sang.mid', startSpil);
                    player.addListener(function (data) {
                        if (data.message === 144) { // 128 is noteOff, 144 is noteOn
                            if (data.channel == 0) //1.stemme.
                            {
                                drawIndividuelNote(data.note);
                            }
                        }
                        else {
                            if (nodeFundet == true) {
                                nodeFundet = false;
                                t++;
                            }
                        }

                        var now = data.now; // where we are now
                        var end = data.end; // time when song ends
                        if (now >= end && tilstandIsPlaying == "playing") {
                            tilstandIsPlaying = "atTheEnd";
                            player.stop();
                            var img = document.getElementById("trykMidiAnimation");
                            img.width = 30;
                            img.height = 30;
                            img.src = "/images/playerEnded.png";
                        }
                        //----REPEAT---2016.07.12------------------------------------
                        if (running == true && tilstandIsPlaying == "atTheEnd")
                        {
                            var check = document.getElementById("CheckboxRepeater");
                            if (check.checked)
                            {
                                running = false;
                                //---2016.07.28---setTimeout ind, ellers blev repeat noget rod----
                                setTimeout(function () { spilEnlyd(); }, 1000);
                                setTimeout(function () { spilEnlyd(); }, 2000);

                            }

                        }
                    });
                }
            });

            function startSpil() {
                //------På grund af iPad, skal lyden aktiveres med KLICK, derfor lukker jeg denne her player.start.-----
                // player.start();
                //  spilEnlyd(); //Starter automatisk når spiller er loadet.
            }
        }


        function drawIndividuelNote(nr) {
            ctx.fillStyle = "#FF0000";
            ctx.drawImage(background, 0, 0);
            var a = parseInt(nr);
            var b = parseInt(arrayHeight[t]);

            if (a == b && nodeFundet == false) {
                nodeFundet = true;

                var x = parseInt(arrayX[t]) + 20;
                var y = parseInt(arrayY[t]) + 10;

                ctx.beginPath();
                ctx.arc(x, y, 20, 0, 2 * Math.PI, false);
                ctx.strokeStyle = "red";
                ctx.fill()
                ctx.stroke();
            }
            else {
            }
        }


    </script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-54970963-3', 'auto');
        ga('send', 'pageview');

    </script>


</body>
</html>
