﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Sang.Composers
{
    public partial class Hojgaard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["wenningsdbConnectionString1"].ToString());
                con.Open();

                //------------HENTE SANG START------------------------------------------------
                //------------HENTE SANG START------------------------------------------------
                //------------HENTE SANG START------------------------------------------------
                SqlCommand cmd;
                cmd = new SqlCommand("SELECT * FROM SangSheet WHERE userID=31 AND type=1 ORDER BY ID ASC;", con);

                SqlDataReader reader = cmd.ExecuteReader();
                List<SangHojgaard> listSang = new List<SangHojgaard>();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        //    Response.Write("<script LANGUAGE='JavaScript' >alert('" + "listBrugere.Count" + "')</script>");
                        //   ListBox1.Items.Add("Song har item");
                        Bruger bruger = new Bruger();

                        //---titel-------------------   
                        String titel = "ingen titel";
                        if (!reader.IsDBNull(1))
                        {
                            titel = reader.GetString(1);
                        }
                        //---------------------------
                        //---type-------------------   
                        int type = 0;
                        if (!reader.IsDBNull(6))
                        {
                            type = reader.GetInt32(6);
                        }
                        //---------------------------
                        //---ID-------------------   
                        int id = 0;
                        if (!reader.IsDBNull(0))
                        {
                            id = reader.GetInt32(0);
                        }
                        //---------------------------
                        SangHojgaard sangValg = new SangHojgaard();
                        titel = titel.TrimStart(' ');
                        sangValg.title = titel;
                        sangValg.type = type;
                        sangValg.SangID = id;
                    //    sangValg.direktLink = "http://theSongApp.com/FindSheetSong.aspx?Sheet=" + id + "_" + titel.Length * id;
                        sangValg.direktLink = "../FindSheetSong.aspx?Sheet=" + id + "_" + (titel.Length * id);

                        Button button = new Button();
                        button.Click += new EventHandler(sangValg.eventMetode);
                        button.Font.Bold = false;
                        button.Font.Italic = true;
                        sangValg.knap = button;

                        //-------Add to listen---------------------
                        listSang.Add(sangValg);
                    }
                }
                fyldTabelMedKnapper(listSang);
                con.Close();
            }
            catch (Exception kj) { }
         

        }
        private void fyldTabelMedKnapper(List<SangHojgaard> listS)
        {
            foreach(SangHojgaard sa in listS)
            {
                
                TableRow tRow = new TableRow();
                Table1.Rows.Add(tRow);

                // Create a new cell and add it to the row.
                TableCell tCell = new TableCell();
                sa.knap.Text = sa.title;
                sa.knap.Width = 300;
                sa.knap.Height = 40;
                sa.knap.Font.Size = 14;             
                sa.knap.CssClass = "btn-default";
                tCell.Controls.Add(sa.knap);

                tRow.Cells.Add(tCell);
            }

        }

        protected void ButtonBack_Click(object sender, EventArgs e)
        {
            if (Session["Bruger"] != null)
            {
                Bruger bruger = (Bruger)Session["Bruger"];
                if (bruger.ID == -1)
                {
                    //-1 betyder direkte link...Så skal skal slettes så vi ikke kan går til andre sider,
                    //Denne her bruger skal KUN se sheet song og ikke andet.
                    // fordi denne bruger eksisterer ikke rigtig.
                    //       Session["Bruger"] = null;
                }
            }

            if (Session["KommerFra"] != null)
            {
                List<String> listeKommerFra = (List<String>)Session["KommerFra"];
                String tilbageTil = "Default.aspx";
                if (listeKommerFra.Count > 0)
                {
                    tilbageTil = listeKommerFra[listeKommerFra.Count - 1];
                    listeKommerFra.RemoveAt(listeKommerFra.Count - 1);
                }
                Session["KommerFra"] = listeKommerFra;

                Server.Transfer("../"+ tilbageTil);
            }
            else
            {
                Server.Transfer("Default.aspx");
            }
        }

}
    class SangHojgaard : IComparable
    {
        public Button knap;
        public String title;
        public String direktLink;
        public int SangID = 0;
        public int type = 0; //Tilstand.
        public bool sheetSong = false;
        public int CompareTo(object obj)
        {
            SangHojgaard shtv = (SangHojgaard)obj;
            return title.CompareTo(shtv.title);
        }


        public void eventMetode(object sender, EventArgs e)
        {
            //Session KommerFra kan ikke bruges her, fordi
            // direkte link til sheet sang vil altid slette linket, fordi
            // disse skal ikke kunne bevæge sig forskellige steder, men kun se
            // en sheet og så bare tilbage til Default om de prøver at sige tilbage
            // som ikke vil give mening, når man kommer direkte ind, der vil ikke
            // være noget tilbage i dette tilfælde...Og her bruger vi også direkte link så
            // vi kan desværre ikke huske hvor vi kommer fra.
            //----------Kommer fra------------------------------         
            List<String> listeKommerFra = (List<String>)HttpContext.Current.Session["KommerFra"];
            //2017.09.01 fordi man ikke kunne komme direkt ind (her er det sangbog)
            if (listeKommerFra == null) { listeKommerFra = new List<String>(); }
            listeKommerFra.Add("/Composers/Hojgaard.aspx");
            HttpContext.Current.Session["KommerFra"] = listeKommerFra;
            //-----------------------------------------------------

            HttpContext.Current.Response.Redirect(direktLink);
        }
    }

}
