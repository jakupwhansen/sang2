﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sang
{
    public partial class PlayMidi : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            createSong();
        }
        MIDISong song;
        static int[] m_nBeats = { 72, 112, 150 };

        private void createSong()
        {
            song = new MIDISong();
            song.AddTrack("track1");            
            song.SetTimeSignature(0, 4, 4);
            song.SetTempo(0, m_nBeats[1]);
            song.SetChannelInstrument(0, 0, 0);

            song.AddTrack("track2");
            song.SetTimeSignature(1, 4, 4);
            song.SetTempo(1, m_nBeats[1]);
            song.SetChannelInstrument(1, 1, 0);
            Random rnd = new Random();
            for (int i = 0; i < 4 * 16; i++)
            {
                int noteNumber = 0;
                do
                {
                    noteNumber = rnd.Next() & 15;
                }
                while (noteNumber > 11);
                noteNumber += (12 * 6);
                int noteTypeNumber = (int)(12 * Math.Pow(2, 0 - (4 - 4)));
                //	song.AddNote(0, 0, noteNumber, noteTypeNumber);
            }

            if (Session["Sang"] != null)
            {
                Sang sang = (Sang)Session["Sang"];
                foreach (Noder no in sang.listNodeLinjer[0].listNoder)
                {
                    if (no.keyNumber > 0 && no.keyNumber < 50 && no.length > 0 && no.length < 100)
                    { //Almindelige noder
                        int nodeLengte = (100 / 16 )* no.length;
                       

                        song.AddNote(0, 0, 23 + 12 + no.keyNumber, nodeLengte);
                    }
                    else if (no.length > 100 && no.length < 200 )
                    {  //Pauser = -1 . Pauser starter med 100, derfor trækkse 100 bare fra.
                        int nodeLengte = (100 / 16) * (no.length-100);
                        song.AddNote(0, 0, -1, nodeLengte);
                    }
                }

               


                //       song.AddNote(1, 0, tone, 80);


                try
                {
                    MemoryStream ms = new MemoryStream();
                    song.Save(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    byte[] src = ms.GetBuffer();
                    byte[] dst = new byte[src.Length];
                    for (int i = 0; i < src.Length; i++)
                    {
                        dst[i] = src[i];
                    }
                    ms.Close();

                    //string strTempFileName = Path.ChangeExtension(Path.GetTempFileName(), ".mid");
                    //FileStream objWriter = File.Create(strTempFileName);
                    string strTempFileName = Path.ChangeExtension("midi/sang", ".mid");
                    FileStream objWriter = File.Create(Server.MapPath("~/midi/sang.mid"));

                    objWriter.Write(dst, 0, dst.Length);
                    objWriter.Close();
                    objWriter.Dispose();
                    objWriter = null;

                    //using (StreamWriter _testData = new StreamWriter(Server.MapPath("~/sound/sang.mid"), true))
                    //{
                    //    _testData.WriteLine(dst); // Write the file.
                    //}   

                    //axMediaPlayer.Ctlcontrols.stop();
                    //axMediaPlayer.URL = strTempFileName;
                    //axMediaPlayer.Ctlcontrols.play();
                }
                catch { }
            }

        }

 

    }
}