﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sang
{
    public partial class NewSongAddChords : System.Web.UI.Page
    {


        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["Bruger"] == null)
            {
                Server.Transfer("Default.aspx");
            }
            else
            {
                //-----------DIREKTE LINK----------------------------------------
                Bruger brug = (Bruger)Session["Bruger"];
                if (brug.ID == -1) //Så kommer man fra direkte link. Må stoppes.
                {
                    Session["Bruger"] = null;
                    return;
                }
                //------------------------------------------------------------------
            }
            if (!IsPostBack)
            {

                Dictionary<int, String> listAkkorder = GeneralData.getListAkkorder();


                DropDownList1.DataSource = listAkkorder;
                DropDownList1.DataTextField = "Value";
                DropDownList1.DataValueField = "Key";
                DropDownList1.DataBind();

                DropDownList2.DataSource = listAkkorder;
                DropDownList2.DataTextField = "Value";
                DropDownList2.DataValueField = "Key";
                DropDownList2.DataBind();

            }
            DropDownList1.Focus();
        }
        protected void xymetoden(Object sender, ImageClickEventArgs e)
        {
            int yDistanceCollision = 20;
            if(e.X == 0 && e.Y == 0)
            {  //Denne sker når man har bruge TextBoxExtra og trykker Enter, så skal intet tegnes.
                return;
            }
            if (Session["Sang"] != null)
            {
                Sang sang = (Sang)Session["Sang"];

                if (Session["EditChord"] != null) //EDIT af akkord
                {
                    if (Session["EditChordSave"] == null)
                    {
                        Akkord ak = sang.chordCollision(new Point(e.X, e.Y), 50, 50);
                        Session["EditChordSave"] = ak;
                    }
                    else //Så skal den ændres.
                    {
                        if (DropDownList1.SelectedIndex > 0)
                        {
                            Akkord ak = (Akkord)Session["EditChordSave"];
                            ak.editIgang = false;

                            //Skal overskrive alt
                            ak.pos = new Point(e.X, e.Y);
                            ak.keyNumber = Convert.ToInt32(DropDownList1.SelectedValue);
                            ak.bass = Convert.ToInt32(DropDownList2.SelectedValue);
                            ak.extraName = TextBox1.Text;
                            if (!CheckBoxChord.Checked)
                            {
                                DropDownList1.SelectedIndex = 0;
                            }
                            if (!CheckBoxBass.Checked)
                            {
                                DropDownList2.SelectedIndex = 0;
                            }
                            if (TextBox1.Text.Length > 0)
                            {
                                if (!CheckBoxExtra.Checked)
                                {
                                    TextBox1.Text = "";
                                }
                            }
                            Session["EditChordSave"] = null;
                        }
                    }
                }
                else //Lave NY Akkord.
                {
                    if (DropDownList1.SelectedIndex > 0)
                    {
                        Akkord akkord = new Akkord();
                        if (CheckBoxSameHeight.Checked)
                        {
                            if (sang.listAkkorder.Count > 0)
                            {
                                //Så kikker vi igennem listen for at se om der er et nivo vi skal bruge
                                int yFundet = -1;
                                foreach (Akkord ak in sang.listAkkorder)
                                {
                                    if ((ak.pos.Y + yDistanceCollision) > e.Y && (ak.pos.Y - yDistanceCollision) < e.Y)
                                    {
                                        yFundet = ak.pos.Y;
                                        break;
                                    }
                                }
                                if (yFundet == -1)
                                {
                                    akkord.pos = new Point(e.X, e.Y);
                                }
                                else
                                {
                                    akkord.pos = new Point(e.X, yFundet);
                                }
                            }
                            else
                            {
                                akkord.pos = new Point(e.X, e.Y);
                            }


                        }
                        else
                        {
                            //---IKKE samme højde, sætte ny akkord ind.
                            akkord.pos = new Point(e.X, e.Y);
                        }


                        akkord.id = sang.listAkkorder.Count;

                        //  akkord.name = DropDownList1.SelectedItem.Text;
                        akkord.keyNumber = Convert.ToInt32(DropDownList1.SelectedValue);
                        akkord.bass = Convert.ToInt32(DropDownList2.SelectedValue);
                        if (!CheckBoxChord.Checked)
                        {
                            DropDownList1.SelectedIndex = 0;
                        }
                        if (!CheckBoxBass.Checked)
                        {
                            DropDownList2.SelectedIndex = 0;
                        }
                        if (TextBox1.Text.Length > 0)
                        {
                            akkord.extraName = TextBox1.Text;
                            if (!CheckBoxExtra.Checked)
                            {
                                TextBox1.Text = "";
                            }
                        }
                        akkord.id = sang.listAkkorder.Count + 1;
                        sang.listAkkorder.Add(akkord);
                    }
                }
            }
        }
        private void saveSong(Sang sang, int userid)
        {
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["wenningsdbConnectionString1"].ToString());
                con.Open();
                //------Gemmer Sangen-------------------------------------------
                SqlCommand cmd = new SqlCommand("insert into Sang  (titel, lyric, melody, date, userID, type) values(@titel,@lyric,@melody, @date, @userID,@type) SELECT SCOPE_IDENTITY();", con);

                cmd.Parameters.AddWithValue("@titel", sang.title);
                cmd.Parameters.AddWithValue("@lyric", sang.lyricWriter);
                cmd.Parameters.AddWithValue("@melody", sang.melodyWriter);
                cmd.Parameters.AddWithValue("@date", DateTime.Now.ToString("yyyy.MM.dd"));
                cmd.Parameters.AddWithValue("@userID", userid);
                cmd.Parameters.AddWithValue("@type", 0); //0=lukket, 1=åben for alle. Dvs, den skal godkendes først.

                object temp = cmd.ExecuteScalar();
                String aktueltID = temp.ToString();//holder ID som lige er blevet oprettet efter insert

                //------Gemmer alle text linjerne---------------------------------------------
                foreach (TextLinje txLinje in sang.listTexter)
                {
                    cmd = new SqlCommand("insert into SangTextLinjer  (sangID, text, posX, posY, linjeID) values(@sangID,@text,@posX,@posY,@linjeID) ;", con);
                    cmd.Parameters.AddWithValue("@sangID", aktueltID);
                    cmd.Parameters.AddWithValue("@text", txLinje.text);
                    cmd.Parameters.AddWithValue("@posX", txLinje.pos.X);
                    cmd.Parameters.AddWithValue("@posY", txLinje.pos.Y);
                    cmd.Parameters.AddWithValue("@linjeID", txLinje.id);

                    int svar = cmd.ExecuteNonQuery();
                }

                //------Gemmer alle Akkorder---------------------------------------------
                foreach (Akkord akkord in sang.listAkkorder)
                {
                    cmd = new SqlCommand("insert into SangAkkorder  (sangID, posX, posY, keyNumber, extraName, bass) values(@sangID, @posX, @posY, @keyNumber, @extraName, @bass) ;", con);
                    cmd.Parameters.AddWithValue("@sangID", aktueltID);
                    cmd.Parameters.AddWithValue("@posX", akkord.pos.X);
                    cmd.Parameters.AddWithValue("@posY", akkord.pos.Y);
                    cmd.Parameters.AddWithValue("@keyNumber", akkord.keyNumber);
                    cmd.Parameters.AddWithValue("@bass", akkord.bass);
                    if (akkord.extraName != null)
                        cmd.Parameters.AddWithValue("@extraName", akkord.extraName);
                    else
                        cmd.Parameters.AddWithValue("@extraName", "");

                    int svar = cmd.ExecuteNonQuery();
                }
                con.Close();
                try
                { //Try catch, fordi når man tester lokalt, kan den ikke send mail, kun public udgaven, og så kører programmet videre uden at crashe.
                 
                    // sendMail("New Song Created by userID: " + userid + " with the title: " + sang.title);
                     sendMails sm = new sendMails();
                     sm.sendMail("New Song Created by userID: " + userid + " with the title: " + sang.title);
                 
                }
                catch (Exception edsl)
                { }

                Server.Transfer("Default.aspx");
            }
            catch (Exception exs)
            {
                Label1.Text = "id: Error ";
                TextBoxBesked.Text = "ID: Error" + exs.ToString();
            }


        }

        protected void btn_SaveSong_Click(object sender, EventArgs e)
        {
            if (CheckBox1.Checked == false)
            {
                TextBoxBesked.Visible = true;
                CheckBox1.Visible = true;
                return;
            }
            else
            {
                Sang sang;
                if (Session["Sang"] != null)
                {
                    sang = (Sang)Session["Sang"];
                }
                else
                {
                    return;
                }
                int userid = 0;
                if (Session["Bruger"] != null)
                {
                    Bruger bruger = (Bruger)Session["Bruger"];
                    userid = bruger.ID;
                }
                saveSong(sang, userid);               
            }
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //TextBoxBesked.Visible = true;
            //TextBoxBesked.Text = DropDownList1.SelectedIndex.ToString();
        }

        protected void btn_UndoChord_Click(object sender, EventArgs e)
        {
            if (Session["Sang"] != null)
            {
                Sang sang = (Sang)Session["Sang"];
                if (sang.listAkkorder.Count > 0)
                {
                    sang.listAkkorder.RemoveAt(sang.listAkkorder.Count - 1);
                    Session["Sang"] = sang;
                }
            }
        }
        public void sendMail(String besked)
        {
            SendMailMessage("localhost", "info@wenningstedt.dk", "New Song created", "jakupwhansen@gmail.com", "Jákup", "New Song ", besked, true);
        }
        public static bool SendMailMessage(string SMTPServer, string fromAddress, string fromName, string toAddress, string toName, string msgSubject, string msgBody, bool IsBodyHtml)
        {
            // Use the new v2.0 mail class to send an E-mail.

            try
            {
                SmtpClient client = new SmtpClient(SMTPServer);
                MailAddress from = new MailAddress(fromAddress, fromName);
                MailAddress to = new MailAddress(toAddress, toName);
                MailMessage message = new MailMessage(from, to);
                message.BodyEncoding = System.Text.Encoding.GetEncoding("iso-8859-1");
                message.Subject = msgSubject;
                message.IsBodyHtml = IsBodyHtml;
                message.Body = msgBody;
                client.Send(message);
            }
            catch (System.Net.Mail.SmtpException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }

        protected void btn_Edit_Click(object sender, EventArgs e)
        {
            if (Session["EditChord"] == null)
            {
                Session["EditChord"] = true;
                btn_Edit.Text = "ON";
                DropDownList1.SelectedIndex = 0;
            }
            else
            {
                Session["EditChord"] = null;
                btn_Edit.Text = "Edit";
                DropDownList1.SelectedIndex = 0;
            }

        }

        protected void btn_Height_Click(object sender, EventArgs e)
        {

        }

        protected void CheckBoxSameHeight_CheckedChanged(object sender, EventArgs e)
        {
            DropDownList1.Focus();
        }

        protected void CheckBoxChord_CheckedChanged(object sender, EventArgs e)
        {
            DropDownList1.Focus();
        }

        protected void CheckBoxBass_CheckedChanged(object sender, EventArgs e)
        {
            DropDownList2.Focus();
        }





    }
}