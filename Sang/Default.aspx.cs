﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sang
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           
           
            if (!IsPostBack)
            {
            }
            if (Session["Bruger"] != null)
            {
                Bruger brug = (Bruger)Session["Bruger"];
                if (brug.ID == -1) //Bruger er IKKE logget på. Bruges til f.eks. direkte link.
                {
                    ButtonGoToFindSong.Text = "Songs";
                    ButtonGoToAddNewSong.Visible = false;
                    ButtonGoToAddNewSheetSong.Visible = false;
                    ButtonGoToSeeAllSheetSongs.Visible = true;
                    ButtonGoToSeeAllSongBooks.Visible = true;
                }
                else //Loget på bruger.
                {
                    ButtonGoToFindSong.Text = "Your Songs";
                    ButtonGoToAddNewSong.Visible = true;
                    ButtonGoToAddNewSheetSong.Visible = true;
                    ButtonGoToSeeAllSheetSongs.Visible = false;
                    ButtonGoToSeeAllSongBooks.Visible = false;
                }
            }
            else
            {
                ButtonGoToFindSong.Text = "Songs";
                ButtonGoToAddNewSong.Visible = false;
                ButtonGoToAddNewSheetSong.Visible = false;
               //-----------Alle må se Sheet---------------------------
                ButtonGoToSeeAllSheetSongs.Visible = true;
                ButtonGoToSeeAllSongBooks.Visible = true;

            }
            //-------Skal komme efter Bruger-------------------------
            if (Session["Admin"] != null)
            {
                ButtonAdmin.Visible = true;
                ButtonGoToAddNewSong.Visible = false;
                ButtonGoToAddNewSheetSong.Visible = false;
                ButtonGoToFindSong.Visible = false;
                ButtonGoToSeeAllSheetSongs.Visible = false;
                ButtonGoToSeeAllSongBooks.Visible = false;

            }
        }

        protected void ButtonGoToFindSong_Click(object sender, EventArgs e)
        {
            Session["KommerFra"] = new List<String> { "Default.aspx" };
            if (Session["Bruger"] == null)
            {
                Bruger brug = new Bruger();
                brug.ID = -1;
                Session["Bruger"] = brug;

                Server.Transfer("FindSong.aspx");
            }
            else
            {
                 Bruger brug = (Bruger)Session["Bruger"];
                 if (brug.ID == -1) //Bruger er IKKE logget på.
                 {
                     Bruger b = new Bruger();
                     b.ID = -1;
                     Session["Bruger"] = b;
                    //---------
                    //  Session["Bruger"] = null;

                   //Indsætter v=0, for at slette parameter, som
                   //viser sig at blive hængende når man har været i en
                   //songBook som jo bruger direkte links med parameters, og jeg
                   //kan ikke bare slette disse, så derfor sender jeg bare denne dumme
                   // v afsted som så overskriver Song parameteresen.
                    Server.Transfer("FindSong.aspx?v=0");
                 }
                 else
                 {
                     //Viser alle DINE sange.
                     Server.Transfer("FindAllSongs.aspx");
                 }
            }
        }

        protected void ButtonGoToAddNewSong_Click(object sender, EventArgs e)
        {
            if (Session["Bruger"] == null)
            {
                Response.Write("<script LANGUAGE='JavaScript' >alert('You must register as a user and logOn for permission to create new songs')</script>");
                return;
            }
            Session["KommerFra"] = new List<String> { "Default.aspx" };
            Session["Sang"] = null; //Der skal nemlig laves ny sang nu.
            Session["EditChord"] = null;
            Session["EditChordSave"] = null;

            Server.Transfer("NewSong.aspx");
        }

        protected void ButtonAdmin_Click(object sender, EventArgs e)
        {
            Session["KommerFra"] = new List<String> { "Default.aspx" };
            Session["Sang"] = null;
            Server.Transfer("AdminOverview.aspx");
        }

        protected void ButtonGoToAddNewSheetSong_Click(object sender, EventArgs e)
        {
            Session["KommerFra"] = new List<String> { "Default.aspx" };
            Session["Sang"] = null;
            Session["EditNode"] = null;
            Session["EditChord"] = null;
            Session["EditText"] = null;
            Session["EditTextSave"] = null;
            Session["EditNodeSave"] = null;
            Session["EditChordSave"] = null;

            Server.Transfer("NewSheetSong.aspx");
        }

        protected void ButtonGoToSeeAllSheetSongs_Click(object sender, EventArgs e)
        {
            ////Alla må godt se Sheet. Men ikke lava ny selvfølgelig.
            Bruger brug = new Bruger();
            brug.ID = -1;
            Session["Bruger"] = brug;
            Session["KommerFra"] = new List<String> { "Default.aspx" };
            Server.Transfer("FindSheetSong.aspx");
        }

        protected void ButtonGoToSeeAllSongBooks_Click(object sender, EventArgs e)
        {
            ////Alla må godt se Sheet. Men ikke lava ny selvfølgelig.
            Bruger brug = new Bruger();
            brug.ID = -1; 
            Session["Bruger"] = brug;
            Session["KommerFra"] = new List<String> { "Default.aspx"};
         //   Response.Redirect("FindSongBooks.aspx");
            Server.Transfer("FindSongBooks.aspx");
        }
    }
}