﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="Sang.About" %>

<!DOCTYPE html>

<html   xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>About</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!--  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script> -->

    <link rel="stylesheet" href="/bootstrap-3.3.5-dist/css/bootstrap.min.css" />
    <script src="/jquery/jquery-1.11.3.min.js"></script>
    <script src="/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/css/StyleSheet1.css" />

    <style type="text/css">
        .auto-style1 {
            font-size: x-large;
        }
    </style>

</head>

<body>
    <form id="form1" runat="server">
        <div>

            <div class="container-fluid">
                <table class="table table-bordered">
                    <tr>
                        <td >TheSongApp is created by Jákup W. Hansen (jakupwhansen@hotmail.com)</td>                       
                    </tr>
                    <tr>
                        <td >Get your own songbook (5 songs for free, or 5$ per year for unlimited number of songs).</td>                       
                    </tr>
                    <tr>
                       <td >More about the app on this <a href="https://www.youtube.com/playlist?list=PLsztL4wSKFGpP3luCA-wlxo6kblyAUhiC"> YouTube playlist</a> </td>                       
                     </tr>
                    <tr>
                       <td ><strong><span class="auto-style1">The basic use (free for everyone, no &quot;log in&quot; )</span></strong><br />
                           <br />
                           <asp:Image ID="Image1" runat="server" ImageUrl="~/images/TheSongAppBasics.png" />
                        </td>                       
                     </tr>
                    <tr>
                       <td ><strong><span class="auto-style1">Your own Songbook (free for 5 songs, must &quot;log in&quot; )<br />
                           <asp:Image ID="Image2" runat="server" ImageUrl="~/images/theSongAppLogIn.png" />
                           </span></strong> </td>                       
                     </tr>
                </table>


            </div>
        </div>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date(); a = s.createElement(o),
                m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-54970963-3', 'auto');
            ga('send', 'pageview');
        </script>
    </form>
</body>
</html>
