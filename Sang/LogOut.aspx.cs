﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sang
{
    public partial class LogOut : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Bruger"] != null)
            {
                Session["Bruger"] = null;
                Session["Sang"] = null;
                Session["listAkkorder"] = null;
                Session["Admin"] = null;
                Session["KommerFra"] = null;
                Label1.Visible = true;
            }
            else
            {
                Server.Transfer("Default.aspx");
            }
        }

        protected void ButtonBack_Click(object sender, EventArgs e)
        {
            Server.Transfer("Default.aspx");
        }
    }
}