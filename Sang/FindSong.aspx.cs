﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sang
{
    public partial class FindSong : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string v = Request.QueryString["song"];
            if (v != null)
            {
                try
                {
                    SqlDataReader reader = null;
                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["wenningsdbConnectionString1"].ToString());
                    con.Open();
                    //------Henter Sangen-------------------------------------------
                    SqlCommand cmd = new SqlCommand("SELECT * FROM Sang WHERE Id=@sangID AND (type=@type OR type=@typeLink);", con);
                    
                    // 0 = Private
                    // 1 = Public
                    // 2 = Link
                    // 3 = Request

                    // input format fra request:   SangID + "_" + title.Length*SangID
                    
                    char[] delimiterChars = { '_' };
                    String[] spiltv = v.Split(delimiterChars);
                    String sangIDlink = spiltv[0];

                    cmd.Parameters.AddWithValue("@sangID", sangIDlink);
                    cmd.Parameters.AddWithValue("@type", 1);
                    cmd.Parameters.AddWithValue("@typeLink", 2);
                    reader = cmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        //Så skal vi tjekke om linkKoden er rigtig.
                        reader.Read(); //Læs den første række ind i reader.
                        int idet = reader.GetInt32(0);
                        String titelen = reader.GetString(1);
                        titelen = titelen.TrimStart(' ');
                        int samletKode = idet * titelen.Length; //ALgoritmen er id * titel.
                        if (spiltv[1].Equals(samletKode.ToString()))
                        {
                            Bruger bruger = new Bruger();
                            bruger.ID = -1; //Betyder denne bliver slettet med det samme når den rammer ShowSong. Skal bruges for at komme ind.
                            bruger.name = "gæst";
                            bruger.type = 1;
                            Session["Bruger"] = bruger;

                            createSongs cs = new createSongs();
                            Session["Sang"] = cs.createSong(sangIDlink);
                            //  createSong(idstring);
                            if (Session["KommerFra"] == null)
                            {
                                List<String> listeKommerFra = new List<String>();
                                listeKommerFra.Add("Default.aspx");
                                Session["KommerFra"] = listeKommerFra;
                            }
                            con.Close();

                            Server.Transfer("ShowSong.aspx");
                        }
                        else
                        {//Forkert Kode efter _ (underscore)
                            Session["Bruger"] = null;
                            Session["Sang"] = null;
                            Session["Admin"] = null;
                            con.Close();
                            Server.Transfer("Default.aspx");
                        }
                    }
                    else
                    { //Prøver med direkte link, men linket findes ikke. Smides
                       //tilbage til default siden.
                        Session["Bruger"] = null;
                        Session["Sang"] = null;
                        Session["listAkkorder"] = null;
                        Session["Admin"] = null;
                        Session["KommerFra"] = null;
                       
                        con.Close();
                        Server.Transfer("Default.aspx");
                    }
                    con.Close();
                }
                catch (Exception lja)
                {
                  
                }
            }


            if (!IsPostBack)//FØRSTE GANG.
            {
                Session["Sang"] = null;
            }
            visListeAfSange();

            
            //visListeAfSange();
        }
        private void visListeAfSange()
        {
            bool svar = false;
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["wenningsdbConnectionString1"].ToString());
                con.Open();
                //------Finder Sange-------------------------------------------
                //TYPE:  0=Ikke offentlig endnu. 1 = offentlig.
                int userId = 0;
                SqlCommand cmd = null;
                if (Session["Bruger"] != null)//Bruger er måske logget på. Skal derfor KUN se sine egne sange
                {
                    cmd = new SqlCommand("SELECT * FROM Sang WHERE userID=@userID ORDER BY titel ASC;", con);
                    Bruger bruger = (Bruger)Session["Bruger"];
                    userId = bruger.ID;
                    if(userId == -1)
                    {  //Bruger ukendt. Alle som ikke er logget på.
                        cmd = new SqlCommand("SELECT * FROM Sang WHERE type=@type  ORDER BY titel ASC;", con);
                    }
                }
                else //Ikke logget på. Derfor se alle offentlige sange.
                {
                    cmd = new SqlCommand("SELECT * FROM Sang WHERE type=@type  ORDER BY titel ASC;", con);
                }
                cmd.Parameters.AddWithValue("@type", 1);
                cmd.Parameters.AddWithValue("@userID", userId);

                SqlDataReader reader = cmd.ExecuteReader();
                List<SangTilValg> listSange = new List<SangTilValg>();
                if (reader.HasRows)
                {
                    int sangTeller = 0;
                    while (reader.Read())
                    {
                        sangTeller++;
                        SangTilValg sangValg = new SangTilValg(this);

                        int id = reader.GetInt32(0);
                        String titel = reader.GetString(1);
                        String lyric = reader.GetString(2);
                        String melody = reader.GetString(3);
                        int type = reader.GetInt32(6);


                        int userIDet = 0;
                        if (!reader.IsDBNull(4))
                        {
                            userIDet = reader.GetInt32(4);
                            sangValg.userID = userIDet;
                        }
                        sangValg.ID = id;
                        sangValg.titel = titel;
                        sangValg.lyric = lyric;
                        sangValg.melody = melody;
                        sangValg.LogOnUserID = userId;
                        sangValg.type = type;

                        Button b = new Button();
                        b.Click += new EventHandler(sangValg.eventMetode);
                        sangValg.knap = b;

                        //-------Add to listen---------------------
                        listSange.Add(sangValg);
                    }
                    this.Title = sangTeller + " songs";
                }
                bool knapSkalMed = false;
                //---2016-09-07---For at sortere ordentling så at f.eks. Mær... ikke kommer først.
                System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.GetCultureInfo("da");

                listSange.Sort();
                foreach (SangTilValg sa in listSange)
                {
                    //-------------------------------------------
                    String txten = sa.titel;
                    txten = txten.ToUpper();
                    knapSkalMed = false;
                    if (DropDownList1.SelectedIndex > 0)
                    {
                        if (txten[0].Equals(DropDownList1.SelectedValue.ToString()[0]))
                        {
                            knapSkalMed = true;
                        }
                        else
                        {
                            knapSkalMed = false;
                        }
                    }
                    else
                    {
                        knapSkalMed = true;
                    }

                    if (knapSkalMed)
                    {
                        // Create new row and add it to the table.
                        TableRow tRow = new TableRow();
                        Table1.Rows.Add(tRow);

                        // Create a new cell and add it to the row.
                        TableCell tCell = new TableCell();
                        sa.knap.Text = sa.titel;
                        sa.knap.Width = 300;
                        sa.knap.Height = 40;
                        sa.knap.Font.Size = 14;
                        sa.knap.Font.Bold = true;
                        sa.knap.CssClass = "btn-default";
                        tCell.Controls.Add(sa.knap);

                        sa.reFresh(); //Tegner titel og hvem ejer sangen
                        tRow.Cells.Add(tCell);
                    }

                }
                con.Close();
            }
            catch (Exception lskd)
            {
            }
        }
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        public void createSong(String idValgt)
        {
            // String idValgt = GridView1.SelectedValue.ToString();
            Sang sang = new Sang();
            try
            {
                int id = Convert.ToInt32(idValgt);
                sang.ID = id;

            }
            catch (Exception lkjs) { }
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["wenningsdbConnectionString1"].ToString());
                con.Open();
                //------Henter Sangen-------------------------------------------
                SqlCommand cmd = new SqlCommand("Select * FROM  Sang WHERE Id=@sangID  ;", con);

                cmd.Parameters.AddWithValue("@sangID", idValgt);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    ListBox1.Items.Add("Song har item" + idValgt);
                    while (reader.Read())
                    {
                        sang.title = reader.GetString(1);
                        sang.lyricWriter = reader.GetString(2);
                        sang.melodyWriter = reader.GetString(3);

                        ListBox1.Items.Add("while" + sang.title + ":" + sang.lyricWriter + ":" + sang.melodyWriter);
                    }
                }
                reader.Close();

                //------Henter textLinjer-------------------------------------------
                SqlCommand cmd2 = new SqlCommand("Select * FROM  SangTextLinjer WHERE sangID=@sangID  ;", con);
                cmd2.Parameters.AddWithValue("@sangID", idValgt);
                SqlDataReader reader2 = cmd2.ExecuteReader();

                if (reader2.HasRows)
                {
                    ListBox1.Items.Add("Har linje");
                    while (reader2.Read())
                    {
                        TextLinje t = new TextLinje();
                        t.text = reader2.GetString(2);
                        t.pos = new Point(reader2.GetInt32(3), reader2.GetInt32(4));
                        t.id = reader2.GetInt32(5);
                        sang.listTexter.Add(t);
                    }
                    foreach (TextLinje tl in sang.listTexter)
                    {
                        ListBox1.Items.Add(tl.text);
                    }

                }
                else
                {
                }
                reader2.Close();

                //------Henter Akkorder-------------------------------------------
                SqlCommand cmd3 = new SqlCommand("Select * FROM  SangAkkorder WHERE sangID=@sangID  ;", con);
                cmd3.Parameters.AddWithValue("@sangID", idValgt);
                SqlDataReader reader3 = cmd3.ExecuteReader();

                if (reader3.HasRows)
                {
                    ListBox1.Items.Add("Har akkorder");
                    while (reader3.Read())
                    {
                        Akkord ak = new Akkord();
                        ak.pos = new Point(reader3.GetInt32(2), reader3.GetInt32(3));
                        ak.keyNumber = reader3.GetInt32(4);
                        int bassen = 0;
                        try
                        {
                            bassen = reader3.GetInt32(7);
                        }
                        catch (Exception els)
                        {
                        }
                        ak.bass = bassen;

                        String extra = reader3.GetString(5);
                        if (extra != null)
                            ak.extraName = extra;
                        else
                            ak.extraName = "";
                        //  ak.id = reader3.GetInt32(6);
                        sang.listAkkorder.Add(ak);
                    }
                    ListBox1.Items.Clear();
                    foreach (Akkord ak in sang.listAkkorder)
                    {
                        ListBox1.Items.Add(ak.keyNumber + " x:" + ak.pos.X + " y:" + ak.pos.Y + " extra:" + ak.extraName);
                    }
                }
                else
                {
                }
                reader3.Close();


                Session["Sang"] = sang;
                Server.Transfer("ShowSong.aspx");
            }
            catch (Exception lkjd)
            {
            }
        }
        protected void ButtonBack_Click(object sender, EventArgs e)
        {
            if (Session["KommerFra"] != null)
            {
                List<String> listeKommerFra = (List<String>)Session["KommerFra"];
                String tilbageTil = listeKommerFra[listeKommerFra.Count - 1];
                listeKommerFra.RemoveAt(listeKommerFra.Count - 1);
                Session["KommerFra"] = listeKommerFra;
            //    Response.Write("<script LANGUAGE='JavaScript' >alert('"+ tilbageTil +"')</script>");
                Server.Transfer(tilbageTil);
            }
            else
            {
                Server.Transfer("Default.aspx");
            }
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //   Response.Write("<script LANGUAGE='JavaScript' >alert('"+DropDownList1.SelectedIndex.ToString() +"')</script>");
        }
        protected void quickFindSongs()
        {
            foreach (Control c in Table1.Controls)
            {
                if (c is Button)
                {
                    Button but = (Button)c;
                    if (but.Text[0] != 'B')
                    {
                        but.Visible = false;
                    }
                }
            }

        }
    }
    class SangTilValg:IComparable
    {
        public FindSong findS;
        public FindAllSongs findAllS;
        public int ID = 0;
        public String titel = "";
        public Button knap;
        public int userID = 0;
        public int LogOnUserID = 0;
        public String lyric = "";
        public String melody = "";
        public int type = 0;
        public SangTilValg(FindSong fs)
        {
            findS = fs;
        }
        public void addRefference(FindAllSongs fas)
        {
            findAllS = fas;
        }
        public void reFresh()
        {
            String mine = "";

            if (userID == LogOnUserID)
            {
                mine = "* ";
                if (type == 0)//Ikke offentlig endnu.
                {
                    knap.Font.Strikeout = true;
                }
            }

            knap.Text = mine + titel;

        }
        public void eventMetode(object sender, EventArgs e)
        {
            String idstring = ID.ToString();
            if (findS != null)
            {
                //----------Kommer fra------------------------------         
                List<String> listeKommerFra = (List<String>)HttpContext.Current.Session["KommerFra"];
                listeKommerFra.Add("FindSong.aspx");
                HttpContext.Current.Session["KommerFra"] = listeKommerFra;
                //-----------------------------------------------------
                findS.createSong(idstring);
            }
            else if( findAllS != null)
            {
                //----------Kommer fra------------------------------         
                List<String> listeKommerFra = (List<String>)HttpContext.Current.Session["KommerFra"];
                listeKommerFra.Add("FindSong.aspx");
                HttpContext.Current.Session["KommerFra"] = listeKommerFra;
                //-----------------------------------------------------
                findAllS.createSong(idstring);
            }
        }

        public int CompareTo(object obj)
        {
            SangTilValg shtv = (SangTilValg)obj;
            return titel.CompareTo(shtv.titel);
        }
    }
}