﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sang
{
   
    public partial class ShowSheetSong : System.Web.UI.Page
    {
    
        protected void Page_Load(object sender, EventArgs e)
        {
            

            if (Session["Bruger"] == null)
            {
                Server.Transfer("Default.aspx");
            }

            if (!IsPostBack)//FØRSTE GANG.
            {
                DropDownListSelectMelody.DataSource = GeneralData.InstrumentNames;
                DropDownListSelectMelody.DataBind();

                DropDownListSelectChords.DataSource = GeneralData.InstrumentNames;
                DropDownListSelectChords.DataBind();

                DropDownList1.DataSource = GeneralData.Beats;
                DropDownList1.DataBind();

            }
        }

        protected void ButtonBack_Click(object sender, EventArgs e)
        {
            if (Session["Bruger"] != null)
            {
                Bruger bruger = (Bruger)Session["Bruger"];
                if (bruger.ID == -1)
                {
                    //-1 betyder direkte link...Så skal skal slettes så vi ikke kan går til andre sider,
                    //Denne her bruger skal KUN se sheet song og ikke andet.
                    // fordi denne bruger eksisterer ikke rigtig.
            //       Session["Bruger"] = null;
                }
            }

            if (Session["KommerFra"] != null)
            {
                List<String> listeKommerFra = (List<String>)Session["KommerFra"];
                String tilbageTil = "Default.aspx";
                if (listeKommerFra.Count > 0)
                {
                    tilbageTil = listeKommerFra[listeKommerFra.Count - 1];
                    listeKommerFra.RemoveAt(listeKommerFra.Count - 1);
                }
                Session["KommerFra"] = listeKommerFra;

                Server.Transfer(tilbageTil);
            }
            else
            {
                Server.Transfer("Default.aspx");
            }
        }



        protected void PlaySong_Click(object sender, EventArgs e)
        {
           
            PlaySong.Visible = false;
            showPlaySong();
        }
        private void showPlaySong()
        {
            NewSheetSong nss = new NewSheetSong();
            nss.createMidiSong(DropDownListSelectMelody.SelectedIndex, DropDownListSelectChords.SelectedIndex, CheckBoxChordsIncluded.Checked);
          //  createMidiSong();          
            PanelPlay.Visible = true;
            DropDownList1.SelectedIndex = 4; //Tempo DropDownBox
            buttonMore.Visible = true;
        }
        private void hidePlaySong()
        {
            Panel1.Visible = false;


        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(Session["Sang"]!= null)
            {
                Sang s = (Sang)Session["Sang"];
                s.tempo = DropDownList1.SelectedIndex;
            }
            NewSheetSong nss = new NewSheetSong();
            nss.createMidiSong(DropDownListSelectMelody.SelectedIndex, DropDownListSelectChords.SelectedIndex, CheckBoxChordsIncluded.Checked);
         
         //   createMidiSong();
        }

        protected void CheckBoxInstruments_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckBoxInstruments.Checked == true)
            {
                DropDownListSelectMelody.Visible = true;
                DropDownListSelectChords.Visible = true;
                LinkDownloadMid.Visible = true;
            }
            else
            {
                DropDownListSelectMelody.Visible = false;
                DropDownListSelectChords.Visible = false;
                LinkDownloadMid.Visible = false;
            }
        }

        protected void buttonMore_Click(object sender, EventArgs e)
        {
            if (Panel1.Visible == false)
            {
                Panel1.Visible = true;
                buttonMore.Text = "Hide";

            }
            else
            {
                Panel1.Visible = false;
                buttonMore.Text = "More";


            }
        }

        protected void ButtonAnimation_Click(object sender, EventArgs e)
        {
            Server.Transfer("ShowSheetSongCanvas.aspx");            
        }
      
        protected void ButtonKeyUp_Click(object sender, EventArgs e)
        {
            ButtonKeyDown.Enabled = true;

            if (Session["sang"] != null)
            {
                Sang sang = (Sang)Session["sang"];
                //------------Flyt noderne Op-------------------------
                foreach (NodeLinjer nl in sang.listNodeLinjer)
                {
                    //----Sange som er lavet i C, har ingen toneart, så den er 0, hvor den
                    // brude have 1.
                    if(nl.key == 0)
                    { nl.key = 1; }
                    //---NODELINJENS KEY ÆNDRES HER-------------
                    if (nl.key < 12)
                    {
                        nl.key++;
                    }
                    else
                    {
                        nl.key = 1;
                    }
                    nl.callibrerNyKeyChange();
                    foreach (Noder no in nl.listNoder)
                    {
                       
                        if (no.length > 1 && no.length < 100)
                        {
                            no.keyNumber = no.keyNumber + 1;
                            no.kalibrerForTranspose();
                            if (no.keyNumber < 36)
                            {

                            }
                            else
                            {
                                ButtonKeyUp.Enabled = false;
                            }
                        }
                     
                    }
                }

                //---------FLytte akkorderne op--------------------------
                foreach (Akkord ak in sang.listAkkorder)
                {
                    if (ak.keyNumber < 12)
                    {
                        ak.keyNumber = ak.keyNumber + 1;
                    }
                    else
                    {
                        ak.keyNumber = 1;
                    }

                    if (ak.bass > 0) //0 er INGEN bass
                    {
                        if (ak.bass < 12)
                        {
                            ak.bass = ak.bass + 1;
                        }
                        else
                        {
                            ak.bass = 1;
                        }
                    }
                }
                Session["sang"] = sang;
            }
            showPlaySong();

        }


        protected void ButtonKeyDown_Click(object sender, EventArgs e)
        {
            ButtonKeyUp.Enabled = true;

            if (Session["sang"] != null)
            {
                Sang sang = (Sang)Session["sang"];
                //------------Flyt noderne Ned-------------------------
                foreach(NodeLinjer nl in sang.listNodeLinjer)
                {
                    //---NODELINJENS KEY ÆNDRES HER-------------
                    if (nl.key > 1)
                    {
                        nl.key--;
                    }
                    else
                    {
                        nl.key = 12;
                    }
                    nl.callibrerNyKeyChange();

                    foreach (Noder no in nl.listNoder)
                    {
                        if (no.length > 1 && no.length < 100)
                        {
                            no.keyNumber = no.keyNumber - 1;
                            no.kalibrerForTranspose();
                            if (no.keyNumber > 6)
                            {

                            }
                            else
                            {
                                ButtonKeyDown.Enabled = false;
                            }
                        }
                       
                    }
                }

                //---------FLytte akkorderne Ned--------------------------
                foreach (Akkord ak in sang.listAkkorder)
                {
                    if (ak.keyNumber > 1)
                    {
                        ak.keyNumber = ak.keyNumber - 1;
                    }
                    else
                    {
                        ak.keyNumber = 12;
                    }
                    if (ak.bass > 0) //0 er INGEN bass
                    {

                        if (ak.bass > 1)
                        {
                            ak.bass = ak.bass - 1;
                        }
                        else
                        {
                            ak.bass = 12;
                        }
                    }
                }
                Session["sang"] = sang;
            }
            showPlaySong();
        }




    }
}