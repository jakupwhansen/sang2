﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;

namespace Sang
{
    public static class GeneralData
    {
        public static int[] Beats = { 40, 60, 80, 100, 120, 140, 160, 180, 200, 220, 240, 260, 280, 300, 320, 340, 360, 380, 400 }; 
        public static string[] InstrumentNames =
		{
			"Acoustic Grand Piano",
			"Bright Acoustic Piano",
			"Electric Grand Piano",
			"Honky-tonk Piano",
			"Electric Piano 1",
			"Electric Piano 2",
			"Harpsichord",
			"Clavinet",
			"Celesta",
			"Glockenspiel",
			"Music Box",
			"Vibraphone",
			"Marimba",
			"Xylophone",
			"Tubular Bells",
			"Dulcimer",
			"Drawbar Organ",
			"Percussive Organ",
			"Rock Organ",
			"Church Organ",
			"Reed Organ",
			"Accordion",
			"Harmonica",
			"Tango Accordion",
			"Acoustic Guitar (nylon)",
			"Acoustic Guitar (steel)",
			"Electric Guitar (jazz)",
			"Electric Guitar (clean)",
			"Electric Guitar (muted)",
			"Overdriven Guitar",
			"Distortion Guitar",
			"Guitar harmonics",
			"Acoustic Bass",
			"Electric Bass (finger)",
			"Electric Bass (pick)",
			"Fretless Bass",
			"Slap Bass 1",
			"Slap Bass 2",
			"Synth Bass 1",
			"Synth Bass 2",
			"Violin",
			"Viola",
			"Cello",
			"Contrabass",
			"Tremolo Strings",
			"Pizzicato Strings",
			"Orchestral Harp",
			"Timpani",
			"String Ensemble 1",
			"String Ensemble 2",
			"Synth Strings 1",
			"Synth Strings 2",
			"Choir Aahs",
			"Voice Oohs",
			"Synth Choir",
			"Orchestra Hit",
			"Trumpet",
			"Trombone",
			"Tuba",
			"Muted Trumpet",
			"French Horn",
			"Brass Section",
			"Synth Brass 1",
			"Synth Brass 2",
			"Soprano Sax",
			"Alto Sax",
			"Tenor Sax",
			"Baritone Sax",
			"Oboe",
			"English Horn",
			"Bassoon",
			"Clarinet",
			"Piccolo",
			"Flute",
			"Recorder",
			"Pan Flute",
			"Blown Bottle",
			"Shakuhachi",
			"Whistle",
			"Ocarina",
			"Lead 1 (square)",
			"Lead 2 (sawtooth)",
			"Lead 3 (calliope)",
			"Lead 4 (chiff)",
			"Lead 5 (charang)",
			"Lead 6 (voice)",
			"Lead 7 (fifths)",
			"Lead 8 (bass + lead)",
			"Pad 1 (new age)",
			"Pad 2 (warm)",
			"Pad 3 (polysynth)",
			"Pad 4 (choir)",
			"Pad 5 (bowed)",
			"Pad 6 (metallic)",
			"Pad 7 (halo)",
			"Pad 8 (sweep)",
			"FX 1 (rain)",
			"FX 2 (soundtrack)",
			"FX 3 (crystal)",
			"FX 4 (atmosphere)",
			"FX 5 (brightness)",
			"FX 6 (goblins)",
			"FX 7 (echoes)",
			"FX 8 (sci-fi)",
			"Sitar",
			"Banjo",
			"Shamisen",
			"Koto",
			"Kalimba",
			"Bag pipe",
			"Fiddle",
			"Shanai",
			"Tinkle Bell",
			"Agogo",
			"Steel Drums",
			"Woodblock",
			"Taiko Drum",
			"Melodic Tom",
			"Synth Drum",
			"Reverse Cymbal",
			"Guitar Fret Noise",
			"Breath Noise",
			"Seashore",
			"Bird Tweet",
			"Telephone Ring",
			"Helicopter",
			"Applause",
			"Gunshot"
		};

        public static String getNodeName(int keyNumber)
        {
            String svar = "";
            Dictionary<int, String> dict = getListAkkorder();
            while(keyNumber > 12)
            {
                keyNumber = keyNumber - 12;
            }

            try
            {
                svar = dict[keyNumber];
            }
            catch(Exception lks)
            {

            }


            return svar;
        }

        public static Dictionary<int, String> getListAkkorder()
        {
            //Læg mærke til at jeg ændrer rækkefølgen af items som lægges i listen.
            //grunden er den at alle de normale akkorder f.eks A skal komme før Ab når man 
            //trykker på A bogstaven og Listen opdateres.
                Dictionary<int, String>    listAkkorder = new Dictionary<int, string>();
                listAkkorder.Add(-1, ""); //-1 betyder INGEN AKKORD/BASS.
                listAkkorder.Add(1, "C");
                listAkkorder.Add(2, "C#");
                listAkkorder.Add(3, "D");
                listAkkorder.Add(5, "E");
                listAkkorder.Add(4, "Eb");
                listAkkorder.Add(6, "F");
                listAkkorder.Add(7, "F#");
                listAkkorder.Add(8, "G");
                listAkkorder.Add(10, "A");
                listAkkorder.Add(9, "Ab");
                listAkkorder.Add(12, "B");
                listAkkorder.Add(11, "Bb");
            
               return listAkkorder;
        }
 
        public static Dictionary<int, String> getListNodeLength()
        {
            Dictionary<int, String> listNodeLength = new Dictionary<int, string>();

            //Har lavet det hele om til 16 dele. Passer bedre med MIDI
            listNodeLength.Add(32, "Whole");
            listNodeLength.Add(48, "Whole Dot");
            listNodeLength.Add(132, "Whole rest");
            listNodeLength.Add(148, "Whole Dot rest");
            listNodeLength.Add(16, "Half");
            listNodeLength.Add(24, "Half Dot");
            listNodeLength.Add(116, "Half rest");
            listNodeLength.Add(124, "Half Dot rest"); 
            listNodeLength.Add(8, "Quarter");
            listNodeLength.Add(12, "Quarter Dot");
            listNodeLength.Add(108, "Quarter rest");
            listNodeLength.Add(112, "Quarter Dot rest");
            listNodeLength.Add(4, "Eighth");
            listNodeLength.Add(6, "Eighth Dot");
            listNodeLength.Add(104, "Eighth rest");
            listNodeLength.Add(106, "Eighth Dot rest"); 
            listNodeLength.Add(2, "Sixteenth");
            listNodeLength.Add(3, "Sixteenth Dot");
            listNodeLength.Add(102, "Sixteenth rest");
            listNodeLength.Add(103, "Sixteenth Dot rest");
            listNodeLength.Add(-1, "line");
            return listNodeLength;

        }

    }
}