﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sang
{
    public partial class BitMapNewSheetSong : System.Web.UI.Page
    {


        protected void Page_Load(object sender, EventArgs e)
        {
            //FØRSTE GANG MAN KOMMER IND, ER DER LAVET ÉT STAF.
            Bitmap objBitmap = default(Bitmap);
            Graphics objGraphics = default(Graphics);
            //int width = 300; //Default, always overwritten
            //int height = 400; //Default, always overwritten
            Sang sang = null;
            if (Session["Sang"] == null)
            {
                sang = new Sang();
                sang.OpsetningAfSangMedNoder(10);
                sang.newNodeLinje();                
                Session["Sang"] = sang;
            //   Response.Write("<script LANGUAGE='JavaScript' >alert('" + "sang.listNodeLinjer[0].listNoder.Count "+ "')</script>");

            }
            else
            {
           //  Response.Write("<script LANGUAGE='JavaScript' >alert('" + "OK" + "')</script>");
                sang = (Sang)Session["Sang"];
     
                //Højden justeres her efter den nederste node linje.
                if (sang.aktuelNodeLinje != null) //Ved redigering er denne ikke sat.
                {
                    sang.height = sang.aktuelNodeLinje.pos.Y + 300;
                }
                else 
                {
                    //Sætter den sidste til at være den aktuelle, som det altid skal være.
                    if (sang.listNodeLinjer != null)
                    {
                       sang.aktuelNodeLinje = sang.listNodeLinjer[sang.listNodeLinjer.Count - 1];
                    }
                }

             

            }
            objBitmap = new Bitmap(sang.width, sang.height);
            using (Graphics graph = Graphics.FromImage(objBitmap))
            {
                Rectangle ImageSize = new Rectangle(0, 0, sang.width, sang.height);
                graph.FillRectangle(Brushes.White, ImageSize);
            }
          //   objBitmap = new Bitmap(Server.MapPath("images/Empty.jpg"), true);
            objGraphics = Graphics.FromImage(objBitmap);

            //----------TEGN ----START------------------------------------------------
            //----------TEGN ----START------------------------------------------------
            //----------TEGN ----START------------------------------------------------

            if (Session["Sang"] != null)
            {
                sang = (Sang)Session["Sang"];
                //---TEXT tegnes her------------------------------
              
                    for (int i = 0; i < sang.listTexter.Count; i++)
                    {
                        TextLinje tl = sang.listTexter[i];
                        if (tl != null)
                        {
                            if (tl.editIgang == true) //EDIT af text markeres
                            {
                                SolidBrush drawBrush = new SolidBrush(Color.Red);
                                objGraphics.FillEllipse(drawBrush, tl.pos.X, tl.pos.Y, 7, 7);
                                drawBrush.Dispose();
                            }
                        }
                        Font fontBanner = new Font("Verdana", 16, FontStyle.Regular | FontStyle.Italic);
                        objGraphics.DrawString(tl.text, fontBanner, new SolidBrush(Color.Black), tl.pos.X, tl.pos.Y);
                        fontBanner.Dispose();
                    }
                
                //-----AKKORDER tegne her-----------------------------------
                Dictionary<int, String> listAkkorder = GeneralData.getListAkkorder();

                for (int i = 0; i < sang.listAkkorder.Count; i++)
                {
                    //-------Almindelige akkorder (A, F#, B)---------------------------
                    if (listAkkorder != null)
                    {
                        Akkord ak = sang.listAkkorder[i];
                        if (ak != null)
                        {
                            if (ak.editIgang == true) //EDIT af Node markers
                            {
                                SolidBrush drawBrush = new SolidBrush(Color.Red);
                                objGraphics.FillEllipse(drawBrush, ak.pos.X, ak.pos.Y, 7, 7);
                                drawBrush.Dispose();
                            }
                        }
                        String nameAkkord = listAkkorder[ak.keyNumber];
                        Font fontBanner = new Font("Verdana", 16, FontStyle.Bold | FontStyle.Italic);

                        objGraphics.DrawString(nameAkkord, fontBanner, new SolidBrush(Color.Black), ak.pos.X, ak.pos.Y);

                        //-------Extra til akkorder (Sus7, Maj7, m)---------------------------
                        float samletAfstand = 0; //Bruges til bass.
                        if (ak.extraName != null)
                        {
                            if (ak.extraName.Length > 0)
                            {
                                SizeF sf = MeasureString(nameAkkord, 16);
                                Font fontBanner2 = new Font("Verdana", 14, FontStyle.Regular | FontStyle.Italic);
                                int extra = -4;
                                if (nameAkkord.Contains("#"))
                                { extra = 0; }
                                else if (nameAkkord.Contains("b"))
                                { extra = -1; }

                                samletAfstand = sf.Width + extra;
                                objGraphics.DrawString(ak.extraName, fontBanner2, new SolidBrush(Color.Black), ak.pos.X + samletAfstand, ak.pos.Y + 2);
                                fontBanner2.Dispose();
                            }
                        }
                        fontBanner.Dispose();
                        if (ak.bass > 0)
                        {
                            if (samletAfstand == 0)//Så er der ingen extra text, kun akkorden
                            {
                                SizeF sf = MeasureString(nameAkkord, 16);
                                Font fontBanner2 = new Font("Verdana", 14, FontStyle.Regular | FontStyle.Italic);
                                int extra = -4;
                                if (nameAkkord.Contains("#"))
                                { extra = 0; }
                                else if (nameAkkord.Contains("b"))
                                { extra = -1; }

                                samletAfstand = sf.Width + extra;

                            }
                            String nameBass = listAkkorder[ak.bass];
                            SizeF sf2 = MeasureString(ak.extraName, 14);
                            float samletAfstandTilBass = sf2.Width;
                            Font fontBanner3 = new Font("Verdana", 14, FontStyle.Regular | FontStyle.Italic);
                            objGraphics.DrawString("/" + nameBass, fontBanner3, new SolidBrush(Color.Black), ak.pos.X + samletAfstand + samletAfstandTilBass, ak.pos.Y + 2);

                            fontBanner3.Dispose();
                        }
                    }
                }
                //---------------Titel, Lyric og Melodi tegne her-------------------------------
                if (sang.title.Length > 1)
                {
                    Font fontBanner = new Font("Verdana", 20, FontStyle.Bold | FontStyle.Regular);
                    objGraphics.DrawString(sang.title, fontBanner, new SolidBrush(Color.Black), 100, 50);
                    fontBanner.Dispose();
                }
                if (sang.lyricWriter.Length > 1)
                {
                    Font fontBanner = new Font("Verdana", 10, FontStyle.Regular | FontStyle.Regular);
                    objGraphics.DrawString("Lyric: " + sang.lyricWriter, fontBanner, new SolidBrush(Color.Black), 5, 3);
                    fontBanner.Dispose();
                }
                if (sang.melodyWriter.Length > 1)
                {
                    Font fontBanner = new Font("Verdana", 10, FontStyle.Regular | FontStyle.Regular);
                    objGraphics.DrawString("Melody: " + sang.melodyWriter, fontBanner, new SolidBrush(Color.Black), 5, 23);
                    fontBanner.Dispose();
                }


                //---Paint Staf & Node´s-----Paint Staf & Node´s-----Paint Staf & Node´s-----Paint Staf & Node´s---
                //---Paint Staf & Node´s-----Paint Staf & Node´s-----Paint Staf & Node´s-----Paint Staf & Node´s---
                sang.paintStaf(objGraphics);
                //---Paint Staf & Node´s-----Paint Staf & Node´s-----Paint Staf & Node´s-----Paint Staf & Node´s---
                //---Paint Staf & Node´s-----Paint Staf & Node´s-----Paint Staf & Node´s-----Paint Staf & Node´s---
               

                ////-----Transparent picture testing-------------------------------
                //Bitmap  imgb = new Bitmap(Server.MapPath("images/clef.gif"), true);
                //imgb.MakeTransparent();
             

                //objGraphics.DrawImage(imgb, 0, 30, 200, 200);
         



            }
            //----------TEGN ----END------------------------------------------------
            //----------TEGN ----END------------------------------------------------
            //----------TEGN ----END------------------------------------------------



            objBitmap.Save(Response.OutputStream, ImageFormat.Gif);
            objBitmap.Dispose();
            objGraphics.Dispose();
        }
        private SizeF MeasureString(String texten, int fontSize)
        {
            Font drawFont = new Font("Arial", fontSize, FontStyle.Regular);
            // Set up string.
            string measureString = texten;

            // Measure string.
            SizeF stringSize = new SizeF();

            Graphics grfx = Graphics.FromImage(new Bitmap(1, 1));

            stringSize = grfx.MeasureString(measureString, drawFont);

            drawFont.Dispose();
            grfx.Dispose();

            return stringSize;
        }

    }
}