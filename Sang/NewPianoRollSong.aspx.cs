﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.Services;
using NAudio.Midi;
using System.Configuration;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;

namespace Sang
{
    [System.Web.Script.Services.ScriptService]
    public partial class NewPianoRollSong : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Bruger"] == null)
            {
                //Server.Transfer("Default.aspx");
            }
        }
        static Node[] listNode = null;

        [WebMethod]
        public static void PassThings(List<Node> nodeList)
        {
            listNode = nodeList.ToArray();
        }
        public class Node
        {
            public int x { get; set; }
            public int y { get; set; }
            public int xStop { get; set; }
            public int noteNumber { get; set; }
            public int noteLength { get; set; }
            public int editMode { get; set; }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            startAlt();
        }
        public void startAlt()
        {
            if (listNode != null)
            {
                String samlet = "";
                foreach (Node n in listNode)
                {
                    samlet += "[ " + n.noteNumber + "-" + n.noteLength + "]";
                }
                TextBox1.Text = samlet;
                try
                {
                    createMidiSongOnlyMelody();
                    TextBox1.Text = "Midi lavet " + samlet;

                }
                catch (Exception klsd)
                {
                    TextBox1.Text = "Fejl i Midi: " + klsd.ToString();
                }

            }
            else
                TextBox1.Text = "tomt";

        }

        public void createMidiSongOnlyMelody()
        {
            //  1.Instantiate a MIDISong object.
            MIDISong song = new MIDISong();

            //  2.Add a track which is specified by a text string naming the track.
            song.AddTrack("Random");

            //  3.Set the time signature.
            song.SetTimeSignature(0, 4, 4);

            //  4.Set the tempo.
            song.SetTempo(0, 112);

            //  5.Set the instrument.
            song.SetChannelInstrument(0, 0, 1);

            //  6.Add notes.
            //song.AddNote(0, 0, 50, 12);
            //song.AddNote(0, 0, 54, 6);
            //song.AddNote(0, 0, 58, 12);
            foreach (Node no in listNode)
            {
                if (no.noteLength < 100)
                {
                    song.AddNote(0, 0, no.noteNumber, (no.noteLength + (no.noteLength / 2)) * 2); //(no.noteLength/2) For at normailsere det til rigtige takter.
                }
                else
                {
                    int nl = no.noteLength - 100;
                    song.AddNote(0, 0, -1, (nl + (nl / 2)) * 2);
                }
            }

            try
            {
                MemoryStream ms = new MemoryStream();
                song.Save(ms);
                ms.Seek(0, SeekOrigin.Begin);

                using (FileStream file = new FileStream(Server.MapPath("~/midi/sangPianoRoll.mid"), FileMode.Create, System.IO.FileAccess.Write))
                {
                    byte[] bytes = new byte[ms.Length];
                    ms.Read(bytes, 0, (int)ms.Length);
                    file.Write(bytes, 0, bytes.Length);
                    ms.Close();
                }
                Session["MidiFileCreatedFromKeyboard"] = true;

                Server.Transfer("NewSheetSong.aspx");
            }
            catch (Exception exds)
            {

            }
        }
    }
}