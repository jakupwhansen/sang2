﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sang
{
    public partial class FindSongBooks : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<SangBook> listen = new List<SangBook>();
            //------Sanghefti-----------------------------
            SangBook sk = new SangBook();
            sk.title = "Sanghefti (FØ)";
            sk.direktLink = "/books/Sanghefti.aspx";
            listen.Add(sk);
            //---------------------------------------

            //------HØJGAARD-----------------------------
            sk = new SangBook();           
            sk.title = "H. J. Højgaard Nótabók (FØ)";
            sk.direktLink = "/Composers/Hojgaard.aspx";
            listen.Add(sk);
            //---------------------------------------
            //------Børnesange-----------------------------
            sk = new SangBook();
            sk.title = "Børnesange (DK)";
            sk.direktLink = "/books/Bornesange.aspx";
            listen.Add(sk);
            //---------------------------------------
            //------Børnesange-----------------------------
            sk = new SangBook();
            sk.title = "Spæliland (FØ)";
            sk.direktLink = "/books/Spaliland.aspx";
            listen.Add(sk);
            //---------------------------------------

            fyldTabelMedKnapper(listen);
        }
        private void fyldTabelMedKnapper(List<SangBook> listS)
        {
            foreach (SangBook sa in listS)
            {

                TableRow tRow = new TableRow();
                Table1.Rows.Add(tRow);

                // Create a new cell and add it to the row.
                TableCell tCell = new TableCell();
                sa.knap = new Button();
                sa.knap.Click += new EventHandler(sa.eventMetode);              
                sa.knap.Text = sa.title;
                sa.knap.Width = 300;
                sa.knap.Height = 40;
                sa.knap.Font.Size = 14;
                sa.knap.Font.Bold = true;
                sa.knap.CssClass = "btn-default";
                tCell.Controls.Add(sa.knap);

                tRow.Cells.Add(tCell);
            }

        }

        protected void ButtonBack_Click(object sender, EventArgs e)
        {
            if (Session["KommerFra"] != null)
            {
                List<String> listeKommerFra = (List<String>)Session["KommerFra"];
                String tilbageTil = listeKommerFra[listeKommerFra.Count - 1];
                listeKommerFra.RemoveAt(listeKommerFra.Count - 1);
                Session["KommerFra"] = listeKommerFra;
                //    Response.Write("<script LANGUAGE='JavaScript' >alert('"+ tilbageTil +"')</script>");
                Server.Transfer(tilbageTil);
            }
            else
            {
                Server.Transfer("Default.aspx");
            }

        }
    }
    class SangBook : IComparable
    {
        public Button knap;
        public String title;
        public String direktLink;
        public int SangID = 0;
        public int type = 0; //Tilstand.
        public bool sheetSong = false;
        public int CompareTo(object obj)
        {
            SangBook shtv = (SangBook)obj;
            return title.CompareTo(shtv.title);
        }


        public void eventMetode(object sender, EventArgs e)
        {
            //----------Kommer fra------------------------------         
            List<String> listeKommerFra = (List<String>)HttpContext.Current.Session["KommerFra"];
            //2017.09.01 fordi man ikke kunne komme direkt ind 
            if (listeKommerFra == null) { listeKommerFra = new List<String>(); }

            listeKommerFra.Add("FindSongBooks.aspx");
            HttpContext.Current.Session["KommerFra"] = listeKommerFra;
            //-----------------------------------------------------


            HttpContext.Current.Server.Transfer(direktLink);
        }
    }

}
