﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace Sang
{
    public partial class FindSheetSong : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string v = Request.QueryString["sheet"];
           ////// Bruger brug = (Bruger)Session["Bruger"];
           //// første gang man kommer ind med sheet, så er ingen bruger lavet,
           ////  men hvis man går tilbaget til oversigten, og så prøver at se alle sheet
           ////  så ryger men tilbage til den sheet sang man startede med, og kan
           ////  ikke se hele oversigten.Derfor tjekker jeg om Bruger er lavet, fordi
           //// det er kun når ingen bruger er lavet(som er første gang) at sheet sangen skal vises, resten
           ////skal vi tillade at se oversigten.
            if (v != null )
            {
                try
                {
                    SqlDataReader reader = null;
                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["wenningsdbConnectionString1"].ToString());
                    con.Open();
                    //------Henter Sangen-------------------------------------------
                    SqlCommand cmd = new SqlCommand("Select * FROM  SangSheet WHERE Id=@sangID  AND (type=@type OR type=@typeLink)  ;", con);

                    // 0 = Private
                    // 1 = Public
                    // 2 = Link
                    // 3 = Request

                    // input format fra request:   SangID + "_" + title.Length*SangID
                    char[] delimiterChars = { '_' };
                    String[] spiltv = v.Split(delimiterChars);
                    String sangIDlink = spiltv[0];


                    cmd.Parameters.AddWithValue("@sangID", sangIDlink);
                    cmd.Parameters.AddWithValue("@type", 1);
                    cmd.Parameters.AddWithValue("@typeLink", 2); //2 = Link.
                    reader = cmd.ExecuteReader();
                   
                    if (reader.HasRows)
                    {
                         //Så skal vi tjekke om linkKoden er rigtig.
                        reader.Read(); //Læs den første række ind i reader.
                        int idet = reader.GetInt32(0);
                        String titelen = reader.GetString(1);
                        titelen = titelen.TrimStart(' '); //Sheet sang har ofte en lang tom tekst i starten, for at flytte titelen lige over noderne, men må fjernes så at knappen ikke bare viser en tom tekst.
                        int samletKode = idet * titelen.Length; //ALgoritmen er id * titel.                        
                        
                        if (spiltv[1].Equals(samletKode.ToString()))
                        {
                            Bruger bruger = new Bruger();
                            bruger.ID = -1; //Betyder denne bliver slettet med det samme når den rammer ShowSheetSong. Skal bruges for at komme ind.
                            bruger.name = "gæst";
                            bruger.type = 1;
                            Session["Bruger"] = bruger;

                            createSongs cs = new createSongs();
                            Session["Sang"] = cs.createSheetSong(sangIDlink);
                            //  createSong(idstring);
                            if (Session["KommerFra"] == null)
                            {
                                List<String> listeKommerFra = new List<String>();
                                listeKommerFra.Add("Default.aspx");
                                Session["KommerFra"] = listeKommerFra;
                            }
                            con.Close();


                            //Respond er bedre her, fordi den ændrer i selve URL´en, og så
                            // kan brugeren komme vider i systemet uden hele tiden at komme til samme URL.
                            // ændret 2016.06.26.
                            Response.Redirect("ShowSheetSong.aspx", true);
                          //  Server.Transfer("ShowSheetSong.aspx");
                        }
                        else
                        {//Forkert Kode efter _ (underscore)
                            Session["Bruger"] = null;
                            Session["Sang"] = null;
                            Session["listAkkorder"] = null;
                            Session["Admin"] = null;
                            Session["KommerFra"] = null;
                            con.Close();
                            Server.Transfer("Default.aspx");
                        }
                    }
                    else
                    { //Prøver med direkte link, men linket findes ikke. Smides
                        //tilbage til default siden.
                        Session["Bruger"] = null;
                        Session["Sang"] = null;
                        Session["Admin"] = null;
                     
                        con.Close();
                        Server.Transfer("Default.aspx");
                    }

                    con.Close();
                }
                catch (Exception lja)
                { }


            }


            if (!IsPostBack)//FØRSTE GANG.
            {
                Session["Sang"] = null;
            }
            visListeAfSange();
        }
        private void visListeAfSange()
        {
            bool svar = false;
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["wenningsdbConnectionString1"].ToString());
                con.Open();
                //------Gemmer Sangen-------------------------------------------
                SqlCommand cmd = new SqlCommand("SELECT * FROM SangSheet WHERE type=@type  ORDER BY titel ASC;", con);
                cmd.Parameters.AddWithValue("@type", 1); //0 = lukket, 1 = åben for alle
                int userId = 0;
              
                //cmd.Parameters.AddWithValue("@userID", userId);

                SqlDataReader reader = cmd.ExecuteReader();
                List<SheetSangTilValg> listSange = new List<SheetSangTilValg>();
                if (reader.HasRows)
                {
                    int sangTeller = 0;
                    // ListBox1.Items.Add("Song har item" + idValgt);
                    while (reader.Read())
                    {
                        sangTeller++;
                        SheetSangTilValg sangValg = new SheetSangTilValg(this);

                        int id = reader.GetInt32(0);
                        String titel = reader.GetString(1);
                        titel = titel.Trim(' ');//Sheet sang har ofte en lang tom tekst i starten, for at flytte titelen lige over noderne, men må fjernes så at knappen ikke bare viser en tom tekst.
                        String lyric = reader.GetString(2);
                        String melody = reader.GetString(3);
                        int type = reader.GetInt32(6);


                        int userIDet = 0;
                        if (!reader.IsDBNull(4))
                        {
                            userIDet = reader.GetInt32(4);
                            sangValg.userID = userIDet;
                        }
                        sangValg.ID = id;
                        sangValg.titel = titel;
                        sangValg.lyric = lyric;
                        sangValg.melody = melody;
                        sangValg.LogOnUserID = userId;
                        sangValg.type = type;

                        Button b = new Button();
                        b.Click += new EventHandler(sangValg.eventMetode);
                        sangValg.knap = b;

                        //-------Add to listen---------------------
                        listSange.Add(sangValg);
                    }
                    this.Title = sangTeller + " songs";

                }
                bool knapSkalMed = false;
                listSange.Sort(); //Eftersom Titel ikke kan bruges i SQL til at sortere, og vi har slette fra titelen, må vi selv sortere listen.
                foreach (SheetSangTilValg sa in listSange)
                {
                    //-------------------------------------------

                    String txten = sa.titel;
                    txten = txten.ToUpper();
                   // txten = txten.Trim(' '); //Er allerede blevet trimmet ovenover.
                    knapSkalMed = false;
                    if (DropDownList1.SelectedIndex > 0)
                    {

                        if (txten[0].Equals(DropDownList1.SelectedValue.ToString()[0]))
                        {
                            knapSkalMed = true;
                        }
                        else
                        {
                            knapSkalMed = false;
                        }
                    }
                    else
                    {
                        knapSkalMed = true;
                    }

                    if (knapSkalMed)
                    {



                        // Create new row and add it to the table.
                        TableRow tRow = new TableRow();
                        Table1.Rows.Add(tRow);

                        // Create a new cell and add it to the row.
                        TableCell tCell = new TableCell();
                        sa.knap.Text = sa.titel.TrimStart(' ');
                        sa.knap.Width = 300;
                        sa.knap.Height = 40;
                        sa.knap.Font.Size = 14;
                        sa.knap.Font.Bold = false;
                        sa.knap.Font.Italic = true;
                        sa.knap.CssClass = "btn-default";
                        tCell.Controls.Add(sa.knap);

                        // sa.reFresh(); //Tegner titel og hvem ejer sangen
                        tRow.Cells.Add(tCell);
                    }

                }
                con.Close();


            }
            catch (Exception lskd)
            {

            }
        }
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        protected void ButtonBack_Click(object sender, EventArgs e)
        {
            if (Session["KommerFra"] != null)
            {
                List<String> listeKommerFra = (List<String>)Session["KommerFra"];
                String tilbageTil = listeKommerFra[listeKommerFra.Count - 1];
                listeKommerFra.RemoveAt(listeKommerFra.Count - 1);
                Session["KommerFra"] = listeKommerFra;

                Server.Transfer(tilbageTil);
            }
            else
            {
                Server.Transfer("Default.aspx");
            }
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //   Response.Write("<script LANGUAGE='JavaScript' >alert('"+DropDownList1.SelectedIndex.ToString() +"')</script>");

        }
    }
    class SheetSangTilValg:IComparable
    {
        public FindSheetSong findS;
        public int ID = 0;
        public String titel = "";
        public Button knap;
        public int userID = 0;
        public int LogOnUserID = 0;
        public String lyric = "";
        public String melody = "";
        public int type = 0;
        public SheetSangTilValg(FindSheetSong fs)
        {
            findS = fs;
        }
        public int CompareTo(object obj)
        {
            SheetSangTilValg shtv = (SheetSangTilValg) obj;
            return titel.CompareTo(shtv.titel);
        }

        public void reFresh()
        {
            String mine = "";

            if (userID == LogOnUserID)
            {
                mine = "* ";
                if (type == 0)//Ikke offentlig endnu.
                {
                    knap.Font.Strikeout = true;
                }
            }

            knap.Text = mine + titel;

        }
        public void eventMetode(object sender, EventArgs e)
        {
            String idstring = ID.ToString();
            createSongs cs = new createSongs();
            Sang sangen = cs.createSheetSong(idstring);
            HttpContext.Current.Session["Sang"] = sangen;

            ////---Bruges til JavaScript til at vise hvilken node spilles.-----------------
            //String samletLength = "";
            //String samletNodeX = "";
            //String samletNodeY = "";
            //String samletNodeHeight = "";
            //int samletLengthint = 0;
            //foreach(NodeLinjer nl in sangen.listNodeLinjer)
            //{
            //    foreach(Noder no in nl.listNoder)
            //    {
            //        if (no.length > 0 && no.length < 99)
            //        {
            //            samletLengthint = samletLengthint + no.length;
            //            samletLength = samletLength +":"+samletLengthint;
            //            samletNodeX = samletNodeX + ":" + no.pos.X;
            //            samletNodeY = samletNodeY + ":" + no.pos.Y;
            //            samletNodeHeight = samletNodeHeight + ":" + (23 + 12 + no.keyNumber);
            //        }
            //    }
            //}
            //HttpContext.Current.Session["CanvasNodeLength"] = samletLength;
            //HttpContext.Current.Session["CanvasNodeX"] = samletNodeX;
            //HttpContext.Current.Session["CanvasNodeY"] = samletNodeY;
            //HttpContext.Current.Session["samletNodeHeight"] = samletNodeHeight;
            
            // findS.createSong(idstring);


            List<String> listeKommerFra = (List<String>)HttpContext.Current.Session["KommerFra"];
            listeKommerFra.Add("FindSheetSong.aspx");
            HttpContext.Current.Session["KommerFra"] = listeKommerFra;



         //   HttpContext.Current.Server.Transfer("ShowSheetSong.aspx");

            //TEST MED CANVAS
            HttpContext.Current.Server.Transfer("ShowSheetSong.aspx");

        }

    }

}