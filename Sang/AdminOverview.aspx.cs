﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sang
{
    public partial class AdminOverview : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if ( Session["Admin"] == null)
            {
                Server.Transfer("Default.aspx");
            }
            
            visListeAfBrugere();
        }
        protected void ButtonBack_Click(object sender, EventArgs e)
        {
            if (Session["KommerFra"] != null)
            {
                List<String> listeKommerFra = (List<String>)Session["KommerFra"];
                String tilbageTil = listeKommerFra[listeKommerFra.Count - 1];
                listeKommerFra.RemoveAt(listeKommerFra.Count - 1);
                Session["KommerFra"] = listeKommerFra;

                Server.Transfer(tilbageTil);
            }
            else
            {
                Server.Transfer("Default.aspx");
            }
        }
        private void visListeAfBrugere()
        {
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["wenningsdbConnectionString1"].ToString());
                con.Open();
                //------Gemmer Sangen-------------------------------------------
                SqlCommand cmd = new SqlCommand("SELECT * FROM SangBruger WHERE type=@type OR type=@typeAdmin  ORDER BY date ASC;", con);
                cmd.Parameters.AddWithValue("@type", 1);
                cmd.Parameters.AddWithValue("@typeAdmin", 9);
                SqlDataReader reader = cmd.ExecuteReader();
                List<Bruger> listBrugere = new List<Bruger>();
               
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                      //  ListBox1.Items.Add("Song har item" );
                        Bruger bruger = new Bruger();

                        int id = reader.GetInt32(0);
                        String email = reader.GetString(1);
                        int password = reader.GetInt32(2);
                        //---name-------------------
                        String name = "ukendt";
                        if (!reader.IsDBNull(3))
                        {
                            name = reader.GetString(3);
                        }
                        //---------------------------
                        
                        //---info-------------------   
                        String info = "ingen info";
                        if (!reader.IsDBNull(4))
                        {
                            info = reader.GetString(4);
                        }
                        //---------------------------
                        
                        String date = reader.GetString(5);
                        int type = reader.GetInt32(6);

                        bruger.ID = id;
                        bruger.email = email;
                        bruger.password = password;
                        bruger.name = name;
                        
                        bruger.info = info;

                        bruger.date = date;
                        bruger.type = type;
                             
                        Button button = new Button();
                        
                        bruger.adminOver = this;
                        button.Click += new EventHandler(bruger.eventMetode);
                        bruger.knap = button;

                        //-------Add to listen---------------------
                        listBrugere.Add(bruger);
                    }
                }
                con.Close();

                ListBox1.Visible = false;

                //----Starter med at lave én knap, til at vise ALLE sange.---
                //----Starter med at lave én knap, til at vise ALLE sange.---
                //----Starter med at lave én knap, til at vise ALLE sange.---
                TableRow tRow = new TableRow();
                Table1.Rows.Add(tRow);
                // Create a new cell and add it to the row.
                TableCell tCell = new TableCell();
                Button nyKnap = new Button();
                nyKnap.Click += new EventHandler(eventShowAllSongs);
                nyKnap.Text = "Show all songs";
                nyKnap.Width = 300;
                nyKnap.Height = 40;
                nyKnap.Font.Size = 14;
                nyKnap.CssClass = "btn-default";
                nyKnap.Font.Bold = true;
                tCell.Controls.Add(nyKnap);
                tRow.Cells.Add(tCell);
                
                //--------Her laves så én knap pr. bruger.--------
                //--------Her laves så én knap pr. bruger.--------
                //--------Her laves så én knap pr. bruger.--------
                foreach (Bruger br in listBrugere)
                {
                   // Response.Write("<script LANGUAGE='JavaScript' >alert('" + "listBrugere.Count" + "')</script>");
                   //  ListBox1.Items.Add(br.name);

                    // Create new row and add it to the table.
                    tRow = new TableRow();
                    Table1.Rows.Add(tRow);

                    // Create a new cell and add it to the row.
                    tCell = new TableCell();
                    br.knap.Text = br.name ;
                    br.knap.Width = 300;
                    br.knap.Height = 40;
                    br.knap.Font.Size = 14;
                    br.knap.Font.Bold = false;
                    br.knap.CssClass = "btn-default";
                    tCell.Controls.Add(br.knap);

                    //br.reFresh(); //Tegner titel og hvem ejer sangen
                    tRow.Cells.Add(tCell);
                }

            }
            catch (Exception lskd)
            {

            }
        }
        public void eventShowAllSongs(object sender, EventArgs e)
        {
            Bruger nyBruger = new Bruger();
            nyBruger.ID = -1;
            Session["BrugerValgt"] = nyBruger; //betyder ALLE.

            //----------Kommer fra------------------------------         
            List<String> listeKommerFra = (List<String>)Session["KommerFra"];
            listeKommerFra.Add("AdminOverview.aspx");
            Session["KommerFra"] = listeKommerFra;
            //-----------------------------------------------------
   


            Server.Transfer("AdminOverviewShowSongs.aspx");      
        }

        public void visHvilkenBrugerDerErValgt(Bruger bru)
        {
           // Response.Write("<script LANGUAGE='JavaScript' >alert('" + valg + "')</script>");
            Session["BrugerValgt"] = bru;

            //----------Kommer fra------------------------------         
            List<String> listeKommerFra = (List<String>)Session["KommerFra"];
            listeKommerFra.Add("AdminOverview.aspx");
            Session["KommerFra"] = listeKommerFra;
            //-----------------------------------------------------
            
            Server.Transfer("AdminOverviewShowSongs.aspx");            
        }
    }
    public class Bruger
    {
        public int ID = 0;
        public String email = "";
        public int password = 0;
        public String name = "";
        public String date = "";
        public String info = "";
        public int type = 0;
        public AdminOverview adminOver;
        public Button knap;

        public void reFresh()
        {
        }
        public void eventMetode(object sender, EventArgs e)
        {
            adminOver.visHvilkenBrugerDerErValgt(this);           
        }

    }
}