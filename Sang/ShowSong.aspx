﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShowSong.aspx.cs" Inherits="Sang.ShowSong" %>

<!DOCTYPE html>

<html  xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Show Song</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
  
 <!--  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script> -->

      <link rel="stylesheet" href="/bootstrap-3.3.5-dist/css/bootstrap.min.css" />
    <script src="/jquery/jquery-1.11.3.min.js"></script> 
    <script src="/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script> 
    <link rel="stylesheet" type="text/css" href="/css/StyleSheet1.css"/>

</head>
<body>
    <form id="form1" runat="server">
        <div>

            <div class="container-fluid">
                <div>

                    <br />

                    <asp:Button CssClass="btn-default" ID="ButtonBack" runat="server" Text="Back" OnClick="ButtonBack_Click" />
                    <asp:Button  CssClass="btn-default" ID="ButtonDown" runat="server" Text="Down" OnClick="ButtonDown_Click" />
                    <asp:Button  CssClass="btn-default" ID="ButtonUp" runat="server" Text="Up" OnClick="ButtonUp_Click" />

                    <asp:Button  CssClass="btn-default" ID="ButtonTouchChords" runat="server" Text="Touch" OnClick="ButtonTouchChords_Click" />

                    <br />

                </div>
                  <br />

                <div style="100%">
    <div style="width:30%;min-width:50px;"></div>
</div>
            <div>
                <img src="BitMapNewSong.aspx" alt="song" style="width:100%; max-width:600px" />
            </div>

            </div>

        </div>
    </form>
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-54970963-3', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>
