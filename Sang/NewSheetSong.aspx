﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewSheetSong.aspx.cs" Inherits="Sang.NewSheetSong" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>New Sheet Song</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!--  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script> -->

    <link rel="stylesheet" href="/bootstrap-3.3.5-dist/css/bootstrap.min.css" />
    <script src="/jquery/jquery-1.11.3.min.js"></script>
    <script src="/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/css/StyleSheet1.css" />

    <script type='text/javascript' src='http://www.midijs.net/lib/midi.js'></script>

    <script type="text/javascript">
        window.onload = function () {
            var scrollY = parseInt('<%=Request.Form["scrollY"] %>');
            if (!isNaN(scrollY)) {
                window.scrollTo(0, scrollY);
            }
        };
        window.onscroll = function () {
            var scrollY = document.body.scrollTop;
            if (scrollY == 0) {
                if (window.pageYOffset) {
                    scrollY = window.pageYOffset;
                }
                else {
                    scrollY = (document.body.parentElement) ? document.body.parentElement.scrollTop : 0;
                }
            }
            if (scrollY > 0) {
                var input = document.getElementById("scrollY");
                if (input == null) {
                    input = document.createElement("input");
                    input.setAttribute("type", "hidden");
                    input.setAttribute("id", "scrollY");
                    input.setAttribute("name", "scrollY");
                    document.forms[0].appendChild(input);
                }
                input.value = scrollY;
            }
        };
    </script>


    <style type="text/css">
        .auto-style1 {
            width: 69px;
            font-size: x-large;
        }

        .auto-style3 {
            height: 20px;
        }

        .jakup-style {
            vertical-align: top;
        }

        .auto-style4 {
            vertical-align: central;
            width: 69px;
        }

        .auto-style6 {
            width: 253px;
        }

        .auto-style7 {
            width: 148px;
        }

        .auto-style8 {
            width: 148px;
            text-align: center;
            font-weight: bold;
        }

        .auto-style9 {
            width: 253px;
            text-align: center;
            font-weight: bold;
        }

        .auto-style10 {
            width: 100%;
        }

        .auto-style11 {
            width: 140px;
        }

        .auto-style12 {
            width: 212px;
        }

        .auto-style13 {
            width: 48%;
        }
    </style>
</head>
<body>



    <form id="form2" runat="server">
        <div class="container-fluid">
            <div>
                <table style="width: 33%;">

                    <tr>
                        <asp:Button CssClass="btn-default" ID="ButtonBack" runat="server" OnClick="ButtonBack_Click" Text="Back" />
                    </tr>
                    <tr>
                        <td class="auto-style4" rowspan="12">
                            <input type="image" runat="server" name="coord" id="Image1" src="BitMapNewSheetSong.aspx" onserverclick="xymetoden" />
                        </td>
                        <td class="auto-style3" colspan="7">

                            <asp:RadioButtonList ID="RadioButtonList1" runat="server" AutoPostBack="True" CellSpacing="10" RepeatDirection="Horizontal" Width="426px" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged" TabIndex="7">
                                <asp:ListItem>Play</asp:ListItem>
                                <asp:ListItem>Note</asp:ListItem>
                                <asp:ListItem>Chord</asp:ListItem>
                                <asp:ListItem>Text</asp:ListItem>
                                <asp:ListItem>Titel</asp:ListItem>
                                <asp:ListItem>Save</asp:ListItem>
                                <asp:ListItem>Seq</asp:ListItem>
                                <asp:ListItem>Midi</asp:ListItem>
                            </asp:RadioButtonList>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style3" colspan="7">
                            <br />
                            <table style="width: 100%;">
                                <tr>
                                    <td class="auto-style9">
                                        <asp:Label ID="LabelKey" runat="server" Text="Key" Visible="False"></asp:Label>
                                    </td>
                                    <td class="auto-style9">
                                        <asp:Label ID="LabelNote" runat="server" Text="Note" Visible="False"></asp:Label>
                                    </td>
                                    <td class="auto-style8">
                                        <asp:Label ID="LabelMessure" runat="server" Text="Messure" Visible="False"></asp:Label>
                                    </td>
                                    <td class="auto-style7">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style6">
                                        <asp:Button CssClass="btn-default" ID="ButtonTransposeUp" runat="server" OnClick="ButtonTransposeUp_Click" Text="Octave Up" Width="110px" Visible="False" />
                                    </td>
                                    <td class="auto-style6">
                                        <asp:Button CssClass="btn-default" ID="ButtonLonger" runat="server" OnClick="ButtonLonger_Click" Text="Length Up" Width="110px" Visible="False" />
                                    </td>
                                    <td class="auto-style7">
                                        <asp:Button CssClass="btn-default" ID="ButtonMesureUp" runat="server" OnClick="ButtonMesureUp_Click" Text="Width Up" Width="110px" Visible="False" />
                                    </td>
                                    <td class="auto-style7">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style6">
                                        <asp:Button CssClass="btn-default" ID="ButtonTransposeDown" runat="server" OnClick="ButtonTransposeDown_Click" Text="Octave Down" Width="110px" Visible="False" />
                                    </td>
                                    <td class="auto-style6">
                                        <asp:Button CssClass="btn-default" ID="ButtonShorter" runat="server" OnClick="ButtonShorter_Click" Text="Length Down" Width="110px" Visible="False" />
                                    </td>
                                    <td class="auto-style7">
                                        <asp:Button CssClass="btn-default" ID="ButtonMesureDown" runat="server" OnClick="ButtonMesureDown_Click" Text="Width Down" Width="110px" Visible="False" />
                                    </td>
                                    <td class="auto-style7">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style6">&nbsp;<asp:DropDownList ID="DropDownListKey" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownListKey_SelectedIndexChanged" Visible="False" Width="100px">
                                    </asp:DropDownList>
                                    </td>
                                    <td class="auto-style6">
                                        <asp:DropDownList ID="DropDownMidiPrLine" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownMidiPrLine_SelectedIndexChanged" Visible="False" Width="100px">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="auto-style7">
                                        <asp:Button CssClass="btn-default" ID="ButtonSamletVeightRight" runat="server" OnClick="ButtonSamletVeightRight_Click" Text="Barline Left" Width="110px" Visible="False" />
                                    </td>
                                    <td class="auto-style7">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                            <asp:Label ID="LabelMessureMessage" runat="server"></asp:Label>
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style3" colspan="7">
                            <asp:ListBox ID="ListBox1" runat="server" Width="296px" Visible="False" Height="26px"></asp:ListBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style3" colspan="7">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="LabelNodeLength" runat="server" Text="Length" Visible="False"></asp:Label>
                            <br />
                            <asp:DropDownList ID="DropDownList1" runat="server" Visible="False" Width="120px">
                            </asp:DropDownList></td>
                        <td>
                            <asp:Label ID="LabelNodeKey" runat="server" Text="Key" Visible="False"></asp:Label>
                            <br />
                            <asp:DropDownList ID="DropDownList2" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged" Visible="False" Width="80px">
                            </asp:DropDownList></td>
                        <td>
                            <asp:Label ID="LabelNodeSharpFlat" runat="server" Text="#/b" Visible="False"></asp:Label>
                            <br />
                            <asp:DropDownList ID="DropDownListSharpFlat" runat="server" Visible="False" Width="80px">
                            </asp:DropDownList></td>
                        <td>&nbsp;<asp:Label ID="LabelNodePrLine" runat="server" Text="Node.pr.line" Visible="False"></asp:Label>
                            <asp:DropDownList ID="DropDownListNodesPrLine" runat="server" OnSelectedIndexChanged="DropDownListNodesPrLine_SelectedIndexChanged" AutoPostBack="True" Visible="False" Width="80px">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:Label ID="LabelNodeDistBetweenNode" runat="server" Text="Dist.Node" Visible="False"></asp:Label>
                            <asp:DropDownList ID="DropDownListDistBeweenNodes" runat="server" OnSelectedIndexChanged="DropDownListDistBeweenNodes_SelectedIndexChanged" AutoPostBack="True" Visible="False" Width="80px">
                            </asp:DropDownList>
                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button CssClass="btn-default" ID="ButtonUndo" runat="server" Font-Bold="True" Font-Size="14pt" Height="29px" OnClick="ButtonUndo_Click" Text="Undo" Width="82px" Visible="False" />
                        </td>
                        <td>
                            <asp:Button CssClass="btn-default" ID="ButtonEdit" runat="server" Font-Bold="True" Font-Size="14pt" Height="29px" OnClick="ButtonEdit_Click" Text="Edit" Width="82px" Visible="False" />
                        </td>
                        <td>
                            <asp:CheckBox ID="CheckBoxDeleteNote" runat="server" TabIndex="6" Visible="False" Text="Delete" AutoPostBack="True" OnCheckedChanged="CheckBoxDeleteNote_CheckedChanged" />
                        </td>
                        <td>
                            <asp:CheckBox ID="CheckBoxInsertNote" runat="server" TabIndex="6" Visible="False" Text="Insert" AutoPostBack="True" OnCheckedChanged="CheckBoxInsertNote_CheckedChanged" />
                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        
                        <td class="auto-style3" colspan="7">                                   
                            <table style="width: 100%;">
                                <tr>
                                    <td class="auto-style6">
                                        <asp:Label ID="LabelChord" runat="server" Text="Chord" Visible="False"></asp:Label>
                                    </td>
                                    <td class="auto-style6">
                                        <asp:Label ID="LabelChordExtra" runat="server" Text="Extra" Visible="False"></asp:Label>
                                    </td>
                                    <td class="auto-style6">
                                        <asp:Label ID="LabelChordBass" runat="server" Text="Bass" Visible="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:DropDownList ID="DropDownListChord" runat="server" Font-Size="16pt" OnSelectedIndexChanged="DropDownListChord_SelectedIndexChanged" TabIndex="1" Visible="False">
                                            <asp:ListItem></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                          <asp:TextBox ID="TextBoxExtra" runat="server" Width="60px" Font-Size="14pt" TabIndex="2" Visible="False"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="DropDownListBass" runat="server" Font-Size="16pt" OnSelectedIndexChanged="DropDownListBass_SelectedIndexChanged" TabIndex="3" Visible="False">
                                            <asp:ListItem></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox ID="CheckBoxChord" runat="server" TabIndex="6" Visible="False" Checked="True" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="CheckBoxExtra" runat="server" TabIndex="6" Visible="False" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="CheckBoxBass" runat="server" TabIndex="6" Visible="False" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button CssClass="btn-default" ID="btn_UndoChord" runat="server" Height="30px" OnClick="btn_UndoChord_Click" Text="Undo" Width="82px" Font-Bold="True" Font-Size="16pt" CausesValidation="False" TabIndex="3" Visible="False" />
                                    </td>
                                    <td>
                                        <asp:Button CssClass="btn-default" ID="btn_EditChord" runat="server" Height="30px" OnClick="btn_EditChord_Click" Text="Edit" Width="82px" Font-Bold="True" Font-Size="16pt" CausesValidation="False" TabIndex="3" Visible="False" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="CheckBoxAkkordSameHeight" runat="server" TabIndex="6" Visible="False" Text="Same height" Checked="True" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>
                                        <asp:CheckBox ID="CheckBoxDeleteChord" runat="server" TabIndex="6" Visible="False" Text="Delete" AutoPostBack="True" />
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:CheckBox ID="CheckBox2" runat="server" Visible="False" TabIndex="7" />
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>                            
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:TextBox ID="TextBoxSongText" runat="server" Width="254px" Visible="False"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button CssClass="btn-default" ID="ButtonUndoText" runat="server" Font-Bold="True" Font-Size="14pt" Height="31px" OnClick="ButtonUndoText_Click" Text="Undo" Width="83px" Visible="False" />
                        </td>
                        <td>
                            <asp:Button CssClass="btn-default" ID="ButtonEditText" runat="server" Font-Bold="True" Font-Size="14pt" Height="31px" OnClick="ButtonEditText_Click" Text="Edit" Width="83px" Visible="False" />
                        </td>
                        <td>
                            <asp:CheckBox ID="CheckBoxTextSameHeight" runat="server" TabIndex="6" Visible="False" Text="Same height" Checked="True" />
                        </td>
                        <td>
                            <asp:Button CssClass="btn-default" ID="ButtonAutoText" runat="server" Font-Bold="True" Font-Size="14pt" Height="31px" OnClick="ButtonAutoText_Click" Text="Auto" Width="83px" Visible="False" />
                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="7">

                            <asp:Panel ID="Panel1" runat="server" Visible="False">
                                <table style="width: 100%;">
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td class="auto-style12">
                                            <!--  <span class="auto-style5">
                                                <a id="LinkPlayMidi" href="#" onclick="MIDIjs.play('midi/sang.mid');">Play Melody (Midi)
                                                </a>
                                            </span>
                                            -->
                                            <span class="auto-style1">
                                                <a href="#" onclick="MIDIjs.play('midi/sang.mid');">
                                                    <img alt="" src="/images/play.png" width="100" height="50" />
                                                </a>
                                            </span></td>
                                        <td>
                                            <asp:DropDownList ID="DropDownListSelectMelody" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList3_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </td>


                                        <td>&nbsp;</td>

                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td class="auto-style12">
                                            <asp:DropDownList ID="DropDownList3" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList3_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="CheckBoxChordsIncluded" runat="server" AutoPostBack="True" OnCheckedChanged="CheckBoxChordsIncluded_CheckedChanged" Text="With Chords" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td class="auto-style12">
                                            <asp:HyperLink ID="LinkDownloadMid" runat="server" NavigateUrl="~/midi/sang.mid">Download Midi file</asp:HyperLink>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="DropDownListSelectChords" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList3_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                                <br />


                            </asp:Panel>

                            <table class="nav-justified">
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelTitle" runat="server" Text="Title: " Visible="False"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxTitle" runat="server" Width="200px" Visible="False"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelLyric" runat="server" Text="Lyric: " Visible="False"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxLyric" runat="server" Width="200px" Visible="False"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="LabelMelody" runat="server" Text="Melody: " Visible="False"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="TextBoxMelody" runat="server" Width="200px" Visible="False"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>
                                        <asp:Button CssClass="btn-default" ID="ButtonSave" runat="server" OnClick="ButtonSave_Click" Text="Save Title" Width="93px" Visible="False" />




                                        <asp:Label ID="Label_Besked" runat="server" Text="Label" Visible="False"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <table class="auto-style10">
                                <tr>
                                    <td class="auto-style11">
                                        <asp:Button CssClass="btn-default" ID="btn_SaveSong" runat="server" Height="60px" OnClick="btn_SaveSong_Click" Text="Save song" Width="180px" Font-Bold="True" Font-Size="16pt" TabIndex="4" Visible="False" />
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="CheckBox1" runat="server" Visible="False" />
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                            <br />




                            &nbsp;<table style="width: 100%;">
                                <tr>
                                    <td class="auto-style13">
                                        <asp:Button CssClass="btn-default" ID="ButtonConverter" runat="server" OnClick="ButtonConverter_Click" Text="Convert Midi to Sheet" Width="222px" Height="45px" Visible="False" />




                                    </td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style13">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style13">




                                        <asp:FileUpload CssClass="btn-default" ID="FileUpload1" runat="server" Height="28px" Width="219px" Visible="False" />
                                    </td>
                                    <td>&nbsp;&nbsp;
                            <asp:CheckBox ID="CheckBoxCubase" runat="server" Text="Cubase" Visible="False" />
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </td>


                    </tr>
                    <tr>
                        <td class="auto-style3" colspan="7">
                            <br />
                        </td>
                    </tr>

                </table>
                <br />
                <br />
            </div>

        </div>
    </form>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-54970963-3', 'auto');
        ga('send', 'pageview');

    </script>
</body>
<script type="text/javascript">
    function reply_click() {
        MIDIjs.play('midi/sang.mid');
    }
</script>
</html>
