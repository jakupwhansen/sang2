﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Sang.Default" %>

<!DOCTYPE html>
<html  xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Welcome to the SongApp</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

 <!--  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script> -->

      <link rel="stylesheet" href="/bootstrap-3.3.5-dist/css/bootstrap.min.css" />
    <script src="/jquery/jquery-1.11.3.min.js"></script> 
    <script src="/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script> 
    <link rel="stylesheet" type="text/css" href="/css/StyleSheet1.css"/>
  
</head>

<body>
    <form id="form1" runat="server">
        <div>
            <div class="container-fluid">
                <p>
                    &nbsp;
                </p>
                <p>
                    <asp:Button CssClass="btn-default" ID ="ButtonGoToFindSong" runat="server" OnClick="ButtonGoToFindSong_Click" Text="Songs" Width="200px" Font-Bold="True" Font-Size="14pt" Height="79px" />
                </p>
                <p>
                    <asp:Button CssClass="btn-default" ID ="ButtonGoToSeeAllSheetSongs" runat="server" OnClick="ButtonGoToSeeAllSheetSongs_Click" Text="Sheet" Width="200px" Font-Bold="True" Font-Size="14pt" Height="79px" Visible="False" />
                </p>
                <p>
                    <asp:Button  CssClass="btn-default" ID="ButtonGoToAddNewSong" runat="server" OnClick="ButtonGoToAddNewSong_Click" Text="Add new song" Width="200px" Font-Bold="True" Font-Size="14pt" Height="40px" Visible="False" />
                </p>
                 <p>
                    <asp:Button  CssClass="btn-default" ID="ButtonGoToAddNewSheetSong" runat="server" OnClick="ButtonGoToAddNewSheetSong_Click" Text="Add new Sheet song" Width="200px" Font-Bold="True" Font-Size="12pt" Height="40px" Visible="False" />
                </p>
                <p>
                    &nbsp;
                </p>

                <div><span style="padding-right: 8em"><a href="LogOn.aspx">log in</a></span> <a href="LogOut.aspx">log out</a></div>
                <div><span style="padding-right: 2em"><a href="CreateUser.aspx">Create songbook</a></span> <a href="About.aspx">About</a></div>
                <p>
                    </p>
                <p>
                    <asp:Button CssClass="btn-default" ID ="ButtonGoToSeeAllSongBooks" runat="server" OnClick="ButtonGoToSeeAllSongBooks_Click" Text="Song Books" Width="200px" Font-Bold="True" Font-Size="14pt" Height="79px" Visible="False" />
                </p>
                
                <p>
                    <asp:Button CssClass="btn-default" ID="ButtonAdmin" runat="server" OnClick="ButtonAdmin_Click" Text="Admin" Width="200px" Font-Bold="True" Font-Size="14pt" Height="40px" Visible="False" />
                </p>
            </div>
        </div>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-54970963-3', 'auto');
        ga('send', 'pageview');
</script>
    </form>
    </body>


</html>
