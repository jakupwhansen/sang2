﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing.Imaging;


namespace Sang
{
    public partial class NewSong : System.Web.UI.Page
    {
        int defaultSize = 16;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Bruger"] == null)
            {
                Server.Transfer("Default.aspx");
            }
            else
            {
                //-----------DIREKTE LINK----------------------------------------
                Bruger brug = (Bruger)Session["Bruger"];
                if (brug.ID == -1) //Så kommer man fra direkte link. Må stoppes.
                {
                    Session["Bruger"] = null;
                    return;
                }
                //------------------------------------------------------------------
            }
        }
        static bool kanBruges(string value)
        {
            bool svar = true;
            //Bruges ikke alligevle.....
            //const string quote = "\"";
            //svar = Regex.IsMatch(value, @"^[-a-zA-Z0-9' ''–'“„,.;?!:äÐæÆøØåÅðÐíÍýÝáÁúÚóÓ" + quote + Environment.NewLine + "]*$");
            if (value.Contains("<") || value.Contains(">"))
            {
                svar = false;
            }

            return svar;
        }
        protected void xymetoden(Object sender, ImageClickEventArgs e)
        {
            //Label_Besked.Visible = true;
            //Label_Besked.Text = "X: " + e.X + " Y:" + e.Y;
        }
        protected void Btn_SaveLine_Click(object sender, EventArgs e)
        {
            Btn_CleanText.Visible = false;
            saveLinesMethod();
        }
        private void saveLinesMethod()
        {
            Label_Besked.Text = "";
            if (!kanBruges(TextBoxTitel.Text))
            {
                Label_Besked.Visible = true;
                Label_Besked.Text = Label_Besked.Text + Environment.NewLine + "Some characters in the title are not allowed";
                return;
            }
            if (TextBoxTitel.Text.Length < 2)
            {
                Label_Besked.Visible = true;
                Label_Besked.Text = Label_Besked.Text + Environment.NewLine + "Title missing. A song must have a title.";
                return;
            }
            if (!kanBruges(TextBoxLyric.Text))
            {
                Label_Besked.Visible = true;
                Label_Besked.Text = Label_Besked.Text + Environment.NewLine + "Some characters in the lyric owner are not allowed";
                return;
            }
            if (!kanBruges(TextBoxMelody.Text))
            {
                Label_Besked.Visible = true;
                Label_Besked.Text = Label_Besked.Text + Environment.NewLine + "Some characters in the melody owner are not allowed";
                return;
            }


            if (!kanBruges(Txt_SongLine.Text))
            {
                Label_Besked.Visible = true;
                Label_Besked.Text = "Some characters in the Song text are not allowed";
                return;
            }

            //if (Session["Sang"] == null)
            //{
            //    Session["Sang"] = new Sang();
            //}
            Session["Sang"] = new Sang();

            Sang sang = (Sang)Session["Sang"];
            sang.title = TextBoxTitel.Text;
            sang.lyricWriter = TextBoxLyric.Text;
            sang.melodyWriter = TextBoxMelody.Text;

            //----Teksten må splittes i linjer--------------
            String gemmer = Txt_SongLine.Text;
            String[] splitter = new String[] { Environment.NewLine };
            String[] gemmerArray = gemmer.Split(splitter, StringSplitOptions.None);
            int startPosX = 20;
            int startPosY = 120;
            int linjeRum = 35;
            int linjeGemmer = linjeRum;
            int i = 0;
            foreach (String stri in gemmerArray)
            {
                int lokalRum = linjeRum * i;
                i++;

                TextLinje t = new TextLinje();
                t.text = stri;

                //----2016.07.16-----Auto find chords in text---------
                if (linjeHarAkkorder(t.text))
                {  //Så har vi fundet en linje med akkorder.
                   //Så skal akkorderne findes
                   //(kan være D#m7, vi har intil nu kun kikket på først Bogstav 'D')
                    String s = t.text;

                    List<String> listen = splitStringTilAkkorder(s);

                    int lengthSamlet = 0;
                    int akordNr = 1;

                    foreach (String str in listen)
                    {
                        if (str.Length > 0)
                        {
                            if (str[0].Equals(" "))
                            {

                            }
                            else
                            {
                                akordNr = findAkkordNummer(str[0].ToString());
                                if (akordNr != -1) //-1 betyder ikke fundet akkord.
                                {  //VI har en akkord her.
                                    String extra = "";
                                    int BassNr = -1;
                                    if (str.Length > 1)
                                    { //kompliceret akkord.
                                      //Kryds og Flat ordna vit her...
                                        String[] splittet = str.Split('/'); //Akkord og Bass splittes her.
                                        int pos = 1;
                                        if (splittet[0].Contains('#'))
                                        {
                                            pos++; //Extra må nu være fra pos 2.
                                            akordNr++;
                                            if (akordNr > 12) //B# vil give 13. Som skal være C=1
                                                akordNr = 1;
                                        }
                                        if (splittet[0].Contains('b'))
                                        {
                                            pos++; //Extra må nu være fra pos 2.
                                            akordNr--;
                                            if (akordNr < 1) //Cb vil give 0. som skal være B=12.
                                                akordNr = 12;
                                        }
                                        if (splittet[0].Length > pos)
                                        {  //Så ved vi at der er EXTRA
                                            try
                                            {
                                                extra = splittet[0].Substring(pos);
                                            }
                                            catch (Exception lkjd) { }
                                        }
                                        //------------------------
                                        if (splittet.Length > 1)
                                        {  //Så har vi Bass.
                                            BassNr = findAkkordNummer(splittet[1][0].ToString());
                                            if (splittet[1].Contains('#'))
                                            {
                                                BassNr++;
                                                if (BassNr > 12)
                                                    BassNr = 1;
                                            }
                                            if (splittet[1].Contains('b'))
                                            {
                                                BassNr--;
                                                if (BassNr < 1)
                                                    BassNr = 12;
                                            }
                                        }
                                        Akkord nyAk = new Akkord();

                                        nyAk.keyNumber = akordNr;
                                        if (extra.Length > 0)
                                            nyAk.extraName = extra;
                                        if (BassNr > 0 && BassNr < 13)
                                            nyAk.bass = BassNr;

                                        nyAk.pos = new Point(startPosX + lengthSamlet, startPosY + lokalRum);
                                        sang.listAkkorder.Add(nyAk);

                                    }
                                    else
                                    {   //Én bogstav akkord
                                        Akkord nyAk = new Akkord();
                                        nyAk.keyNumber = akordNr;
                                        nyAk.pos = new Point(startPosX + lengthSamlet, startPosY + lokalRum);
                                        sang.listAkkorder.Add(nyAk);
                                    }

                                }

                            }
                        }

                        lengthSamlet += Convert.ToInt32(MeasureString(str).Width);
                    }
                    t.text = "";
                }
                t.pos = new Point(startPosX, startPosY + lokalRum);
                t.id = i;
                sang.listTexter.Add(t);
            }

            Label_Besked.Text = "";
            Label_Besked.Visible = false;
            Btn_SaveLine.Visible = false;
            Txt_SongLine.Visible = false;
            Btn_NullStil.Visible = true;
            Image1.Src = "BitMapNewSong.aspx";
            Btn_AddChords.Visible = true;

            bool tilstand = false;
            TextBoxTitel.Visible = tilstand;
            TextBoxLyric.Visible = tilstand;
            TextBoxMelody.Visible = tilstand;
            Label2.Visible = tilstand;
            Label3.Visible = tilstand;
            Label4.Visible = tilstand;

        }
        private List<String> splitStringTilAkkorder(String text)
        {
            List<String> liste = new List<String>();
            String aktuel = "";
            bool akkordIgang = false;
            foreach (char c in text)
            {
                if (akkordIgang == false && findAkkordNummer(c.ToString()) != -1)
                {  //Akkord fundet.                       
                    liste.Add(aktuel);
                    akkordIgang = true;
                    aktuel = c.ToString();
                }
                else if (c.Equals(' '))
                {
                    if (akkordIgang == true)
                    {   //Akkord termineret nu. 

                        liste.Add(aktuel);
                        aktuel = c.ToString();
                        akkordIgang = false;
                    }
                    else
                    {
                        aktuel = aktuel + c;
                    }
                }
                else
                {
                    aktuel = aktuel + c;
                }
            }
            liste.Add(aktuel);
            return liste;
        }
        private int findAkkordNummer(String c)
        {
            int svar = -1;
            if (c.Equals("C"))
                svar = 1;
            if (c.Equals("D"))
                svar = 3;
            if (c.Equals("E"))
                svar = 5;
            if (c.Equals("F"))
                svar = 6;
            if (c.Equals("G"))
                svar = 8;
            if (c.Equals("A"))
                svar = 10;
            if (c.Equals("B") || c.Equals("H"))
                svar = 12;
            return svar;
        }
        private bool linjeHarAkkorder(String text)
        {
            bool svar = false;
            String[] listOfWords = splitStringToListOfWords(text);
            if (erStartBogstavFraListenEnAkkord(listOfWords))
            { //Chords
                svar = true;
                //---Har lukket denne mulighed...forvirrer for meget------
                //Btn_StretchPlus.Visible = true;
                //Btn_StretchMinus.Visible = true;
            }

            return svar;
        }
        private bool erStartBogstavFraListenEnAkkord(String[] arr)
        {
            bool svar = true;
            bool real = false;
            foreach (String st in arr)
            {
                if (st.Length > 0)
                {
                    real = true; //Så har vi været her inde.
                    if (st[0].Equals('A') || st[0].Equals('B') ||
                        st[0].Equals('C') || st[0].Equals('D') ||
                        st[0].Equals('E') || st[0].Equals('F') ||
                        st[0].Equals('G') || st[0].Equals('H')
                        )
                    {

                    }
                    else
                    {  //IKKE AKKORD.
                        svar = false;
                    }
                }
            }
            if (svar == true)
            {  //Kan stadigvæk være en tekst.
                foreach (String st in arr)
                {
                    if (st.Any(c =>
                            c == ',' || c == '.' || c == ';' ||
                            c == 'ø' || c == 'æ' || c == 'y' ||
                            c == 'k' || c == 'l' || c == 'n' ||
                            c == 'p' || c == 'q' || c == 'r' ||
                            c == 't' || c == 'v' || c == 'w' ||
                            c == 'x' || c == 'z' || c == 'z' ||
                            c == 'ð' || c == 'ó' || c == 'á' ||
                            c == 'å' || c == 'ú' || c == 'ý' ||
                            //--Store Bogstaver-----
                            c == 'Ø' || c == 'Æ' || c == 'Y' ||
                            c == 'K' || c == 'L' || c == 'N' ||
                            c == 'P' || c == 'Q' || c == 'R' ||
                            c == 'T' || c == 'V' || c == 'W' ||
                            c == 'X' || c == 'Z' || c == 'Z' ||
                            c == 'Ð' || c == 'Ó' || c == 'Á' ||
                            c == 'Å' || c == 'Ú' || c == 'Ý')
                           )
                    {

                        svar = false;
                        break;
                    }
                }
            }
            if (real == false)
            {
                svar = false;
            }
            return svar;
        }
        private String[] splitStringToListOfWords(String s)
        {
            int leng = s.Length + 1;
            while (s.Length < leng)
            {
                leng = s.Length;
                s = s.Replace("  ", " ");
            }
            string[] words = s.Split(' ');

            return words;
        }
        private SizeF MeasureString(String texten)
        {
            if (Session["FontSize"] == null)
            {
                Session["FontSize"] = defaultSize; //16 som default.
            }

            int fontSize = (int)Session["FontSize"];

        //    Font fontBanner = new Font("Consolas", fontSize, FontStyle.Regular);
            Font fontBanner = new Font("Verdana", fontSize, FontStyle.Regular);
            string measureString = texten;

            // Measure string.
            SizeF stringSize = new SizeF();

            StringFormat sf = StringFormat.GenericTypographic;
            sf.Trimming = StringTrimming.None;
            sf.FormatFlags = StringFormatFlags.MeasureTrailingSpaces;

            Graphics grfx = Graphics.FromImage(new Bitmap(200, 200));
//            TextRenderingHintAntiAlias
//            grfx.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;
            grfx.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
            StringFormat sFormat = new StringFormat(StringFormat.GenericTypographic);
            PointF origin = new PointF(0, 0);
            stringSize = grfx.MeasureString(measureString, fontBanner, origin, sf);
            fontBanner.Dispose();
            grfx.Dispose();

            return stringSize;
        }


        protected void Btn_NullStil_Click(object sender, EventArgs e)
        {
            Session["Sang"] = null;

            Btn_AddChords.Visible = false;
            Label_Besked.Text = "";
            Label_Besked.Visible = true;
            Btn_SaveLine.Visible = true;
            Txt_SongLine.Visible = true;
            Btn_NullStil.Visible = false;

            bool tilstand = true;
            TextBoxTitel.Visible = tilstand;
            TextBoxLyric.Visible = tilstand;
            TextBoxMelody.Visible = tilstand;
            Label2.Visible = tilstand;
            Label3.Visible = tilstand;
            Label4.Visible = tilstand;
            Btn_StretchPlus.Visible = false;
            Btn_StretchMinus.Visible = false;

        }
        protected void Btn_AddChords_Click(object sender, EventArgs e)
        {
            Server.Transfer("NewSongAddChords.aspx");
        }
        protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {

        }
        protected void ButtonBack_Click(object sender, EventArgs e)
        {
            Server.Transfer("Default.aspx");
        }

        protected void Btn_StretchPlus_Click(object sender, EventArgs e)
        {
            if (Session["FontSize"] == null)
            {
                Session["FontSize"] = defaultSize;
            }
            int fontSize = (int)Session["FontSize"];
            fontSize++;
            Session["FontSize"] = fontSize;
            saveLinesMethod();
        }

        protected void Btn_StretchMinus_Click(object sender, EventArgs e)
        {
            if (Session["FontSize"] == null)
            {
                Session["FontSize"] = defaultSize;
            }
            int fontSize = (int)Session["FontSize"];
            fontSize--;
            Session["FontSize"] = fontSize;
            saveLinesMethod();
        }

        protected void Btn_CleanText_Click(object sender, EventArgs e)
        {
            // Fjerner alle PAB her---------------------
            Txt_SongLine.Text = Txt_SongLine.Text.Replace("\t", " ");
            //--------------------------------------------
            Btn_CleanText.Visible = false;
            //---------------------------------------

            String gemmer = Txt_SongLine.Text;
            String[] splitter = new String[] { Environment.NewLine };
            String[] gemmerArray = gemmer.Split(splitter, StringSplitOptions.None);
            List<String> listeNy = new List<string>();
            foreach (String stri in gemmerArray)
            {
                if (linjeHarAkkorder(stri))
                {

                    List<String> listen = splitStringTilAkkorder(stri);
                    String SamletSammen = "";
                    foreach (String str in listen)
                    {
                        if (str.Length > 0)
                        {
                            String gem = str;
                            if (str[0].Equals(' '))// Starter med mellemrum, så er det ikke en akkord.
                            {  //Så skal vi lægge halvdele af tomrum til.
                                // En primitiv måde at lave Consolas om til Verdana..ikke helt perfekt.
                                gem = str.Replace("  ", "   ");
                            }
                            SamletSammen += gem;
                        }
                    }
                    listeNy.Add(SamletSammen);

                }
                else
                {
                    listeNy.Add(stri);
                }
            }
            String vis = "";
            foreach(String s in listeNy)
            {
                vis += s + Environment.NewLine;
            }
            Txt_SongLine.Text = vis; 
        }

        protected void Txt_SongLine_TextChanged(object sender, EventArgs e)
        {

        }
    }
    class Sang
    {
        public List<TextLinje> listTexter = new List<TextLinje>();
        public List<Akkord> listAkkorder = new List<Akkord>();
        public List<NodeLinjer> listNodeLinjer = new List<NodeLinjer>();
        public String title = "";
        public String lyricWriter = "";
        public String melodyWriter = "";
        public int userID = 0;
        public NodeLinjer aktuelNodeLinje;
        public int type = 0; //Tilstand som sangen har.
        public int ID = 0; //Alle sange har sit eget ID.
        public int width = 300; //Default, always overwritten
        public int height = 400; //Default, always overwritten
        public int afstanImellemNoderne = 50;
        public int antalNoderPrNodeLinje = 0;
        private int firstLinjeX = 20;
        private int firstLinjeY = 100;
        public int startAfstandX = 200;
        public int extraMargen = 50;
        public int nodeLinjePosXStart = 5;
        public int nodeLinjePosYStart = 150;
        public int nodeLinjePosYimellemNodeLinjer = 250;
        public int tempo = 4;
        public bool tegneNodeNavn = false;
        public void textAutoInsert(String text)
        {
           // text = text.Replace("  ", " ");
            String[] listen = text.Split(' ');
            int NodeLinje = 0;
            NodeLinjer nl = listNodeLinjer[NodeLinje];
            int Nodepos = 0;
            listTexter.Clear();

            for (int i = 0; i < listen.Length; i++)
            {
                String s = listen[i];
                if (Nodepos > nl.listNoder.Count - 1)
                {
                    NodeLinje++;
                    if (NodeLinje >= listNodeLinjer.Count )
                    {
                        break;
                        // SÅ er der ikke flere noder.
                    }
                    else
                    {
                        //Så bruger vi næste nodeLinjer.
                        nl = listNodeLinjer[NodeLinje];
                        Nodepos = 0;
                    }

                }
                if (nl.listNoder[Nodepos].length > 100 || nl.listNoder[Nodepos].length < 0)//Pauser > 100 og -1 er taktlinje.
                {
                    i--;
                }
                else
                {
                    TextLinje tl = new TextLinje();
                    tl.text = s;
                    SizeF sf = MeasureString(s, 16);
                    float fl = sf.Width/2;
                    int flint = Convert.ToInt32(fl);
                    int x = nl.listNoder[Nodepos].pos.X - flint + 15;
                    int y = nl.pos.Y + 200;

                    tl.pos = new Point(x, y);
                    listTexter.Add(tl);
                }
                Nodepos++;
            }

        }
        private SizeF MeasureString(String texten, int fontSize)
        {
            Font drawFont = new Font("Arial", fontSize, FontStyle.Regular);
            // Set up string.
            string measureString = texten;

            // Measure string.
            SizeF stringSize = new SizeF();

            Graphics grfx = Graphics.FromImage(new Bitmap(1, 1));

            stringSize = grfx.MeasureString(measureString, drawFont);

            drawFont.Dispose();
            grfx.Dispose();

            return stringSize;
        }

        public TextLinje textCollision(Point ind, int width, int height)
        {
            TextLinje svar = null;
            foreach (TextLinje tl in listTexter)
            {
                if (Collision(tl.pos, ind, width, height))
                {
                    svar = tl;
                    svar.editIgang = true;
                    break;
                }
            }
            return svar;
        }

        public Akkord chordCollision(Point ind, int width, int height)
        {
            Akkord svar = null;
            foreach (Akkord ak in listAkkorder)
            {
                if (Collision(ak.pos, ind, width, height))
                {
                    svar = ak;
                    svar.editIgang = true;
                    break;
                }
            }
            return svar;
        }
        public Noder nodeCollision(Point ind, int width, int height)
        {
            Noder svar = null;
            foreach (NodeLinjer nl in listNodeLinjer)
            {
                foreach (Noder no in nl.listNoder)
                {
                    if (Collision(no.pos, ind, width, height))
                    {
                        svar = no;
                        svar.editIgang = true;
                        break;
                    }
                }
            }

            return svar;
        }

        private bool Collision(Point a, Point b, int width, int height)
        {
            bool svar = false;

            if (a.X - b.X > 0 && a.X - b.X < width || b.X - a.X < width && b.X - a.X > 0)
            {
                if (a.Y - b.Y > 0 && a.Y - b.Y < height || b.Y - a.Y < height && b.Y - a.Y > 0)
                {
                    svar = true;
                }
            }
            return svar;
        }


        public void nullstillAkkorderTilNoder()
        {
            foreach (NodeLinjer nl in listNodeLinjer)
            {
                foreach (Noder no in nl.listNoder)
                {
                    no.akkord = null;
                }
            }
        }
        public void callibrerAkkorderTilNoder()
        {   //Bruges således at en akkord tilknyttes en Noder. Så
            //kan midi nemlig også spille akkorder.
            List<Akkord> fundetAkkorder = new List<Akkord>();
            int distance = afstanImellemNoderne / 2;

            //---FØRST MÅ ALLE NODER NULLSTILLE AKKORDER.-------
            nullstillAkkorderTilNoder();

            //----SÅ Kalibrer vi akkorderne--------------------------
            int flytX = 10;
            foreach (NodeLinjer nl in listNodeLinjer)
            {
                foreach (Noder no in nl.listNoder)
                {
                    foreach (Akkord ak in listAkkorder)
                    {
                        if (ak.pos.Y < no.pos.Y) //Mulig kandidat fundet.
                        {
                            if (fundetAkkorder.Contains(ak) == false)
                            {
                                if ((ak.pos.X - flytX) - no.pos.X >= -distance && (ak.pos.X - flytX) - no.pos.X < distance)
                                {
                                    no.akkord = ak;
                                    ak.pos.X = no.pos.X + flytX; //Flytter akkorden, så man kan se om den er fundet.
                                    fundetAkkorder.Add(ak);

                                }
                            }
                        }
                    }
                }

            }
        }

        public void opdaterNoderAfterKeyChange()
        {
            foreach (NodeLinjer nl in listNodeLinjer)
            {
                foreach (Noder nr in nl.listNoder)
                {
                    nr.kalibrer();//IGEN
                }
            }
        }
        public void paintStaf(Graphics objGraphics)
        {  //Paint noder here også.
            foreach (NodeLinjer nl in listNodeLinjer)
            {
                nl.paintMe(objGraphics);
            }

            bool paintExtraInfo = false;//Bruges til test.

            foreach (NodeLinjer nl in listNodeLinjer)
            {
                for (int i = 0; i < nl.listNoder.Count; i++)
                {
                    Noder node = nl.listNoder[i];
                    if (node.length == -1) //Taktstrege
                    {
                        Pen _notePen = new Pen(Color.Black, 2);
                        int yPos = node.parrentNodeLinje.pos.Y + 69;
                        int xPos = node.pos.X + 20;
                        objGraphics.DrawLine(_notePen, xPos, yPos, xPos, yPos + 82);
                        _notePen.Dispose();
                    }
                    //---------------NODER NODER NODER NODER NODER-----------------------
                    //---------------NODER NODER NODER NODER NODER-----------------------
                    //---------------NODER NODER NODER NODER NODER-----------------------
                    //---------------NODER NODER NODER NODER NODER-----------------------
                    //---------------NODER NODER NODER NODER NODER-----------------------

                    if (node.editIgang == true) //EDIT af Node----EDIT af node-----
                    {
                        SolidBrush drawBrush = new SolidBrush(Color.Red);
                        objGraphics.FillEllipse(drawBrush, node.pos.X, node.pos.Y, 7, 7);
                        drawBrush.Dispose();
                    }
                    if (node.length == 48) //  16/16 Dot = 24/16
                    {
                        string filePath = HttpContext.Current.Server.MapPath("images/Full.gif");
                        Bitmap imgb = new Bitmap(filePath, true);
                        imgb.MakeTransparent();
                        objGraphics.DrawImage(imgb, node.pos.X, node.pos.Y - 5, 40, 30);
                        //-------------------DOT-----------------------------------
                        int extraIfDownDot = 63;
                        SolidBrush drawBrush = new SolidBrush(Color.Black);
                        objGraphics.FillEllipse(drawBrush, node.pos.X + 35, node.pos.Y - 62 + extraIfDownDot, 7, 7);
                        drawBrush.Dispose();
                        imgb.Dispose();
                        //---------------TEGNE SHARP or FLAT  FRA BILLEDE-----------------------
                        if (node.sharpOrFlat != 0) //0 = ikke valgt.
                        {
                            if (node.sharpOrFlat == 1)
                            {
                                filePath = HttpContext.Current.Server.MapPath("images/sharp.gif");
                                imgb = new Bitmap(filePath, true);
                                imgb.MakeTransparent();
                                objGraphics.DrawImage(imgb, node.pos.X - 18, node.pos.Y - 15, 25, 50);
                                imgb.Dispose();
                            }
                            else if (node.sharpOrFlat == -1)
                            {
                                filePath = HttpContext.Current.Server.MapPath("images/Flat.gif");
                                imgb = new Bitmap(filePath, true);
                                imgb.MakeTransparent();
                                objGraphics.DrawImage(imgb, node.pos.X - 17, node.pos.Y - 25, 25, 50);
                                imgb.Dispose();
                            }
                        }

                    }
                    if (node.length == 32) //  16/16 = 4/4
                    {
                        string filePath = HttpContext.Current.Server.MapPath("images/Full.gif");
                        Bitmap imgb = new Bitmap(filePath, true);
                        imgb.MakeTransparent();
                        objGraphics.DrawImage(imgb, node.pos.X, node.pos.Y - 5, 40, 30);
                        imgb.Dispose();
                        //---------------TEGNE SHARP or FLAT  FRA BILLEDE-----------------------
                        if (node.sharpOrFlat != 0) //0 = ikke valgt.
                        {
                            if (node.sharpOrFlat == 1)
                            {
                                filePath = HttpContext.Current.Server.MapPath("images/sharp.gif");
                                imgb = new Bitmap(filePath, true);
                                imgb.MakeTransparent();
                                objGraphics.DrawImage(imgb, node.pos.X - 18, node.pos.Y - 15, 25, 50);
                                imgb.Dispose();
                            }
                            else if (node.sharpOrFlat == -1)
                            {
                                filePath = HttpContext.Current.Server.MapPath("images/Flat.gif");
                                imgb = new Bitmap(filePath, true);
                                imgb.MakeTransparent();
                                objGraphics.DrawImage(imgb, node.pos.X - 17, node.pos.Y - 25, 25, 50);
                                imgb.Dispose();
                            }
                        }

                    }
                    if (node.length == 24) // 12/16 Half Dot 
                    {
                        //---------------TEGNE NY SLAGS NODE FRA BILLEDE-----------------------
                        string filePath = HttpContext.Current.Server.MapPath("images/HalfNote.gif");
                        int extraIfDown = 0;
                        if (node.StafNumber > 14)
                        {
                            filePath = HttpContext.Current.Server.MapPath("images/HalfNoteDown.gif");
                            extraIfDown = 50;
                        }
                        Bitmap imgb = new Bitmap(filePath, true);
                        imgb.MakeTransparent();
                        objGraphics.DrawImage(imgb, node.pos.X, node.pos.Y - 57 + extraIfDown, 30, 85);
                        //-------------------DOT-----------------------------------
                        int extraIfDownDot = 63;
                        SolidBrush drawBrush = new SolidBrush(Color.Black);
                        objGraphics.FillEllipse(drawBrush, node.pos.X + 35, node.pos.Y - 62 + extraIfDownDot, 7, 7);
                        drawBrush.Dispose();
                        imgb.Dispose();
                        //---------------TEGNE SHARP or FLAT  FRA BILLEDE-----------------------
                        if (node.sharpOrFlat != 0) //0 = ikke valgt.
                        {
                            if (node.sharpOrFlat == 1)
                            {
                                filePath = HttpContext.Current.Server.MapPath("images/sharp.gif");
                                imgb = new Bitmap(filePath, true);
                                imgb.MakeTransparent();
                                objGraphics.DrawImage(imgb, node.pos.X - 18, node.pos.Y - 15, 25, 50);
                                imgb.Dispose();
                            }
                            else if (node.sharpOrFlat == -1)
                            {
                                filePath = HttpContext.Current.Server.MapPath("images/Flat.gif");
                                imgb = new Bitmap(filePath, true);
                                imgb.MakeTransparent();
                                objGraphics.DrawImage(imgb, node.pos.X - 17, node.pos.Y - 25, 25, 50);
                                imgb.Dispose();
                            }
                        }
                    }

                    if (node.length == 16) // 8/16
                    {
                        //---------------TEGNE NY SLAGS NODE FRA BILLEDE-----------------------
                        string filePath = HttpContext.Current.Server.MapPath("images/HalfNote.gif");
                        int extraIfDown = 0;
                        if (node.StafNumber > 14)
                        {
                            filePath = HttpContext.Current.Server.MapPath("images/HalfNoteDown.gif");
                            extraIfDown = 50;
                        }
                        Bitmap imgb = new Bitmap(filePath, true);
                        imgb.MakeTransparent();
                        objGraphics.DrawImage(imgb, node.pos.X, node.pos.Y - 57 + extraIfDown, 30, 85);
                        imgb.Dispose();
                        //---------------TEGNE SHARP or FLAT  FRA BILLEDE-----------------------
                        if (node.sharpOrFlat != 0) //0 = ikke valgt.
                        {
                            if (node.sharpOrFlat == 1)
                            {
                                filePath = HttpContext.Current.Server.MapPath("images/sharp.gif");
                                imgb = new Bitmap(filePath, true);
                                imgb.MakeTransparent();
                                objGraphics.DrawImage(imgb, node.pos.X - 18, node.pos.Y - 15, 25, 50);
                                imgb.Dispose();
                            }
                            else if (node.sharpOrFlat == -1)
                            {
                                filePath = HttpContext.Current.Server.MapPath("images/Flat.gif");
                                imgb = new Bitmap(filePath, true);
                                imgb.MakeTransparent();
                                objGraphics.DrawImage(imgb, node.pos.X - 17, node.pos.Y - 25, 25, 50);
                                imgb.Dispose();
                            }
                        }
                    }
                    else if (node.length == 12) //  6/16 Quart Dot.
                    {
                        //---------------TEGNE NY SLAGS NODE FRA BILLEDE-----------------------
                        string filePath = HttpContext.Current.Server.MapPath("images/KvartNode.gif");
                        int extraIfDown = 0;
                        if (node.StafNumber > 14)
                        {
                            filePath = HttpContext.Current.Server.MapPath("images/KvartNodeDown.gif");
                            extraIfDown = 53;
                        }

                        Bitmap imgb = new Bitmap(filePath, true);
                        imgb.MakeTransparent();
                        objGraphics.DrawImage(imgb, node.pos.X, node.pos.Y - 62 + extraIfDown, 40, 90);
                        //-------------------DOT-----------------------------------
                        int extraIfDownDot = 63;
                        SolidBrush drawBrush = new SolidBrush(Color.Black);
                        objGraphics.FillEllipse(drawBrush, node.pos.X + 35, node.pos.Y - 62 + extraIfDownDot, 7, 7);
                        drawBrush.Dispose();
                        imgb.Dispose();
                        //---------------TEGNE SHARP or FLAT  FRA BILLEDE-----------------------
                        if (node.sharpOrFlat != 0) //0 = ikke valgt.
                        {
                            if (node.sharpOrFlat == 1)
                            {
                                filePath = HttpContext.Current.Server.MapPath("images/sharp.gif");
                                imgb = new Bitmap(filePath, true);
                                imgb.MakeTransparent();
                                objGraphics.DrawImage(imgb, node.pos.X - 18, node.pos.Y - 15, 25, 50);
                                imgb.Dispose();
                            }
                            else if (node.sharpOrFlat == -1)
                            {
                                filePath = HttpContext.Current.Server.MapPath("images/Flat.gif");
                                imgb = new Bitmap(filePath, true);
                                imgb.MakeTransparent();
                                objGraphics.DrawImage(imgb, node.pos.X - 17, node.pos.Y - 25, 25, 50);
                                imgb.Dispose();
                            }
                        }

                    }
                    else if (node.length == 8) // 4/16
                    {
                        //---------------TEGNE NY SLAGS NODE FRA BILLEDE-----------------------
                        string filePath = HttpContext.Current.Server.MapPath("images/KvartNode.gif");
                        int extraIfDown = 0;
                        if (node.StafNumber > 14)
                        {
                            filePath = HttpContext.Current.Server.MapPath("images/KvartNodeDown.gif");
                            extraIfDown = 53;
                        }

                        Bitmap imgb = new Bitmap(filePath, true);
                        imgb.MakeTransparent();
                        objGraphics.DrawImage(imgb, node.pos.X, node.pos.Y - 62 + extraIfDown, 40, 90);
                        imgb.Dispose();
                        //---------------TEGNE SHARP or FLAT  FRA BILLEDE-----------------------
                        if (node.sharpOrFlat != 0) //0 = ikke valgt.
                        {
                            if (node.sharpOrFlat == 1)
                            {
                                filePath = HttpContext.Current.Server.MapPath("images/sharp.gif");
                                imgb = new Bitmap(filePath, true);
                                imgb.MakeTransparent();
                                objGraphics.DrawImage(imgb, node.pos.X - 18, node.pos.Y - 15, 25, 50);
                                imgb.Dispose();
                            }
                            else if (node.sharpOrFlat == -1)
                            {
                                filePath = HttpContext.Current.Server.MapPath("images/Flat.gif");
                                imgb = new Bitmap(filePath, true);
                                imgb.MakeTransparent();
                                objGraphics.DrawImage(imgb, node.pos.X - 17, node.pos.Y - 25, 25, 50);
                                imgb.Dispose();
                            }
                        }
                    }
                    else if (node.length == 4) // 2/16 = 1/8
                    {
                        //---------------TEGNE NY SLAGS NODE FRA BILLEDE-----------------------
                        string filePath = HttpContext.Current.Server.MapPath("images/EightNote.gif");
                        int extraIfDown = 0;
                        int extraToLeft = 0;
                        if (node.StafNumber > 14)
                        {
                            filePath = HttpContext.Current.Server.MapPath("images/EightNoteDown.gif");
                            extraIfDown = 50;
                            extraToLeft = 10;
                        }
                        Bitmap imgb = new Bitmap(filePath, true);
                        imgb.MakeTransparent();
                        objGraphics.DrawImage(imgb, node.pos.X - extraToLeft, node.pos.Y - 60 + extraIfDown, 50, 90);
                        imgb.Dispose();
                        //---------------TEGNE SHARP or FLAT  FRA BILLEDE-----------------------
                        if (node.sharpOrFlat != 0) //0 = ikke valgt.
                        {
                            if (node.sharpOrFlat == 1)
                            {
                                filePath = HttpContext.Current.Server.MapPath("images/sharp.gif");
                                imgb = new Bitmap(filePath, true);
                                imgb.MakeTransparent();
                                objGraphics.DrawImage(imgb, node.pos.X - 18, node.pos.Y - 15, 25, 50);
                                imgb.Dispose();
                            }
                            else if (node.sharpOrFlat == -1)
                            {
                                filePath = HttpContext.Current.Server.MapPath("images/Flat.gif");
                                imgb = new Bitmap(filePath, true);
                                imgb.MakeTransparent();
                                objGraphics.DrawImage(imgb, node.pos.X - 17, node.pos.Y - 25, 25, 50);
                                imgb.Dispose();
                            }
                        }
                    }
                    else if (node.length == 6) // 3/16 = 1/8 Dot
                    {
                        //---------------TEGNE NY SLAGS NODE FRA BILLEDE-----------------------
                        string filePath = HttpContext.Current.Server.MapPath("images/EightNote.gif");
                        int extraIfDown = 0;
                        int extraToLeft = 0;
                        if (node.StafNumber > 14)
                        {
                            filePath = HttpContext.Current.Server.MapPath("images/EightNoteDown.gif");
                            extraIfDown = 50;
                            extraToLeft = 10;
                        }
                        Bitmap imgb = new Bitmap(filePath, true);
                        imgb.MakeTransparent();
                        objGraphics.DrawImage(imgb, node.pos.X - extraToLeft, node.pos.Y - 60 + extraIfDown, 50, 90);
                        //-------------------DOT-----------------------------------
                        int extraIfDownDot = 63;
                        SolidBrush drawBrush = new SolidBrush(Color.Black);
                        objGraphics.FillEllipse(drawBrush, node.pos.X + 35, node.pos.Y - 62 + extraIfDownDot, 7, 7);
                        drawBrush.Dispose();

                        imgb.Dispose();
                        //---------------TEGNE SHARP or FLAT  FRA BILLEDE-----------------------
                        if (node.sharpOrFlat != 0) //0 = ikke valgt.
                        {
                            if (node.sharpOrFlat == 1)
                            {
                                filePath = HttpContext.Current.Server.MapPath("images/sharp.gif");
                                imgb = new Bitmap(filePath, true);
                                imgb.MakeTransparent();
                                objGraphics.DrawImage(imgb, node.pos.X - 18, node.pos.Y - 15, 25, 50);
                                imgb.Dispose();
                            }
                            else if (node.sharpOrFlat == -1)
                            {
                                filePath = HttpContext.Current.Server.MapPath("images/Flat.gif");
                                imgb = new Bitmap(filePath, true);
                                imgb.MakeTransparent();
                                objGraphics.DrawImage(imgb, node.pos.X - 17, node.pos.Y - 25, 25, 50);
                                imgb.Dispose();
                            }
                        }
                    }
                    else if (node.length == 3) // 3/32
                    {
                        //---------------TEGNE NY SLAGS NODE FRA BILLEDE-----------------------
                        string filePath = HttpContext.Current.Server.MapPath("images/SixteentNode.gif");
                        int extraIfDown = -3;
                        int extraToLeft = 0;
                        int extraDotRight = -6;
                        if (node.StafNumber > 14)
                        {
                            filePath = HttpContext.Current.Server.MapPath("images/SixteentNodeDown.gif");
                            extraIfDown = 53;
                            extraToLeft = 10;
                            extraDotRight = 3;
                        }
                        Bitmap imgb = new Bitmap(filePath, true);
                        imgb.MakeTransparent();
                        objGraphics.DrawImage(imgb, node.pos.X - extraToLeft, node.pos.Y - 60 + extraIfDown, 50, 90);
                        //-------------------DOT-----------------------------------
                        int extraIfDownDot = 63;
                        SolidBrush drawBrush = new SolidBrush(Color.Black);
                        objGraphics.FillEllipse(drawBrush, node.pos.X + 35 + extraDotRight, node.pos.Y - 62 + extraIfDownDot, 7, 7);
                        drawBrush.Dispose();

                        imgb.Dispose();
                        //---------------TEGNE SHARP or FLAT  FRA BILLEDE-----------------------
                        if (node.sharpOrFlat != 0) //0 = ikke valgt.
                        {
                            if (node.sharpOrFlat == 1)
                            {
                                filePath = HttpContext.Current.Server.MapPath("images/sharp.gif");
                                imgb = new Bitmap(filePath, true);
                                imgb.MakeTransparent();
                                objGraphics.DrawImage(imgb, node.pos.X - 18, node.pos.Y - 15, 25, 50);
                                imgb.Dispose();
                            }
                            else if (node.sharpOrFlat == -1)
                            {
                                filePath = HttpContext.Current.Server.MapPath("images/Flat.gif");
                                imgb = new Bitmap(filePath, true);
                                imgb.MakeTransparent();
                                objGraphics.DrawImage(imgb, node.pos.X - 17, node.pos.Y - 25, 25, 50);
                                imgb.Dispose();
                            }
                        }
                    }

                    else if (node.length == 2) // 1/16
                    {
                        //---------------TEGNE NY SLAGS NODE FRA BILLEDE-----------------------
                        string filePath = HttpContext.Current.Server.MapPath("images/SixteentNode.gif");
                        int extraIfDown = -3;
                        int extraToLeft = 0;
                        if (node.StafNumber > 14)
                        {
                            filePath = HttpContext.Current.Server.MapPath("images/SixteentNodeDown.gif");
                            extraIfDown = 53;
                            extraToLeft = 10;
                        }
                        Bitmap imgb = new Bitmap(filePath, true);
                        imgb.MakeTransparent();
                        objGraphics.DrawImage(imgb, node.pos.X - extraToLeft, node.pos.Y - 60 + extraIfDown, 50, 90);
                        imgb.Dispose();
                        //---------------TEGNE SHARP or FLAT  FRA BILLEDE-----------------------
                        if (node.sharpOrFlat != 0) //0 = ikke valgt.
                        {
                            if (node.sharpOrFlat == 1)
                            {
                                filePath = HttpContext.Current.Server.MapPath("images/sharp.gif");
                                imgb = new Bitmap(filePath, true);
                                imgb.MakeTransparent();
                                objGraphics.DrawImage(imgb, node.pos.X - 18, node.pos.Y - 15, 25, 50);
                                imgb.Dispose();
                            }
                            else if (node.sharpOrFlat == -1)
                            {
                                filePath = HttpContext.Current.Server.MapPath("images/Flat.gif");
                                imgb = new Bitmap(filePath, true);
                                imgb.MakeTransparent();
                                objGraphics.DrawImage(imgb, node.pos.X - 17, node.pos.Y - 25, 25, 50);
                                imgb.Dispose();
                            }
                        }
                    }
                    //-----NODER som er under linjen, skal have en extra lille linje----------------------------
                    //-----NODER som er under linjen, skal have en extra lille linje----------------------------
                    //-----NODER som er under linjen, skal have en extra lille linje----------------------------
                    //-----NODER som er under linjen, skal have en extra lille linje----------------------------
                    //-----NODER som er under linjen, skal have en extra lille linje----------------------------
                    if (node.StafNumber < 9)
                    {
                        int Ypos = node.parrentNodeLinje.listePoint[0].Y + node.heightForDrawing + 2;
                        Pen _notePen = new Pen(Color.Black, 2);
                        objGraphics.DrawLine(_notePen, node.pos.X - 3, Ypos, node.pos.X + 35, Ypos);

                        if (node.StafNumber < 7)
                        {
                            Ypos = Ypos + node.heightForDrawing;
                            objGraphics.DrawLine(_notePen, node.pos.X - 3, Ypos, node.pos.X + 35, Ypos);
                        }
                        _notePen.Dispose();
                    }


                    //--------REST---REST------REST----REST----------------------------------------
                    //--------REST---REST------REST----REST----------------------------------------
                    //--------REST---REST------REST----REST----------------------------------------
                    //--------REST---REST------REST----REST----------------------------------------
                    //--------REST---REST------REST----REST----------------------------------------
                    else if (node.length == 148) //REST
                    {
                        String filePath = HttpContext.Current.Server.MapPath("images/WholeRest.gif");
                        Bitmap imgb = new Bitmap(filePath, true);
                        int yPos = node.parrentNodeLinje.pos.Y + 91;
                        int xPos = node.pos.X + 20;
                        //imgb.MakeTransparent();
                        objGraphics.DrawImage(imgb, xPos, yPos, 25, 10);
                        //-------------------DOT-----------------------------------
                        int extraY = 2;
                        int extraX = 32;
                        SolidBrush drawBrush = new SolidBrush(Color.Black);
                        objGraphics.FillEllipse(drawBrush, xPos + extraX, yPos + extraY, 7, 7);
                        drawBrush.Dispose();
                        imgb.Dispose();
                    }

                    else if (node.length == 132) //REST
                    {
                        String filePath = HttpContext.Current.Server.MapPath("images/WholeRest.gif");
                        Bitmap imgb = new Bitmap(filePath, true);
                        int yPos = node.parrentNodeLinje.pos.Y + 91;
                        int xPos = node.pos.X + 20;
                        //imgb.MakeTransparent();
                        objGraphics.DrawImage(imgb, xPos, yPos, 25, 10);
                        imgb.Dispose();
                    }
                    else if (node.length == 124) //REST Vender modsat af den ovenover. Dvs. at y er lidt forskudt.
                    {
                        String filePath = HttpContext.Current.Server.MapPath("images/WholeRest.gif");
                        Bitmap imgb = new Bitmap(filePath, true);
                        int yPos = node.parrentNodeLinje.pos.Y + 99;
                        int xPos = node.pos.X + 20;
                        //imgb.MakeTransparent();
                        objGraphics.DrawImage(imgb, xPos, yPos, 25, 10);
                        //-------------------DOT-----------------------------------
                        int extraY = 0;
                        int extraX = 32;
                        SolidBrush drawBrush = new SolidBrush(Color.Black);
                        objGraphics.FillEllipse(drawBrush, xPos + extraX, yPos + extraY, 7, 7);
                        drawBrush.Dispose();
                        imgb.Dispose();
                    }
                    else if (node.length == 116) //REST Vender modsat af den ovenover. Dvs. at y er lidt forskudt.
                    {
                        String filePath = HttpContext.Current.Server.MapPath("images/WholeRest.gif");
                        Bitmap imgb = new Bitmap(filePath, true);
                        int yPos = node.parrentNodeLinje.pos.Y + 99;
                        int xPos = node.pos.X + 20;
                        //imgb.MakeTransparent();
                        objGraphics.DrawImage(imgb, xPos, yPos, 25, 10);
                        imgb.Dispose();
                    }
                    else if (node.length == 112) //REST
                    {
                        String filePath = HttpContext.Current.Server.MapPath("images/KvartRest.gif");
                        Bitmap imgb = new Bitmap(filePath, true);
                        int yPos = node.parrentNodeLinje.pos.Y + 74;
                        int xPos = node.pos.X + 10;
                        imgb.MakeTransparent();
                        objGraphics.DrawImage(imgb, xPos, yPos, 25, 70);
                        //-------------------DOT-----------------------------------
                        int extraY = 20;
                        int extraX = 27;
                        SolidBrush drawBrush = new SolidBrush(Color.Black);
                        objGraphics.FillEllipse(drawBrush, xPos + extraX, yPos + extraY, 7, 7);
                        drawBrush.Dispose();
                        imgb.Dispose();
                    }
                    else if (node.length == 108) //REST
                    {
                        String filePath = HttpContext.Current.Server.MapPath("images/KvartRest.gif");
                        Bitmap imgb = new Bitmap(filePath, true);
                        int yPos = node.parrentNodeLinje.pos.Y + 74;
                        int xPos = node.pos.X + 10;
                        imgb.MakeTransparent();
                        objGraphics.DrawImage(imgb, xPos, yPos, 25, 70);
                        imgb.Dispose();
                    }
                    else if (node.length == 106) //REST
                    {
                        String filePath = HttpContext.Current.Server.MapPath("images/EightRest.gif");
                        Bitmap imgb = new Bitmap(filePath, true);
                        int yPos = node.parrentNodeLinje.pos.Y + 88;
                        int xPos = node.pos.X + 10;
                        imgb.MakeTransparent();
                        objGraphics.DrawImage(imgb, xPos, yPos, 25, 40);
                        //-------------------DOT-----------------------------------
                        int extraY = 10;
                        int extraX = 27;
                        SolidBrush drawBrush = new SolidBrush(Color.Black);
                        objGraphics.FillEllipse(drawBrush, xPos + extraX, yPos + extraY, 7, 7);
                        drawBrush.Dispose();
                        imgb.Dispose();
                    }
                    else if (node.length == 104) //REST
                    {
                        String filePath = HttpContext.Current.Server.MapPath("images/EightRest.gif");
                        Bitmap imgb = new Bitmap(filePath, true);
                        int yPos = node.parrentNodeLinje.pos.Y + 88;
                        int xPos = node.pos.X + 10;
                        imgb.MakeTransparent();
                        objGraphics.DrawImage(imgb, xPos, yPos, 25, 40);
                        imgb.Dispose();
                    }
                    else if (node.length == 103) //REST
                    {
                        String filePath = HttpContext.Current.Server.MapPath("images/SixteenRest.gif");
                        Bitmap imgb = new Bitmap(filePath, true);
                        int yPos = node.parrentNodeLinje.pos.Y + 88;
                        int xPos = node.pos.X + 10;
                        imgb.MakeTransparent();
                        objGraphics.DrawImage(imgb, xPos, yPos, 35, 60);
                        //-------------------DOT-----------------------------------
                        int extraY = 10;
                        int extraX = 33;
                        SolidBrush drawBrush = new SolidBrush(Color.Black);
                        objGraphics.FillEllipse(drawBrush, xPos + extraX, yPos + extraY, 7, 7);
                        drawBrush.Dispose();
                        imgb.Dispose();
                    }

                    else if (node.length == 102) //REST
                    {
                        String filePath = HttpContext.Current.Server.MapPath("images/SixteenRest.gif");
                        Bitmap imgb = new Bitmap(filePath, true);
                        int yPos = node.parrentNodeLinje.pos.Y + 88;
                        int xPos = node.pos.X + 10;
                        imgb.MakeTransparent();
                        objGraphics.DrawImage(imgb, xPos, yPos, 35, 60);
                        imgb.Dispose();
                    }
                    //----Tegner stafNummer----------------------------
                    if (paintExtraInfo)
                    {
                        Font fontBanner = new Font("Verdana", 10, FontStyle.Regular | FontStyle.Regular);
                        SolidBrush solidBr = new SolidBrush(Color.Black);
                        String akkordFindes = "";
                        if (node.akkord != null)
                            akkordFindes = node.akkord.keyNumber.ToString();
                        String akkFind = "/akkord=" + akkordFindes;
                        akkFind = "";
                        objGraphics.DrawString(node.StafNumber.ToString() + "/" + node.keyNumber.ToString() + "/" + node.length.ToString() + akkFind, fontBanner, solidBr, node.pos.X + 2, node.pos.Y + 20);
                        fontBanner.Dispose();
                        solidBr.Dispose();
                    }
                    //----Tegner Node Navn----------------------------
                    if (tegneNodeNavn == true)
                    {
                        if (node.length > 0 && node.length < 100)
                        {
                            Font fontBanner = new Font("Verdana", 12, FontStyle.Regular | FontStyle.Bold);
                            SolidBrush solidBr = new SolidBrush(Color.Black);

                            objGraphics.DrawString(GeneralData.getNodeName(node.keyNumber), fontBanner, solidBr, node.pos.X + 2, node.pos.Y + 20);
                            fontBanner.Dispose();
                            solidBr.Dispose();
                        }
                    }

                }

            }
        }
        public void OpsetningAfSangMedNoder(int antalNoderPrNodeLinje)
        {
            this.antalNoderPrNodeLinje = antalNoderPrNodeLinje;
            width = antalNoderPrNodeLinje * afstanImellemNoderne + startAfstandX + extraMargen;
            title = "";
            lyricWriter = "";
            melodyWriter = "";
            TextLinje t1 = new TextLinje();
            t1.pos = new Point(firstLinjeX, firstLinjeY);
            t1.text = "";
            t1.id = 1;
            listTexter.Add(t1);
            if (aktuelNodeLinje != null)
            {
                aktuelNodeLinje.maxNumberOfNodes = antalNoderPrNodeLinje;
            }
        }
        public void newNodeLinje()
        {
            //Den første nodelinje
            NodeLinjer noli = new NodeLinjer(this);
            noli.length = antalNoderPrNodeLinje * afstanImellemNoderne + startAfstandX;
            noli.pos.X = nodeLinjePosXStart;
            if (listNodeLinjer.Count == 0)
            {
                noli.pos.Y = nodeLinjePosYStart;

            }
            else
            {
                noli.pos.Y = nodeLinjePosYStart + (nodeLinjePosYimellemNodeLinjer * listNodeLinjer.Count);
            }
            //Opdaterer linjeren med de nye x og y pos.
            noli.callibrer();
            aktuelNodeLinje = noli;
            listNodeLinjer.Add(noli);
            noli.maxNumberOfNodes = antalNoderPrNodeLinje;

        }
        public void changeSongLength(int numberofitems)
        {
            antalNoderPrNodeLinje = numberofitems;
            width = antalNoderPrNodeLinje * afstanImellemNoderne + startAfstandX + extraMargen;
            if (aktuelNodeLinje != null)
            {
                aktuelNodeLinje.maxNumberOfNodes = numberofitems;
                aktuelNodeLinje.length = antalNoderPrNodeLinje * afstanImellemNoderne + startAfstandX;
            }
        }
    }
    class TextLinje
    {
        public int id = 0;
        public Point pos = new Point(0, 0);
        public String text = "";
        public bool editIgang = false;
    }
    class Akkord
    {
        public int id = 0;
        public Point pos = new Point(0, 0);
        public int keyNumber = 0; //1 til 12
        public String extraName = ""; //like: sus, m7, Maj7.
        public int bass = 0;
        public bool editIgang = false;
        public List<int> getAkkordNodesToPlay()
        {
            List<int> svar = new List<int>();
            svar.Add(0);
            if (extraName.Equals("")) //Dur
            {
                svar.Add(4);
                svar.Add(7);
            }
            else if (extraName.Equals("m6")) //Dur
            {
                svar.Add(3);
                svar.Add(7);
                svar.Add(9);
            }
            else if (extraName.Equals("6")) //Dur
            {
                svar.Add(4);
                svar.Add(7);
                svar.Add(9);
            }
            else if (extraName.Equals("7")) //Dur7
            {
                svar.Add(4);
                svar.Add(7);
                svar.Add(10);
            }
            else if (extraName.Equals("m")) //Mol 
            {
                svar.Add(3);
                svar.Add(7);
            }
            else if (extraName.Equals("m7")) //Mol
            {
                svar.Add(3);
                svar.Add(7);
                svar.Add(10);
            }
            else if (extraName.Equals("Maj")) //Maj
            {
                svar.Add(4);
                svar.Add(7);
                svar.Add(11);
            }
            else if (extraName.Equals("Dim") || extraName.Equals("dim") || extraName.Equals("o") || extraName.Equals("ø")) //Dim
            {
                svar.Add(3);
                svar.Add(6);
            }
            else if (extraName.Equals("sus") || extraName.Equals("sus4")) //Dim
            {
                svar.Add(5);
                svar.Add(7);
            }
            else if (extraName.Equals("b5")) //Flat 5
            {
                svar.Add(4);
                svar.Add(6);
            }
            else if (extraName.Equals("aug") || extraName.Equals("AUG")) //Flat 5
            {
                svar.Add(4);
                svar.Add(8);
            }
            return svar;
        }
    }
    class NodeLinjer
    {
        public int ID;
        public Sang sang = null;
        public int key = 1; //Skal starte med C dur. Som er 1. rettet 2016.06.26.
        public int length = 0;
        public int maxNumberOfNodes = 0;

        //pos bestemmer hvor alle 5 linjerne skal placeres.
        public Point pos = new Point(0, 0);

        //Der er 5 Point i denne her liste, en for hver linje
        public List<Point> listePoint = new List<Point>();
        public List<Noder> listNoder = new List<Noder>();
        //        public List<Point> listGkey = new List<Point>();
        public Dictionary<int, int> listSheetToTone = new Dictionary<int, int>();
        public int[] ListgKeyArray = new int[4];
        public int[] ListFlat1Array = new int[4];
        public int[] ListFlat2Array = new int[4];
        public int[] ListFlat3Array = new int[4];
        public int[] ListFlat4Array = new int[4];
        public int[] ListFlat5Array = new int[4];
        public int[] ListSharp1Array = new int[4];
        public int[] ListSharp2Array = new int[4];
        public int[] ListSharp3Array = new int[4];
        public int[] ListSharp4Array = new int[4];
        public int[] ListSharp5Array = new int[4];
        public int[] ListSharp6Array = new int[4];
        int xMove = -299; //Default så placeres gnøglens g punkt på 0,0.
        int yMove = -173; //Default så placeres gnøglens g punkt på 0,0.
        public void paintMe(Graphics g)
        {
            //-----Linjerne-------------------------------------
            foreach (Point p in listePoint)
            {
                Pen _notePen = new Pen(Color.Black, 2);

                g.DrawLine(_notePen, p.X, p.Y, length, p.Y);
                _notePen.Dispose();
            }
            //------G-Nøgle---------------------------
            //------------NY udgave af G-Nøgle med gennemsigtigt billede------------
            //-----Transparent picture testing-------------------------------
            int fed = 5;
            Pen greenPen = new Pen(Color.Black, fed);

            string filePath = HttpContext.Current.Server.MapPath("images/clef.gif");
            Bitmap imgb = new Bitmap(filePath, true);
            imgb.MakeTransparent();

            g.DrawImage(imgb, ListgKeyArray[0], ListgKeyArray[1], ListgKeyArray[2], ListgKeyArray[3]);
            imgb.Dispose();

            //------ tegn FLAT--------------------------
            filePath = HttpContext.Current.Server.MapPath("images/Flat.gif");
            imgb = new Bitmap(filePath, true);
            imgb.MakeTransparent();


            fed = 3;
            greenPen = new Pen(Color.Black, fed);

            if (key == 6)
            {
                g.DrawImage(imgb, ListFlat1Array[0], ListFlat1Array[1], ListFlat1Array[2], ListFlat1Array[3]);
            }
            else if (key == 11)
            {
                g.DrawImage(imgb, ListFlat1Array[0], ListFlat1Array[1], ListFlat1Array[2], ListFlat1Array[3]);
                g.DrawImage(imgb, ListFlat2Array[0], ListFlat2Array[1], ListFlat2Array[2], ListFlat2Array[3]);
            }
            else if (key == 4)
            {
                g.DrawImage(imgb, ListFlat1Array[0], ListFlat1Array[1], ListFlat1Array[2], ListFlat1Array[3]);
                g.DrawImage(imgb, ListFlat2Array[0], ListFlat2Array[1], ListFlat2Array[2], ListFlat2Array[3]);
                g.DrawImage(imgb, ListFlat3Array[0], ListFlat3Array[1], ListFlat3Array[2], ListFlat3Array[3]);
            }
            else if (key == 9)
            {
                g.DrawImage(imgb, ListFlat1Array[0], ListFlat1Array[1], ListFlat1Array[2], ListFlat1Array[3]);
                g.DrawImage(imgb, ListFlat2Array[0], ListFlat2Array[1], ListFlat2Array[2], ListFlat2Array[3]);
                g.DrawImage(imgb, ListFlat3Array[0], ListFlat3Array[1], ListFlat3Array[2], ListFlat3Array[3]);
                g.DrawImage(imgb, ListFlat4Array[0], ListFlat4Array[1], ListFlat4Array[2], ListFlat4Array[3]);
            }
            else if (key == 2)
            {
                g.DrawImage(imgb, ListFlat1Array[0], ListFlat1Array[1], ListFlat1Array[2], ListFlat1Array[3]);
                g.DrawImage(imgb, ListFlat2Array[0], ListFlat2Array[1], ListFlat2Array[2], ListFlat2Array[3]);
                g.DrawImage(imgb, ListFlat3Array[0], ListFlat3Array[1], ListFlat3Array[2], ListFlat3Array[3]);
                g.DrawImage(imgb, ListFlat4Array[0], ListFlat4Array[1], ListFlat4Array[2], ListFlat4Array[3]);
                g.DrawImage(imgb, ListFlat5Array[0], ListFlat5Array[1], ListFlat5Array[2], ListFlat5Array[3]);
            }


            greenPen.Dispose();
            imgb.Dispose();

            //------ tegn SHARP--------------------------
            fed = 3;
            greenPen = new Pen(Color.Black, fed);

            filePath = HttpContext.Current.Server.MapPath("images/Sharp.gif");
            imgb = new Bitmap(filePath, true);
            imgb.MakeTransparent();


            if (key == 8)
            {
                g.DrawImage(imgb, ListSharp1Array[0], ListSharp1Array[1], ListSharp1Array[2], ListSharp1Array[3]);
            }
            else if (key == 3)
            {
                g.DrawImage(imgb, ListSharp1Array[0], ListSharp1Array[1], ListSharp1Array[2], ListSharp1Array[3]);
                g.DrawImage(imgb, ListSharp2Array[0], ListSharp2Array[1], ListSharp2Array[2], ListSharp2Array[3]);
            }
            else if (key == 10)
            {
                g.DrawImage(imgb, ListSharp1Array[0], ListSharp1Array[1], ListSharp1Array[2], ListSharp1Array[3]);
                g.DrawImage(imgb, ListSharp2Array[0], ListSharp2Array[1], ListSharp2Array[2], ListSharp2Array[3]);
                g.DrawImage(imgb, ListSharp3Array[0], ListSharp3Array[1], ListSharp3Array[2], ListSharp3Array[3]);
            }
            else if (key == 5)
            {
                g.DrawImage(imgb, ListSharp1Array[0], ListSharp1Array[1], ListSharp1Array[2], ListSharp1Array[3]);
                g.DrawImage(imgb, ListSharp2Array[0], ListSharp2Array[1], ListSharp2Array[2], ListSharp2Array[3]);
                g.DrawImage(imgb, ListSharp3Array[0], ListSharp3Array[1], ListSharp3Array[2], ListSharp3Array[3]);
                g.DrawImage(imgb, ListSharp4Array[0], ListSharp4Array[1], ListSharp4Array[2], ListSharp4Array[3]);
            }
            else if (key == 12)
            {
                g.DrawImage(imgb, ListSharp1Array[0], ListSharp1Array[1], ListSharp1Array[2], ListSharp1Array[3]);
                g.DrawImage(imgb, ListSharp2Array[0], ListSharp2Array[1], ListSharp2Array[2], ListSharp2Array[3]);
                g.DrawImage(imgb, ListSharp3Array[0], ListSharp3Array[1], ListSharp3Array[2], ListSharp3Array[3]);
                g.DrawImage(imgb, ListSharp4Array[0], ListSharp4Array[1], ListSharp4Array[2], ListSharp4Array[3]);
                g.DrawImage(imgb, ListSharp5Array[0], ListSharp5Array[1], ListSharp5Array[2], ListSharp5Array[3]);
            }
            else if (key == 7)
            {
                g.DrawImage(imgb, ListSharp1Array[0], ListSharp1Array[1], ListSharp1Array[2], ListSharp1Array[3]);
                g.DrawImage(imgb, ListSharp2Array[0], ListSharp2Array[1], ListSharp2Array[2], ListSharp2Array[3]);
                g.DrawImage(imgb, ListSharp3Array[0], ListSharp3Array[1], ListSharp3Array[2], ListSharp3Array[3]);
                g.DrawImage(imgb, ListSharp4Array[0], ListSharp4Array[1], ListSharp4Array[2], ListSharp4Array[3]);
                g.DrawImage(imgb, ListSharp5Array[0], ListSharp5Array[1], ListSharp5Array[2], ListSharp5Array[3]);
                g.DrawImage(imgb, ListSharp6Array[0], ListSharp6Array[1], ListSharp6Array[2], ListSharp6Array[3]);
            }

            greenPen.Dispose();
            imgb.Dispose();


        }
        public NodeLinjer(Sang s)
        {
            sang = s;
            listePoint.Add(new Point(50 + pos.X, 150 + pos.Y)); //E
            listePoint.Add(new Point(50 + pos.X, 130 + pos.Y)); //G
            listePoint.Add(new Point(50 + pos.X, 110 + pos.Y)); //B
            listePoint.Add(new Point(50 + pos.X, 90 + pos.Y)); //D
            listePoint.Add(new Point(50 + pos.X, 70 + pos.Y)); //F
                                                               //-------------G-nøglen------------------------------------
            flytGkey(0, 0); //Starter den op her.
                            //------------Sheet til Tone Dictionary laves her------------
                            // default så det ikke krasjer. Vil blive overskrevet når der skiftes toneart.--
            listSheetToTone.Add(4, 6);//F
            listSheetToTone.Add(5, 8);//G
            listSheetToTone.Add(6, 10);//A
            listSheetToTone.Add(7, 12);//B
            listSheetToTone.Add(8, 13);//C
            listSheetToTone.Add(9, 15);//D
            listSheetToTone.Add(10, 17);//E
            listSheetToTone.Add(11, 18);//F
            listSheetToTone.Add(12, 20);//G
            listSheetToTone.Add(13, 22);//A
            listSheetToTone.Add(14, 24);//B
            listSheetToTone.Add(15, 25);//C
            listSheetToTone.Add(16, 27);//D
            listSheetToTone.Add(17, 29);//E
            listSheetToTone.Add(18, 30);//F
            listSheetToTone.Add(19, 32);//G
            listSheetToTone.Add(20, 34);//A
            listSheetToTone.Add(21, 36);//B
            listSheetToTone.Add(22, 37);//C


        }
        public void flytGkey(int x, int y)
        {
            // flytGkey(pos.X + 360, pos.Y + 203);
            int LokalxMove = xMove + x + 350;
            int LokalyMove = yMove + y + 210;

            ListgKeyArray[0] = LokalxMove; //X
            ListgKeyArray[1] = LokalyMove; //Y
            ListgKeyArray[2] = 50; //Width
            ListgKeyArray[3] = 151; //Højden af billedet, er konstant.

        }
        public void flytFlat(int x, int y)
        {

            //drawBstaf(0, 0);
            //drawBstaf(25, -33);
            //drawBstaf(50, 10);
            //drawBstaf(75, -22);
            //drawBstaf(100, 20);

            int xRalativ = 0;
            int yRelativ = 0;
            int LokalxMove = xMove + x + xRalativ;
            int LokalyMove = yMove + y + yRelativ;

            int LokalxMoveFlat = xMove + x + 385 + xRalativ;
            int LokalyMoveFlat = yMove + y + 118 + yRelativ;


            ListFlat1Array[0] = LokalxMoveFlat; //X
            ListFlat1Array[1] = LokalyMoveFlat; //Y
            ListFlat1Array[2] = 25;// Width 
            ListFlat1Array[3] = 50; //Højden af billedet, er konstant.



            xRalativ = 10;
            yRelativ = -33;
            LokalxMove = xMove + x + xRalativ;
            LokalyMove = yMove + y + yRelativ;

            LokalxMoveFlat = xMove + x + 385 + xRalativ;
            LokalyMoveFlat = yMove + y + 118 + yRelativ;

            ListFlat2Array[0] = LokalxMoveFlat; //X
            ListFlat2Array[1] = LokalyMoveFlat; //Y
            ListFlat2Array[2] = 25;// Width 
            ListFlat2Array[3] = 50; //Højden af billedet, er konstant.


            xRalativ = 30;
            yRelativ = 10;
            LokalxMove = xMove + x + xRalativ;
            LokalyMove = yMove + y + yRelativ;

            LokalxMoveFlat = xMove + x + 385 + xRalativ;
            LokalyMoveFlat = yMove + y + 118 + yRelativ;

            ListFlat3Array[0] = LokalxMoveFlat; //X
            ListFlat3Array[1] = LokalyMoveFlat; //Y
            ListFlat3Array[2] = 25;// Width 
            ListFlat3Array[3] = 50; //Højden af billedet, er konstant.


            xRalativ = 40;
            yRelativ = -22;
            LokalxMove = xMove + x + xRalativ;
            LokalyMove = yMove + y + yRelativ;

            LokalxMoveFlat = xMove + x + 385 + xRalativ;
            LokalyMoveFlat = yMove + y + 118 + yRelativ;

            ListFlat4Array[0] = LokalxMoveFlat; //X
            ListFlat4Array[1] = LokalyMoveFlat; //Y
            ListFlat4Array[2] = 25;// Width 
            ListFlat4Array[3] = 50; //Højden af billedet, er konstant.


            xRalativ = 60;
            yRelativ = 20;
            LokalxMove = xMove + x + xRalativ;
            LokalyMove = yMove + y + yRelativ;

            LokalxMoveFlat = xMove + x + 385 + xRalativ;
            LokalyMoveFlat = yMove + y + 118 + yRelativ;

            ListFlat5Array[0] = LokalxMoveFlat; //X
            ListFlat5Array[1] = LokalyMoveFlat; //Y
            ListFlat5Array[2] = 25;// Width 
            ListFlat5Array[3] = 50; //Højden af billedet, er konstant.


        }
        public void flytSharp(int x, int y)
        {
            //drawBstaf(0, 0);
            //drawBstaf(25, 33);
            //drawBstaf(50, -10);
            //drawBstaf(75, 22);
            //drawBstaf(100, 55);
            //drawBstaf(120, 10);

            int xRalativ = 0;
            int yRelativ = 0;
            int lokalxMove = xMove + x + xRalativ;
            int lokalyMove = yMove + y + yRelativ;

            int LokalxMoveSharp = xMove + x + 385 + xRalativ;
            int LokalyMoveSharp = yMove + y + 87 + yRelativ;


            ListSharp1Array[0] = LokalxMoveSharp; //X
            ListSharp1Array[1] = LokalyMoveSharp; //Y
            ListSharp1Array[2] = 25;// Width 
            ListSharp1Array[3] = 50; //Højden af billedet, er konstant.




            //drawBstaf(0, 0);
            //drawBstaf(25, 33);
            //drawBstaf(50, -10);
            //drawBstaf(75, 22);
            //drawBstaf(100, 55);
            //drawBstaf(120, 10);

            xRalativ = 15;
            yRelativ = 30;
            lokalxMove = xMove + x + xRalativ;
            lokalyMove = yMove + y + yRelativ;

            LokalxMoveSharp = xMove + x + 385 + xRalativ;
            LokalyMoveSharp = yMove + y + 87 + yRelativ;


            ListSharp2Array[0] = LokalxMoveSharp; //X
            ListSharp2Array[1] = LokalyMoveSharp; //Y
            ListSharp2Array[2] = 25;// Width 
            ListSharp2Array[3] = 50; //Højden af billedet, er konstant.



            //drawBstaf(0, 0);
            //drawBstaf(25, 33);
            //drawBstaf(50, -10);
            //drawBstaf(75, 22);
            //drawBstaf(100, 55);
            //drawBstaf(120, 10);

            xRalativ = 30;
            yRelativ = -10;
            lokalxMove = xMove + x + xRalativ;
            lokalyMove = yMove + y + yRelativ;

            LokalxMoveSharp = xMove + x + 385 + xRalativ;
            LokalyMoveSharp = yMove + y + 87 + yRelativ;


            ListSharp3Array[0] = LokalxMoveSharp; //X
            ListSharp3Array[1] = LokalyMoveSharp; //Y
            ListSharp3Array[2] = 25;// Width 
            ListSharp3Array[3] = 50; //Højden af billedet, er konstant.



            xRalativ = 45;
            yRelativ = 20;
            lokalxMove = xMove + x + xRalativ;
            lokalyMove = yMove + y + yRelativ;

            LokalxMoveSharp = xMove + x + 385 + xRalativ;
            LokalyMoveSharp = yMove + y + 87 + yRelativ;


            ListSharp4Array[0] = LokalxMoveSharp; //X
            ListSharp4Array[1] = LokalyMoveSharp; //Y
            ListSharp4Array[2] = 25;// Width 
            ListSharp4Array[3] = 50; //Højden af billedet, er konstant.



            xRalativ = 60;
            yRelativ = 51;
            lokalxMove = xMove + x + xRalativ;
            lokalyMove = yMove + y + yRelativ;

            LokalxMoveSharp = xMove + x + 385 + xRalativ;
            LokalyMoveSharp = yMove + y + 87 + yRelativ;


            ListSharp5Array[0] = LokalxMoveSharp; //X
            ListSharp5Array[1] = LokalyMoveSharp; //Y
            ListSharp5Array[2] = 25;// Width 
            ListSharp5Array[3] = 50; //Højden af billedet, er konstant.


            xRalativ = 75;
            yRelativ = 8;
            lokalxMove = xMove + x + xRalativ;
            lokalyMove = yMove + y + yRelativ;

            LokalxMoveSharp = xMove + x + 385 + xRalativ;
            LokalyMoveSharp = yMove + y + 87 + yRelativ;


            ListSharp6Array[0] = LokalxMoveSharp; //X
            ListSharp6Array[1] = LokalyMoveSharp; //Y
            ListSharp6Array[2] = 25;// Width 
            ListSharp6Array[3] = 50; //Højden af billedet, er konstant.

        }
        public void callibrer()
        {
            for (int i = 0; i < listePoint.Count; i++)
            {
                Point p = listePoint[i];
                int x = p.X + pos.X;
                int y = p.Y + pos.Y;
                Point nyP = new Point(x, y);
                listePoint[i] = nyP;
            }
            flytGkey(pos.X, pos.Y);
            flytFlat(pos.X + 10, pos.Y + 130);
            flytSharp(pos.X + 10, pos.Y + 130);
        }
        public void callibrerNyKeyChange()
        {
            if (key == 1)
            {
                listSheetToTone = new Dictionary<int, int>();
                listSheetToTone.Add(4, 6);//F
                listSheetToTone.Add(5, 8);//G
                listSheetToTone.Add(6, 10);//A
                listSheetToTone.Add(7, 12);//B
                listSheetToTone.Add(8, 13);//C
                listSheetToTone.Add(9, 15);//D
                listSheetToTone.Add(10, 17);//E
                listSheetToTone.Add(11, 18);//F
                listSheetToTone.Add(12, 20);//G
                listSheetToTone.Add(13, 22);//A
                listSheetToTone.Add(14, 24);//B
                listSheetToTone.Add(15, 25);//C
                listSheetToTone.Add(16, 27);//D
                listSheetToTone.Add(17, 29);//E
                listSheetToTone.Add(18, 30);//F
                listSheetToTone.Add(19, 32);//G
                listSheetToTone.Add(20, 34);//A
                listSheetToTone.Add(21, 36);//B
                listSheetToTone.Add(22, 37);//C
            }
            else if (key == 2)//C# (5 flats)
            {
                listSheetToTone = new Dictionary<int, int>();
                listSheetToTone.Add(4, 6);//F
                listSheetToTone.Add(5, 8 - 1);//G
                listSheetToTone.Add(6, 10 - 1);//A
                listSheetToTone.Add(7, 12 - 1);//B
                listSheetToTone.Add(8, 13);//C
                listSheetToTone.Add(9, 15 - 1);//D
                listSheetToTone.Add(10, 17 - 1);//E
                listSheetToTone.Add(11, 18);//F
                listSheetToTone.Add(12, 20 - 1);//G
                listSheetToTone.Add(13, 22 - 1);//A
                listSheetToTone.Add(14, 24 - 1);//B
                listSheetToTone.Add(15, 25);//C
                listSheetToTone.Add(16, 27 - 1);//D
                listSheetToTone.Add(17, 29 - 1);//E
                listSheetToTone.Add(18, 30);//F
                listSheetToTone.Add(19, 32 - 1);//G
                listSheetToTone.Add(20, 34 - 1);//A
                listSheetToTone.Add(21, 36 - 1);//B
                listSheetToTone.Add(22, 37);//C
            }
            else if (key == 3)//D (2 kryds)
            {
                listSheetToTone = new Dictionary<int, int>();
                listSheetToTone.Add(4, 6 + 1);//F
                listSheetToTone.Add(5, 8);//G
                listSheetToTone.Add(6, 10);//A
                listSheetToTone.Add(7, 12);//B
                listSheetToTone.Add(8, 13 + 1);//C
                listSheetToTone.Add(9, 15);//D
                listSheetToTone.Add(10, 17);//E
                listSheetToTone.Add(11, 18 + 1);//F
                listSheetToTone.Add(12, 20);//G
                listSheetToTone.Add(13, 22);//A
                listSheetToTone.Add(14, 24);//B
                listSheetToTone.Add(15, 25 + 1);//C
                listSheetToTone.Add(16, 27);//D
                listSheetToTone.Add(17, 29);//E
                listSheetToTone.Add(18, 30 + 1);//F
                listSheetToTone.Add(19, 32);//G
                listSheetToTone.Add(20, 34);//A
                listSheetToTone.Add(21, 36);//B
                listSheetToTone.Add(22, 37 + 1);//C
            }
            else if (key == 4)//Eb (3 flats)
            {
                listSheetToTone = new Dictionary<int, int>();
                listSheetToTone.Add(4, 6);//F
                listSheetToTone.Add(5, 8);//G
                listSheetToTone.Add(6, 10 - 1);//A
                listSheetToTone.Add(7, 12 - 1);//B
                listSheetToTone.Add(8, 13);//C
                listSheetToTone.Add(9, 15);//D
                listSheetToTone.Add(10, 17 - 1);//E
                listSheetToTone.Add(11, 18);//F
                listSheetToTone.Add(12, 20);//G
                listSheetToTone.Add(13, 22 - 1);//A
                listSheetToTone.Add(14, 24 - 1);//B
                listSheetToTone.Add(15, 25);//C
                listSheetToTone.Add(16, 27);//D
                listSheetToTone.Add(17, 29 - 1);//E
                listSheetToTone.Add(18, 30);//F
                listSheetToTone.Add(19, 32);//G
                listSheetToTone.Add(20, 34 - 1);//A
                listSheetToTone.Add(21, 36 - 1);//B
                listSheetToTone.Add(22, 37);//C
            }
            else if (key == 5)//E (4 kryds)
            {
                listSheetToTone = new Dictionary<int, int>();
                listSheetToTone.Add(4, 6 + 1);//F
                listSheetToTone.Add(5, 8 + 1);//G
                listSheetToTone.Add(6, 10);//A
                listSheetToTone.Add(7, 12);//B
                listSheetToTone.Add(8, 13 + 1);//C
                listSheetToTone.Add(9, 15 + 1);//D
                listSheetToTone.Add(10, 17);//E
                listSheetToTone.Add(11, 18 + 1);//F
                listSheetToTone.Add(12, 20 + 1);//G
                listSheetToTone.Add(13, 22);//A
                listSheetToTone.Add(14, 24);//B
                listSheetToTone.Add(15, 25 + 1);//C
                listSheetToTone.Add(16, 27 + 1);//D
                listSheetToTone.Add(17, 29);//E
                listSheetToTone.Add(18, 30 + 1);//F
                listSheetToTone.Add(19, 32 + 1);//G
                listSheetToTone.Add(20, 34);//A
                listSheetToTone.Add(21, 36);//B
                listSheetToTone.Add(22, 37 + 1);//C
            }
            else if (key == 6)//F (1 flat)
            {
                listSheetToTone = new Dictionary<int, int>();
                listSheetToTone.Add(4, 6);//F
                listSheetToTone.Add(5, 8);//G
                listSheetToTone.Add(6, 10);//A
                listSheetToTone.Add(7, 12 - 1);//B
                listSheetToTone.Add(8, 13);//C
                listSheetToTone.Add(9, 15);//D
                listSheetToTone.Add(10, 17);//E
                listSheetToTone.Add(11, 18);//F
                listSheetToTone.Add(12, 20);//G
                listSheetToTone.Add(13, 22);//A
                listSheetToTone.Add(14, 24 - 1);//B
                listSheetToTone.Add(15, 25);//C
                listSheetToTone.Add(16, 27);//D
                listSheetToTone.Add(17, 29);//E
                listSheetToTone.Add(18, 30);//F
                listSheetToTone.Add(19, 32);//G
                listSheetToTone.Add(20, 34);//A
                listSheetToTone.Add(21, 36 - 1);//B
                listSheetToTone.Add(22, 37);//C
            }
            else if (key == 7)//F# (6 kryds)
            {
                // Fejl ændret efter transpotion. 2016.06.26. B var +1 skal være elmindeligt B.
                // og E var almindelig men skal have + 1. B og E var begge forkerte.
                listSheetToTone = new Dictionary<int, int>();
                listSheetToTone.Add(4, 6 + 1);//F
                listSheetToTone.Add(5, 8 + 1);//G
                listSheetToTone.Add(6, 10 + 1);//A
                listSheetToTone.Add(7, 12);//B
                listSheetToTone.Add(8, 13 + 1);//C
                listSheetToTone.Add(9, 15 + 1);//D
                listSheetToTone.Add(10, 17 + 1);//E
                listSheetToTone.Add(11, 18 + 1);//F
                listSheetToTone.Add(12, 20 + 1);//G
                listSheetToTone.Add(13, 22 + 1);//A
                listSheetToTone.Add(14, 24);//B
                listSheetToTone.Add(15, 25 + 1);//C
                listSheetToTone.Add(16, 27 + 1);//D
                listSheetToTone.Add(17, 29 + 1);//E
                listSheetToTone.Add(18, 30 + 1);//F
                listSheetToTone.Add(19, 32 + 1);//G
                listSheetToTone.Add(20, 34 + 1);//A
                listSheetToTone.Add(21, 36);//B
                listSheetToTone.Add(22, 37 + 1);//C
            }
            else if (key == 8)//G (1 kryds)
            {
                listSheetToTone = new Dictionary<int, int>();
                listSheetToTone.Add(4, 6 + 1);//F
                listSheetToTone.Add(5, 8);//G
                listSheetToTone.Add(6, 10);//A
                listSheetToTone.Add(7, 12);//B
                listSheetToTone.Add(8, 13);//C
                listSheetToTone.Add(9, 15);//D
                listSheetToTone.Add(10, 17);//E
                listSheetToTone.Add(11, 18 + 1);//F
                listSheetToTone.Add(12, 20);//G
                listSheetToTone.Add(13, 22);//A
                listSheetToTone.Add(14, 24);//B
                listSheetToTone.Add(15, 25);//C
                listSheetToTone.Add(16, 27);//D
                listSheetToTone.Add(17, 29);//E
                listSheetToTone.Add(18, 30 + 1);//F
                listSheetToTone.Add(19, 32);//G
                listSheetToTone.Add(20, 34);//A
                listSheetToTone.Add(21, 36);//B
                listSheetToTone.Add(22, 37);//C

            }
            else if (key == 9)//Ab (4 flats)
            {
                listSheetToTone = new Dictionary<int, int>();
                listSheetToTone.Add(4, 6);//F
                listSheetToTone.Add(5, 8);//G
                listSheetToTone.Add(6, 10 - 1);//A
                listSheetToTone.Add(7, 12 - 1);//B
                listSheetToTone.Add(8, 13);//C
                listSheetToTone.Add(9, 15 - 1);//D
                listSheetToTone.Add(10, 17 - 1);//E
                listSheetToTone.Add(11, 18);//F
                listSheetToTone.Add(12, 20);//G
                listSheetToTone.Add(13, 22 - 1);//A
                listSheetToTone.Add(14, 24 - 1);//B
                listSheetToTone.Add(15, 25);//C
                listSheetToTone.Add(16, 27 - 1);//D
                listSheetToTone.Add(17, 29 - 1);//E
                listSheetToTone.Add(18, 30);//F
                listSheetToTone.Add(19, 32);//G
                listSheetToTone.Add(20, 34 - 1);//A
                listSheetToTone.Add(21, 36 - 1);//B
                listSheetToTone.Add(22, 37);//C
            }
            else if (key == 10)//A (3 kryds)
            {
                listSheetToTone = new Dictionary<int, int>();
                listSheetToTone.Add(4, 6 + 1);//F
                listSheetToTone.Add(5, 8 + 1);//G
                listSheetToTone.Add(6, 10);//A
                listSheetToTone.Add(7, 12);//B
                listSheetToTone.Add(8, 13 + 1);//C
                listSheetToTone.Add(9, 15);//D
                listSheetToTone.Add(10, 17);//E
                listSheetToTone.Add(11, 18 + 1);//F
                listSheetToTone.Add(12, 20 + 1);//G
                listSheetToTone.Add(13, 22);//A
                listSheetToTone.Add(14, 24);//B
                listSheetToTone.Add(15, 25 + 1);//C
                listSheetToTone.Add(16, 27);//D
                listSheetToTone.Add(17, 29);//E
                listSheetToTone.Add(18, 30 + 1);//F
                listSheetToTone.Add(19, 32 + 1);//G
                listSheetToTone.Add(20, 34);//A
                listSheetToTone.Add(21, 36);//B
                listSheetToTone.Add(22, 37 + 1);//C
            }
            else if (key == 11)//Bb (2 flats)
            {
                listSheetToTone = new Dictionary<int, int>();
                listSheetToTone.Add(4, 6);//F
                listSheetToTone.Add(5, 8);//G
                listSheetToTone.Add(6, 10);//A
                listSheetToTone.Add(7, 12 - 1);//B
                listSheetToTone.Add(8, 13);//C
                listSheetToTone.Add(9, 15);//D
                listSheetToTone.Add(10, 17 - 1);//E
                listSheetToTone.Add(11, 18);//F
                listSheetToTone.Add(12, 20);//G
                listSheetToTone.Add(13, 22);//A
                listSheetToTone.Add(14, 24 - 1);//B
                listSheetToTone.Add(15, 25);//C
                listSheetToTone.Add(16, 27);//D
                listSheetToTone.Add(17, 29 - 1);//E
                listSheetToTone.Add(18, 30);//F
                listSheetToTone.Add(19, 32);//G
                listSheetToTone.Add(20, 34);//A
                listSheetToTone.Add(21, 36 - 1);//B
                listSheetToTone.Add(22, 37);//C
            }
            else if (key == 12)//B (5 kryds)
            {
                listSheetToTone = new Dictionary<int, int>();
                listSheetToTone.Add(4, 6 + 1);//F
                listSheetToTone.Add(5, 8 + 1);//G
                listSheetToTone.Add(6, 10 + 1);//A
                listSheetToTone.Add(7, 12);//B
                listSheetToTone.Add(8, 13 + 1);//C
                listSheetToTone.Add(9, 15 + 1);//D
                listSheetToTone.Add(10, 17);//E
                listSheetToTone.Add(11, 18 + 1);//F
                listSheetToTone.Add(12, 20 + 1);//G
                listSheetToTone.Add(13, 22 + 1);//A
                listSheetToTone.Add(14, 24);//B
                listSheetToTone.Add(15, 25 + 1);//C
                listSheetToTone.Add(16, 27 + 1);//D
                listSheetToTone.Add(17, 29);//E
                listSheetToTone.Add(18, 30 + 1);//F
                listSheetToTone.Add(19, 32 + 1);//G
                listSheetToTone.Add(20, 34 + 1);//A
                listSheetToTone.Add(21, 36);//B
                listSheetToTone.Add(22, 37 + 1);//C
            }



        }
        public void removeNoteAndChord(Noder n)
        {
            Akkord akk = n.findAkkordForNote();
            if (akk != null)
            {
                // her fjernes Akkord fra listen.
                sang.listAkkorder.Remove(akk);
            }
            // her fjernes Node fra listen.
            listNoder.Remove(n);
        }
    }
    class Noder
    {
        public int id = 0;
        public Point pos = new Point(0, 0);
        public int keyNumber = -1; // 6 - 37  (12 noder pr.oktav.)Den rigtige Tonehøjden til lyden.
                                   //  public int StafkeyNumber = -1; //1 til 12
        public int StafNumber = -1; // 4 - 22 (7 noder pr. oktav) Linje nummer på papiret.
        public int length = 4; //1 - 8  = almindelig node. -1 er ikke node men en skillelinje der skal tegnes på NodeLinjer.
        public String noteWav = "a0.wav";
        public int sequenceNummer = 1;
        public int sharpOrFlat = 0;//-1 = flat og +1 er sharp.
        public int type = 0;

        public NodeLinjer parrentNodeLinje;
        public int heightForDrawing = 18;
        public int widthForDrawing = 24;
        public bool knyttetSammen = false; //Bruges til 1/8 noder.
        public Akkord akkord = null; //Indført således at MIDI også kan spille Akkorder.09.11.2015
        public bool editIgang = false;
        public Akkord findAkkordForNote()
        {
            Akkord svar = null;
            int flytX = 10; //  fra metoden callibrerAkkorderTilNoder i Sang classen lægges der 10 til X.
            foreach (Akkord akk in parrentNodeLinje.sang.listAkkorder)
            {
                // Vi bruger listePoint[0] fordi den holder den første(højeste) linje af nodeLinjerne.
                if (akk.pos.Y < parrentNodeLinje.listePoint[0].Y && akk.pos.Y > parrentNodeLinje.listePoint[0].Y - parrentNodeLinje.sang.nodeLinjePosYimellemNodeLinjer)
                {
                    if (akk.pos.X == pos.X + flytX) //Så har vi fundet en akkord som tilhører denne her Note.
                    {
                        if (akk.editIgang == false)
                        {
                            svar = akk;
                            svar.editIgang = true;
                        }
                    }
                }
            }
            return svar;
        }
        public void kalibrerNewXposForNoteAndChord()
        {
            //---en node kan have en Akkord med identisk X pos. Den skal også ændres, ellers hænger akkorderne i luften eller tilhører en forkert node.

            //--Kalibrer Akkord X--------------------
            Akkord fundet = findAkkordForNote();
            int flytX = 10; //  fra metoden callibrerAkkorderTilNoder i Sang classen lægges der 10 til X.
            if (fundet != null)
            {
                fundet.pos.X = parrentNodeLinje.sang.startAfstandX + parrentNodeLinje.sang.afstanImellemNoderne * sequenceNummer + flytX;
            }

            //---Kalibrer X -----------------------
            pos.X = parrentNodeLinje.sang.startAfstandX + parrentNodeLinje.sang.afstanImellemNoderne * sequenceNummer;
        }
        public void kalibrer()
        {
            Point parrentCopi = new Point(0, 0);
            if (parrentNodeLinje.pos != null)
            {
                parrentCopi = parrentNodeLinje.pos;
            }
            //--------------------------------------------------------------------

            //---Kalibrer Y ------------------------------------------------------
            int y = pos.Y;
            int height = 0;
            height = 200 + parrentCopi.Y;// F
            if (y >= height - 5)
            {
                StafNumber = 4;
                keyNumber = parrentNodeLinje.listSheetToTone[StafNumber];
                y = height;
                noteWav = "a0.wav";
            }
            height = 190 + parrentCopi.Y;// g
            if ((y >= height - 5) && (y < height + 5))
            {
                StafNumber = 5;
                keyNumber = parrentNodeLinje.listSheetToTone[StafNumber];
                y = height;
                noteWav = "a0.wav";
            }
            height = 180 + parrentCopi.Y;// A
            if ((y >= height - 5) && (y < height + 5))
            {
                StafNumber = 6;
                keyNumber = parrentNodeLinje.listSheetToTone[StafNumber];
                y = height;
                noteWav = "a0.wav";
            }

            height = 170 + parrentCopi.Y;// B
            if ((y >= height - 5) && (y < height + 5))
            {
                StafNumber = 7;
                keyNumber = parrentNodeLinje.listSheetToTone[StafNumber];
                y = height;
                noteWav = "b0.wav";
            }

            height = 160 + parrentCopi.Y;// C
            if ((y >= height - 5) && (y < height + 5))
            {
                StafNumber = 8;
                keyNumber = parrentNodeLinje.listSheetToTone[StafNumber];
                y = height;
                noteWav = "c0.wav";
            }
            height = 150 + parrentCopi.Y;// D
            if ((y >= height - 5) && (y < height + 5))
            {
                StafNumber = 9;
                keyNumber = parrentNodeLinje.listSheetToTone[StafNumber];
                y = height;
                noteWav = "d0.wav";
            }
            height = 140 + parrentCopi.Y;// E
            if ((y >= height - 5) && (y < height + 5))
            {
                StafNumber = 10;
                keyNumber = parrentNodeLinje.listSheetToTone[StafNumber];
                y = height;
                noteWav = "e0.wav";
            }
            height = 130 + parrentCopi.Y;// F
            if ((y >= height - 5) && (y < height + 5))
            {
                StafNumber = 11;
                keyNumber = parrentNodeLinje.listSheetToTone[StafNumber];
                y = height;
                noteWav = "f0.wav";
            }
            height = 120 + parrentCopi.Y; // G
            if ((y >= height - 5) && (y < height + 5))
            {
                StafNumber = 12;
                keyNumber = parrentNodeLinje.listSheetToTone[StafNumber];
                y = height;
                noteWav = "g0.wav";
            }
            height = 110 + parrentCopi.Y; // A
            if ((y >= height - 5) && (y < height + 5))
            {
                StafNumber = 13;
                keyNumber = parrentNodeLinje.listSheetToTone[StafNumber];
                y = height;
                noteWav = "a1.wav";
            }
            height = 100 + parrentCopi.Y; // B
            if ((y >= height - 5) && (y < height + 5))
            {
                StafNumber = 14;
                keyNumber = parrentNodeLinje.listSheetToTone[StafNumber];
                y = height;
                noteWav = "b1.wav";
            }
            height = 90 + parrentCopi.Y; // C
            if ((y >= height - 5) && (y < height + 5))
            {
                StafNumber = 15;
                keyNumber = parrentNodeLinje.listSheetToTone[StafNumber];
                y = height;
                noteWav = "c1.wav";
            }
            height = 80 + parrentCopi.Y; // D
            if ((y >= height - 5) && (y < height + 5))
            {
                StafNumber = 16;
                keyNumber = parrentNodeLinje.listSheetToTone[StafNumber];
                y = height;
                noteWav = "d1.wav";
            }
            height = 70 + parrentCopi.Y; // E
            if ((y >= height - 5) && (y < height + 5))
            {
                StafNumber = 17;
                keyNumber = parrentNodeLinje.listSheetToTone[StafNumber];
                y = height;
                noteWav = "e1.wav";
            }
            height = 60 + parrentCopi.Y; // F
            if ((y >= height - 5) && (y < height + 5))
            {
                StafNumber = 18;
                keyNumber = parrentNodeLinje.listSheetToTone[StafNumber];
                y = height;
                noteWav = "f1.wav";
            }
            height = 50 + parrentCopi.Y; // G
            if ((y >= height - 5) && (y < height + 5))
            {
                StafNumber = 19;
                keyNumber = parrentNodeLinje.listSheetToTone[StafNumber];
                y = height;
                noteWav = "";
            }
            height = 40 + parrentCopi.Y; // A
            if ((y >= height - 5) && (y < height + 5))
            {
                StafNumber = 20;
                keyNumber = parrentNodeLinje.listSheetToTone[StafNumber];
                y = height;
                noteWav = "";
            }
            height = 30 + parrentCopi.Y; // B
            if ((y >= height - 5) && (y < height + 5))
            {
                StafNumber = 21;
                keyNumber = parrentNodeLinje.listSheetToTone[StafNumber];
                y = height;
                noteWav = "";
            }
            height = 20 + parrentCopi.Y; // C
            if (y < height + 5)
            {
                StafNumber = 22;
                keyNumber = parrentNodeLinje.listSheetToTone[StafNumber];
                y = height;
                noteWav = "";
            }
            pos.Y = y;
            //---Sharp or Flat-------------
            if (sharpOrFlat == -1)
            {
                keyNumber = keyNumber - 1;
            }
            else if (sharpOrFlat == 1)
            {
                keyNumber = keyNumber + 1;
            }

            //---Kalibrer X -----------------------

            pos.X = parrentNodeLinje.sang.startAfstandX + parrentNodeLinje.sang.afstanImellemNoderne * sequenceNummer;
        }
        public void kalibrerFromKeyNumber(int indKeyNumber)
        {
            Point parrentCopi = new Point(0, 0);
            if (parrentNodeLinje.pos != null)
            {
                parrentCopi = parrentNodeLinje.pos;
            }
            //------------------------------------------------
            if (indKeyNumber > 37 && length < 100 && length > 0)
            {
                int tooHeigh = (int)HttpContext.Current.Session["TooHeigh"];
                tooHeigh++;
                HttpContext.Current.Session["TooHeigh"] = tooHeigh;
            }
            else if (indKeyNumber < 6 && length < 100 && length > 0)
            {
                int tooHeigh = (int)HttpContext.Current.Session["TooHeigh"];
                tooHeigh--;
                HttpContext.Current.Session["TooHeigh"] = tooHeigh;

            }

            //--- Finde StafNumber -------------------------
            int gemmer = parrentNodeLinje.key;

            // listSheetToTone ændres afhængig af Toneart. Og her bruger jeg denne Dictunary 
            // modsat, jeg fanger key ved at give dem Value, smart.
            StafNumber = parrentNodeLinje.listSheetToTone.FirstOrDefault(x => x.Value == indKeyNumber).Key;

            if (length == -1) //Skillelinje indsættes.
            {
                StafNumber = 10; //Hvis StafNumber er under 7, lægge to små linjer til node linjerne. Det er til de lave noder, men ikke bruges til skille linjer.
            }

            int y = pos.Y;
            int height = 0;

            if (length > 100 && length < 200) //Pause og ikke alm. node.
            {
                StafNumber = 24; // Sættes til højere end en node kan være.

                //----for at pause også skal have en y værdi. 2016.06.19-----
                // ellers kan den ikke fanges når man skal EDIT.
                height = 100 + parrentCopi.Y; // B
                y = height;
            }
            // Så skal jeg fange # og b som ikke er med i tonearten.
            if (StafNumber == 0)
            {
                sharpOrFlat = 1;
                StafNumber = parrentNodeLinje.listSheetToTone.FirstOrDefault(x => x.Value == indKeyNumber - 1).Key;
            }


            //---Kalibrer Y ------------------------------------------------------

            if (StafNumber == 4)
            {
                height = 200 + parrentCopi.Y;// F
                y = height;
                keyNumber = indKeyNumber;
                noteWav = "";
            }
            if (StafNumber == 5)
            {
                height = 190 + parrentCopi.Y;// g
                y = height;
                keyNumber = indKeyNumber;
                noteWav = "";
            }
            if (StafNumber == 6)
            {
                height = 180 + parrentCopi.Y;// A
                y = height;
                keyNumber = indKeyNumber;
                noteWav = "";
            }
            if (StafNumber == 7)
            {
                height = 170 + parrentCopi.Y;// B
                y = height;
                keyNumber = indKeyNumber;
                noteWav = "";
            }
            if (StafNumber == 8)
            {
                height = 160 + parrentCopi.Y;// C
                y = height;
                keyNumber = indKeyNumber;
                noteWav = "";
            }
            if (StafNumber == 9)
            {
                height = 150 + parrentCopi.Y;// D
                y = height;
                keyNumber = indKeyNumber;
                noteWav = "";
            }
            if (StafNumber == 10)
            {
                height = 140 + parrentCopi.Y;// E
                y = height;
                keyNumber = indKeyNumber;
                noteWav = "";
            }
            if (StafNumber == 11)
            {
                height = 130 + parrentCopi.Y;// F
                y = height;
                keyNumber = indKeyNumber;
                noteWav = "";
            }
            if (StafNumber == 12)
            {
                height = 120 + parrentCopi.Y; // G
                y = height;
                keyNumber = indKeyNumber;
                noteWav = "";
            }
            if (StafNumber == 13)
            {
                height = 110 + parrentCopi.Y; // A
                y = height;
                keyNumber = indKeyNumber;
                noteWav = "";
            }
            if (StafNumber == 14)
            {
                height = 100 + parrentCopi.Y; // B
                y = height;
                keyNumber = indKeyNumber;
                noteWav = "";
            }
            if (StafNumber == 15)
            {
                height = 90 + parrentCopi.Y; // C
                y = height;
                keyNumber = indKeyNumber;
                noteWav = "";
            }
            if (StafNumber == 16)
            {
                height = 80 + parrentCopi.Y; // D
                y = height;
                keyNumber = indKeyNumber;
                noteWav = "";
            }
            if (StafNumber == 17)
            {
                height = 70 + parrentCopi.Y; // E
                y = height;
                keyNumber = indKeyNumber;
                noteWav = "";
            }
            if (StafNumber == 18)
            {
                height = 60 + parrentCopi.Y; // F
                y = height;
                keyNumber = indKeyNumber;
                noteWav = "";
            }
            if (StafNumber == 19)
            {
                height = 50 + parrentCopi.Y; // G
                y = height;
                keyNumber = indKeyNumber;
                noteWav = "";
            }
            if (StafNumber == 20)
            {
                height = 40 + parrentCopi.Y; // A
                y = height;
                keyNumber = indKeyNumber;
                noteWav = "";
            }
            if (StafNumber == 21)
            {
                height = 30 + parrentCopi.Y; // B
                y = height;
                keyNumber = indKeyNumber;
                noteWav = "";
            }
            if (StafNumber == 22)
            {
                height = 20 + parrentCopi.Y; // C
                y = height;
                keyNumber = indKeyNumber;
                noteWav = "";
            }
            pos.Y = y;

            //---Kalibrer X -----------------------

            pos.X = parrentNodeLinje.sang.startAfstandX + parrentNodeLinje.sang.afstanImellemNoderne * sequenceNummer;

            // SkilleLinje placeres sidst på NodeLinjerne, så skal vi rykke den lidt til venstre
            // ellers hænger den ude i luften for sig selv, og det ser lidt dumt ud.
            if (parrentNodeLinje.maxNumberOfNodes == sequenceNummer)
            {
                if (length == -1 && StafNumber == 10) //Skillelinje indsættes.
                {
                    pos.X = pos.X - 20;
                }
            }
        }
        public void kalibrerForTranspose()
        {
            Point parrentCopi = new Point(0, 0);
            if (parrentNodeLinje.pos != null)
            {
                parrentCopi = parrentNodeLinje.pos;
            }

            //--- Finde StafNumber -------------------------
            int gemmer = parrentNodeLinje.key;

            // Denne lokaleKeyNumber er nødvendig, fordi hvis der er sharp eller flat, så
            // findes den ikke listSheetToTone listen, og så bliver den bare ståend, eller flytter
            // sig ikke rigtigt.
            int lokalKeyNumber = keyNumber;
            if (sharpOrFlat == 1)
            {
                lokalKeyNumber--;
            }
            else if (sharpOrFlat == -1)
            {
                lokalKeyNumber++;
            }

            // listSheetToTone ændres afhængig af Toneart. Og her bruger jeg denne Dictunary 
            // modsat, jeg fanger key ved at give dem Value, smart.
            StafNumber = parrentNodeLinje.listSheetToTone.FirstOrDefault(x => x.Value == lokalKeyNumber).Key;
            if (StafNumber == 0)
            {
                int a = parrentNodeLinje.key;
            }
            int y = pos.Y;
            int height = 0;


            //---Kalibrer Y ------------------------------------------------------

            if (StafNumber == 4)
            {
                height = 200 + parrentCopi.Y;// F
                y = height;
            }
            if (StafNumber == 5)
            {
                height = 190 + parrentCopi.Y;// g
                y = height;
            }
            if (StafNumber == 6)
            {
                height = 180 + parrentCopi.Y;// A
                y = height;
            }
            if (StafNumber == 7)
            {
                height = 170 + parrentCopi.Y;// B
                y = height;
            }
            if (StafNumber == 8)
            {
                height = 160 + parrentCopi.Y;// C
                y = height;
            }
            if (StafNumber == 9)
            {
                height = 150 + parrentCopi.Y;// D
                y = height;
            }
            if (StafNumber == 10)
            {
                height = 140 + parrentCopi.Y;// E
                y = height;
            }
            if (StafNumber == 11)
            {
                height = 130 + parrentCopi.Y;// F
                y = height;
            }
            if (StafNumber == 12)
            {
                height = 120 + parrentCopi.Y; // G
                y = height;
            }
            if (StafNumber == 13)
            {
                height = 110 + parrentCopi.Y; // A
                y = height;
            }
            if (StafNumber == 14)
            {
                height = 100 + parrentCopi.Y; // B
                y = height;
            }
            if (StafNumber == 15)
            {
                height = 90 + parrentCopi.Y; // C
                y = height;
            }
            if (StafNumber == 16)
            {
                height = 80 + parrentCopi.Y; // D
                y = height;
            }
            if (StafNumber == 17)
            {
                height = 70 + parrentCopi.Y; // E
                y = height;
            }
            if (StafNumber == 18)
            {
                height = 60 + parrentCopi.Y; // F
                y = height;
            }
            if (StafNumber == 19)
            {
                height = 50 + parrentCopi.Y; // G
                y = height;
            }
            if (StafNumber == 20)
            {
                height = 40 + parrentCopi.Y; // A
                y = height;
            }
            if (StafNumber == 21)
            {
                height = 30 + parrentCopi.Y; // B
                y = height;
            }
            if (StafNumber == 22)
            {
                height = 20 + parrentCopi.Y; // C
                y = height;
            }
            pos.Y = y;

        }
    }


}