﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminOverviewShowSongs.aspx.cs" Inherits="Sang.AdminOverviewShowSongs" %>

<!DOCTYPE html>

<html  xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>AdminOverviewShowSongs </title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
   
    <!--  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script> -->

    <link rel="stylesheet" href="/bootstrap-3.3.5-dist/css/bootstrap.min.css" />
    <script src="/jquery/jquery-1.11.3.min.js"></script>
    <script src="/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/css/StyleSheet1.css" />

    <script type = "text/javascript">
        window.onload = function () {
            var scrollY = parseInt('<%=Request.Form["scrollY"] %>');
        if (!isNaN(scrollY)) {
            window.scrollTo(0, scrollY);
        }     
    };
    window.onscroll = function () {
        var scrollY = document.body.scrollTop;
        if (scrollY == 0) {
            if (window.pageYOffset) {
                scrollY = window.pageYOffset;
            }
            else {
                scrollY = (document.body.parentElement) ? document.body.parentElement.scrollTop : 0;
            }
        }
        if (scrollY > 0) {
            var input = document.getElementById("scrollY");
            if (input == null) {
                input = document.createElement("input");
                input.setAttribute("type", "hidden");
                input.setAttribute("id", "scrollY");
                input.setAttribute("name", "scrollY");
                document.forms[0].appendChild(input);
            }
            input.value = scrollY;
        }
    };

</script>



    <style type="text/css">
        .auto-style1 {
            height: 26px;
        }
        .auto-style2 {
            width: 169px;
            vertical-align: top;
            text-align: left;
        }
        .auto-style3 {
            width: 100%;
        }
        .auto-style4 {
            width: 169px;
            vertical-align: top;
            text-align: left;
            height: 26px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
         <div class="container-fluid">

            <br />

             <div class="container-fluid">
                <table style="width: 79%;">
                    <tr>
                        <td class="auto-style4">

            <asp:Button CssClass="btn-default" ID="ButtonBack" runat="server" OnClick="ButtonBack_Click" Text="Back" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <table style="width: 100%;">
                                <tr>
                                    <td class="auto-style1">
                                        <asp:Table ID="Table1" runat="server">
                                        </asp:Table>
                                    </td>
                                    <td style="vertical-align:top">

            &nbsp;
                                        <table class="nav-justified">
                                            <tr>
                                                <td>

                                                    <br />
                                                    <br />
                                                    <table class="auto-style3">
                                                        <tr>
                                                            <td>

            <asp:Button CssClass="btn-default" ID="ButtonActivateDelet" runat="server" OnClick="ButtonActivateDelet_Click" Text="Activate Delete" />
                                                            </td>
                                                            <td>
                                                    <asp:CheckBox ID="CheckBox1" runat="server" Visible="False" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="auto-style1">

            <asp:Button CssClass="btn-default" ID="ButtonActivateChangeOwnerOfSong" runat="server" OnClick="ButtonActivateChangeOwnerOfSong_Click" Text="Owner Change" />
                                                            </td>
                                                            <td class="auto-style1">
                                                    <asp:CheckBox ID="CheckBoxOwnerChange" runat="server" Visible="False" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>

                                                                &nbsp;</td>
                                                            <td>
                                                                <asp:DropDownList ID="DropDownListUsers" runat="server" DataSourceID="SqlDataSource1" DataTextField="name" DataValueField="Id" Visible="False">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td class="auto-style2">
                                                    &nbsp;</td>
                                                <td>
                                                    <br />
                                                    <br />
                                                    <br />
                                                    <table class="nav-justified">
                                                        <tr>
                                                            <td>
                                                                <asp:Button CssClass="btn-default" ID="ButtonSendApprovement" runat="server" OnClick="ButtonSendApprovement_Click" Text="Send Approvement" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="LabelAktivBruger" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="TextBoxBeskedTilBruger" runat="server" Height="75px" TextMode="MultiLine" Width="232px"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                    <br />
                                                </td>
                                            </tr>
                                        </table>
&nbsp;
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div>
                            </div>

                        </td>
                    </tr>
                </table>
            </div>
             <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:wenningsdbConnectionString1 %>" DeleteCommand="DELETE FROM [SangBruger] WHERE [Id] = @Id" InsertCommand="INSERT INTO [SangBruger] ([email], [password], [name], [info], [date], [type]) VALUES (@email, @password, @name, @info, @date, @type)" ProviderName="<%$ ConnectionStrings:wenningsdbConnectionString1.ProviderName %>" SelectCommand="SELECT [Id], [email], [password], [name], [info], [date], [type] FROM [SangBruger]" UpdateCommand="UPDATE [SangBruger] SET [email] = @email, [password] = @password, [name] = @name, [info] = @info, [date] = @date, [type] = @type WHERE [Id] = @Id">
                 <DeleteParameters>
                     <asp:Parameter Name="Id" Type="Int32" />
                 </DeleteParameters>
                 <InsertParameters>
                     <asp:Parameter Name="email" Type="String" />
                     <asp:Parameter Name="password" Type="Int32" />
                     <asp:Parameter Name="name" Type="String" />
                     <asp:Parameter Name="info" Type="String" />
                     <asp:Parameter Name="date" Type="String" />
                     <asp:Parameter Name="type" Type="Int32" />
                 </InsertParameters>
                 <UpdateParameters>
                     <asp:Parameter Name="email" Type="String" />
                     <asp:Parameter Name="password" Type="Int32" />
                     <asp:Parameter Name="name" Type="String" />
                     <asp:Parameter Name="info" Type="String" />
                     <asp:Parameter Name="date" Type="String" />
                     <asp:Parameter Name="type" Type="Int32" />
                     <asp:Parameter Name="Id" Type="Int32" />
                 </UpdateParameters>
             </asp:SqlDataSource>
            <br />

        </div>

    </form>
</body>
</html>
