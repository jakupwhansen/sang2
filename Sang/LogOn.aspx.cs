﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sang
{
    public partial class LogOn : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            TextBoxEmail.Focus();
        }


        protected void ButtonLogOn_Click(object sender, EventArgs e)
        {
            logOnNu();
        }
        protected void Button1_Click(object sender, EventArgs e)
        {

              Server.Transfer("Default.aspx");
        }
        private void logOnNu()
        {
            ////----Til udvikling-----------------------
            //Session["Admin"] = 2;
            //Server.Transfer("Default.aspx");
            //return;
            ////----Til udvikling-----------------------


            int password = 0;
            try
            {
                password = Convert.ToInt32(TextBoxPassword.Text);
            }
            catch (Exception sk)
            {
                LabelBesked.Text = "Noget galt med password";
            }
            if (logOn(TextBoxEmail.Text, password))
            {
                Server.Transfer("Default.aspx");
            }
            else
            {
                LabelBesked.Text = "Wrong email or password";
            }

        }
        private bool logOn(String email, int password)
        {
            bool svar = false;
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["wenningsdbConnectionString1"].ToString());
                con.Open();
                //------Gemmer Sangen-------------------------------------------
                SqlCommand cmd = new SqlCommand("SELECT * FROM SangBruger WHERE email=@email AND password=@password;", con);
                cmd.Parameters.Add("@password", SqlDbType.Int);
                cmd.Parameters["@password"].Value = password;
                cmd.Parameters.Add("@email", SqlDbType.VarChar);
                cmd.Parameters["@email"].Value = email;

               SqlDataReader reader = cmd.ExecuteReader();
               if (reader.HasRows)
               {
                   // ListBox1.Items.Add("Song har item" + idValgt);
                   if (reader.Read())
                   {
                       int userID = reader.GetInt32(0);
                       String name = reader.GetString(3);
                       int type = reader.GetInt32(6);

                       Bruger bruger = new Bruger();
                       bruger.ID = userID;
                       bruger.name = name;
                       bruger.type = type;
                       Session["Bruger"] = bruger;
                       svar = true;
                       LabelBesked.Text = "OK";
                       if(bruger.type == 9)
                       {
                           Session["Admin"] = bruger;
                       }
                   }
                   else
                   {
                       Session["Admin"] = null;
                       Session["Bruger"] = null;
                       svar = false;
                       LabelBesked.Text = "ikke OK";
                   }
               }              
               con.Close();
            }
            catch (Exception lskd)
            {

            }
            return svar;
        }
    }
    //class Bruger
    //{
    //    public int userId = 0;
    //    public String navn = "";
    //}
}