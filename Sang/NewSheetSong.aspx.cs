﻿using NAudio.Midi;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;


namespace Sang
{
    public partial class NewSheetSong : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            // MaintainScrollPositionOnPostBack = true;


            if (Session["Bruger"] == null)
            {
                Server.Transfer("Default.aspx");
            }
            else
            {
                //-----------DIREKTE LINK----------------------------------------
                Bruger brug = (Bruger)Session["Bruger"];
                if (brug.ID == -1) //Så kommer man fra direkte link. Må stoppes.
                {
                    Session["Bruger"] = null;
                    return;
                }
                //------------------------------------------------------------------
                //------------Her kommer bruger direkte ind fra NewPianoRollSong-----------
                if (Session["MidiFileCreatedFromKeyboard"] != null)
                {
                    Session["MidiFileCreatedFromKeyboard"] = null;
                    DirektConverterMidiFile();
                }

            }

            if (!IsPostBack)//FØRSTE GANG.
            {
                //----------Til Midi til sang------------------
                Dictionary<int, String> listAkkorderMidi = GeneralData.getListAkkorder();
                DropDownListKey.DataSource = listAkkorderMidi;
                DropDownListKey.DataTextField = "Value";
                DropDownListKey.DataValueField = "Key";
                DropDownListKey.DataBind();

                //------------------------------------------


                // Session["Sang"] = null;
                DropDownListSelectMelody.DataSource = GeneralData.InstrumentNames;
                DropDownListSelectMelody.DataBind();

                DropDownListSelectChords.DataSource = GeneralData.InstrumentNames;
                DropDownListSelectChords.DataBind();


                Dictionary<int, String> listAkkorder = GeneralData.getListAkkorder();
                DropDownList2.DataSource = listAkkorder;
                DropDownList2.DataTextField = "Value";
                DropDownList2.DataValueField = "Key";
                DropDownList2.DataBind();

                Dictionary<int, String> listNodeLength = GeneralData.getListNodeLength();
                DropDownList1.DataSource = listNodeLength;
                DropDownList1.DataTextField = "Value";
                DropDownList1.DataValueField = "Key";
                DropDownList1.DataBind();
                DropDownList1.SelectedIndex = 8;


                Dictionary<int, String> listSharpFlat = new Dictionary<int, string>();

                listSharpFlat.Add(0, ""); //-1 betyder INGEN AKKORD/BASS.
                listSharpFlat.Add(-1, "b");
                listSharpFlat.Add(1, "#");

                DropDownListSharpFlat.DataSource = listSharpFlat;
                DropDownListSharpFlat.DataTextField = "Value";
                DropDownListSharpFlat.DataValueField = "Key";
                DropDownListSharpFlat.DataBind();

                Dictionary<int, String> listNodesPrLine = new Dictionary<int, string>();

                listNodesPrLine.Add(10, "10");
                listNodesPrLine.Add(11, "11");
                listNodesPrLine.Add(12, "12"); //-1 betyder INGEN AKKORD/BASS.
                listNodesPrLine.Add(13, "13"); //-1 betyder INGEN AKKORD/BASS.
                listNodesPrLine.Add(14, "14"); //-1 betyder INGEN AKKORD/BASS.
                listNodesPrLine.Add(15, "15"); //-1 betyder INGEN AKKORD/BASS.
                listNodesPrLine.Add(16, "16"); //-1 betyder INGEN AKKORD/BASS.
                listNodesPrLine.Add(17, "17"); //-1 betyder INGEN AKKORD/BASS.
                listNodesPrLine.Add(18, "18"); //-1 betyder INGEN AKKORD/BASS.
                listNodesPrLine.Add(19, "19"); //-1 betyder INGEN AKKORD/BASS.
                listNodesPrLine.Add(20, "20"); //-1 betyder INGEN AKKORD/BASS.
                listNodesPrLine.Add(21, "21"); //-1 betyder INGEN AKKORD/BASS.
                listNodesPrLine.Add(22, "22"); //-1 betyder INGEN AKKORD/BASS.
                listNodesPrLine.Add(23, "23"); //-1 betyder INGEN AKKORD/BASS.
                listNodesPrLine.Add(24, "24"); //-1 betyder INGEN AKKORD/BASS.
                listNodesPrLine.Add(25, "25"); //-1 betyder INGEN AKKORD/BASS.
                listNodesPrLine.Add(26, "26"); //-1 betyder INGEN AKKORD/BASS.
                listNodesPrLine.Add(27, "27"); //-1 betyder INGEN AKKORD/BASS.
                listNodesPrLine.Add(28, "28"); //-1 betyder INGEN AKKORD/BASS.
                listNodesPrLine.Add(29, "29"); //-1 betyder INGEN AKKORD/BASS.
                listNodesPrLine.Add(30, "30"); //-1 betyder INGEN AKKORD/BASS.

                DropDownListNodesPrLine.DataSource = listNodesPrLine;
                DropDownListNodesPrLine.DataTextField = "Value";
                DropDownListNodesPrLine.DataValueField = "Key";
                DropDownListNodesPrLine.DataBind();

                //----Midi DropDown----------------------------
                DropDownMidiPrLine.DataSource = listNodesPrLine;
                DropDownMidiPrLine.DataTextField = "Value";
                DropDownMidiPrLine.DataValueField = "Key";
                DropDownMidiPrLine.DataBind();

                Dictionary<int, String> listDistBetweenNodes = new Dictionary<int, string>();

                listDistBetweenNodes.Add(50, "5");
                listDistBetweenNodes.Add(60, "6");
                listDistBetweenNodes.Add(70, "7");
                listDistBetweenNodes.Add(80, "8");
                listDistBetweenNodes.Add(90, "9");
                listDistBetweenNodes.Add(100, "10");
                listDistBetweenNodes.Add(110, "11");
                listDistBetweenNodes.Add(120, "12");
                listDistBetweenNodes.Add(130, "13");
                listDistBetweenNodes.Add(140, "14");
                listDistBetweenNodes.Add(150, "15");
                DropDownListDistBeweenNodes.DataSource = listDistBetweenNodes;
                DropDownListDistBeweenNodes.DataTextField = "Value";
                DropDownListDistBeweenNodes.DataValueField = "Key";
                DropDownListDistBeweenNodes.DataBind();


                DropDownListChord.DataSource = listAkkorder;
                DropDownListChord.DataTextField = "Value";
                DropDownListChord.DataValueField = "Key";
                DropDownListChord.DataBind();

                DropDownListBass.DataSource = listAkkorder;
                DropDownListBass.DataTextField = "Value";
                DropDownListBass.DataValueField = "Key";
                DropDownListBass.DataBind();

                DropDownList3.DataSource = GeneralData.Beats;
                DropDownList3.DataBind();

            }
            if (RadioButtonList1.SelectedIndex == 2)//Akkorder
            {
                DropDownListChord.Focus();
            }
            else if (RadioButtonList1.SelectedIndex == 3)//Text
            {
                TextBoxSongText.Focus();
            }
            else if (RadioButtonList1.SelectedIndex == 1)//Node
            {
                DropDownList1.Focus();
            }
            if (Session["Admin"] != null)
            {
                //if (Session["Sang"] != null)
                //{
                //    Sang sang = (Sang)Session["Sang"];
                //    HttpContext.Current.Response.Write("<script LANGUAGE='JavaScript' >alert('" + "SangID=" + sang.ID + "')</script>");
                //}
            }
        }
        protected void xymetoden(Object sender, ImageClickEventArgs e)
        {
            int yDistanceCollision = 20;

            if (e.X == 0 && e.Y == 0)
            {  //Denne sker når man har bruge TextBoxExtra og trykker Enter, så skal intet tegnes.
                return;
            }



            if (Session["Sang"] != null)
            {
                Sang sang = (Sang)Session["Sang"];


                //Label1.Text = "Antal akkorder:" + sang.listAkkorder.Count;
                //----Akkord-----------------------------------------
                //----Akkord-----------------------------------------
                //----Akkord-----------------------------------------
                //----Akkord-----------------------------------------
                if (RadioButtonList1.SelectedIndex == 2)
                {
                    if (Session["EditChord"] != null) //EDIT af akkord
                    {
                        if (Session["EditChordSave"] == null)
                        {
                            // Indført 2016.07.04.
                            Akkord ak = sang.chordCollision(new Point(e.X, e.Y), 50, 50);
                            Session["EditChordSave"] = ak;
                        }
                        else //Så skal den ændres.
                        {
                            Akkord ak = (Akkord)Session["EditChordSave"];
                            ak.editIgang = false;

                            Akkord denValgteAkkord = (Akkord)Session["EditChordSave"];

                            if (CheckBoxDeleteChord.Checked) //---SLET Akkord------
                            {
                                sang.listAkkorder.Remove(denValgteAkkord);

                            }
                            else //---ÆNDRE akkord------
                            {
                                //Skal overskrive alt
                                //---Udvidet---2016.06.29----Fordi Y skal være den samme og ikke ændres, ser irriterende ud, når den ændrede akkord ikke er lige med de andre.
                                //---------------------------Så bruges denne i læste linje-----------------------------
                                ak.pos = new Point(e.X, denValgteAkkord.pos.Y);
                                ak.keyNumber = Convert.ToInt32(DropDownListChord.SelectedValue);
                                ak.bass = Convert.ToInt32(DropDownListBass.SelectedValue);
                                ak.extraName = TextBoxExtra.Text;

                                //-----------------------------------------------------------
                            }
                            if (!CheckBoxChord.Checked)
                            {
                                DropDownListChord.SelectedIndex = 0;
                            }
                            if (!CheckBoxBass.Checked)
                            {
                                DropDownListBass.SelectedIndex = 0;
                            }
                            if (TextBoxExtra.Text.Length > 0)
                            {
                                if (!CheckBoxExtra.Checked)
                                {
                                    TextBoxExtra.Text = "";
                                }
                            }
                            Session["EditChordSave"] = null;
                        }
                    }
                    else //Laver ny Akkord
                    {
                        if (DropDownListChord.SelectedIndex > 0)
                        {
                            Akkord akkord = new Akkord();
                            if (CheckBoxAkkordSameHeight.Checked)
                            {
                                if (sang.listAkkorder.Count > 0)
                                {
                                    //Så kikker vi igennem listen for at se om der er et nivo vi skal bruge
                                    int yFundet = -1;
                                    foreach (Akkord ak in sang.listAkkorder)
                                    {
                                        if ((ak.pos.Y + yDistanceCollision) > e.Y && (ak.pos.Y - yDistanceCollision) < e.Y)
                                        {
                                            yFundet = ak.pos.Y;
                                            break;
                                        }
                                    }
                                    if (yFundet == -1)
                                    {
                                        akkord.pos = new Point(e.X, e.Y);
                                    }
                                    else
                                    {
                                        akkord.pos = new Point(e.X, yFundet);
                                    }
                                }
                                else
                                {
                                    akkord.pos = new Point(e.X, e.Y);
                                }
                            }
                            else
                            {
                                akkord.pos = new Point(e.X, e.Y);
                            }

                            akkord.id = sang.listAkkorder.Count;
                            //  akkord.name = DropDownList1.SelectedItem.Text;
                            akkord.keyNumber = Convert.ToInt32(DropDownListChord.SelectedValue);
                            akkord.bass = Convert.ToInt32(DropDownListBass.SelectedValue);
                            if (!CheckBoxChord.Checked)
                            {
                                DropDownListChord.SelectedIndex = 0;
                            }
                            if (!CheckBoxBass.Checked)
                            {
                                DropDownListBass.SelectedIndex = 0;
                            }
                            if (TextBoxExtra.Text.Length > 0)
                            {
                                akkord.extraName = TextBoxExtra.Text;
                                // Response.Write("<script LANGUAGE='JavaScript' >alert('"+"EXTRA"+"')</script>");

                                if (!CheckBoxExtra.Checked)
                                {
                                    TextBoxExtra.Text = "";

                                }
                            }
                            akkord.id = sang.listAkkorder.Count + 1;
                            sang.listAkkorder.Add(akkord);
                        }

                    }
                }


                //----Node-----------------------------------------
                //----Node-----------------------------------------
                //----Node-----------------------------------------
                //----Node-----------------------------------------
                if (RadioButtonList1.SelectedIndex == 1)
                {
                    if (Session["EditNode"] != null) //Så er vi i Edit mode.
                    {
                        //-----Ingen Note er valgt endnu, men måske vi rammer en nu.
                        //-----Ingen Note er valgt endnu, men måske vi rammer en nu.
                        if (Session["EditNodeSave"] == null) 
                        {
                            Noder no = sang.nodeCollision(new Point(e.X, e.Y), 20, 80);
                            Session["EditNodeSave"] = no;

                            //----DELETE NOTE start-----------------------
                            //----DELETE NOTE start-----------------------
                            //----DELETE NOTE start----------------------- 
                       
                            if (CheckBoxDeleteNote.Checked && no != null)
                            {
                                //Først skal ALLE akkorder sættes til ikke edit igang.
                                foreach (Akkord a in sang.listAkkorder)
                                {
                                    a.editIgang = false;
                                }
                                //Først slette den valgte node med (mulig) akkord.
                                no.parrentNodeLinje.removeNoteAndChord(no);

                                //Så skal alle de andre noder som kommer efter flyttes ét til venstre.
                                foreach (Noder n in no.parrentNodeLinje.listNoder)
                                {  
                                    if (n.sequenceNummer > no.sequenceNummer)
                                    {
                                        n.sequenceNummer--;
                                        n.kalibrerNewXposForNoteAndChord();
                                    }
                                }//Nu er alle flyttet ét tilbage.

                                //så sættes ALLE akkorder sættes til ikke edit igang.
                                foreach (Akkord a in sang.listAkkorder)
                                {
                                    a.editIgang = false;
                                }
                                Session["EditNodeSave"] = null;
                            }
                            //----DELETE NOTE end-----------------------
                            //----DELETE NOTE end-----------------------
                            //----DELETE NOTE end-----------------------
                        }
                        else //Node er valgt. Så skal den ændres.
                        {
                            Noder no = (Noder)Session["EditNodeSave"];

                            //----INSERT NOTE Start--------------------------------
                            //----INSERT NOTE Start--------------------------------
                            //----INSERT NOTE Start--------------------------------
                            if (CheckBoxInsertNote.Checked && no != null)
                            {
                                no.editIgang = false;
                                int seqNr = no.sequenceNummer;
                                //Først skal ALLE akkorder sættes til ikke edit igang.
                                foreach (Akkord a in sang.listAkkorder)
                                {
                                    a.editIgang = false;
                                }

                                foreach (Noder n in no.parrentNodeLinje.listNoder)
                                {  // det første er at flytte alle noder + akkorder ét frem, til højre.
                                    if (n.sequenceNummer >= seqNr)
                                    {
                                        n.sequenceNummer++;
                                        //Her bliver akkorden sat til editIgang true, således at den
                                        //ikke skal flytte mere end én gang til højre.
                                        n.kalibrerNewXposForNoteAndChord();
                                    }                                   
                                }
                                //Så sættes alle akorder igen til ikke at være i edit tilstand.
                                foreach (Akkord a in sang.listAkkorder)
                                {
                                    a.editIgang = false;
                                }
                                //---- Så skal der laves en NY Node, og den skal sætte ind også.-------
                                //---- Så skal der laves en NY Node, og den skal sætte ind også.-------
                              
                                //---Midlertidig for testing-------
                                //Session["EditNodeSave"] = null;
                                //return;

                                Noder node = new Noder();
                                node.pos = new Point(e.X, e.Y - (node.heightForDrawing / 2));//Således at museklikket passer.
                                int nodelength = 4;//Default, men ændres af DropDownList boxen.
                                try
                                {
                                    nodelength = Convert.ToInt32(DropDownList1.SelectedValue);
                                }
                                catch (Exception lksd) { }
                                node.length = nodelength;

                                int sharpFlat = 0;//Betyder ikke valgt. -1 er flat og +1 er sharp.
                                try
                                {
                                    sharpFlat = Convert.ToInt32(DropDownListSharpFlat.SelectedValue);
                                }
                                catch (Exception lks) { }
                                DropDownListSharpFlat.SelectedIndex = 0;
                                node.sharpOrFlat = sharpFlat;

                                // Så indsættes den og kalibreres på en specielt måde.
                                node.parrentNodeLinje = no.parrentNodeLinje;
                                no.parrentNodeLinje.listNoder.Insert(seqNr, node);
                                //----------
                                node.sequenceNummer = seqNr;
                                node.kalibrerNewXposForNoteAndChord();

                                node.kalibrer();
                                // Så kalibreres den på den nye måde med nyt sequence nummer
                                // Den nye node skal have Y værdien som no havde.
                              
                              

                                //----- Så kan det være at der nu er for mange noder til aktuel nodelinjer, så må den sidste fjernes.
                                //----- Så kan det være at der nu er for mange noder til aktuel nodelinjer, så må den sidste fjernes.
                                if (sang.aktuelNodeLinje.listNoder.Count > sang.aktuelNodeLinje.maxNumberOfNodes)
                                {
                                    Noder gemmer = sang.aktuelNodeLinje.listNoder[0];
                                    foreach(Noder n in sang.aktuelNodeLinje.listNoder)
                                    {
                                        if(gemmer.sequenceNummer < n.sequenceNummer)
                                        {
                                            gemmer = n;
                                        }
                                    }
                                    sang.aktuelNodeLinje.listNoder.Remove(gemmer);
                                }
                                Session["EditNodeSave"] = null;
                                //----INSERT NOTE end--------------------------------
                                //----INSERT NOTE end--------------------------------
                                //----INSERT NOTE end--------------------------------
                            }
                            else
                            {
                                //----EDIT NOTE Start--------------------------
                                //----EDIT NOTE Start--------------------------
                                //----EDIT NOTE Start--------------------------
                                no.pos.Y = e.Y - (no.heightForDrawing / 2);

                                //2016.06.26. Edit havde ikke taget højde for at nodelinjen måske var i anden toneart end C.
                                no.parrentNodeLinje.callibrerNyKeyChange();
                                no.kalibrer();
                                no.editIgang = false;
                                try
                                {
                                    no.length = Convert.ToInt32(DropDownList1.SelectedValue);
                                }
                                catch (Exception lksd) { }

                                int sharpFlat = 0;//Betyder ikke valgt. -1 er flat og +1 er sharp.
                                try
                                {
                                    sharpFlat = Convert.ToInt32(DropDownListSharpFlat.SelectedValue);
                                }
                                catch (Exception lks) { }
                                no.sharpOrFlat = sharpFlat;

                                Session["EditNodeSave"] = null;
                                //----EDIT NOTE end--------------------------
                                //----EDIT NOTE end--------------------------
                                //----EDIT NOTE end--------------------------
                            }
                        }

                    }
                    else // NY NODE LAVES.
                    {

                        Noder node = new Noder();
                        node.pos = new Point(e.X, e.Y - (node.heightForDrawing / 2));//Således at museklikket passer.
                        int nodelength = 4;//Default, men ændres af DropDownList boxen.
                        try
                        {
                            nodelength = Convert.ToInt32(DropDownList1.SelectedValue);
                        }
                        catch (Exception lksd) { }
                        node.length = nodelength;

                        int sharpFlat = 0;//Betyder ikke valgt. -1 er flat og +1 er sharp.
                        try
                        {
                            sharpFlat = Convert.ToInt32(DropDownListSharpFlat.SelectedValue);
                        }
                        catch (Exception lks) { }
                        DropDownListSharpFlat.SelectedIndex = 0;
                        node.sharpOrFlat = sharpFlat;

                        if (sang.aktuelNodeLinje.listNoder != null)
                        {
                            node.sequenceNummer = sang.aktuelNodeLinje.listNoder.Count;
                        }
                        if (node.sequenceNummer < sang.aktuelNodeLinje.maxNumberOfNodes)
                        {
                            //Så ved hver enkel node, hvilken nodelinje den tilhører.
                            node.parrentNodeLinje = sang.aktuelNodeLinje;
                            //og hver node gemmes hos én nodeLinje, således kan man
                            //skifte toneart.
                            sang.aktuelNodeLinje.listNoder.Add(node);

                            node.kalibrer();
                            //     Response.Write("<script LANGUAGE='JavaScript' >alert('"+sang.aktuelNodeLinje.maxNumberOfNodes+"')</script>");
                        }
                        else //Så skal der laves et NYT nodeLinje
                        {
                            int currentY = sang.aktuelNodeLinje.pos.Y;
                            sang.newNodeLinje();
                            //Så ved hver enkel node, hvilken nodelinje den tilhører.
                            node.parrentNodeLinje = sang.aktuelNodeLinje;
                        }
                    }
                }
                //----SangText-----------------------------------------
                //----SangText-----------------------------------------
                //----SangText-----------------------------------------
                //----SangText-----------------------------------------
                if (RadioButtonList1.SelectedIndex == 3)
                {
                    if (Session["EditText"] != null) //Så er vi i Edit mode.
                    {
                        if (Session["EditTextSave"] == null)
                        {
                            TextLinje te = sang.textCollision(new Point(e.X, e.Y), 20, 80);
                            Session["EditTextSave"] = te;
                        }
                        else //Så skal den ændres.
                        {
                            TextLinje te = (TextLinje)Session["EditTextSave"];
                            te.pos = new Point(e.X, e.Y);
                            te.text = TextBoxSongText.Text;
                            te.editIgang = false;
                            TextBoxSongText.Text = "";
                            Session["EditTextSave"] = null;
                        }
                    }
                    else  //NY TEXT LAVES.
                    {
                        TextLinje tl = new TextLinje();
                        if (CheckBoxTextSameHeight.Checked)
                        {
                            if (sang.listTexter.Count > 0)
                            {
                                //Så kikker vi igennem listen for at se om der er et nivo vi skal bruge
                                int yFundet = -1;
                                foreach (TextLinje tx in sang.listTexter)
                                {
                                    if ((tx.pos.Y + yDistanceCollision) > e.Y && (tx.pos.Y - yDistanceCollision) < e.Y)
                                    {
                                        yFundet = tx.pos.Y;
                                        break;
                                    }
                                }
                                if (yFundet == -1)
                                {
                                    tl.pos = new Point(e.X, e.Y);
                                }
                                else
                                {
                                    tl.pos = new Point(e.X, yFundet);
                                }
                            }
                            else
                            {
                                tl.pos = new Point(e.X, e.Y);
                            }
                        }
                        else
                        {
                            tl.pos = new Point(e.X, e.Y);
                        }
                        if (TextBoxSongText.Text.Length > 0)
                        {
                            tl.text = TextBoxSongText.Text;
                            sang.listTexter.Add(tl);
                            TextBoxSongText.Text = "";
                        }
                    }
                }
            }
        }

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["Sang"] != null)
            {
                Sang sang = (Sang)Session["Sang"];
                sang.aktuelNodeLinje.key = Convert.ToInt32(DropDownList2.SelectedValue);

                ////--Hvis NodeLinjer har skiftet tone art, må vi kalibrere den nu----
                sang.aktuelNodeLinje.callibrerNyKeyChange();
                sang.opdaterNoderAfterKeyChange();
                DropDownList2.SelectedIndex = 0;
            }
        }

        protected void ButtonUndo_Click(object sender, EventArgs e)
        {
            if (Session["Sang"] != null)
            {
                Sang sang = (Sang)Session["Sang"];

                if (sang.aktuelNodeLinje.listNoder.Count > 0)
                {
                    Noder slettes = sang.aktuelNodeLinje.listNoder[sang.aktuelNodeLinje.listNoder.Count - 1];
                    sang.aktuelNodeLinje.listNoder.Remove(slettes);
                    if (slettes.akkord != null)
                    {
                        slettes.akkord = null;
                    }
                    Session["Sang"] = sang;
                }
                else
                {
                    if (sang.listNodeLinjer.Count > 1)
                    {
                        sang.listNodeLinjer.Remove(sang.aktuelNodeLinje);
                        sang.aktuelNodeLinje = sang.listNodeLinjer[sang.listNodeLinjer.Count - 1];
                    }
                }

            }
        }

        protected void DropDownListNodesPrLine_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["Sang"] != null)
            {
                Sang sang = (Sang)Session["Sang"];
                sang.changeSongLength(Convert.ToInt32(DropDownListNodesPrLine.SelectedValue));

            }


        }

        protected void ButtonSaveSheet_Click(object sender, EventArgs e)
        {



        }
        private void Save()
        {
            if (CheckBox1.Visible == false)
            {
                CheckBox1.Visible = true;
            }
            if (CheckBox1.Visible == true)
            {
                if (CheckBox1.Checked == true)
                {
                    if (Session["Sang"] != null)
                    {
                        Sang sang = (Sang)Session["Sang"];
                        if (sang.title.Length > 2)
                        {
                            int brugerID = 1;
                            if (Session["Bruger"] != null)
                            {
                                Bruger bruger = (Bruger)Session["Bruger"];
                                brugerID = bruger.ID;
                            }
                            bool gikFint = saveSong(sang, brugerID);
                            if (gikFint)
                            {
                                Response.Write("<script LANGUAGE='JavaScript' >alert('" + "Your Sheet Song has been saved." + "')</script>");
                                Server.Transfer("Default.aspx");
                            }
                        }
                        else
                        {
                            Response.Write("<script LANGUAGE='JavaScript' >alert('" + "No song title found." + "')</script>");

                        }
                    }
                }
                else
                {
                    Response.Write("<script LANGUAGE='JavaScript' >alert('" + "Check the checkbox and then try and Save again." + "')</script>");

                }
            }

        }
        private bool saveSong(Sang sang, int userid)
        {
            bool svar = false;
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["wenningsdbConnectionString1"].ToString());
                con.Open();
                //------Gemmer Sangen-------------------------------------------
                SqlCommand cmdSheet = new SqlCommand("insert into SangSheet (titel, lyric, melody, date, userID, type, height, width, tempo) values(@titel,@lyric,@melody, @date, @userID,@type, @height, @width, @tempo) SELECT SCOPE_IDENTITY();", con);

                cmdSheet.Parameters.AddWithValue("@titel", sang.title);
                cmdSheet.Parameters.AddWithValue("@lyric", sang.lyricWriter);
                cmdSheet.Parameters.AddWithValue("@melody", sang.melodyWriter);
                cmdSheet.Parameters.AddWithValue("@date", DateTime.Now.ToString("yyyy.MM.dd"));
                cmdSheet.Parameters.AddWithValue("@userID", userid);
                cmdSheet.Parameters.AddWithValue("@type", 0); //0=lukket, 1=åben for alle. Dvs, den skal godkendes først.
                cmdSheet.Parameters.AddWithValue("@height", sang.height);
                cmdSheet.Parameters.AddWithValue("@width", sang.width);
                cmdSheet.Parameters.AddWithValue("@tempo", sang.tempo);

                object temp = cmdSheet.ExecuteScalar();
                String aktueltID = temp.ToString();//holder ID som lige er blevet oprettet efter insert
                cmdSheet.Dispose();

                //------Gemmer alle text linjerne---------------------------------------------
                SqlCommand cmdText = new SqlCommand("insert into SangSheetTextLinjer  (sangID, text, posX, posY, linjeID) values(@sangID,@text,@posX,@posY,@linjeID) ;", con);
                cmdText.Parameters.Add("@sangID", System.Data.SqlDbType.VarChar);
                cmdText.Parameters.Add("@text", System.Data.SqlDbType.VarChar);
                cmdText.Parameters.Add("@posX", System.Data.SqlDbType.Int);
                cmdText.Parameters.Add("@posY", System.Data.SqlDbType.Int);
                cmdText.Parameters.Add("@linjeID", System.Data.SqlDbType.Int);
                foreach (TextLinje txLinje in sang.listTexter)
                {
                    cmdText.Parameters["@sangID"].Value = aktueltID;
                    cmdText.Parameters["@text"].Value = txLinje.text;
                    cmdText.Parameters["@posX"].Value = txLinje.pos.X;
                    cmdText.Parameters["@posY"].Value = txLinje.pos.Y;
                    cmdText.Parameters["@linjeID"].Value = txLinje.id;

                    cmdText.ExecuteNonQuery();
                }
                cmdText.Dispose();
                //------Gemmer alle Akkorder---------------------------------------------
                SqlCommand cmdAkkorder = new SqlCommand("insert into SangSheetAkkorder  (sangID, posX, posY, keyNumber, extraName, bass) values(@sangID, @posX, @posY, @keyNumber, @extraName, @bass) ;", con);
                cmdAkkorder.Parameters.Add("@sangID", System.Data.SqlDbType.VarChar);
                cmdAkkorder.Parameters.Add("@posX", System.Data.SqlDbType.Int);
                cmdAkkorder.Parameters.Add("@posY", System.Data.SqlDbType.Int);
                cmdAkkorder.Parameters.Add("@keyNumber", System.Data.SqlDbType.Int);
                cmdAkkorder.Parameters.Add("@bass", System.Data.SqlDbType.Int);
                cmdAkkorder.Parameters.Add("@extraName", System.Data.SqlDbType.VarChar);


                foreach (Akkord akkord in sang.listAkkorder)
                {
                    cmdAkkorder.Parameters["@sangID"].Value = aktueltID;
                    cmdAkkorder.Parameters["@posX"].Value = akkord.pos.X;
                    cmdAkkorder.Parameters["@posY"].Value = akkord.pos.Y;
                    cmdAkkorder.Parameters["@keyNumber"].Value = akkord.keyNumber;
                    cmdAkkorder.Parameters["@bass"].Value = akkord.bass;
                    if (akkord.extraName != null)
                        cmdAkkorder.Parameters["@extraName"].Value = akkord.extraName;
                    else
                        cmdAkkorder.Parameters["@extraName"].Value = "";

                    cmdAkkorder.ExecuteNonQuery();
                }
                cmdAkkorder.Dispose();
                //------Node Linjer og dens Noder---------------------------------------------

                SqlCommand cmdLinjer = new SqlCommand("insert into SangSheetNodeLinjer  (sangID, posX, posY,Linjekey, Linjelength,MaxNumberOfNodes) values(@sangID, @posX, @posY,@Linjekey, @Linjelength,@MaxNumberOfNodes) SELECT SCOPE_IDENTITY();", con);
                cmdLinjer.Parameters.Add("@sangID", System.Data.SqlDbType.VarChar);
                cmdLinjer.Parameters.Add("@posX", System.Data.SqlDbType.Int);
                cmdLinjer.Parameters.Add("@posY", System.Data.SqlDbType.Int);
                cmdLinjer.Parameters.Add("@Linjekey", System.Data.SqlDbType.Int);
                cmdLinjer.Parameters.Add("@Linjelength", System.Data.SqlDbType.Int);
                cmdLinjer.Parameters.Add("@MaxNumberOfNodes", System.Data.SqlDbType.Int);

                foreach (NodeLinjer nl in sang.listNodeLinjer)
                {
                    cmdLinjer.Parameters["@sangID"].Value = aktueltID;
                    cmdLinjer.Parameters["@posX"].Value = nl.pos.X;
                    cmdLinjer.Parameters["@posY"].Value = nl.pos.Y;
                    cmdLinjer.Parameters["@Linjekey"].Value = nl.key;
                    cmdLinjer.Parameters["@Linjelength"].Value = nl.length;
                    cmdLinjer.Parameters["@MaxNumberOfNodes"].Value = nl.maxNumberOfNodes;

                    object sheetTemp = cmdLinjer.ExecuteScalar();
                    String sheetID = sheetTemp.ToString();//holder ID som lige er blevet oprettet efter insert

                    //-----Noder-----Noder------
                    SqlCommand cmdNoder = new SqlCommand("insert into SangSheetNoder  (NodeLinjeID, posX, posY, keyNumber,stafNumber,Nodelength,sequenceNumber,sharpOrFlat,type, SangID) values(@NodeLinjeID, @posX, @posY, @keyNumber,@stafNumber,@Nodelength,@sequenceNumber,@sharpOrFlat,@type, @SangID) ;", con);
                    cmdNoder.Parameters.Add("@NodeLinjeID", System.Data.SqlDbType.VarChar);
                    cmdNoder.Parameters.Add("@posX", System.Data.SqlDbType.Int);
                    cmdNoder.Parameters.Add("@posY", System.Data.SqlDbType.Int);
                    cmdNoder.Parameters.Add("@keyNumber", System.Data.SqlDbType.Int);
                    cmdNoder.Parameters.Add("@stafNumber", System.Data.SqlDbType.Int);
                    cmdNoder.Parameters.Add("@Nodelength", System.Data.SqlDbType.Int);
                    cmdNoder.Parameters.Add("@sequenceNumber", System.Data.SqlDbType.Int);
                    cmdNoder.Parameters.Add("@sharpOrFlat", System.Data.SqlDbType.Int);
                    cmdNoder.Parameters.Add("@type", System.Data.SqlDbType.Int);
                    cmdNoder.Parameters.Add("@SangID", System.Data.SqlDbType.VarChar); //Bruges når der skal slettes sang.

                    foreach (Noder no in nl.listNoder)
                    {
                        cmdNoder.Parameters["@NodeLinjeID"].Value = sheetID;
                        cmdNoder.Parameters["@posX"].Value = no.pos.X;
                        cmdNoder.Parameters["@posY"].Value = no.pos.Y;
                        cmdNoder.Parameters["@keyNumber"].Value = no.keyNumber;
                        cmdNoder.Parameters["@stafNumber"].Value = no.StafNumber;
                        cmdNoder.Parameters["@Nodelength"].Value = no.length;
                        cmdNoder.Parameters["@sequenceNumber"].Value = no.sequenceNummer;
                        cmdNoder.Parameters["@sharpOrFlat"].Value = no.sharpOrFlat;
                        cmdNoder.Parameters["@type"].Value = no.type;
                        cmdNoder.Parameters["@SangID"].Value = aktueltID; //Bruges når der skal slettes sang.

                        cmdNoder.ExecuteNonQuery();
                    }
                    cmdNoder.Dispose();
                }
                cmdLinjer.Dispose();
                con.Close();
                try
                {
                    //Try catch, fordi når man tester lokalt, kan den ikke send mail, kun public udgaven, og så kører programmet videre uden at crashe.
                    sendMails sm = new sendMails();
                    sm.sendMail("New Sheet Song Created by userID: " + userid + " with the title: " + sang.title.TrimStart());

                }
                catch (Exception edsl)
                { }
                svar = true;
            }
            catch (Exception exs)
            {
                String besked = exs.Message;
                Response.Write("<script LANGUAGE='JavaScript' >alert('" + "Saving the song went wrong. Please try again. Error message=" + besked + "')</script>");
                String dato = "fejl";

                using (StreamWriter w = new StreamWriter(Server.MapPath("~/Log/" + dato + ".txt"), true))
                {
                    w.WriteLine(exs.ToString()); // Write the text
                }

                //        Server.Transfer("Default.aspx");
                //     TextBoxBesked.Text = "ID: Error" + exs.ToString();
            }

            return svar;
        }

        static bool kanBruges(string value)
        {
            bool svar = true;
            //Bruges ikke alligevle.....
            //const string quote = "\"";
            //svar = Regex.IsMatch(value, @"^[-a-zA-Z0-9' ''–'“„,.;?!:äÐæÆøØåÅðÐíÍýÝáÁúÚóÓ" + quote + Environment.NewLine + "]*$");
            if (value.Contains("<") || value.Contains(">"))
            {
                svar = false;
            }

            return svar;
        }

        protected void ButtonSave_Click(object sender, EventArgs e)
        {
            Label_Besked.Text = "";
            if (!kanBruges(TextBoxTitle.Text))
            {
                Label_Besked.Visible = true;
                Label_Besked.Text = Label_Besked.Text + Environment.NewLine + "Some characters in the title are not allowed";
                return;
            }
            if (TextBoxTitle.Text.Length < 2)
            {
                Label_Besked.Visible = true;
                Label_Besked.Text = Label_Besked.Text + Environment.NewLine + "Title missing. A song must have a title.";
                return;
            }
            if (!kanBruges(TextBoxLyric.Text))
            {
                Label_Besked.Visible = true;
                Label_Besked.Text = Label_Besked.Text + Environment.NewLine + "Some characters in the lyric owner are not allowed";
                return;
            }
            if (!kanBruges(TextBoxMelody.Text))
            {
                Label_Besked.Visible = true;
                Label_Besked.Text = Label_Besked.Text + Environment.NewLine + "Some characters in the melody owner are not allowed";
                return;
            }


            if (Session["Sang"] != null)
            {
                Sang sang = (Sang)Session["Sang"];
                if (TextBoxTitle.Visible == true)
                {
                    sang.title = TextBoxTitle.Text;
                    sang.melodyWriter = TextBoxMelody.Text;
                    sang.lyricWriter = TextBoxLyric.Text;
                }
            }
        }

        protected void btn_SaveSong_Click(object sender, EventArgs e)
        {
            Save();
        }

        protected void btn_UndoChord_Click(object sender, EventArgs e)
        {
            if (Session["Sang"] != null)
            {
                Sang sang = (Sang)Session["Sang"];

                if (sang.listAkkorder.Count > 0)
                {
                    sang.listAkkorder.RemoveAt(sang.listAkkorder.Count - 1);
                    Session["Sang"] = sang;
                }
            }

        }



        protected void DropDownListBass_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void DropDownListChord_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (RadioButtonList1.SelectedValue.Equals("Titel"))
            {
                showTitel();
            }
            else
            {
                hideTitel();
            }
            if (RadioButtonList1.SelectedValue.Equals("Note"))
            {
                showNode();
            }
            else
            {
                hideNode();
            }
            if (RadioButtonList1.SelectedValue.Equals("Chord"))
            {
                showChord();
            }
            else
            {
                hideChord();
            }
            if (RadioButtonList1.SelectedValue.Equals("Text"))
            {
                showSongText();
            }
            else
            {
                hideSongText();
            }
            if (RadioButtonList1.SelectedValue.Equals("Save"))
            {
                showSave();
            }
            else
            {
                hideSave();
            }
            if (RadioButtonList1.SelectedValue.Equals("Play"))
            {
                showPlaySong();
            }
            else
            {
                hidePlaySong();
            }
            if (RadioButtonList1.SelectedValue.Equals("Midi"))
            {
                showMidi();
            }
            else
            {
                hideMidi();
            }
            if (RadioButtonList1.SelectedValue.Equals("Seq"))
            {
                showSequencer();
            }
            else
            {
            }


        }
        //KUN DENNE I BRUG NU....Den anden er slettet.
        public void createMidiSong(int melodyInstrument, int ChordInstrument, bool ChordsIncluded)
        {
            if (Session["Sang"] != null)
            {
                Sang sang = (Sang)Session["Sang"];
                int tempo = sang.tempo;
                int taktDele = 32;
                MIDISong song = new MIDISong();

                song.AddTrack("track0");
                song.SetTimeSignature(0, 4, 4);
                song.SetTempo(0, GeneralData.Beats[tempo] * 2);
                song.SetChannelInstrument(0, 0, melodyInstrument);

                song.AddTrack("track1");
                song.SetTimeSignature(1, 4, 4);
                song.SetTempo(1, GeneralData.Beats[tempo] * 2);
                song.SetChannelInstrument(1, 1, ChordInstrument);
                song.AddTrack("track2");
                song.SetTimeSignature(2, 4, 4);
                song.SetTempo(2, GeneralData.Beats[tempo] * 2);
                song.SetChannelInstrument(2, 1, ChordInstrument);
                song.AddTrack("track3");
                song.SetTimeSignature(3, 4, 4);
                song.SetTempo(3, GeneralData.Beats[tempo] * 2);
                song.SetChannelInstrument(3, 1, ChordInstrument);
                song.AddTrack("track4");
                song.SetTimeSignature(4, 4, 4);
                song.SetTempo(4, GeneralData.Beats[tempo] * 2);
                song.SetChannelInstrument(4, 1, ChordInstrument);

                song.AddTrack("track5");//BASS
                song.SetTimeSignature(5, 4, 4);
                song.SetTempo(5, GeneralData.Beats[tempo] * 2);
                song.SetChannelInstrument(5, 1, ChordInstrument);

                if (ChordsIncluded == true)
                {
                    sang.callibrerAkkorderTilNoder();
                }
                else
                {
                    sang.nullstillAkkorderTilNoder();
                }
                //-----Starter med at indsætte én tom node, fordi
                //-----en afspiller til animationvisning af aktiv node, ikke tager den første
                //-----node med, ved ikke hvorfor.
                song.AddNote(0, 1, -1, 2);
                song.AddNote(1, 1, -1, 2);
                song.AddNote(2, 1, -1, 2);
                song.AddNote(3, 1, -1, 2);
                song.AddNote(4, 1, -1, 2);
                song.AddNote(5, 1, -1, 2);

                foreach (NodeLinjer nl in sang.listNodeLinjer)
                {
                    foreach (Noder no in nl.listNoder)
                    {
                        if ((no.keyNumber > 0 && no.keyNumber < 50) && (no.length > 0 && no.length < 100))
                        {
                            //Almindelige noder. NU er det blevet til 32 dele.
                            int nodeLengte = (100 / taktDele) * no.length;

                            //Melodi
                            song.AddNote(0, 0, 23 + 12 + no.keyNumber, nodeLengte);

                            //Akkord
                            if (no.akkord == null)
                            {
                                song.AddNote(1, 1, -1, nodeLengte);
                                song.AddNote(2, 1, -1, nodeLengte);
                                song.AddNote(3, 1, -1, nodeLengte);
                                song.AddNote(4, 1, -1, nodeLengte);
                            }
                            else
                            {
                                for (int i = 0; i < 4; i++) //VIGTIGT. ALLE må køre 4 gange, eller bliver det hele forskudt.
                                {
                                    if (i < no.akkord.getAkkordNodesToPlay().Count)
                                    {
                                        song.AddNote(i + 1, 1, 23 + 12 + 12 + no.akkord.getAkkordNodesToPlay()[i] + no.akkord.keyNumber, nodeLengte);
                                    }
                                    else
                                    {
                                        song.AddNote(i + 1, 1, -1, nodeLengte); //Sætter tom ind.
                                    }
                                }
                            }
                            //BASS BASS---------------------
                            if (no.akkord == null)
                            {
                                song.AddNote(5, 1, -1, nodeLengte);
                            }
                            else
                            {
                                if (no.akkord.bass > 0)
                                {
                                    song.AddNote(5, 1, 23 + 12 + no.akkord.bass, nodeLengte);
                                }
                                else //INGEN BASS, MEN SÅ SPILLER VI GRUNDTONENS BASS.
                                {
                                    //  song.AddNote(5, 1, -1, nodeLengte);
                                    song.AddNote(5, 1, 23 + 12 + no.akkord.getAkkordNodesToPlay()[0] + no.akkord.keyNumber, nodeLengte);
                                }
                            }
                        }
                        else if (no.length > 100 && no.length < 200)
                        {  //Pauser = -1 . Pauser starter med 100, derfor trækkse 100 bare fra.
                            int nodeLengte = (100 / taktDele) * (no.length - 100);
                            //Pausen
                            song.AddNote(0, 0, -1, nodeLengte);

                            if (no.akkord == null)
                            {
                                //Akkorderne
                                song.AddNote(1, 1, -1, nodeLengte);
                                song.AddNote(2, 1, -1, nodeLengte);
                                song.AddNote(3, 1, -1, nodeLengte);
                                song.AddNote(4, 1, -1, nodeLengte);
                            }
                            else
                            {
                                for (int i = 0; i < 4; i++) //VIGTIGT. ALLE må køre 4 gange, eller bliver det hele forskudt.
                                {
                                    if (i < no.akkord.getAkkordNodesToPlay().Count)
                                    {
                                        song.AddNote(i + 1, 1, 23 + 12 + 12 + no.akkord.getAkkordNodesToPlay()[i] + no.akkord.keyNumber, nodeLengte);
                                    }
                                    else
                                    {
                                        song.AddNote(i + 1, 1, -1, nodeLengte); //Sætter tom ind.
                                    }
                                }
                            }
                            //BASS BASS---------------------
                            if (no.akkord == null)
                            {
                                song.AddNote(5, 1, -1, nodeLengte);
                            }
                            else
                            {
                                if (no.akkord.bass > 0)
                                {
                                    song.AddNote(5, 1, 23 + 12 + no.akkord.bass, nodeLengte);
                                }
                                else
                                {
                                    song.AddNote(5, 1, -1, nodeLengte);
                                }
                            }
                        }
                    }
                }
                try
                {



                    MemoryStream ms = new MemoryStream();
                    song.Save(ms);
                    ms.Seek(0, SeekOrigin.Begin);

                    using (FileStream file = new FileStream(Server.MapPath("~/midi/sang.mid"), FileMode.Create, System.IO.FileAccess.Write))
                    {
                        byte[] bytes = new byte[ms.Length];
                        ms.Read(bytes, 0, (int)ms.Length);
                        file.Write(bytes, 0, bytes.Length);
                        ms.Close();
                    }


                    //song.Save(ms);

                    //ms.Seek(0, SeekOrigin.Begin);
                    //byte[] src = ms.GetBuffer();
                    //byte[] dst = new byte[src.Length];
                    //for (int i = 0; i < src.Length; i++)
                    //{
                    //    dst[i] = src[i];
                    //}
                    //ms.Close();

                    //string strTempFileName = Path.ChangeExtension("midi/sang", ".mid");
                    //FileStream objWriter = File.Create(Server.MapPath("~/midi/sang.mid"));

                    //objWriter.Write(dst, 0, dst.Length);
                    //objWriter.Close();
                    //objWriter.Dispose();
                    //objWriter = null;
                }
                catch { }
            }

        }

        //En kopi som har hver enkel stemme til sin egen channel, så kan man se hver enkel stemme individuelt.
        //public void createMidiSong(int melodyInstrument, int ChordInstrument, bool ChordsIncluded)
        //{
        //    if (Session["Sang"] != null)
        //    {
        //        Sang sang = (Sang)Session["Sang"];
        //        int tempo = sang.tempo;
        //        int taktDele = 32;
        //        MIDISong song = new MIDISong();

        //        song.AddTrack("track0");
        //        song.SetTimeSignature(0, 4, 4);
        //        song.SetTempo(0, GeneralData.Beats[tempo] * 2);
        //        song.SetChannelInstrument(0, 0, melodyInstrument);

        //        song.AddTrack("track1");
        //        song.SetTimeSignature(1, 4, 4);
        //        song.SetTempo(1, GeneralData.Beats[tempo] * 2);
        //        song.SetChannelInstrument(1, 1, ChordInstrument);
        //        song.AddTrack("track2");
        //        song.SetTimeSignature(2, 4, 4);
        //        song.SetTempo(2, GeneralData.Beats[tempo] * 2);
        //        song.SetChannelInstrument(2, 2, ChordInstrument);
        //        song.AddTrack("track3");
        //        song.SetTimeSignature(3, 4, 4);
        //        song.SetTempo(3, GeneralData.Beats[tempo] * 2);
        //        song.SetChannelInstrument(3, 3, ChordInstrument);
        //        song.AddTrack("track4");
        //        song.SetTimeSignature(4, 4, 4);
        //        song.SetTempo(4, GeneralData.Beats[tempo] * 2);
        //        song.SetChannelInstrument(4, 4, ChordInstrument);

        //        song.AddTrack("track5");//BASS
        //        song.SetTimeSignature(5, 4, 4);
        //        song.SetTempo(5, GeneralData.Beats[tempo] * 2);
        //        song.SetChannelInstrument(5, 5, ChordInstrument);

        //        if (ChordsIncluded == true)
        //        {
        //            sang.callibrerAkkorderTilNoder();
        //        }
        //        else
        //        {
        //            sang.nullstillAkkorderTilNoder();
        //        }
        //        //-----Starter med at indsætte én tom node, fordi
        //        //-----en afspiller til animationvisning af aktiv node, ikke tager den første
        //        //-----node med, ved ikke hvorfor.
        //        song.AddNote(0, 0, -1, 2);
        //        song.AddNote(1, 1, -1, 2);
        //        song.AddNote(2, 2, -1, 2);
        //        song.AddNote(3, 3, -1, 2);
        //        song.AddNote(4, 4, -1, 2);
        //        song.AddNote(5, 5, -1, 2);

        //        foreach (NodeLinjer nl in sang.listNodeLinjer)
        //        {
        //            foreach (Noder no in nl.listNoder)
        //            {
        //                if ((no.keyNumber > 0 && no.keyNumber < 50) && (no.length > 0 && no.length < 100))
        //                {
        //                    //Almindelige noder. NU er det blevet til 32 dele.
        //                    int nodeLengte = (100 / taktDele) * no.length;

        //                    //Melodi
        //                    song.AddNote(0, 0, 23 + 12 + no.keyNumber, nodeLengte);

        //                    //Akkord
        //                    if (no.akkord == null)
        //                    {
        //                        song.AddNote(1, 1, -1, nodeLengte);
        //                        song.AddNote(2, 2, -1, nodeLengte);
        //                        song.AddNote(3, 3, -1, nodeLengte);
        //                        song.AddNote(4, 4, -1, nodeLengte);
        //                    }
        //                    else
        //                    {
        //                        for (int i = 0; i < 4; i++) //VIGTIGT. ALLE må køre 4 gange, eller bliver det hele forskudt.
        //                        {
        //                            if (i < no.akkord.getAkkordNodesToPlay().Count)
        //                            {
        //                                song.AddNote(i + 1, 1 + i, 23 + 12 + 12 + no.akkord.getAkkordNodesToPlay()[i] + no.akkord.keyNumber, nodeLengte);
        //                            }
        //                            else
        //                            {
        //                                song.AddNote(i + 1, 1 + i, -1, nodeLengte); //Sætter tom ind.
        //                            }
        //                        }
        //                    }
        //                    //BASS BASS---------------------
        //                    if (no.akkord == null)
        //                    {
        //                        song.AddNote(5, 5, -1, nodeLengte);
        //                    }
        //                    else
        //                    {
        //                        if (no.akkord.bass > 0)
        //                        {
        //                            song.AddNote(5, 5, 23 + 12 + no.akkord.bass, nodeLengte);
        //                        }
        //                        else //INGEN BASS, MEN SÅ SPILLER VI GRUNDTONENS BASS.
        //                        {
        //                            //  song.AddNote(5, 1, -1, nodeLengte);
        //                            song.AddNote(5, 5, 23 + 12 + no.akkord.getAkkordNodesToPlay()[0] + no.akkord.keyNumber, nodeLengte);
        //                        }
        //                    }
        //                }
        //                else if (no.length > 100 && no.length < 200)
        //                {  //Pauser = -1 . Pauser starter med 100, derfor trækkse 100 bare fra.
        //                    int nodeLengte = (100 / taktDele) * (no.length - 100);
        //                    //Pausen
        //                    song.AddNote(0, 0, -1, nodeLengte);

        //                    if (no.akkord == null)
        //                    {
        //                        //Akkorderne
        //                        song.AddNote(1, 1, -1, nodeLengte);
        //                        song.AddNote(2, 2, -1, nodeLengte);
        //                        song.AddNote(3, 3, -1, nodeLengte);
        //                        song.AddNote(4, 4, -1, nodeLengte);
        //                    }
        //                    else
        //                    {
        //                        for (int i = 0; i < 4; i++) //VIGTIGT. ALLE må køre 4 gange, eller bliver det hele forskudt.
        //                        {
        //                            if (i < no.akkord.getAkkordNodesToPlay().Count)
        //                            {
        //                                song.AddNote(i + 1, 1 + i, 23 + 12 + 12 + no.akkord.getAkkordNodesToPlay()[i] + no.akkord.keyNumber, nodeLengte);
        //                            }
        //                            else
        //                            {
        //                                song.AddNote(i + 1, 1 + i, -1, nodeLengte); //Sætter tom ind.
        //                            }
        //                        }
        //                    }
        //                    //BASS BASS---------------------
        //                    if (no.akkord == null)
        //                    {
        //                        song.AddNote(5, 5, -1, nodeLengte);
        //                    }
        //                    else
        //                    {
        //                        if (no.akkord.bass > 0)
        //                        {
        //                            song.AddNote(5, 5, 23 + 12 + no.akkord.bass, nodeLengte);
        //                        }
        //                        else
        //                        {
        //                            song.AddNote(5, 5, -1, nodeLengte);
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        try
        //        {



        //            MemoryStream ms = new MemoryStream();
        //            song.Save(ms);
        //            ms.Seek(0, SeekOrigin.Begin);

        //            using (FileStream file = new FileStream(Server.MapPath("~/midi/sang.mid"), FileMode.Create, System.IO.FileAccess.Write))
        //            {
        //                byte[] bytes = new byte[ms.Length];
        //                ms.Read(bytes, 0, (int)ms.Length);
        //                file.Write(bytes, 0, bytes.Length);
        //                ms.Close();
        //            }


        //            //song.Save(ms);

        //            //ms.Seek(0, SeekOrigin.Begin);
        //            //byte[] src = ms.GetBuffer();
        //            //byte[] dst = new byte[src.Length];
        //            //for (int i = 0; i < src.Length; i++)
        //            //{
        //            //    dst[i] = src[i];
        //            //}
        //            //ms.Close();

        //            //string strTempFileName = Path.ChangeExtension("midi/sang", ".mid");
        //            //FileStream objWriter = File.Create(Server.MapPath("~/midi/sang.mid"));

        //            //objWriter.Write(dst, 0, dst.Length);
        //            //objWriter.Close();
        //            //objWriter.Dispose();
        //            //objWriter = null;
        //        }
        //        catch { }
        //    }

        //}

        private void showSequencer()
        {
            Server.Transfer("NewPianoRollSong.aspx");
        }
        private void showMidi()
        {
            String filename = (String)Session["MidiFilename"];
            //--------------Hide/Show buttons--------------------------
            Boolean tilstand = true;
            if (filename == null)
            {
                tilstand = true;
            }
            else
            {
                tilstand = false;
            }

            FileUpload1.Visible = tilstand;
            ButtonConverter.Visible = tilstand;
            CheckBoxCubase.Visible = tilstand;

            if (filename == null)
            {
                tilstand = false;
            }
            else
            {
                tilstand = true;
            }


            ButtonMesureDown.Visible = tilstand;
            ButtonMesureUp.Visible = tilstand;
            ButtonLonger.Visible = tilstand;
            ButtonShorter.Visible = tilstand;
            ButtonTransposeUp.Visible = tilstand;
            ButtonTransposeDown.Visible = tilstand;
            ButtonTransposeUp.Visible = tilstand;
            ButtonTransposeDown.Visible = tilstand;
            ButtonSamletVeightRight.Visible = tilstand;
            DropDownListKey.Visible = tilstand;
            DropDownMidiPrLine.Visible = tilstand;
            LabelKey.Visible = tilstand;
            LabelNote.Visible = tilstand;
            LabelMessure.Visible = tilstand;

        }
        private void showMidiAfterFileLoad()
        {
            //--------------Hide/Show buttons--------------------------
            Boolean tilstand = false;
            FileUpload1.Visible = tilstand;
            ButtonConverter.Visible = tilstand;
            CheckBoxCubase.Visible = tilstand;

            tilstand = true;

            ButtonMesureDown.Visible = tilstand;
            ButtonMesureUp.Visible = tilstand;
            ButtonLonger.Visible = tilstand;
            ButtonShorter.Visible = tilstand;
            ButtonTransposeUp.Visible = tilstand;
            ButtonTransposeDown.Visible = tilstand;
            ButtonTransposeUp.Visible = tilstand;
            ButtonTransposeDown.Visible = tilstand;
            ButtonSamletVeightRight.Visible = tilstand;
            DropDownListKey.Visible = tilstand;
            DropDownMidiPrLine.Visible = tilstand;
            LabelKey.Visible = tilstand;
            LabelNote.Visible = tilstand;
            LabelMessure.Visible = tilstand;
            LabelMessureMessage.Visible = tilstand;

        }
        private void hideMidi()
        {
            //--------------Hide/Show buttons--------------------------
            Boolean tilstand = false;
            FileUpload1.Visible = tilstand;
            ButtonConverter.Visible = tilstand;
            CheckBoxCubase.Visible = tilstand;

            tilstand = false;

            ButtonMesureDown.Visible = tilstand;
            ButtonMesureUp.Visible = tilstand;
            ButtonLonger.Visible = tilstand;
            ButtonShorter.Visible = tilstand;
            ButtonTransposeUp.Visible = tilstand;
            ButtonTransposeDown.Visible = tilstand;
            ButtonTransposeUp.Visible = tilstand;
            ButtonTransposeDown.Visible = tilstand;
            ButtonSamletVeightRight.Visible = tilstand;
            DropDownListKey.Visible = tilstand;
            DropDownMidiPrLine.Visible = tilstand;
            LabelKey.Visible = tilstand;
            LabelNote.Visible = tilstand;
            LabelMessure.Visible = tilstand;
            LabelMessureMessage.Visible = tilstand;
    }

        private void showPlaySong()
        {
            createMidiSong(DropDownListSelectMelody.SelectedIndex, DropDownListSelectChords.SelectedIndex, CheckBoxChordsIncluded.Checked); //Tempo = medium
            Panel1.Visible = true;
            DropDownList3.SelectedIndex = 4;

        }
        private void hidePlaySong()
        {
            Panel1.Visible = false;
        }
        private void hideTitel()
        {
            TextBoxTitle.Visible = false;
            TextBoxLyric.Visible = false;
            TextBoxMelody.Visible = false;
            ButtonSave.Visible = false;
            LabelTitle.Visible = false;
            LabelLyric.Visible = false;
            LabelMelody.Visible = false;
        }
        private void showTitel()
        {
            TextBoxTitle.Visible = true;
            TextBoxLyric.Visible = true;
            TextBoxMelody.Visible = true;
            ButtonSave.Visible = true;
            LabelTitle.Visible = true;
            LabelLyric.Visible = true;
            LabelMelody.Visible = true;
        }
        private void showNode()
        {
            ButtonEdit.Text = "Edit";
            LabelNodeLength.Visible = true;
            LabelNodeKey.Visible = true;
            LabelNodePrLine.Visible = true;
            LabelNodeSharpFlat.Visible = true;
            DropDownList1.Visible = true;
            DropDownList2.Visible = true;
            DropDownListSharpFlat.Visible = true;
            DropDownListNodesPrLine.Visible = true;
            DropDownListDistBeweenNodes.Visible = true;
            LabelNodeDistBetweenNode.Visible = true;
            ButtonUndo.Visible = true;
            ButtonEdit.Visible = true;
            CheckBoxDeleteNote.Visible = false;          
            CheckBoxInsertNote.Visible = false;           

        }
        private void hideNode()
        {
            Session["EditNode"] = null;          
            LabelNodeLength.Visible = false;
            LabelNodeKey.Visible = false;
            LabelNodePrLine.Visible = false;
            LabelNodeSharpFlat.Visible = false;
            DropDownList1.Visible = false;
            DropDownList2.Visible = false;
            DropDownListSharpFlat.Visible = false;
            DropDownListNodesPrLine.Visible = false;
            DropDownListDistBeweenNodes.Visible = false;
            LabelNodeDistBetweenNode.Visible = false;
            ButtonUndo.Visible = false;
            ButtonEdit.Visible = false;
            CheckBoxDeleteNote.Visible = false;
            CheckBoxDeleteNote.Checked = false;
            CheckBoxInsertNote.Visible = false;
            CheckBoxInsertNote.Checked = false;
        }
        private void showChord()
        {
            LabelChord.Visible = true;
            LabelChordBass.Visible = true;
            LabelChordExtra.Visible = true;
            DropDownListChord.Visible = true;
            DropDownListBass.Visible = true;
            TextBoxExtra.Visible = true;
            CheckBoxChord.Visible = true;
            CheckBoxExtra.Visible = true;
            CheckBoxBass.Visible = true;
            btn_UndoChord.Visible = true;
            btn_EditChord.Visible = true;
            CheckBoxAkkordSameHeight.Visible = true;
            //  CheckBoxDeleteChord.Visible = false;
            ButtonBack.Visible = false;
        }

        private void hideChord()
        {
            LabelChord.Visible = false;
            LabelChordBass.Visible = false;
            LabelChordExtra.Visible = false;
            DropDownListChord.Visible = false;
            DropDownListBass.Visible = false;
            TextBoxExtra.Visible = false;
            CheckBoxChord.Visible = false;
            CheckBoxExtra.Visible = false;
            CheckBoxBass.Visible = false;
            btn_UndoChord.Visible = false;
            btn_EditChord.Visible = false;
            CheckBoxAkkordSameHeight.Visible = false;
            CheckBoxDeleteChord.Visible = false;
            CheckBoxDeleteChord.Checked = false;
            ButtonBack.Visible = true;

        }
        public void showSongText()
        {
            TextBoxSongText.Visible = true;
            ButtonUndoText.Visible = true;
            ButtonEditText.Visible = true;
            ButtonAutoText.Visible = true;
            CheckBoxTextSameHeight.Visible = true;
        }
        public void hideSongText()
        {
            TextBoxSongText.Visible = false;
            ButtonUndoText.Visible = false;
            ButtonEditText.Visible = false;
            ButtonAutoText.Visible = false;
            CheckBoxTextSameHeight.Visible = false;
        }

        public void showSave()
        {
            btn_SaveSong.Visible = true;
            CheckBox1.Visible = true;
        }
        public void hideSave()
        {
            btn_SaveSong.Visible = false;
            CheckBox1.Visible = false;
            CheckBox1.Checked = false;
        }

        protected void ButtonUndoText_Click(object sender, EventArgs e)
        {

            if (Session["Sang"] != null)
            {
                Sang sang = (Sang)Session["Sang"];

                if (sang.listTexter.Count > 0)
                {
                    sang.listTexter.RemoveAt(sang.listTexter.Count - 1);
                    Session["Sang"] = sang;
                }
            }
        }

        protected void DropDownListDistBeweenNodes_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["Sang"] != null)
            {
                Sang sang = (Sang)Session["Sang"];
                int nyDistance = Convert.ToInt32(DropDownListDistBeweenNodes.SelectedValue);
                if (nyDistance > sang.afstanImellemNoderne)
                {
                    sang.afstanImellemNoderne = nyDistance;
                    sang.changeSongLength(sang.antalNoderPrNodeLinje); //REFRESHER bare efter afstanden er ændret.
                }
                else
                {
                    Response.Write("<script LANGUAGE='JavaScript' >alert('" + "Distanc can only be increased, not decreased." + "')</script>");

                }
            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {

        }

        protected void DropDownList3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["Sang"] != null)
            {
                Sang s = (Sang)Session["Sang"];
                s.tempo = DropDownList3.SelectedIndex;
            }
            createMidiSong(DropDownListSelectMelody.SelectedIndex, DropDownListSelectChords.SelectedIndex, CheckBoxChordsIncluded.Checked);
        }

        protected void CheckBoxChordsIncluded_CheckedChanged(object sender, EventArgs e)
        {
            showPlaySong();
        }

        protected void ButtonBack_Click(object sender, EventArgs e)
        {
            Session["MidiFilename"] = null;
            Server.Transfer("Default.aspx");
        }

        protected void ButtonEdit_Click(object sender, EventArgs e)
        {
            if (Session["EditNode"] == null)
            {
                Session["EditNode"] = true;
                ButtonEdit.Text = "ON";
                CheckBoxDeleteNote.Visible = true;
                CheckBoxInsertNote.Visible = true;
                DropDownListChord.SelectedIndex = 0;
            }
            else
            {
                Session["EditNode"] = null;
                ButtonEdit.Text = "Edit";
                CheckBoxDeleteNote.Visible = false;
                CheckBoxDeleteNote.Checked = false; 
                CheckBoxInsertNote.Visible = false;
                CheckBoxInsertNote.Checked = false;
                DropDownListChord.SelectedIndex = 0;
            }
        }

        protected void btn_EditChord_Click(object sender, EventArgs e)
        {
            if (Session["EditChord"] == null)
            {
                Session["EditChord"] = true;
                btn_EditChord.Text = "ON";
                CheckBoxDeleteChord.Visible = true;
            }
            else
            {
                Session["EditChord"] = null;
                btn_EditChord.Text = "Edit";
                CheckBoxDeleteChord.Visible = false;
            }

        }

        protected void ButtonEditText_Click(object sender, EventArgs e)
        {
            if (Session["EditText"] == null)
            {
                Session["EditText"] = true;
                ButtonEditText.Text = "ON";
            }
            else
            {
                Session["EditText"] = null;
                ButtonEditText.Text = "Edit";
            }

        }

        protected void ButtonAutoText_Click(object sender, EventArgs e)
        {          
            if (Session["Sang"] != null)
            {
                Sang sang = (Sang)Session["Sang"];
                sang.textAutoInsert(TextBoxSongText.Text);
            }
        }


        protected void SubmitButton_Click(object sender, EventArgs e)
        {

        }
        static int lengthExtra = 0;
        static int gange = 1;
        protected void ButtonConverter_Click(object sender, EventArgs e)
        {
            ListBox1.Items.Clear();
            if (FileUpload1.HasFile)
            {
                try
                {
                    if (FileUpload1.PostedFile.ContentType == "audio/mid")
                    {
                        if (FileUpload1.PostedFile.ContentLength < 102400)
                        {
                            string filename = Path.GetFileName(FileUpload1.FileName);
                            FileUpload1.SaveAs(Server.MapPath("~/") + filename);
                            ListBox1.Items.Add("Upload status: File uploaded!");
                            Session["MidiFilename"] = filename;
                            Session["TooHeigh"] = 0;
                            Session["MessureWidth"] = 2;


                            //-----så kører vi Midi filen igennem systemt-----------
                            List<Node> listen = findMidiNoder(filename);
                            findRest(listen);
                            listen.Sort();
                            scrink(listen);
                            MidiNoteToSong(listen);

                            showMidiAfterFileLoad();

                        }
                        else
                            ListBox1.Items.Add("Upload status: The file has to be less than 100 kb!");
                    }
                    else
                        ListBox1.Items.Add("Upload status: Only .mid files are accepted!");
                }
                catch (Exception ex)
                {
                    ListBox1.Items.Add("Upload status: The file could not be uploaded. The following error occured: " + ex.Message);
                }
            }
            else
            {
                ListBox1.Items.Add("Ingen fil");
            }
        }

        protected void DirektConverterMidiFile()
        {
            ListBox1.Items.Clear();
                try
                {
                        string filename = "/midi/sangPianoRoll.mid";
                            Session["MidiFilename"] = filename;
                            Session["TooHeigh"] = 0;
                            Session["MessureWidth"] = 2;


                            //-----så kører vi Midi filen igennem systemt-----------
                            List<Node> listen = findMidiNoder(filename);
                            findRest(listen);
                            listen.Sort();
                            scrink(listen);
                            MidiNoteToSong(listen);

                            showMidiAfterFileLoad();

                 }
                catch (Exception ex)
                {
                    ListBox1.Items.Add("Upload status: The file could not be uploaded. The following error occured: " + ex.Message);
                }
         
        }

        int startPlace = 0;
        private List<Node> findMidiNoder(String filename)
        {
            String url = Server.MapPath("~/") + filename;
            MidiFile mf = null;
            bool firstNodeFound = false;
            List<Node> listen = new List<Node>();
            try
            {
                mf = new MidiFile(url, false);
                int antalNoderPrLinje = 10;
                if (Session["AntalNoderPrLinje"] != null)
                {
                    antalNoderPrLinje = (int)Session["AntalNoderPrLinje"];
                }
                Sang sang = new Sang();
                sang.OpsetningAfSangMedNoder(antalNoderPrLinje);
                sang.newNodeLinje();
                int keyValgt = 1;
                if (Session["MidiKey"] != null)
                {
                    keyValgt = (int)Session["MidiKey"];
                }
                sang.aktuelNodeLinje.key = keyValgt;
                sang.aktuelNodeLinje.callibrerNyKeyChange();
                Session["Sang"] = sang;
                // }
            }
            catch (Exception lkjd)
            {
                ListBox1.Items.Add("Load file gik galt: " + lkjd.ToString());
            }
            ListBox1.Items.Add("Tracks: " + mf.Events.Tracks);
            for (int i = 0; i < mf.Events.Tracks; i++)
            {
                try
                {
                    int id = 0;
                    IList<MidiEvent> ilm = mf.Events.GetTrackEvents(i);

                    foreach (MidiEvent mev in ilm)
                    {
                        // listBox1.Items.Add(mev.ToString());
                        if (mev.CommandCode == MidiCommandCode.NoteOn)
                        {

                            ListBox1.Items.Add("-----------ON----------------");

                            // ListBox1.Items.Add(mev.ToString());
                            bool keepOn = true;
                            NoteOnEvent noe = (NoteOnEvent)mev;
                            try
                            {
                                int a = noe.NoteLength;
                            }
                            catch (Exception dksj)
                            {
                                keepOn = false;
                            }
                            if (keepOn)
                            {
                                Node n = new Node();
                                n.channel = mev.Channel;
                                n.ID = id;
                                id++;
                                n.number = noe.NoteNumber;
                                n.number = n.number - 24;
                                n.name = noe.NoteName;
                                n.rest = false;


                                n.length = noe.NoteLength;
                                n.AbsoluteTime = noe.AbsoluteTime;
                                ListBox1.Items.Add("  Note længde " + n.length);


                                if (firstNodeFound == false)
                                {
                                    firstNodeFound = true;
                                    startPlace = Convert.ToInt32(n.AbsoluteTime);
                                }

                                ListBox1.Items.Add("  Absolut Time  " + n.AbsoluteTime);
                                ListBox1.Items.Add("  Note name " + n.name);
                                ListBox1.Items.Add("  Note number " + n.number);
                                ListBox1.Items.Add("  Note ID " + n.ID);
                                listen.Add(n);
                            }
                        }
                    }

                }
                catch (Exception lskdj)
                {
                    ListBox1.Items.Add("Error: " + lskdj.ToString());
                }

            }
            return listen;
        }
        private void findRest(List<Node> li)
        {
            List<Node> listRest = new List<Node>();
            for (int i = 0; i < li.Count - 1; i++)
            {
                long AbsolutEnd = li[i].AbsoluteTime + li[i].length;
                if (AbsolutEnd + 10 < li[i + 1].AbsoluteTime)
                {
                    Node n = new Node();
                    n.rest = true;
                    n.channel = li[i].channel;
                    n.ID = i;
                    n.AbsoluteTime = AbsolutEnd;
                    n.length = li[i + 1].AbsoluteTime - n.AbsoluteTime;
                    n.number = 25;
                    listRest.Add(n);
                }
            }
            foreach (Node n in listRest)
            {
                li.Add(n);
            }
        }
        int[] arrLength = new int[] { 2, 3, 4, 6, 8, 12, 16, 24, 32, 48 };

        private void scrink(List<Node> li)
        {
            int noteLenght = 5;
            if (Session["SelectedNoteLength"] != null)
            {
                noteLenght = (int)Session["SelectedNoteLength"];
            }

            long ratioFound = 0;
            ratioFound = (li[0].length / arrLength[noteLenght]);

            ListBox1.Items.Clear();
            ListBox1.Items.Add("ratioFound: " + ratioFound);

            if (CheckBoxCubase.Checked) //Cubase problemet.
            {
                ListBox1.Items.Add("  ----CUBASE PROBLEME-----");
                ratioFound = (li[0].length + 2) / arrLength[noteLenght]; //+ 2 fordi cubase er lavet sådan,,mærkeligt.
                long prev = startPlace;
                foreach (Node n in li)
                {
                    n.length = (n.length + 2) / ratioFound;
                    n.AbsoluteTime = prev + 2;
                    prev = prev + n.length;
                }
            }
            else
            {

                long prev = startPlace;
                foreach (Node n in li)
                {
                    n.length = n.length / ratioFound;
                    n.AbsoluteTime = prev;
                    prev = prev + n.length;
                }
            }
            printMessureMessage();
        }
        int[] listeLinjer = new int[] { 64, 48, 32, 24, 16, 12, 8, 6, 4, 3, 2 };

        private void MidiNoteToSong(List<Node> listen)
        {
            if (Session["Transpose"] == null)
            {
                Session["Transpose"] = -24;
            }
            if (Session["samletLength"] == null)
            {
                Session["samletLength"] = 0;
            }

            Session["TooHeigh"] = 0;

            int transpose = (int)Session["Transpose"];

            int tellerRest = 0;
            int rækkefølgeIlisten = 0;
            int samletLength = (int)Session["samletLength"];

            foreach (Node n in listen)
            {
                ListBox1.Items.Add("Draw Rækkefølge: " + n.ID);

                //------ NY NODE LAVES.----------
                Sang sang = null;
                if (Session["Sang"] != null)
                {
                    sang = (Sang)Session["Sang"];
                }

                Noder node = new Noder();
                if (n.rest == true)
                {
                    //Pauser ligger i mellem 100 og 200.
                    //en quart pause er 104. Der lægges
                    // 100 til længden på noden.
                    node.length = (int)n.length + 100;
                }
                else
                {
                    node.length = (int)n.length;
                }

            ligeLavetNyNodeLinje:
                if (sang.aktuelNodeLinje.listNoder != null)
                {
                    node.sequenceNummer = sang.aktuelNodeLinje.listNoder.Count;
                }
                if (node.sequenceNummer < sang.aktuelNodeLinje.maxNumberOfNodes)
                {
                    //Så ved hver enkel node, hvilken nodelinje den tilhører.
                    node.parrentNodeLinje = sang.aktuelNodeLinje;
                    //og hver node gemmes hos én nodeLinje, således kan man
                    //skifte toneart.
                    sang.aktuelNodeLinje.listNoder.Add(node);

                    n.number = n.number + (transpose) + 1; //+1 fordi alt er forskudt med én
                    node.kalibrerFromKeyNumber(n.number);
                    //     Response.Write("<script LANGUAGE='JavaScript' >alert('"+sang.aktuelNodeLinje.maxNumberOfNodes+"')</script>");
                    ListBox1.Items.Add(" ---- " + node.keyNumber + "----");
                    ListBox1.Items.Add(" len: " + node.length + " posY " + node.pos.Y);

                    //---------------SKILLELINJER-------------------------------------------
                    //---------------SKILLELINJER-------------------------------------------
                    //---------------SKILLELINJER-------------------------------------------

                    //  Her lægges n.length til samlet længde, så vi kan udregne om der skal indsættes en skillelinje
                    samletLength = samletLength + (int)n.length;
                    //   ListBox1.Items.Add("Skillelinje samlet vægt intil nu");
                    //   ListBox1.Items.Add("------- " + samletLength + " --------");

                    int[] listeLinjer = new int[] { 64, 48, 32, 24, 16, 12, 8, 6, 4, 3, 2 };
                    int msl = (int)Session["MessureWidth"];


                    for (int i = 1; i < 200; i++)
                    {
                        if (samletLength == listeLinjer[msl] * i)
                        {
                            ListBox1.Items.Add("------- " + samletLength + " --------");

                            Noder skilleLinje = new Noder();
                            skilleLinje.sequenceNummer = sang.aktuelNodeLinje.listNoder.Count;
                            skilleLinje.length = -1;
                            skilleLinje.keyNumber = -1;
                            skilleLinje.StafNumber = 10; //så kommer der ikke de extra linjer under nodelinjerne
                            skilleLinje.parrentNodeLinje = sang.aktuelNodeLinje;
                            sang.aktuelNodeLinje.listNoder.Add(skilleLinje);
                            skilleLinje.kalibrerFromKeyNumber(10);
                        }
                    }
                }
                else //Så skal der laves et NYT nodeLinje
                {
                    int currentY = sang.aktuelNodeLinje.pos.Y;
                    sang.newNodeLinje();
                    //Så ved hver enkel node, hvilken nodelinje den tilhører.
                    node.parrentNodeLinje = sang.aktuelNodeLinje;
                    //-----Sætte ny toneart------------------------
                    int keyValgt = 1;
                    if (Session["MidiKey"] != null)
                    {
                        keyValgt = (int)Session["MidiKey"];
                    }
                    node.parrentNodeLinje.key = keyValgt;
                    sang.aktuelNodeLinje.key = keyValgt;
                    sang.aktuelNodeLinje.callibrerNyKeyChange();

                    goto ligeLavetNyNodeLinje; //Noden som er lige nu, skal med, derfor tilbage engang til.
                }
                rækkefølgeIlisten++;
            }
            int tooheigh = (int)HttpContext.Current.Session["TooHeigh"];
            if (tooheigh > 0)
            {
                HttpContext.Current.Response.Write("<script LANGUAGE='JavaScript' >alert('You should transpose the song DOWN, it is too heigh.')</script>");
            }
            else if (tooheigh < 0)
            {
                HttpContext.Current.Response.Write("<script LANGUAGE='JavaScript' >alert('You should transpose the song UP, it is too low.')</script>");
            }
            ListBox1.Items.Add("TellerRest: " + tellerRest);
        }

        protected void ButtonTransposeDown_Click(object sender, EventArgs e)
        {
            //---Null stille det hele----------------------
            Session["Sang"] = null;
            ListBox1.Items.Clear();
            //----------------------------------------------
            String filename = (String)Session["MidiFilename"];
            int transpose = (int)Session["Transpose"];
            transpose = transpose - 12;
            Session["Transpose"] = transpose;

            //-----Så genindlæses midi filen igen--------------
            List<Node> listen = findMidiNoder(filename);
            findRest(listen);
            listen.Sort();
            scrink(listen);
            MidiNoteToSong(listen);
        }

        protected void ButtonTransposeUp_Click(object sender, EventArgs e)
        {
            //---Null stille det hele----------------------
            Session["Sang"] = null;
            ListBox1.Items.Clear();
            //----------------------------------------------
            String filename = (String)Session["MidiFilename"];
            int transpose = (int)Session["Transpose"];
            transpose = transpose + 12;
            Session["Transpose"] = transpose;

            //-----Så genindlæses midi filen igen--------------
            List<Node> listen = findMidiNoder(filename);
            findRest(listen);
            listen.Sort();
            scrink(listen);
            MidiNoteToSong(listen);
        }

        protected void DropDownListKey_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["MidiKey"] = Convert.ToInt32(DropDownListKey.SelectedValue);
            DropDownListKey.SelectedIndex = 0;
            //---Null stille det hele----------------------
            Session["Sang"] = null;
            ListBox1.Items.Clear();
            //----------------------------------------------
            String filename = (String)Session["MidiFilename"];

            //-----Så genindlæses midi filen igen--------------
            List<Node> listen = findMidiNoder(filename);
            findRest(listen);
            listen.Sort();
            scrink(listen);
            MidiNoteToSong(listen);

        }

        protected void ButtonLonger_Click(object sender, EventArgs e)
        {
            //---Null stille det hele----------------------
            Session["Sang"] = null;
            ListBox1.Items.Clear();
            //----------------------------------------------
            String filename = (String)Session["MidiFilename"];
            int noteLenght = 5;
            if (Session["SelectedNoteLength"] != null)
            {
                noteLenght = (int)Session["SelectedNoteLength"];
                noteLenght++;
                Session["SelectedNoteLength"] = noteLenght;
            }
            else
            {
                Session["SelectedNoteLength"] = 6;
            }
            //-----Så genindlæses midi filen igen--------------
            List<Node> listen = findMidiNoder(filename);
            findRest(listen);
            listen.Sort();
            scrink(listen);
            MidiNoteToSong(listen);
        }

        protected void ButtonShorter_Click(object sender, EventArgs e)
        {
            //---Null stille det hele----------------------
            Session["Sang"] = null;
            ListBox1.Items.Clear();
            //----------------------------------------------
            String filename = (String)Session["MidiFilename"];
            int noteLenght = 5;
            if (Session["SelectedNoteLength"] != null)
            {
                noteLenght = (int)Session["SelectedNoteLength"];
                noteLenght--;
                Session["SelectedNoteLength"] = noteLenght;
            }
            else
            {
                Session["SelectedNoteLength"] = 4;
            }

            //-----Så genindlæses midi filen igen--------------
            List<Node> listen = findMidiNoder(filename);
            findRest(listen);
            listen.Sort();
            scrink(listen);
            MidiNoteToSong(listen);
        }

        protected void DropDownMidiPrLine_SelectedIndexChanged(object sender, EventArgs e)
        {
            int antalNoderPrLinje = Convert.ToInt32(DropDownMidiPrLine.SelectedValue);
            Session["AntalNoderPrLinje"] = antalNoderPrLinje;

            //---Null stille det hele----------------------
            Session["Sang"] = null;
            ListBox1.Items.Clear();
            //----------------------------------------------
            String filename = (String)Session["MidiFilename"];


            //-----Så genindlæses midi filen igen--------------
            List<Node> listen = findMidiNoder(filename);
            findRest(listen);
            listen.Sort();
            scrink(listen);
            MidiNoteToSong(listen);
        }

        protected void ButtonSamletVeightRight_Click(object sender, EventArgs e)
        {
            Sang sangen = null;
            if (Session["Sang"] != null)
            {
                sangen = (Sang)Session["Sang"];
            }
            int samletVægt = (int)Session["samletLength"];
            int pos = 0;
            for (int i = 0; i < sangen.listNodeLinjer[0].listNoder.Count; i++)
            {
                if (sangen.listNodeLinjer[0].listNoder[i].length == -1)//-1 er length of SkilleLinje.
                {
                    pos = i - 1;
                    //    HttpContext.Current.Response.Write("<script LANGUAGE='JavaScript' >alert('Pos:" + pos + " length:" + sangen.listNodeLinjer[0].listNoder[pos].length + "')</script>");
                    break;
                }
            }
            int lengthV = sangen.listNodeLinjer[0].listNoder[pos].length;
            if (lengthV > 100)
            {
                lengthV = lengthV - 100;
            }
            samletVægt = samletVægt + lengthV;
            Session["samletLength"] = samletVægt;

            //---Null stille det hele----------------------
            Session["Sang"] = null;
            ListBox1.Items.Clear();
            //----------------------------------------------
            String filename = (String)Session["MidiFilename"];
            //-----Så genindlæses midi filen igen--------------
            List<Node> listen = findMidiNoder(filename);
            findRest(listen);
            listen.Sort();
            scrink(listen);
            MidiNoteToSong(listen);


        }

        protected void ButtonMesureUp_Click(object sender, EventArgs e)
        {
            int ms = (int)Session["MessureWidth"];
            ms--;
            if (ms < 0)
            {
                ms = 0;
            }
            Session["MessureWidth"] = ms;

            //---Null stille det hele----------------------
            Session["Sang"] = null;
            ListBox1.Items.Clear();
            //----------------------------------------------
            String filename = (String)Session["MidiFilename"];
            //-----Så genindlæses midi filen igen--------------
            List<Node> listen = findMidiNoder(filename);
            findRest(listen);
            listen.Sort();
            scrink(listen);
            MidiNoteToSong(listen);

            //-----------------Vise Taktens længde og nodens længde-------------------------
            printMessureMessage();
        }

        protected void ButtonMesureDown_Click(object sender, EventArgs e)
        {
            int ms = (int)Session["MessureWidth"];
            ms++;
            if (ms > 10)
            {
                ms = 10;
            }
            Session["MessureWidth"] = ms;


            //---Null stille det hele----------------------
            Session["Sang"] = null;
            ListBox1.Items.Clear();
            //----------------------------------------------
            String filename = (String)Session["MidiFilename"];
            //-----Så genindlæses midi filen igen--------------
            List<Node> listen = findMidiNoder(filename);
            findRest(listen);
            listen.Sort();
            scrink(listen);
            MidiNoteToSong(listen);

            //-----------------Vise Taktens længde og nodens længde-------------------------
            printMessureMessage();
        }
        private void printMessureMessage()
        {
            try
            {
                int ms = (int)Session["MessureWidth"];
                int noteLenght = (int)Session["SelectedNoteLength"];
                LabelMessureMessage.Text = "First note length: " + arrLength[noteLenght] + " Messure Length: " + listeLinjer[ms];
            }
            catch (Exception ljd)
            { }
        }

        protected void CheckBoxInsertNote_CheckedChanged(object sender, EventArgs e)
        {
            DropDownListChord.SelectedIndex = 0;
            CheckBoxDeleteNote.Checked = false;
        }

        protected void CheckBoxDeleteNote_CheckedChanged(object sender, EventArgs e)
        {
            DropDownListChord.SelectedIndex = 0;
            CheckBoxInsertNote.Checked = false;

        }

    }
    class Node : IComparable
    {
        public int ID;
        public int number;
        public long length;
        public long AbsoluteTime;
        public String name;
        public bool rest = false;
        public int channel = 0;


        public int CompareTo(object obj)
        {
            Node ind = (Node)obj;
            if (ind.AbsoluteTime > AbsoluteTime)
                return -1;
            else
                return 1;
        }
    }

}