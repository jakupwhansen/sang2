﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LogOn.aspx.cs" Inherits="Sang.LogOn" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>LogOn</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

 <!--  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> 
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script> -->

      <link rel="stylesheet" href="/bootstrap-3.3.5-dist/css/bootstrap.min.css" />
    <script src="/jquery/jquery-1.11.3.min.js"></script> 
    <script src="/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script> 
    <link rel="stylesheet" type="text/css" href="/css/StyleSheet1.css"/>

    <style type="text/css">
        .auto-style1 {
            width: 93px;
        }
        .auto-style2 {
            width: 226px;
            text-align: right;
        }
    </style>
</head>

<body>
    <form id="form1" runat="server">

        <div class="container-fluid">
            
            <br />
            <asp:Button CssClass="btn-default" ID="Button1" runat="server" OnClick="Button1_Click" Text="back" TabIndex="3" Visible="False" />
            <br />
            <table style="width: 100%;">
                <tr>
                    <td class="auto-style1">
                        <asp:Label ID="Label1" runat="server" Text="Email:"></asp:Label>
                    </td>
                    <td class="auto-style2">
                        <asp:TextBox ID="TextBoxEmail" runat="server" TextMode="Email" Width="263px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Label ID="LabelBesked" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        <asp:Label ID="Label2" runat="server" Text="Password:"></asp:Label>
                    </td>
                    <td class="auto-style2">
                        <asp:TextBox ID="TextBoxPassword" runat="server" Width="263px" TextMode="Password"></asp:TextBox>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style1">&nbsp;</td>
                    <td class="auto-style2">
                        <asp:Button CssClass="btn-default" ID="ButtonLogOn" runat="server" OnClick="ButtonLogOn_Click" Text="LogOn" Width="77px" TabIndex="2" />
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </table>
            
        </div>
    </form>
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-54970963-3', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>
