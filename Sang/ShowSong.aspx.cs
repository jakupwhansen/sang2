﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sang
{
    public partial class ShowSong : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Bruger"] == null)
            {
                //Problem her....fungerer ikke .
                Server.Transfer("Default.aspx");
            }  
        }

        protected void ButtonBack_Click(object sender, EventArgs e)
        {

            if (Session["KommerFra"] != null)
            {
                List<String> listeKommerFra = (List<String>)Session["KommerFra"];
                String tilbageTil = listeKommerFra[listeKommerFra.Count - 1];
                listeKommerFra.RemoveAt(listeKommerFra.Count - 1);
                Session["KommerFra"] = listeKommerFra;

                Server.Transfer(tilbageTil);
            }
            else
            {
                Server.Transfer("Default.aspx");
            }
        }

        protected void ButtonUp_Click(object sender, EventArgs e)
        {
            if (Session["sang"] != null)
            {
                Sang sang = (Sang)Session["sang"];
                foreach (Akkord ak in sang.listAkkorder)
                {
                    if (ak.keyNumber < 12)
                    {
                        ak.keyNumber = ak.keyNumber + 1;
                    }
                    else
                    {
                        ak.keyNumber = 1;
                    }

                    if (ak.bass > 0) //0 er INGEN bass
                    {
                        if (ak.bass < 12)
                        {
                            ak.bass = ak.bass + 1;
                        }
                        else
                        {
                            ak.bass = 1;
                        }
                    }
                }
                Session["sang"] = sang;
            }

        }

        protected void ButtonDown_Click(object sender, EventArgs e)
        {
            if (Session["sang"] != null)
            {
                Sang sang = (Sang)Session["sang"];
                foreach (Akkord ak in sang.listAkkorder)
                {
                    if (ak.keyNumber > 1)
                    {
                        ak.keyNumber = ak.keyNumber - 1;
                    }
                    else
                    {
                        ak.keyNumber = 12;
                    }
                    if (ak.bass > 0) //0 er INGEN bass
                    {

                        if (ak.bass > 1)
                        {
                            ak.bass = ak.bass - 1;
                        }
                        else
                        {
                            ak.bass = 12;
                        }
                    }
                }
                Session["sang"] = sang;
            }

        }

        protected void ButtonTouchChords_Click(object sender, EventArgs e)
        {
            if (Session["KommerFra"] != null)
            {
                List<String> listeKommerFra = (List<String>)Session["KommerFra"];
                listeKommerFra.Add("ShowSong.aspx");
            }       

            Server.Transfer("ShowSongTouchChords.aspx");

        }
    }
}